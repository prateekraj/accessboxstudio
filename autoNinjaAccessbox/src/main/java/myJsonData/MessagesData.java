package myJsonData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import entity.CallForAction;
import entity.Messages;
import utils.MyIO;
import android.content.Context;
//import android.util.Log;

public class MessagesData {
	
	public static String FILE_NAME = "message_list_release.txt";
	//private static final String TAG = "MessagesDataClass";
	
	public static void markAsRead (Context ctxt, int msgId) {
		JSONObject tempObj;
		JSONArray JSONMessages, finalMessages;
		
		try {
			JSONMessages = readMessagesFile(ctxt);
			finalMessages = new JSONArray();

			for (int i = 0; i < JSONMessages.length(); i ++) {
				tempObj = JSONMessages.getJSONObject(i);
				if (tempObj.getInt("id") == msgId) {
					tempObj.put("read", true);
					//Log.d(TAG, "Found Match for ID: " + msgId);

				}
				finalMessages.put(tempObj);
			}
			
			writeMessagesFile(ctxt, finalMessages);

		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
	}
	
	public static int countUnread(Context ctxt) {
		int returnCount = 0;
		JSONArray JSONMessages;
		JSONObject tempObj;
		
		try {
			JSONMessages = readMessagesFile(ctxt);

			for (int i = 0; i < JSONMessages.length(); i ++) {
				tempObj = JSONMessages.getJSONObject(i);
				if (!tempObj.getBoolean("read")) {
					returnCount ++;
				}
			}
		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
		return returnCount;
	}
	
	public static void deleteMessage (Context ctxt, int msgId) {
		JSONArray jsoArr = new JSONArray(), jsoArrNew = new JSONArray();

		try {
			jsoArr = readMessagesFile(ctxt);

			JSONObject obj;
			for (int j = 0; j < jsoArr.length(); j ++) {
				obj = jsoArr.getJSONObject(j);
				if (obj.getInt("id") == msgId) {
					//matching message found
					if (obj.has("img")) {
						if (!obj.getString("img").trim().equalsIgnoreCase("")) {
							//message contains image, delete image also!
							//ctxt.deleteFile(obj.getString("img"));
						}
					}
				}
				else { 
					jsoArrNew.put(obj);
				}
			}
			
			writeMessagesFile(ctxt, jsoArrNew);

		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
	}
	
	public static Messages[] getMessagesFromFile (Context ctxt) {
		Messages[] msgData = {}; 
		JSONArray jsoArr = readMessagesFile(ctxt);
		
		int i = 0;
		JSONObject obj;
		msgData = new Messages[jsoArr.length()];
		try {
			for (i = 0; i < jsoArr.length(); i ++) {
				obj = jsoArr.getJSONObject(i);
				
				if (obj.has("img")) {
					//new (post images-in-message) version message
					if (obj.getInt("cfa_type") != 0) {
						msgData[i] = new Messages(obj.getInt("id"), 
										obj.getString("subject"), 
										obj.getString("text"), 
										obj.getString("date"), 
										obj.getBoolean("read"),
										new CallForAction(obj.getInt("cfa_type"), "", 
											obj.getString("cfa_details")),
										obj.getString("img"));
					}
					else {
						msgData[i] = new Messages(obj.getInt("id"), 
								obj.getString("subject"), 
								obj.getString("text"), 
								obj.getString("date"), 
								obj.getBoolean("read"),
								obj.getString("img"));
					}
				}
				else {
					//old (pre images-in-message) version message
					if (obj.getInt("cfa_type") != 0) {
						msgData[i] = new Messages(obj.getInt("id"), 
										obj.getString("subject"), 
										obj.getString("text"), 
										obj.getString("date"), 
										obj.getBoolean("read"),
										new CallForAction(obj.getInt("cfa_type"), "", 
											obj.getString("cfa_details")));
					}
					else {
						msgData[i] = new Messages(obj.getInt("id"), 
								obj.getString("subject"), 
								obj.getString("text"), 
								obj.getString("date"), 
								obj.getBoolean("read"));
					}
				}
			}
		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
		return msgData;
	}
	
	private static JSONArray readMessagesFile (Context ctxt) {
		JSONArray ret = new JSONArray();

		String readString = MyIO.fileToString(ctxt, FILE_NAME);
		
		if (!readString.trim().equalsIgnoreCase("")) {
			try {
				//Log.d(TAG, "readString length = " + readString.length());
				JSONObject jso = new JSONObject(readString);

				ret = jso.getJSONArray("message_list");
			} catch (JSONException e) {
				//Log.e(TAG, "JSONException: " + e.toString());
			}
		}
		else {
			//Log.d(TAG, "file doesnt exists");
		}
		return ret;
	}
	
	private static void writeMessagesFile (Context ctxt, JSONArray msgArray) {
		try {
			//save file
			
			JSONObject finalJSONObj = new JSONObject();
			finalJSONObj.put("message_list", msgArray);

			// Write the string to the file
			MyIO.writeStringToFile(ctxt, FILE_NAME, finalJSONObj.toString());
			
			//Log.d(TAG, "File updated with: " + finalJSONObj.toString());
		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
	}
	
	public static void appendMessages (Context ctxt, JSONArray msgArray) {
		JSONArray currMsgs = readMessagesFile(ctxt);
		try {
			for (int i = 0; i < currMsgs.length(); i ++) {
				msgArray.put(currMsgs.getJSONObject(i));
			}
		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
		
		writeMessagesFile(ctxt, msgArray);
 	}
	/*
	 * Message CSV list for testing
	public static String idCSV(Context ctxt) {
		JSONArray currMsgs = readMessagesFile(ctxt);
		String ret = "";
		for (int i = 0; i < currMsgs.length(); i ++) {
			try {
				ret += currMsgs.getJSONObject(i).getString("id") + ":" + currMsgs.getJSONObject(i).getBoolean("read") + ",";
			} catch (JSONException e) {
				//Log.e(TAG, "JSONException: " + e.toString());
			}
		}
		return ret;
	}
	*/
}
