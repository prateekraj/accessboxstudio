package myJsonData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import db.TableContract.UserDB;
import utils.MyIO;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

public class MyJSONData {
	private JSONObject jso;
	private static SimpleDateFormat fmtChoshen = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat fmtDisplay = new SimpleDateFormat("dd MMM yyyy");
	private static SimpleDateFormat fmtServer = new SimpleDateFormat("yyyy-MM-dd");

	public static final int TYPE_OWN = 0;

	public static final String OWN_DATA_FILE = "own_data.txt";
	public static final String FIELD_OWN_EMAIL_ID = "email_id";
	public static final String FIELD_OWN_PAYMENT_ID = "payment_id";
	public static final String FIELD_OWN_PAYMENT_AMOUNT = "payment_amount";

	private static String getFileNameFromType(int type) {
		switch (type) {
		/*
		 * case TYPE_CAR: return CAR_DATA_FILE;
		 */
		/*
		 * case TYPE_EMERGENCY: return EMG_DATA_FILE;
		 */
			case TYPE_OWN :
				return OWN_DATA_FILE;
				/*
				 * case TYPE_LOGIN: return LOGIN_DATA_FILE;
				 */
			default :
				return "";
		}
	}

	public MyJSONData(Context context, int type) {
		jso = new JSONObject();
		try {
			jso = new JSONObject(MyIO.fileToString(context, getFileNameFromType(type)));
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}
	}

	public String fetchData(String name) {
		try {
			return jso.getString(name).trim();
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}
		return "";
	}

	public JSONArray fetchJSONArray(String name) {
		try {
			return jso.getJSONArray(name);
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}
		return null;
	}

	public JSONObject fetchJSONObject(String name) {
		try {
			return jso.getJSONObject(name);
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}
		return null;
	}
	public static boolean editMyData(Context context, String[] name, String[] value, int type) {
		if (name.length != value.length) {
			// Log.d(TAG, "Key Value length mismatch");
			return false;
		}
		String fileToEdit = getFileNameFromType(type);
		if (fileToEdit.equalsIgnoreCase(""))
			return false;

		try {
			JSONObject jsoTemp = new JSONObject(MyIO.fileToString(context, fileToEdit));

			for (int i = 0; i < name.length; i++) {
				jsoTemp.put(name[i], value[i]);
			}

			// Log.d(TAG, "FileWrite: " + jsoTemp.toString());
			return MyIO.writeStringToFile(context, fileToEdit, jsoTemp.toString());
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}

		return false;
	}
	public static boolean editMyData(Context context, String key, JSONArray jsonArray, int type) {
		String fileToEdit = getFileNameFromType(type);
		if (fileToEdit.equalsIgnoreCase(""))
			return false;

		try {
			JSONObject jsoTemp = new JSONObject(MyIO.fileToString(context, fileToEdit));

			jsoTemp.put(key, jsonArray);

			// Log.d(TAG, "FileWrite: " + jsoTemp.toString());
			return MyIO.writeStringToFile(context, fileToEdit, jsoTemp.toString());
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}

		return false;
	}

	public static boolean editMyData(Context context, String key, JSONObject jsonObject, int type) {
		String fileToEdit = getFileNameFromType(type);
		if (fileToEdit.equalsIgnoreCase(""))
			return false;

		try {
			JSONObject jsoTemp = new JSONObject(MyIO.fileToString(context, fileToEdit));

			jsoTemp.put(key, jsonObject);

			// Log.d(TAG, "FileWrite: " + jsoTemp.toString());
			return MyIO.writeStringToFile(context, fileToEdit, jsoTemp.toString());
		} catch (JSONException e) {
			// Log.e(TAG, "JSON Exception: " + e.toString());
		}

		return false;
	}

	public static boolean deleteMapping(Context context, String name, int type) {
		return editMyData(context, new String[]{name}, new String[]{null}, type);
	}

	public static boolean dataFileExists(Context ctxt, int type) {
		String fileToChk = getFileNameFromType(type);
		if (fileToChk.equalsIgnoreCase(""))
			return false;

		return MyIO.fileExistsInContext(ctxt, fileToChk);
	}

	public static boolean createMyData(Context context, String value, int type) {
		// function can be used to create/overwrite the entire file
		// String value = the JSON string to be stored in the file

		String fileToCreate = getFileNameFromType(type);
		if (fileToCreate.equalsIgnoreCase(""))
			return false;

		return MyIO.writeStringToFile(context, fileToCreate, value);
	}

	public static boolean createMyData(Context context, String[] key, String[] value, int type) {
		JSONObject keyValue = null;

		try {
			if (key.length == value.length) {
				keyValue = new JSONObject();

				for (int i = 0; i < key.length; i++) {

					keyValue.put(key[i], value[i]);
				}

				return createMyData(context, keyValue.toString(), type);

			} else {
				return false;
			}

		} catch (JSONException e) {
			return false;

		}

	}

	public static Date parseFileDate(String fileDate) {
		try {
			return fmtServer.parse(fileDate);
		} catch (ParseException e) {
			// return null;
		}
		return null;
	}
	public static Date parseSelectedDate(String fileDate) {
		try {
			return fmtChoshen.parse(fileDate);
		} catch (ParseException e) {
			// return null;
		}
		return null;
	}
	/**
	 * change date format from server type/db format(yyyy-MM-dd) to display
	 * format (dd MMM yyyy)
	 * 
	 * @param fileDate
	 *            (server/db format(yyyy-MM-dd))
	 * @return
	 */
	public static String formatDateServerToDisplay(String fileDate) {
		Date dt = null;
		try {
			dt = fmtServer.parse(fileDate);
		} catch (ParseException e) {
			// return null;
		}
		if (dt != null) {
			return fmtDisplay.format(dt);
		} else {
			return "";
		}
	}
	/**
	 * change date format from dialog format(dd-MM-yyyy) to display
	 * format (dd MMM yyyy)
	 * 
	 * @param fileDate
	 *(dialog format(dd-MM-yyyy))
	 * @return
	 */
	public static String formatDateDialogToDisplay(String fileDate) {
		Date dt = null;
		try {
			dt = fmtChoshen.parse(fileDate);
		} catch (ParseException e) {
			// return null;
		}
		if (dt != null) {
			return fmtDisplay.format(dt);
		} else {
			return "";
		}
	}
	/**
	 * format to send to server. If chosen then convert to db/server format and
	 * send. If from db no need to convert
	 * 
	 * @param date
	 * @return
	 */
	public static String getServerFormatFromChosen(String date) {
		Date parsedDate;
		String stringDate = "";
		try {
			parsedDate = fmtChoshen.parse(date);
			stringDate = fmtServer.format(parsedDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stringDate;

	}
	public boolean keyNameExistsInData(String keyName) {
		return jso.has(keyName);
	}

	public static String formatFileDateToDisplay(String fileDate) {
		Date dt = parseFileDate(fileDate);
		if (dt != null) {
			return fmtDisplay.format(dt);
		} else {
			return "";
		}
	}


}
