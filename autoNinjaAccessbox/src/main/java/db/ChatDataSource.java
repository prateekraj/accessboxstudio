package db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import db.TableContract.ChatDB;
import db.TableContract.HotlinesDB;

import entity.ChatMessages;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ChatDataSource {
	private static final String TAG = null;
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	@SuppressLint("SimpleDateFormat")
	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm\ndd MMM yyyy"), sdfServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	public ChatDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}
	
	public int update(int rowId, ContentValues cv) {
		open();
		int a = database.update(ChatDB.TABLE_CHAT_NAME, cv, ChatDB.COL_CHAT_MSG_ID + "=?", new String[]{"" + rowId});
		// Log.d(TAG, "update result: " + a + " rows | rowId : " + rowId);

		close();
		return a;
	}

	public long insert(ContentValues cv) {
		open();
		long a = database.insert(ChatDB.TABLE_CHAT_NAME, ChatDB.COL_CHAT_MSG_ID, cv);
		close();
		return a;
	}

	public long insertServerMessages(ContentValues cv) {
		open();
		int x = cv.getAsInteger(ChatDB.COL_CHAT_SERVER_MSG_ID);
		long a;
		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, new String[]{ChatDB.COL_CHAT_SERVER_MSG_ID}, ChatDB.COL_CHAT_SERVER_MSG_ID + "=?", new String[]{"" + x}, null, null,
				null);
		if (c.moveToFirst()) {
			// already exists so ignore
			a = -1;
			// Log.d(TAG, "already existed server msg with id: " + x);
		} else {
			a = database.insert(ChatDB.TABLE_CHAT_NAME, ChatDB.COL_CHAT_MSG_ID, cv);
			// Log.d(TAG, "insert id : " + a);
		}
		close();
		return a;
	}

	/*
	 * public int multiInsert(ContentValues [] cvs) { SQLiteDatabase db =
	 * this.getWritableDatabase(); int count = 0; for (ContentValues cv : cvs) {
	 * if (db.insert(TABLE_CHAT_NAME, COL_CHAT_MSG_ID, cv) != -1) { count++; } }
	 * db.close(); return count; }
	 */
	public ArrayList<ChatMessages> selectAll() {
		// Log.d(TAG, "SelectAll!!!");
		open();
		ArrayList<ChatMessages> retData;

		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, null, null, null, null, null, null);
		retData = getMessagesFromCursor(c);

		close();
		return retData;
	}

	public ArrayList<ChatMessages> selectForRole(int roleId) {
		// roleId comes from ChatMessages.SENDER_CEO or
		// ChatMessages.SENDER_SERVICE_ADVISOR
		// Log.d(TAG, "SelectForRole: " + roleId);
		open();
		ArrayList<ChatMessages> retData;

		String selection = ChatDB.COL_CHAT_RECEIVER + "=? OR " + ChatDB.COL_CHAT_SENDER + "=?";
		String[] selectionArgs = {"" + roleId, "" + roleId};
		String orderBy = ChatDB.COL_CHAT_MSG_ID + " ASC";

		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, null, selection, selectionArgs, null, null, orderBy);
		retData = getMessagesFromCursor(c);

		database.close();
		return retData;
	}

	public void markAllReadForRole(int roleId) {
		open();
		ContentValues cv = new ContentValues();
		cv.put(ChatDB.COL_CHAT_READ_FLAG, 1);

		int x = database.update(ChatDB.TABLE_CHAT_NAME, cv, ChatDB.COL_CHAT_SENDER + "=? AND " + ChatDB.COL_CHAT_READ_FLAG + "=?", new String[]{"" + roleId, "" + 0});
		// Log.d(TAG, "No of rows marked read: " + x);

		close();
	}

	public int getUnreadCountForRole(int roleId) {
		open();
		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, new String[]{"COUNT(" + ChatDB.COL_CHAT_MSG_ID + ") AS cnt"}, ChatDB.COL_CHAT_SENDER + "=? AND " + ChatDB.COL_CHAT_READ_FLAG
				+ "=?", new String[]{"" + roleId, "" + 0}, null, null, null);

		int x = 0;
		if (c.moveToFirst()) {
			x = c.getInt(c.getColumnIndex("cnt"));
		} else {
			// Log.d(TAG, "[SHOULD NOT HAPPEN] Count unread for " + roleId +
			// " returned empty cursor!");
			x = 0;
		}

		close();
		return x;
	}

	public ArrayList<ChatMessages> selectForSending() {
		// roleId comes from ChatMessages.SENDER_CEO or
		// ChatMessages.SENDER_SERVICE_ADVISOR
		Log.d(TAG, "SelectForSending!");

		String derivedStuckInSending = "stuck_in_sending";
		double requeStuckInSendAfter = 15.0; // minutes
		open();
		ArrayList<ChatMessages> retData;
		String selection = ChatDB.COL_CHAT_DELIVERY_FLAG + " < 0 " + " OR (" + ChatDB.COL_CHAT_DELIVERY_FLAG + " = 0 " + " AND " + derivedStuckInSending + " > "
				+ (requeStuckInSendAfter / (24.0 * 60.0)) + ") ";

		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, new String[]{"*, JULIANDAY('" + getCurrentServerTime() + "') - JULIANDAY(" + ChatDB.COL_CHAT_SENDING_STARTED_AT
				+ ") AS " + derivedStuckInSending}, selection, new String[]{}, null, null, null);
		retData = getMessagesFromCursor(c);
		Log.d(TAG, "Selected count: " + retData.size());

		close();
		return retData;
	}

	private ArrayList<ChatMessages> getMessagesFromCursor(Cursor c) {
		ArrayList<ChatMessages> retData = new ArrayList<ChatMessages>();

		if (c.moveToFirst()) {
			do {
				ChatMessages cMsg = new ChatMessages(c.getInt(c.getColumnIndex(ChatDB.COL_CHAT_MSG_ID)), c.getInt(c.getColumnIndex(ChatDB.COL_CHAT_SENDER)),
						c.getInt(c.getColumnIndex(ChatDB.COL_CHAT_RECEIVER)), c.getInt(c.getColumnIndex(ChatDB.COL_CHAT_TYPE)), c.getString(c
								.getColumnIndex(ChatDB.COL_CHAT_MSG)), c.getString(c.getColumnIndex(ChatDB.COL_CHAT_TIMESTAMP)), c.getInt(c
								.getColumnIndex(ChatDB.COL_CHAT_READ_FLAG)) == 1 ? true : false, c.getInt(c.getColumnIndex(ChatDB.COL_CHAT_DELIVERY_FLAG)),
						c.getString(c.getColumnIndex(ChatDB.COL_CHAT_CITY)));

				retData.add(cMsg);

				String logger = "";
				for (int i = 0; i < c.getColumnCount(); i++) {
					logger += c.getColumnName(i) + " : " + c.getString(i) + ", ";
				}
				Log.d(TAG, "Row : " + logger);
			} while (c.moveToNext());
		} else {
			// Log.d(TAG, "cursor empty!!!");
		}

		return retData;
	}
	public static String getCurrentTime() {
		return sdf.format(new Date());
	}

	public static String getCurrentServerTime() {
		return sdfServer.format(new Date());
	}

	public static String getServerTimeString(String serverTime) {
		try {
			return sdf.format(sdfServer.parse(serverTime));
		} catch (ParseException e) {
			return "";
		}
	}

	public int getLargestServerMsgId() {
		open();
		Cursor c = database.query(ChatDB.TABLE_CHAT_NAME, new String[]{"MAX(" + ChatDB.COL_CHAT_SERVER_MSG_ID + ") AS " + ChatDB.DERIVED_COL_LARGEST_MSG_ID}, null, null, null,
				null, null);

		if (c.moveToFirst()) {
			return c.getInt(c.getColumnIndex(ChatDB.DERIVED_COL_LARGEST_MSG_ID));
		} else {
			return 0;
		}
	}

}
