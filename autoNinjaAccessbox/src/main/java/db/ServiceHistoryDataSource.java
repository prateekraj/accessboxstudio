package db;

import config.StaticConfig;
import db.TableContract.ServiceDetailsDB;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ServiceHistoryDataSource {
	// Database fields
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;

	public ServiceHistoryDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}

	// service history db
	public int deleteServiceHistory() {
		open();
		int a = database.delete(ServiceDetailsDB.TABLE_SERVICE_HISTORY, null, null);
		// Log.d(TAG, "insert id : " + a);
		close();
		return a;
	}
	/**
	 * get the car service history
	 * 
	 * @return
	 */
	public Cursor getAllServiceHistoryOfTheCarId() {
		open();
		String selection = ServiceDetailsDB.COL_SERVICE_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		Cursor c = database.query(ServiceDetailsDB.TABLE_SERVICE_HISTORY, null, selection, args, null, null, null);
		return c;
	}
	
	public long insertServiceHistory(ContentValues cv) {
		open();
		long a = database.insert(ServiceDetailsDB.TABLE_SERVICE_HISTORY, null, cv);
		// Log.d(TAG, "insert id : " + a);
		close();
		return a;
	}
}
