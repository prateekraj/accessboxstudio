package db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import config.ConfigInfo;
import config.StaticConfig;
import config.ConfigInfo.Hotline.GroupEntity;
import db.TableContract.HotlinesDB;
import db.TableContract.UserDB;
import fragment.HotlinesFragment;

public class HotlinesDataSource {
	
	
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	ArrayList<GroupEntity> hotlineSelector = new ArrayList<GroupEntity>();
	ArrayList<GroupEntity> hotlineOtherSelector = new ArrayList<GroupEntity>();
	
	
	public HotlinesDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}
	/**
	 * method to insert values into hotlines table
	 * 
	 * @param cv
	 * @return
	 */
	public long insertIntoHotlines(ContentValues cv) {
		open();
		long a = database.insert(HotlinesDB.TABLE_HOTLINES, HotlinesDB.COL_HOTLINE_ID, cv);
		// Log.d(TAG, "insert id : " + a);

		close();
		return a;
	}

	/**
	 * method to insert values into helper hotlines table
	 * 
	 * @param cv
	 * @return
	 */
	public long insertIntoHelperHotline(ContentValues cv) {
		open();
		long a = database.insert(HotlinesDB.TABLE_HOTLINE_HELPER, null, cv);
		// Log.d(TAG, "insert id : " + a);

		close();
		return a;
	}
	/**
	 * Method to truncate the hotlines table for new entry
	 * 
	 * @return
	 */
	public int deleteFromHotlines() {
		open();
		int a = database.delete(HotlinesDB.TABLE_HOTLINES, null, null);
		// Log.d(TAG, "insert id : " + a);
		close();
		return a;
	}
	
	/**
	 * get the hotline numbers to populate the hotlist list
	 * 
	 * @return
	 */
	public ArrayList<Integer> getFlaggedDepartmentIds(int flag) {
		// Log.d(TAG, "SelectAll!!!");
		String selection = HotlinesDB.COL_HELPER_HOTLINE_FLAG + " =?";
		String[] args = {"" + flag};
		open();
		ArrayList<Integer> retData = new ArrayList<Integer>();
		try{
		Cursor c = database.query(HotlinesDB.TABLE_HOTLINE_HELPER, null, selection, args, null, null, HotlinesDB.COL_HELPER_HOTLINE_ID);
		if (c.moveToFirst()) {
			do{
			retData.add(c.getInt(c.getColumnIndex(HotlinesDB.COL_HELPER_HOTLINE_ID)));
		}while (c.moveToNext());
		}
		}catch(SQLException e){
			System.out.println("SQL Exception"+e);
			
		}
		close();
		return retData;
	}
	/**
	 * get the hotline numbers to populate the hotlist list
	 * 
	 * @return
	 */
	public Cursor getAllHotlines() {
		ArrayList<ConfigInfo.Hotline> retData;
		Cursor c = database.query(HotlinesDB.TABLE_HOTLINES, null, null, null, null, null, HotlinesDB.COL_HOTLINE_ID);
		return c;
	}
	/**
	 * get all hotline depts where log flag is more than 0
	 * 
	 * @return
	 */
	public Cursor getDataFromHelper() {
		open();
		ArrayList<ConfigInfo.Hotline> retData;
		String selection = HotlinesDB.COL_LOG_COUNT + " > 0 ";
		Cursor c = database.query(HotlinesDB.TABLE_HOTLINE_HELPER, null, selection, new String[]{}, null, null, HotlinesDB.COL_HELPER_HOTLINE_ID);
		return c;
	}

	/**
	 * get the hotline number from the hotline name
	 * 
	 * @return
	 *//*

	public Cursor getHotlineFromName(String departmentName) {
		// Log.d(TAG, "SelectAll!!!");
		open();
		ArrayList<ConfigInfo.Hotline> retData;

		String selection = HotlinesDB.COL_HOTLINE_DEPARTMENT_NAME + "=?";
		String[] selectionArgs = {"" + departmentName};
		String orderBy = HotlinesDB.COL_NAME + " ASC";

		Cursor c = database.query(HotlinesDB.TABLE_HOTLINES, null, selection, selectionArgs, null, null, orderBy); // retData
																									// =
																									// getNumbersFromCursor(c);
		db.close();
		return c;
	}
*/
	/**
	 * get the hotline numbers from the hotline id
	 * 
	 * @return
	 */

	public Cursor getHotlineFromId(int deparmentId) {
		// Log.d(TAG, "SelectAll!!!");
		open();
		ArrayList<ConfigInfo.Hotline> retData;

		String selection = HotlinesDB.COL_HOTLINE_ID + "=?";
		String[] selectionArgs = {"" + deparmentId};
		String orderBy = HotlinesDB.COL_NAME + " ASC";

		Cursor c = database.query(HotlinesDB.TABLE_HOTLINES, null, selection, selectionArgs, null, null, orderBy); // retData
		return c;
	}

	/**
	 * method to extract from te db by particular department id and populate in an arraylist
	 * of arraylists with (hotline id-1) being the index.
	 * 
	 * @param c
	 * @return
	 */
	public void setNumbersFromCursor(Cursor c, int flag) {
		hotlineSelector = new ArrayList<ConfigInfo.Hotline.GroupEntity>();
		hotlineOtherSelector = new ArrayList<ConfigInfo.Hotline.GroupEntity>();
		ArrayList<ConfigInfo.Hotline> retData = new ArrayList<ConfigInfo.Hotline>();
		int id = 1;
		if (c.moveToFirst()) {
			//id = c.getInt(c.getColumnIndex(COL_HOTLINE_ID));
			do {
					if(flag== 1){
						hotlineSelector = addToHotlineList(hotlineSelector, c);
						//System.out.println("ADDIND TO MASTER FOR ID: "+c.getInt(c.getColumnIndex(HotlinesDB.COL_HOTLINE_ID)));
					}else if(flag == 0){
						hotlineOtherSelector = addToHotlineList(hotlineOtherSelector, c);
					}
			} while (c.moveToNext());
			
		} else {
			/*hotlineSelector.clear();
			System.out.println("HOTLINE SELECTOR SIZE: "+hotlineSelector.size());*/
		}
		if(hotlineSelector.size()>0){
		StaticConfig.masterHotlineSelector.add(hotlineSelector);
		//System.out.println("MASTER SIZE:"+StaticConfig.masterHotlineSelector.size());
		HotlinesFragment.setOtherHotlines(hotlineSelector);
		}
		if(hotlineOtherSelector.size()>0){
			HotlinesFragment.setOtherHotlines(hotlineOtherSelector);
			}
	}
	
	

	private ArrayList<GroupEntity> addToHotlineList(ArrayList<GroupEntity> hotlineSelectorTemp, Cursor c) {
		hotlineSelectorTemp.add(new GroupEntity(c.getString(c.getColumnIndex(HotlinesDB.COL_HOTLINE_DEPARTMENT_NAME)),
				c.getString(c.getColumnIndex(HotlinesDB.COL_NUMBER)), c.getString(c.getColumnIndex(HotlinesDB.COL_NAME)), c.getInt(c.getColumnIndex(HotlinesDB.COL_HOTLINE_ID))));
		return hotlineSelectorTemp;
	}

	public int getStoredChangeSet() {
		open();
		int changeSet = 0;
		Cursor c = getAllHotlines();
		if (c.moveToFirst()) {
			changeSet = c.getInt(c.getColumnIndex(HotlinesDB.COL_CHANGE_SET));
		} else {

		}
		close();
		return changeSet;
	}
	/**
	 * update hotlines helper details
	 * 
	 * @param cv
	 * @return
	 */
	public Boolean updateHotlinesHelper(ContentValues cv, String hotlineLabel) {
		open();
		String[] args = {"" + hotlineLabel};
		String selection = HotlinesDB.COL_HELPER_HOTLINE_DEPARTMENT_NAME + " =?";
		long a = database.update(HotlinesDB.TABLE_HOTLINE_HELPER, cv, selection, args);
		close();
		if (a > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * get stored flag from hotline helper
	 * @param hotline_name
	 * @return
	 */
	public int getStoredflag(String hotline_name) {
		open();
		int storedLogFlag =  0;
		String[] args = {"" + hotline_name};
		String selection = HotlinesDB.COL_HELPER_HOTLINE_DEPARTMENT_NAME + " =?";
		Cursor c = database.query(HotlinesDB.TABLE_HOTLINE_HELPER, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			storedLogFlag = c.getInt(c.getColumnIndex(HotlinesDB.COL_LOG_COUNT));
		}
		close();
		return storedLogFlag;
	}
	/**
	 * method to delete the helper table+++++++++ 
	 */
		public int deleteHelper() {
			open();
			int a = database.delete(HotlinesDB.TABLE_HOTLINE_HELPER, null, null);
			// Log.d(TAG, "insert id : " + a);
			close();
			return a;
		} 
}
