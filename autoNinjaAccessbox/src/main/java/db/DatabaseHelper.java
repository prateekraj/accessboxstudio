package db;

import db.TableContract.CarDetailsDB;
import db.TableContract.ChatDB;
import db.TableContract.HotlinesDB;
import db.TableContract.LocationsDB;
import db.TableContract.PSFTable;
import db.TableContract.ServiceBookingDB;
import db.TableContract.ServiceDetailsDB;
import db.TableContract.UserDB;
import entity.ChatMessages;
import entity.MyCar;
import fragment.HotlinesFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import config.ConfigInfo;
import config.StaticConfig;
import config.ConfigInfo.Hotline.GroupEntity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = "DatabaseHelper";

	private static final int DB_VERSION = 35;
	private static final String DB_NAME = "chat_database";
	
	private SQLiteDatabase db;

	/*//psf table detail
		public static final String TABLE_PSF_NAME = "psf_table"; // psf table
		public static final String COL_PSF_RATING = "psf_rate"; //psf rating
		public static final String COL_PSF_MSG_ID = "psf_msg_id";*/
		
		private static final String psfQuery = "CREATE TABLE " + PSFTable.TABLE_PSF_NAME + " ("
		 + PSFTable.COL_PSF_MSG_ID + " INTEGER PRIMARY KEY, "
		 + PSFTable.COL_PSF_RATING + " REAL DEFAULT 0);";
		
		private static final String serviceBookingQuery = "CREATE TABLE " + ServiceBookingDB.TABLE_SERVICE_BOOKING + " ("
				 + ServiceBookingDB.COL_BOOKING_ID + " TEXT DEFAULT '', "
				 + ServiceBookingDB.COL_CAR_ID + " INTEGER DEFAULT 0, "
				 + ServiceBookingDB.COL_CANCELLED_FLAG + " INTEGER DEFAULT 0, "
				 + ServiceBookingDB.COL_BOOKING_MESSAGE_ID + " TEXT PRIMARY KEY DEFAULT '');";
		
	private static final String queryCarData = "CREATE TABLE " + CarDetailsDB.TABLE_CAR_DETAILS + " (" + CarDetailsDB.COL_CAR_ID + " INTEGER PRIMARY KEY DEFAULT 0, "
			+ CarDetailsDB.COL_FIELD_CAR_REGNUM + " TEXT DEFAULT '', " + CarDetailsDB.COL_FIELD_CAR_MODEL + " TEXT DEFAULT '', "
			+ CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE + " TEXT DEFAULT '', " + CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT + " TEXT DEFAULT '', "
			+ CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY + " TEXT DEFAULT '', " + CarDetailsDB.COL_FIELD_CAR_INSURANCE_PROVIDER + " TEXT DEFAULT '', "
			+ CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY + " TEXT DEFAULT '', " + CarDetailsDB.COL_ALARM_SERVICE + " TEXT DEFAULT '', " + CarDetailsDB.COL_ALARM_INSURANCE + " TEXT DEFAULT '', "
			+ CarDetailsDB.COL_ALARM_PUC + " TEXT DEFAULT '', " + CarDetailsDB.COL_LAST_SERVICE_REMINDER_CHECK + " TEXT DEFAULT '', " + CarDetailsDB.COL_LAST_PUC_REMINDER_CHECK
			+ " TEXT DEFAULT '', " + CarDetailsDB.COL_LAST_INSURANCE_REMINDER_CHECK
			+ " TEXT DEFAULT '', "
			//+ CarDetailsDB.COL_CAR_CHANGE_SET + " INTEGER DEFAULT -1, "
			+ CarDetailsDB.COL_FIELD_CAR_DATA_VERIFIED + " TEXT DEFAULT 'true', " + CarDetailsDB.COL_FIELD_CAR_PHOTO_PATH + " TEXT DEFAULT '', "
			+ CarDetailsDB.COL_FIELD_CAR_PHOTO_ATTEMPT_PATH + " TEXT DEFAULT '', " + CarDetailsDB.COL_FIELD_CAR_INSURANCE_DOCUMENT + " TEXT DEFAULT '', " 
			+ CarDetailsDB.COL_CAR_OWNER + " TEXT DEFAULT '', " + CarDetailsDB.COL_FIELD_CAR_PUC_DOCUMENT
			// + " TEXT DEFAULT '' " + ")";
			+ " TEXT DEFAULT '');";

	private static final String queryCarservicehistory = "CREATE TABLE " + ServiceDetailsDB.TABLE_SERVICE_HISTORY + " (" + ServiceDetailsDB.COL_SERVICE_CAR_ID + " INTEGER DEFAULT 0, "
			+ ServiceDetailsDB.COL_SERVICE_DATE + " TEXT DEFAULT '', " + ServiceDetailsDB.COL_SERVICE_WORK_TYPE + " TEXT DEFAULT '', " + ServiceDetailsDB.COL_SERVICE_MILEAGE + " TEXT DEFAULT '', "
			+ ServiceDetailsDB.COL_SERVICE_ADVISOR + " TEXT DEFAULT '', " + ServiceDetailsDB.COL_SERVICE_TECHNICIAN + " TEXT DEFAULT '', " + ServiceDetailsDB.COL_SERVICE_AMOUNT + " TEXT DEFAULT '', "
			+ ServiceDetailsDB.COL_SERVICE_OUTLET + " TEXT DEFAULT '');";

	private static final String query = "CREATE TABLE " + ChatDB.TABLE_CHAT_NAME + " (" + ChatDB.COL_CHAT_MSG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ ChatDB.COL_CHAT_SERVER_MSG_ID + " INTEGER DEFAULT -1, " + ChatDB.COL_CHAT_MSG + " TEXT, " + ChatDB.COL_CHAT_SENDER + " INTEGER, " + ChatDB.COL_CHAT_RECEIVER
			+ " INTEGER, " + ChatDB.COL_CHAT_TYPE + " INTEGER, " + ChatDB.COL_CHAT_CITY + " VARCHAR(100) DEFAULT '', " + ChatDB.COL_CHAT_READ_FLAG
			+ " INTEGER DEFAULT 0, " + ChatDB.COL_CHAT_DELIVERY_FLAG + " INTEGER DEFAULT -2, " 
			+ ChatDB.COL_CHAT_SENDING_STARTED_AT + " TIMESTAMP, " + ChatDB.COL_CHAT_TIMESTAMP + " VARCHAR(20) DEFAULT '00:00, 00 Xxx 0000');";

	private static final String createOwnTable = "CREATE TABLE " + UserDB.TABLE_USER_DETAILS + " (" + UserDB.COL_FIELD_OWN_NUMBER
			+ " INTEGER PRIMARY KEY DEFAULT -1, " + UserDB.COL_FIELD_OWN_OTP + " TEXT DEFAULT '', " + UserDB.COL_FIELD_ERROR + " TEXT DEFAULT '', "
			+ UserDB.COL_FIELD_JSON_TEXT + " TEXT DEFAULT '', " + UserDB.COL_FIELD_LAST_API_CHECK + " TEXT DEFAULT '', "
			+ UserDB.COL_FIELD_RANDOM_HOUR
			+ " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_LAST_REMINDER_MESSAGE_ID
			+ " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_SECOND_CHECK
			+ " TEXT DEFAULT '', " // default is // queued
			+ UserDB.COL_FIELD_OWN_TOOLTIPS_CHANGESET + " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_OWN_OFFERS_CHANGESET
			+ " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_OWN_LATEST_VERSION
			+ " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION
			+ " INTEGER DEFAULT 0, "
			+ UserDB.COL_CAR_CHANGE_SET + " INTEGER DEFAULT -1, "
			+ UserDB.COL_FIELD_OWN_UPDATE_REMINDED_FOR + " TEXT DEFAULT '', " 
			+ UserDB.COL_FIELD_OWN_ADVISOR_CHAT_CITY_NAME + " TEXT DEFAULT '', "
			+ UserDB.COL_FIELD_OWN_CEO_CHAT_CITY_NAME + " TEXT DEFAULT '', "
			+ UserDB.COL_FIELD_OWN_REFERRAL_CODE + " TEXT DEFAULT '', " + UserDB.COL_FIELD_OWN_REG_NUMBER + " TEXT DEFAULT '', "
			+ UserDB.COL_FIELD_OWN_REWARD_POINTS_TOTAL + " TEXT DEFAULT '', " + UserDB.COL_FIELD_OWN_REWARD_CHANGESET + " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_EMERGENCY_CONTACT_NAME + " TEXT DEFAULT '', " + UserDB.COL_FIELD_OWN_PAYMENT_CHANGESET + " INTEGER DEFAULT 0, "
			+ UserDB.COL_FIELD_EMERGENCY_CONTACT_NUMBER + " INTEGER DEFAULT 0);";// login
																																					// number

	private static final String createHotlineTable = "CREATE TABLE " + HotlinesDB.TABLE_HOTLINES
			//+ " (" + COL_HOTLINE_FLAG + " INTEGER DEFAULT 0, " 
			+ " ("   + HotlinesDB.COL_HOTLINE_ID + " INTEGER DEFAULT 0, " 
			+ HotlinesDB.COL_HOTLINE_DEPARTMENT_NAME + " TEXT DEFAULT '', " 
			+ HotlinesDB.COL_NAME + " TEXT DEFAULT '', " 
			+ HotlinesDB.COL_NUMBER + " TEXT DEFAULT '', " 
			+ HotlinesDB.COL_CHANGE_SET + " INTEGER DEFAULT 0 );";
			//+ "UNIQUE (" + HotlinesDB.COL_NAME + "," + HotlinesDB.COL_NUMBER + "));";
	
	private static final String createHotlineHelperTable = "CREATE TABLE " + HotlinesDB.TABLE_HOTLINE_HELPER 
			+ " (" + HotlinesDB.COL_HELPER_HOTLINE_ID + " INTEGER DEFAULT 0, " 
			+ HotlinesDB.COL_HELPER_HOTLINE_FLAG + " INTEGER DEFAULT 0, " 
			+ HotlinesDB.COL_LOG_COUNT + " INTEGER DEFAULT 0, "
			+ HotlinesDB.COL_HELPER_HOTLINE_DEPARTMENT_NAME + " TEXT DEFAULT '');";
	
	private static final String createLocationsTable = "CREATE TABLE " + LocationsDB.TABLE_LOCATIONS
			+ " ("   + LocationsDB.COL_LOC_NAME + " TEXT DEFAULT '', " 
			+ LocationsDB.COL_LOC_ID + " INTEGER DEFAULT 0, "
			+ LocationsDB.COL_LOC_CHAT_ADVISOR_ENABLED + " INTEGER DEFAULT 0, " 
			+ LocationsDB.COL_LOC_CHAT_CEO_ENABLED + " INTEGER DEFAULT 0, " 
			+ LocationsDB.COL_LOC_ENQUIRY_ENABLED + " INTEGER DEFAULT 0, " 
			+ LocationsDB.COL_LOC_PAYMENT_ENABLED + " INTEGER DEFAULT 0, "
			+ LocationsDB.COL_LOC_SERVICE_ENABLED + " INTEGER DEFAULT 0, "
			+ LocationsDB.COL_LOC_TEST_DRIVE_ENABLED + " INTEGER DEFAULT 0, "
			+ LocationsDB.COL_LOC_CHANGE_SET + " INTEGER DEFAULT 0 );";

	/*@SuppressLint("SimpleDateFormat")
	private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm\ndd MMM yyyy"), sdfServer = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");*/

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(query); // create table
		db.execSQL(queryCarData);
		db.execSQL(createHotlineTable); // create table
		db.execSQL(createHotlineHelperTable);
		db.execSQL(queryCarservicehistory);
		db.execSQL(createOwnTable);
		db.execSQL(createLocationsTable);
		db.execSQL(psfQuery);//create table
		db.execSQL(serviceBookingQuery);
		System.out.println("HOTLINE QUERY:" + createHotlineTable);

		// Log.d(TAG, "onCreate query : " + query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		switch(oldVersion){
			case 34:
				try {
					//db.execSQL("DROP TABLE IF EXISTS " + TABLE_HOTLINES);
					db.execSQL(queryCarData);
					db.execSQL(createHotlineTable); // create table
					db.execSQL(createHotlineHelperTable);
					db.execSQL(queryCarservicehistory);
					db.execSQL(createOwnTable);
					db.execSQL(createLocationsTable);
					db.execSQL(psfQuery);//create table
					db.execSQL(serviceBookingQuery);
				} catch (SQLException e) {
					Log.e(TAG, e.getMessage());
				}
			break;
			case 35://new will be 36
				try {
					db.execSQL("ALTER TABLE " + UserDB.TABLE_USER_DETAILS + " ADD COLUMN " + UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION + " INTEGER DEFAULT 0 ");
					db.execSQL("ALTER TABLE " + HotlinesDB.TABLE_HOTLINE_HELPER + " ADD COLUMN " + HotlinesDB.COL_LOG_COUNT + " INTEGER DEFAULT 0 ");
					db.execSQL("ALTER TABLE " + UserDB.TABLE_USER_DETAILS + " ADD COLUMN " + UserDB.COL_FIELD_OWN_PAYMENT_CHANGESET + " INTEGER DEFAULT 0 ");
					db.execSQL("ALTER TABLE " + LocationsDB.TABLE_LOCATIONS + " ADD COLUMN " + LocationsDB.COL_LOC_ID + " INTEGER DEFAULT 0 ");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			break;
				
		}
		
		// onUpgrade - add column COL_CHAT_SENDING_STARTED_AT
		try {
			db.execSQL("ALTER TABLE " + ChatDB.TABLE_CHAT_NAME + " ADD COLUMN " + ChatDB.COL_CHAT_SENDING_STARTED_AT + " TIMESTAMP ");
		} catch (SQLException e) {
			Log.e(TAG, ChatDB.COL_CHAT_SENDING_STARTED_AT + " column exists already, I guess!\n" + e.getMessage());
		}
		ContentValues cv = new ContentValues();
		cv.put(ChatDB.COL_CHAT_SENDING_STARTED_AT, ChatDataSource.getCurrentServerTime());
		db.update(ChatDB.TABLE_CHAT_NAME, cv, null, null);

		// onUpgrade - add column COL_CHAT_CITY
		try {
			db.execSQL("ALTER TABLE " + ChatDB.TABLE_CHAT_NAME + " ADD COLUMN " + ChatDB.COL_CHAT_CITY + " VARCHAR(100) DEFAULT '' ");
		} catch (SQLException e) {
			Log.e(TAG, ChatDB.COL_CHAT_CITY + " column exists already, I guess!\n" + e.getMessage());
		}
	}

}
