package db;

import config.StaticConfig;
import db.TableContract.CarDetailsDB;
import db.TableContract.ServiceBookingDB;
import db.TableContract.UserDB;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ServiceBookingDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;

	public ServiceBookingDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}
	/**
	 * update service booking details
	 * 
	 * @param cv
	 * @return
	 */
	public Boolean updateServiceDetails(ContentValues cv, String message_id) {
		open();
		String selection = ServiceBookingDB.COL_BOOKING_MESSAGE_ID + " =?";
		String[] args = {message_id};
		long a = database.update(ServiceBookingDB.TABLE_SERVICE_BOOKING, cv, selection, args);
		close();
		if (a > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * get flag type from message id
	 * 
	 * @return
	 */
	public int getCancelledFlag(String message_id) {
		open();
		String selection = ServiceBookingDB.COL_BOOKING_MESSAGE_ID + " =?";
		String[] args = {message_id};
		Cursor c = database.query(ServiceBookingDB.TABLE_SERVICE_BOOKING, null, selection, args, null, null, null);
		int  flag = 0;
		if (c.moveToFirst()) {
			flag = c.getInt((c.getColumnIndex(ServiceBookingDB.COL_CANCELLED_FLAG)));
		} 
		//c.close();
		close();
		return flag;

	}
	/**
	 * inserting the booking details to db
	 * 
	 * @param cv
	 * @return
	 */
	public long insertBookingDetails(ContentValues cv) {
		open();
		long a = database.insert(ServiceBookingDB.TABLE_SERVICE_BOOKING, null, cv);
		close();
		return a;
	}
	public Cursor getDetails(String messageId){
		open();
		String selection = ServiceBookingDB.COL_BOOKING_MESSAGE_ID + " =?";
		String[] args = {messageId};
		Cursor c = database.query(ServiceBookingDB.TABLE_SERVICE_BOOKING, null, selection, args, null, null, null);
		return c;
	}

}
