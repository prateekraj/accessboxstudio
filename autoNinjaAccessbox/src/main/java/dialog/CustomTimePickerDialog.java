package dialog;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import config.StaticConfig;

import fragment.ServiceBaseFragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class CustomTimePickerDialog extends TimePickerDialog {

    private final static int TIME_PICKER_INTERVAL = 15;
    private TimePicker timePicker;
    private final OnTimeSetListener callback;

    public CustomTimePickerDialog(Context context, OnTimeSetListener callBack,
            int hourOfDay, int minute, boolean is24HourView) {
        super(context, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL,
                is24HourView);
        this.callback = callBack;
    }
    
    public CustomTimePickerDialog(Context context, int apptheme, OnTimeSetListener callBack,
			int hourOfDay, int minute, boolean is24HourView) {
		// TODO Auto-generated constructor stub
		super(context, apptheme, callBack, hourOfDay, minute / TIME_PICKER_INTERVAL,
                is24HourView);
		this.callback = callBack;
	}


	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (checkTime()) {
			ServiceBaseFragment.validTime = true;
		} else {
			ServiceBaseFragment.validTime = false;
		}
		if (callback != null && timePicker != null) {
			timePicker.clearFocus();
			callback.onTimeSet(timePicker, timePicker.getCurrentHour(),
					timePicker.getCurrentMinute());

		}

	}

	/**
	 * checking if the chosen time is between the working hours or not
	 * 
	 * @return true if within the time range
	 */
	private boolean checkTime() {
		String start = StaticConfig.BOOK_SERVICE_START_SLOT;
		String end = StaticConfig.BOOK_SERVICE_END_SLOT;
		Date startTime, endTime;
		Calendar calendar1, calendar2;
		try {
			startTime = new SimpleDateFormat("HH:mm:ss").parse(start);
			endTime = new SimpleDateFormat("HH:mm:ss").parse(end);

			calendar1 = Calendar.getInstance();
			calendar1.setTime(startTime);
			calendar2 = Calendar.getInstance();
			calendar2.setTime(endTime);
			calendar2.add(Calendar.DATE, 1);

			int hourChosen = timePicker.getCurrentHour();
			int minuteChosen = timePicker.getCurrentMinute()
					* TIME_PICKER_INTERVAL;

			if (hourChosen >= calendar1.getTime().getHours()
					&& minuteChosen >= calendar1.getTime().getMinutes()
					&& hourChosen < calendar2.getTime().getHours()) {
				return true;
			} else if (hourChosen == calendar2.getTime().getHours()
					&& minuteChosen <= calendar2.getTime().getMinutes()) {
				return true;
			} else if (hourChosen == calendar2.getTime().getHours()
					&& minuteChosen > calendar2.getTime().getMinutes()) {
				return false;
			}

		} catch (ParseException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}
    @Override
    protected void onStop() {
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            Field timePickerField = classForid.getField("timePicker");
            this.timePicker = (TimePicker) findViewById(timePickerField
                    .getInt(null));
            Field field = classForid.getField("minute");

            NumberPicker mMinuteSpinner = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));
            mMinuteSpinner.setMinValue(0);
            mMinuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1);
            List<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            mMinuteSpinner.setDisplayedValues(displayedValues
                    .toArray(new String[0]));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Override
    public void setTitle(CharSequence title) {
    	super.setTitle("Set Time");
    }

}