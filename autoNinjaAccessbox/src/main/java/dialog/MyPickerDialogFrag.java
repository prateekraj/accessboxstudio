package dialog;

import java.util.Calendar;

import com.myaccessbox.appcore.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;


import config.StaticConfig;

import fragment.MyCarBaseFragment;

public class MyPickerDialogFrag extends DialogFragment
		implements OnTimeSetListener, 
				OnDateSetListener, 
				DialogInterface.OnClickListener {
	
	public static final int TYPE_DATE_PICKER = 7;
	public static final int TYPE_TIME_PICKER = 14;
	public static final int TYPE_SERVICE_LIST_PICKER = 21;
	public static final int TYPE_CHAT_CEO_CITY_LIST_PICKER = 28;
	public static final int TYPE_ENQUIRY_LIST_PICKER = 35;
	public static final int TYPE_CHAT_ADVISOR_CITY_LIST_PICKER = 42;
	public static final int TYPE_ENQUIRY_LOC_LIST_PICKER = 48;
	public static final int TYPE_DATE_PICKER_AFTER_2_DAYS = 56;
	
	
	private static boolean _current_dialog_is_open = false;
	
	private OnDateSetListener mListener;
	
	@Override
	public void onDetach() {
		this.mListener = null;
		super.onDetach();
	}
	@Override
	public void onCancel(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onCancel(dialog);
	}
	@Override
	public void onDismiss(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onDismiss(dialog);
	}
	public interface onSendResultListener {
		public void onSendResult(String result, int requestCode);
	}
	
	private static final String KEY = "type";
	
	private int myType = 0;

	public static MyPickerDialogFrag newInstance(int type) {
		Bundle bundle = new Bundle();
		bundle.putInt(KEY, type);
		MyPickerDialogFrag dFrag = new MyPickerDialogFrag();
		dFrag.setArguments(bundle);
		return dFrag;
	}
		
	@SuppressLint("NewApi")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		savedInstanceState = getArguments();
		myType = savedInstanceState.getInt(KEY);
		
		_current_dialog_is_open = true;

		Calendar c;
		final DatePickerDialog dlg;
		int year,month,day;
		AlertDialog.Builder builder;
		switch (myType) {
		case TYPE_DATE_PICKER:
	        // Use the current date as the default date in the picker
	         c = Calendar.getInstance();
	         year = c.get(Calendar.YEAR);
	         month = c.get(Calendar.MONTH);
	         day = c.get(Calendar.DAY_OF_MONTH);
	        // dlg = new DatePickerDialog(getSherlockActivity(), this, year, month, day);
	         dlg = new DatePickerDialog(getActivity(), R.style.PickerDialogTheme, getConstructorListener(), year, month, day);
	         dlg.getDatePicker().setMinDate(c.getTime().getTime());
	        //if request code matches mycar request codes for insurance/PUC expiry dates
	        //		then set title accordingly
	         if (hasJellyBeanAndAbove()) {
					dlg.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getString(android.R.string.ok),
							new DialogInterface.OnClickListener() {
								@SuppressLint("NewApi")
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Log.d("dialog-state", "Pos: " + _current_dialog_is_open);

									DatePicker dp = dlg.getDatePicker();
									onDateSet(dp, dp.getYear(), dp.getMonth(), dp.getDayOfMonth());;
								}
							});
					dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getActivity().getString(android.R.string.cancel),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Log.d("dialog-state", "Neg: " + _current_dialog_is_open);
									_current_dialog_is_open = false;
								}
							});
				}

	        if (getTargetRequestCode() == MyCarBaseFragment.REQUEST_CODE_INSURANCE_SET_DATE) {
	        	dlg.setTitle("Insurance Expiry Date:");
	        }
	        if (getTargetRequestCode() == MyCarBaseFragment.REQUEST_CODE_PUC_SET_DATE) {
	        	dlg.setTitle("PUC Expiry Date:");
        }
	        // Create a new instance of DatePickerDialog and return it
	        return dlg;
		case TYPE_DATE_PICKER_AFTER_2_DAYS:
	        // Use the current date as the default date in the picker
	        c = Calendar.getInstance();
	        c.add(Calendar.DATE, 2);
	        year = c.get(Calendar.YEAR);
	        month = c.get(Calendar.MONTH);
	        day = c.get(Calendar.DAY_OF_MONTH);
	       // dlg = new DatePickerDialog(getSherlockActivity(), this, year, month, day); 
	        dlg = new DatePickerDialog(getActivity(), R.style.PickerDialogTheme, getConstructorListener(), year, month, day);
	        dlg.getDatePicker().setMinDate(c.getTime().getTime());
 	        //if request code matches mycar request codes for insurance/PUC expiry dates
 	        //		then set title accordingly
	        if (hasJellyBeanAndAbove()) {
				dlg.setButton(DialogInterface.BUTTON_POSITIVE, getActivity().getString(android.R.string.ok),
						new DialogInterface.OnClickListener() {
							@SuppressLint("NewApi")
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Log.d("dialog-state", "Pos: " + _current_dialog_is_open);

								DatePicker dp = dlg.getDatePicker();
								onDateSet(dp, dp.getYear(), dp.getMonth(), dp.getDayOfMonth());;
							}
						});
				dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getActivity().getString(android.R.string.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Log.d("dialog-state", "Neg: " + _current_dialog_is_open);
								_current_dialog_is_open = false;
							}
						});
			}

 	        if (getTargetRequestCode() == MyCarBaseFragment.REQUEST_CODE_INSURANCE_SET_DATE) {
 	        	dlg.setTitle("Insurance Expiry Date:");
 	        }
 	        if (getTargetRequestCode() == MyCarBaseFragment.REQUEST_CODE_PUC_SET_DATE) {
 	        	dlg.setTitle("PUC Expiry Date:");
 	        }
 	        // Create a new instance of DatePickerDialog and return it
 	        return dlg;
		case TYPE_TIME_PICKER:
	        // Use the current time as the default time in the picker
	        c = Calendar.getInstance();
	        int hour = c.get(Calendar.HOUR_OF_DAY);
	        int minute = c.get(Calendar.MINUTE);

	        // Create a new instance of TimePickerDialog and return it
			int sdk = android.os.Build.VERSION.SDK_INT;
	        if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
	        	return new TimePickerDialog(getActivity(), this, hour, minute, false);
	        }
	        else {
	        	CustomTimePickerDialog ctpd = new CustomTimePickerDialog(getActivity(), R.style.PickerDialogTheme, this, hour, minute, false);
	        	ctpd.setButton(DialogInterface.BUTTON_NEGATIVE, getActivity().getString(android.R.string.cancel),new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						_current_dialog_is_open = false;
						
					}
				});
	        	ctpd.setTitle("Set Time");
	        	return ctpd;
	        }
		case TYPE_SERVICE_LIST_PICKER:
			builder = new Builder(getActivity());
			
			builder.setTitle("Choose Service Center");
			builder.setItems(StaticConfig.LOCS_SERVICE_STATIONS, this);
			
			return builder.create();
		case TYPE_ENQUIRY_LOC_LIST_PICKER:
			builder = new Builder(getActivity());
			
			builder.setTitle("Select One");
			builder.setItems(StaticConfig.LOCS_ENQUIRY, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					sendResultToTarget(StaticConfig.LOCS_ENQUIRY[which]);
					
				}
			});
			return builder.create();
		case TYPE_CHAT_ADVISOR_CITY_LIST_PICKER:
			builder = new Builder(getActivity());
			
			builder.setTitle("Choose Your Location");
			builder.setItems(StaticConfig.LOCS_CHAT_ADVISOR, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					sendResultToTarget(StaticConfig.LOCS_CHAT_ADVISOR[which]);
				}
			});
			builder.setCancelable(false);
			
			return builder.create();
		case TYPE_ENQUIRY_LIST_PICKER:
			builder = new Builder(getActivity());
			
			builder.setTitle("Select One");
			builder.setItems(StaticConfig.ENQUIRY_TYPE_LIST, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					sendResultToTarget(StaticConfig.ENQUIRY_TYPE_LIST[which]);
					
				}
			});
			return builder.create();
		case TYPE_CHAT_CEO_CITY_LIST_PICKER:
			builder = new Builder(getActivity());
			
			builder.setTitle("Choose Your Location");
			builder.setItems(StaticConfig.LOCS_CHAT_CEO, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					sendResultToTarget(StaticConfig.LOCS_CHAT_CEO[which]);
				}
			});
			builder.setCancelable(false);
			
			return builder.create();
			
		}
		
		return null;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		sendResultToTarget(StaticConfig.LOCS_SERVICE_STATIONS[which]);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		String dateToSet = (dayOfMonth < 10 ? "0" + dayOfMonth : dayOfMonth) + "-" + ((monthOfYear+1) < 10 ? "0"+(monthOfYear+1) : (monthOfYear+1)) + "-" + year;
		
		sendResultToTarget(dateToSet);
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		String timeToSet = (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute);
		
		sendResultToTarget(timeToSet);
	}
	
	private void sendResultToTarget (String str) {
		_current_dialog_is_open = false;

		((onSendResultListener) getTargetFragment()).onSendResult(str, getTargetRequestCode());
	}
	
	private static boolean hasJellyBeanAndAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }
    
    private OnDateSetListener getConstructorListener() {
        return hasJellyBeanAndAbove() ? null : mListener;
    }

	public static boolean isPickerDialogOpen() {
		return _current_dialog_is_open;
	}
}
