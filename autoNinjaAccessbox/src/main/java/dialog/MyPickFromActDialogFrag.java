package dialog;


import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import config.StaticConfig;

public class MyPickFromActDialogFrag extends DialogFragment
		implements DialogInterface.OnClickListener {
	
	public static final int TYPE_LOCATE_LIST_PICKER = 11;
		
	private static boolean _current_dialog_is_open = false;

	public interface onSendResultListener {
		public void onSendResult(String result, int requestCode);
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onCancel(dialog);
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onDismiss(dialog);
	}
	
	private static final String KEY = "type";
	private int myType = 0;
	private String[] mList;
	private HashMap<Integer, String> listMap;

	public static MyPickFromActDialogFrag newInstance(int type) {
		Bundle bundle = new Bundle();
		bundle.putInt(KEY, type);
		
		MyPickFromActDialogFrag dFrag = new MyPickFromActDialogFrag();
		dFrag.setArguments(bundle);
		return dFrag;
	}
		
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		savedInstanceState = getArguments();
		myType = savedInstanceState.getInt(KEY);

		_current_dialog_is_open = true;

		switch (myType) {
		case TYPE_LOCATE_LIST_PICKER:
			AlertDialog.Builder builder = new Builder(getActivity());
			
			mList = StaticConfig.getLocatorList();
			listMap = (HashMap<Integer, String>) StaticConfig.LOCATOR_OPTIONS;
			
			builder.setTitle("Locator Options: ");
			builder.setItems(mList, this);
			
			return builder.create();
		}
		return null;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		for (Map.Entry<Integer, String> entry : listMap.entrySet()) {
			if (entry.getValue().equalsIgnoreCase(mList[which])) {
				sendResultToTarget("" + entry.getKey());
				break;
			}
		}
	}

	private void sendResultToTarget (String str) {
		_current_dialog_is_open = false;

		((onSendResultListener) getActivity()).onSendResult(str, myType);
	}

	public static boolean isPickerDialogOpen() {
		return _current_dialog_is_open;
	}
}
