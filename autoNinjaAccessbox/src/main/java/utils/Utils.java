package utils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import myJsonData.MyJSONData;

import com.myaccessbox.appcore.R;

import config.StaticConfig;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

public class Utils {

	public  static boolean isNetworkConnected(Context cxt) {
		ConnectivityManager connMgr = (ConnectivityManager)cxt.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
		if (activeNetworkInfo == null)
			return false;
		else
			return activeNetworkInfo.isConnected();
	}
	/*
	 * Registration Number validation
	 */
	public static boolean isRegNumValid(String str)
    {
        boolean isValid = false;
        String expression = "[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}";
        CharSequence input = str;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(input);
        if(matcher.matches())
        {
            isValid = true;
        }
        return isValid;
    }
	private void downlodPDF(String imgNam, int type) {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		String url = "";
		File folder = null;
		switch(type){
			//offers
			case 0:
				 url = StaticConfig.API_OFFER_IMAGES_BASE + imgNam;
				 folder = new File(extStorageDirectory, "OffersPdf");
				break;
			//messages
			case 1:
				 url = StaticConfig.API_MESSAGE_IMAGES_BASE + imgNam;
				 folder = new File(extStorageDirectory, "pdf");
				 break;
		}
		// saving pdf into file
		if(null!=folder){
			folder.mkdir();
			File file = new File(folder, imgNam);
			try {
				file.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			new Downloader(new UICallback() {
				@Override
				public void OnUICallback(Boolean fetched) {
					// TODO Auto-generated method stub
					
				}
			}).execute(url,""+file);	
		}
		
			
	}
	@SuppressWarnings("deprecation")
	public static int validDate(String setString) {
		
		Date today = new Date();
		Calendar calender_today = Calendar.getInstance();
		calender_today.setTime(today);
		calender_today.set(Calendar.HOUR_OF_DAY, 0);
		calender_today.set(Calendar.MINUTE, 0);
		calender_today.set(Calendar.SECOND, 0);
		calender_today.set(Calendar.MILLISECOND, 0);
		today = calender_today.getTime();
		
		Date dt = MyJSONData.parseSelectedDate(setString);
		if (dt.getDay() == 0) {
			return 1;
		} else if(dt.equals(today)){
			return 2;
		}

		return 0;
	}
	public static int validBooking(String date) {
 		try {
 			Date dt = MyJSONData.parseSelectedDate(date);
 			Date tomorrowDate = new Date();
 			Date today = new Date();
			/*Date today = new Date();
 			Calendar calender_today = Calendar.getInstance();
 			calender_today.setTime(today);
 			calender_today.set(Calendar.HOUR_OF_DAY, 0);
 			calender_today.set(Calendar.MINUTE, 0);
 			calender_today.set(Calendar.SECOND, 0);
 			calender_today.set(Calendar.MILLISECOND, 0);
			today = calender_today.getTime();*/

 			Calendar calender_tom = Calendar.getInstance();
 			calender_tom.setTime(tomorrowDate);
 			calender_tom.add(Calendar.DATE,1);
 			calender_tom.set(Calendar.HOUR_OF_DAY, 0);
 			calender_tom.set(Calendar.MINUTE, 0);
 			calender_tom.set(Calendar.SECOND, 0);
 			calender_tom.set(Calendar.MILLISECOND, 0);
 			tomorrowDate = calender_tom.getTime();
 			String beforeTime = StaticConfig.BOOK_SERVICE_BEFORE;
 			Date beforeDate = new SimpleDateFormat("HH:mm:ss").parse(beforeTime);
 			Calendar calendarLimit = Calendar.getInstance();
 			calendarLimit.setTime(beforeDate);
 			//Date bookingTime = new SimpleDateFormat("HH:mm").parse(time);
 			Calendar calendarSet = Calendar.getInstance();
 			calendarLimit.set(Calendar.YEAR, calendarSet.get(Calendar.YEAR));
 			calendarLimit.set(Calendar.MONTH, calendarSet.get(Calendar.MONTH));
 			calendarLimit.set(Calendar.DAY_OF_MONTH, calendarSet.get(Calendar.DAY_OF_MONTH));
 			if(dt.equals(tomorrowDate) && calendarSet.getTime().before(calendarLimit.getTime())){
 				return 0;
 				//can book
			}/*else if(dt.getDay()==0){
 				return 1;
			}*/else if(dt.equals(tomorrowDate) && calendarSet.getTime().after(calendarLimit.getTime())){
 				return 2;
 				//can book day after tomorrow
			}else if(dt.equals(today)){
			}/*else if(dt.equals(today)){
 				return 3;
			}else return 0;
			}*/else return 0;
 		} catch (ParseException e) {

 			// TODO Auto-generated catch block

 			e.printStackTrace();

 		}

 		return 0;

 	}
}
