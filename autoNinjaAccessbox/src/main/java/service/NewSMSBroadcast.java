package service;

import config.StaticConfig;
import fragment.LoginPasswordFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class NewSMSBroadcast extends BroadcastReceiver {

	private static final String MESSAGE_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	@Override
	public void onReceive(Context context, Intent intent) {

		if(intent.getAction().equals(MESSAGE_RECEIVED)){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            String msgBody = "";
            if (bundle != null){
                //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();
                        //String msgBody = msgs[i].getMessageBody();
                        //System.out.println("Message From :"+ msg_from);
                        //System.out.println("Message Body :"+ msgBody);
                    }
                    if(msgs.length > -1){
                    	msg_from = msgs[0].getOriginatingAddress();
                    	if(msg_from.endsWith("ANINJA")){
                    		msgBody = msgs[0].getMessageBody();
                    		String[] otp = msgs[0].getMessageBody().split("Password");
                    		String[] otpArray = otp[1].trim().split("\\s+");
                    		StaticConfig.FIELD_LOGIN_OTP = removeLastChar(otpArray[1]);
                    		//System.out.println("StaticConfig.FIELD_LOGIN_OTP- "+ StaticConfig.FIELD_LOGIN_OTP );
                    		if(!StaticConfig.FIELD_LOGIN_OTP.equalsIgnoreCase(""))
                    		{
                    			LoginPasswordFragment.otpBroadcasted();
                    		}
                    		
                    	}
                    } else {
                    	Toast.makeText(context, "Failed to read OTP, Please Enter Manually", 0).show();
                    }
                }catch(Exception e){
//                            Log.d("Exception caught",e.getMessage());
                }
            }
        }
	}
	/**
		Otp contains dot(.) at the end, so using this method to remove it
	 */
	private String removeLastChar(String otpMessage) {
		return otpMessage.substring(0, otpMessage.length()-1);
		
	}

}
