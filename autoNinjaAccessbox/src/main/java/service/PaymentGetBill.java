/*
package service;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.citrus.mobile.Callback;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class PaymentGetBill extends AsyncTask<Void, Void, Void>{
	String billurl;

    JSONObject response;
    Callback callback;
    Context context;

    public PaymentGetBill(String url, double amount, Callback callback) {
        billurl = url;

        if (billurl != null) {
            if (billurl.contains("?")) {
                this.billurl = this.billurl + "&amount=" + amount;
            } else {
                this.billurl = this.billurl + "?amount=" + amount;
            }
        }

        this.callback = callback;
    }

    @Override
    protected Void doInBackground(Void... params) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(billurl);
        HttpResponse httpResponse;

        try {
            httpResponse = httpClient.execute(httpGet);
            response = new JSONObject(EntityUtils.toString(httpResponse.getEntity()));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (response != null) {
       //     Log.d("Citrus", "BILL RESPONSE::: " + response.toString());
       //     Toast.makeText(context, response.toString(), 0).show();
            callback.onTaskexecuted(response.toString(), "");
        } else {
            callback.onTaskexecuted("", "Is your billing url correct?");
        }
    }
}
*/
