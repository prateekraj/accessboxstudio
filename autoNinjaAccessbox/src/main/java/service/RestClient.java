package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RestClient  {

	private static String convertStreamToString(InputStream is) {
		/*
		 * To convert the InputStream to String we use the BufferedReader.readLine()
		 * method. We iterate until the BufferedReader return null which means
		 * there's no more data to read. Each line will appended to a StringBuilder
		 * and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				//sb.append(line + "\n");
				sb.append(line); 
				// for our purpose we do not need the line breaks in the received stream... 
				// so just append the whole as a single line => remove the (+ "\n") part
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/* This is a test function which will connects to a given
	 * rest service and prints it's response to Android Log with
	 * labels "Praeda".
	 */

	public static JSONObject connectJSON(String url) {
		HttpClient httpclient = new DefaultHttpClient();

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Examine the response status
			//Log.i("Praeda", response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream).trim();
				//Log.d("Praeda", result);

				// A Simple JSONObject Creation
				JSONObject json=new JSONObject(result);
				// Closing the input stream will trigger connection release

				instream.close();
				return json;
			}
			else {
				return new JSONObject();
			}

		} catch (ClientProtocolException e) {
			//Log.d("Praeda", "ClientProtocolException: " + e.toString());
		} catch (JSONException e) {
			//Log.d("Praeda", "JSONException: " + e.toString());
		} catch (IOException e) {
			//Log.d("Praeda", "IOException: " + e.toString());
		}

		return new JSONObject();
	}

	public static String connectJSONString(String url) {
		HttpClient httpclient = new DefaultHttpClient();

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Examine the response status
			//Log.i("Praeda", response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream);
				//Log.i("Praeda", result);

				// A Simple JSONObject Creation
				JSONObject json=new JSONObject(result);
				// Closing the input stream will trigger connection release

				instream.close();
				return json.toString();
			}
			else {
				return null;
			}

		} catch (ClientProtocolException e) {
			//e.printStackTrace();
		} catch (JSONException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}

		return null;
	}
	
	public static JSONObject connectJSONbyPost(String url, HttpEntity entityToSend) {
		//Log.d("RestClient", "connectJSONbyPost called!");
		HttpClient httpclient = new DefaultHttpClient();
		
		HttpPost httpPost = new HttpPost(url);
		
		// Execute the request
		HttpResponse response;
		try {
			httpPost.setEntity(entityToSend);

			response = httpclient.execute(httpPost);
			// Examine the response status

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream).trim();
				//Log.d("RestClient", result);

				// A Simple JSONObject Creation
				JSONObject json=new JSONObject(result);
				// Closing the input stream will trigger connection release

				instream.close();
				return json;
			}
			else {
				//Log.d("RestClient", "Response is null!");
				return new JSONObject();
			}
		} catch (ClientProtocolException e) {
			//Log.e("RestClient", "ClientProtocolException: " + e.getMessage());
		} catch (JSONException e) {
			//Log.e("RestClient", "JSONException: " + e.getMessage());
		} catch (IOException e) {
			//Log.e("RestClient", "IOException: " + e.getMessage());
		} 

		return new JSONObject();
	}

	//not used... only a template for testing
	public static void connect(String url)
	{

		HttpClient httpclient = new DefaultHttpClient();

		// Prepare a request object
		HttpGet httpget = new HttpGet(url); 

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			// Examine the response status
			//Log.i("Praeda",response.getStatusLine().toString());

			// Get hold of the response entity
			HttpEntity entity = response.getEntity();
			// If the response does not enclose an entity, there is no need
			// to worry about connection release

			if (entity != null) {

				// A Simple JSON Response Read
				InputStream instream = entity.getContent();
				String result= convertStreamToString(instream);
				//Log.i("Praeda",result);

				// A Simple JSONObject Creation
				JSONObject json=new JSONObject(result);
				//Log.i("Praeda","<jsonobject>\n"+json.toString()+"\n</jsonobject>");

				// A Simple JSONObject Parsing
				JSONArray nameArray=json.names();
				JSONArray valArray=json.toJSONArray(nameArray);
				for(int i=0;i<valArray.length();i++)
				{
					/*
					Log.i("Praeda","<jsonname"+i+">\n"+nameArray.getString(i)+"\n</jsonname"+i+">\n"
							+"<jsonvalue"+i+">\n"+valArray.getString(i)+"\n</jsonvalue"+i+">");
					*/
				}

				// A Simple JSONObject Value Pushing
				json.put("sample key", "sample value");
				//Log.i("Praeda","<jsonobject>\n"+json.toString()+"\n</jsonobject>");

				// Closing the input stream will trigger connection release
				instream.close();
			}


		} catch (ClientProtocolException e) {
			//e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (JSONException e) {
			//e.printStackTrace();
		}
	}
}