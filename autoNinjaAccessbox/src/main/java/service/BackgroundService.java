package service;

import entity.ChatMessages;
import entity.Messages;
import entity.MyCar;
import entity.Offers;
import fragment.ChatAdvisorFragment;
import fragment.ChatCEOFragment;
import fragment.HotlinesFragment;
import fragment.OffersListFragment;
import fragment.PaymentHistoryFragment;
import fragment.RewardHistoryFragment;
import fragment.TooltipsListFragment;
import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

import myJsonData.MessagesData;
import myJsonData.MyJSONData;
import utils.Downloader;
import utils.MyIO;
import utils.Preference;
import utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.UserDataHandler;

import activity.MainActivity;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
//import android.util.Log;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.myaccessbox.appcore.R;

import config.StaticConfig;

import db.CarDetailsDatasource;
import db.ChatDataSource;
import db.DatabaseHelper;
import db.HotlinesDataSource;
import db.ServiceHistoryDataSource;
import db.TableContract.CarDetailsDB;
import db.TableContract.ChatDB;
import db.TableContract.ServiceDetailsDB;
import db.UserDetailsDataSource;
import db.TableContract.UserDB;

public class BackgroundService extends Service {// public static final
	public static final String INTENT_EXTRA_NEW_DELAY_KEY = "new_delay";
	public static final String INTENT_FILTER_CHAT_UPDATE_ACTION = "com.myaccessbox.actions.chatMessageUpdate";
	public static final String INTENT_FILTER_REWARD_HISTORY_UPDATE_ACTION = "com.myaccessbox.actions.rewardHistoryUpdate";
	public static final String INTENT_FILTER_INBOX_MESSAGE_UPDATE_ACTION = "com.myaccessbox.inboxMessageUpdate";
	public static final String INTENT_FILTER_PAYMENT_HISTORY_UPDATE_ACTION = "com.myaccessbox.actions.paymentHistoryUpdate";
	public static final int DELAY_DEFAULT = 5 * 60000; // 5*60000 = 5 minutes
	public static final int DELAY_EXPRESS = 10000; // 10000 = 10 seconds
	public static final int DELAY_MEDIUM = 30000; // 30000 = 30 seconds
	public static final int DELAY_LONG = 30 * 60000; // 30*60000 = 30 minutes

	// private static final
	private static final String TAG = "UpdaterService";

	// private static
	private static int DELAY = DELAY_DEFAULT;
	private static int CHAT_ROLE_OBSERVER = 0;

	private static boolean REWARD_HISTORY_ROLE_OBSERVER = false;
	private static boolean INBOX_MESSAGE_ROLE_OBSERVER = false;
	private static boolean PAYMENT_HISTORY_ROLE_OBSERVER = false;

	// private
	private boolean runFlag = false;
	private boolean threadIsSleeping = false;
	private boolean internalInterrupt = false;
	private Updater udtr;
	private NotificationManager notifyManager;
	private Context ctxt;
	public CarDetailsDatasource carDetailDataSource;
	public UserDetailsDataSource userDetailsDataSource;
	public ServiceHistoryDataSource servceHistryDataSource;
	public HotlinesDataSource hotlineDataSource;
	public ChatDataSource chatDataSource;
	private long mobile = 0;
	private int changeset = 0;
	private int offers_changeset = 0;
	private int rewards_changeset = 0;
	private int payment_changeset = 0;
	// private String chat_largest_message_id = "";
	private String otp = "";
	private SimpleDateFormat sdf, sdf2;
	private boolean firstReceive = false;
	private String registration = "";
	private String gcmRegId = "";	

	private Tracker tracker;
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onCreate() {
		super.onCreate();

		udtr = new Updater();
		notifyManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
		ctxt = getApplicationContext();
		sdf = new SimpleDateFormat("dd-MM-yyyy");
		sdf2 = new SimpleDateFormat("MMM dd, yyyy");
		tracker = EasyTracker.getInstance(this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		 Log.d(TAG, "onStartCommand called!");

		boolean b = false;
		if (intent != null) {
			DELAY = intent.getIntExtra(INTENT_EXTRA_NEW_DELAY_KEY, DELAY_DEFAULT);
			b = intent.hasExtra(INTENT_EXTRA_NEW_DELAY_KEY);
		}
		if (!this.runFlag) {
			this.runFlag = true;
			this.udtr.start();
			// Log.d(TAG, "Not running so onStarted");
		} else {
			// Log.d(TAG, "Already running!!");
			if (b && threadIsSleeping) {
				internalInterrupt = true;
				udtr.interrupt();
			} else {
			}
		}

		// Log.d(TAG, "Current DELAY = " + DELAY);
		return START_STICKY;

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		runFlag = false;
		udtr.interrupt();
		udtr = null;
		// Log.d(TAG, "onDestroyed");
	}

	private class Updater extends Thread {
		public Updater() {
			super("UpdaterService-Updater");
		}

		@Override
		public void run() {
			// System.out.println("DB BG STARTED");
			BackgroundService usrvs = BackgroundService.this;
			carDetailDataSource = new CarDetailsDatasource(ctxt);
			userDetailsDataSource = new UserDetailsDataSource(ctxt);
			servceHistryDataSource = new ServiceHistoryDataSource(ctxt);
			hotlineDataSource = new HotlinesDataSource(ctxt);
			chatDataSource = new ChatDataSource(ctxt);
			ContentValues cv = new ContentValues();
			while (usrvs.runFlag) {
				 Log.d(TAG, "Updater Running");

				if (userDetailsDataSource.isPhoneNumberExist()) {
					try {
						SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
						MyJSONData ownData = new MyJSONData(ctxt, MyJSONData.TYPE_OWN);
						Calendar nowCal = Calendar.getInstance();
						String nowStr = (nowCal.get(Calendar.DAY_OF_MONTH) > 9 ? nowCal.get(Calendar.DAY_OF_MONTH) : ("0" + nowCal
								.get(Calendar.DAY_OF_MONTH)))
								+ "-"
								+ (nowCal.get(Calendar.MONTH) + 1 > 9 ? (nowCal.get(Calendar.MONTH) + 1) : ("0" + (nowCal.get(Calendar.MONTH) + 1)))
								+ "-" + nowCal.get(Calendar.YEAR);
						Date nowDate = sdf.parse(nowStr);
						String[] lastChkFlag = userDetailsDataSource.getLastApiCheck().split(":");
						Date lastUpdate = sdf.parse(lastChkFlag[0]);
						int rndHr = userDetailsDataSource.getRandomHr();
						int nowHr = nowCal.get(Calendar.HOUR_OF_DAY);
						// Check for reminders and alarms
						if (StaticConfig.myCar.getCarId() > 0 && nowHr > 9 && nowHr < 19 && !firstReceive) {
							reminderCheck(nowCal, nowDate, nowStr);
						}
						// check internet connection before API call
						if (Utils.isNetworkConnected(ctxt)) {
							firstReceive = false;
							Cursor c = userDetailsDataSource.getUserDetails();
							mobile = c.getLong(c.getColumnIndex(UserDB.COL_FIELD_OWN_NUMBER));
							registration = URLEncoder.encode(c.getString(c.getColumnIndex(UserDB.COL_FIELD_OWN_REG_NUMBER)), "UTF-8");
							changeset = c.getInt(c.getColumnIndex(UserDB.COL_FIELD_OWN_TOOLTIPS_CHANGESET));
							offers_changeset = c.getInt(c.getColumnIndex(UserDB.COL_FIELD_OWN_OFFERS_CHANGESET));
							rewards_changeset = c.getInt(c.getColumnIndex(UserDB.COL_FIELD_OWN_REWARD_CHANGESET));
							payment_changeset = c.getInt(c.getColumnIndex(UserDB.COL_FIELD_OWN_PAYMENT_CHANGESET));
							userDetailsDataSource.close();
							otp = userDetailsDataSource.getUserOtp();
							//gcmRegId = URLEncoder.encode(StaticConfig.REG_ID_GCM, "UTF-8");
							gcmRegId = prefs.getString("regId", "");
							// System.out.println("OTP:" + otp);
							if (StaticConfig.myCar.getCarId() > 0 || userDetailsDataSource.getError().trim().equalsIgnoreCase("DATA_NOT_FOUND")) {
								// check once a day
								cv = new ContentValues();
								if (lastUpdate.before(nowDate) && nowHr >= rndHr) {
									// Dealer data API call to check mycar once
									// a day at around noon
									checkAPImyCarUpdate();
									cv.put(UserDB.COL_FIELD_LAST_API_CHECK, nowStr + ":A");
									userDetailsDataSource.updateUserDetails(cv);
								} else if (lastUpdate.equals(nowDate) && nowHr >= (rndHr + 12) && lastChkFlag[1].equalsIgnoreCase("A")) {
									checkAPImyCarUpdate();
									cv.put(UserDB.COL_FIELD_LAST_API_CHECK, nowStr + ":P");
									userDetailsDataSource.updateUserDetails(cv);
								}
							} else {
								// Dealer data API call to check mycar
								if(Preference.getBooleanPreference(getApplicationContext(), Preference.CHECK_CAR_API_BOOLEAN, false)){
									//System.out.println("BAckground : 4 "+Preference.getBooleanPreference(getApplicationContext(), Preference.CHECK_CAR_API_BOOLEAN, false));
									Preference.setBooleanPreference(getApplicationContext(), Preference.CHECK_CAR_API_BOOLEAN, false);
									checkAPImyCarUpdate();
							 }
							}

							String android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
							String executeURL = StaticConfig.API_FETCH_CMS_DATA + "?phone=" + mobile;
							executeURL += "&changeset=" + changeset;
							executeURL += "&offers_changeset=" + offers_changeset;
							executeURL += "&rewards_changeset=" + rewards_changeset;
							executeURL += "&chat_changeset=" + chatDataSource.getLargestServerMsgId();
							executeURL += "&otp=" + otp;
							executeURL += "&device_type=" + "android";
							executeURL += "&payment_changeset=" + payment_changeset; 
							executeURL += "&" + StaticConfig.getCMSPass();
							String refCodeStr = URLEncoder.encode(userDetailsDataSource.getReferralCode(), "UTF-8");
							if (StaticConfig.FEATURE_REFERRAL_CODE && !refCodeStr.equalsIgnoreCase("")) {
								executeURL += "&referral_code=" + refCodeStr;
							}
							if(!gcmRegId.equalsIgnoreCase("")){
							executeURL += "&device_token=" + gcmRegId;
							}

							//System.out.println("Just Checking- "+executeURL);
							JSONObject newJSONObj = RestClient.connectJSON(executeURL);
							//Log.d(TAG, "APP VERSION JSON------------------------------RestClient result for messages.php: " + newJSONObj.toString());
							JSONObject tempObj;

							if (newJSONObj.has("android_version")) {
								System.out.println("Android Version- "+ newJSONObj.getString("android_version").trim());
								cv = new ContentValues();
								cv.put(UserDB.COL_FIELD_OWN_LATEST_VERSION, newJSONObj.getString("android_version").trim());
								userDetailsDataSource.updateUserDetails(cv);
							}

							// checking for rewards
							if (StaticConfig.FEATURE_REWARD_POINT) {
								cv = new ContentValues();
								if (newJSONObj.has("rewards")) {
									JSONObject rewardJsonObject = newJSONObj.getJSONObject("rewards");
									if (rewardJsonObject.has("latest_rp_trxn_id") && rewardJsonObject.getInt("latest_rp_trxn_id") > 0) {

										boolean updateRewardsDone = false;
										if (rewardJsonObject.has("rewards_history")) {

											JSONArray jsonArray = rewardJsonObject.getJSONArray("rewards_history");
											if (jsonArray.length() > 0) {
												updateRewardsDone = RewardHistoryFragment.appendRewardsToDisk(ctxt, jsonArray);
												if (REWARD_HISTORY_ROLE_OBSERVER) {
													LocalBroadcastManager.getInstance(ctxt).sendBroadcast(new Intent(INTENT_FILTER_REWARD_HISTORY_UPDATE_ACTION));
												}

											}
										}

										if (updateRewardsDone) {
											cv.put(UserDB.COL_FIELD_OWN_REWARD_CHANGESET,
													Integer.parseInt(rewardJsonObject.getString("latest_rp_trxn_id")));
											cv.put(UserDB.COL_FIELD_OWN_REWARD_POINTS_TOTAL,
													Integer.parseInt(rewardJsonObject.getString("total_points")));
											userDetailsDataSource.updateUserDetails(cv);
										}
									}
								}
							}
							
							// checking for rewards
							if (StaticConfig.FEATURE_PAYMENT_HISTORY_FEATURE) {
								cv = new ContentValues();
								if (newJSONObj.has("payments")) {
									JSONObject paymentJsonObject = newJSONObj.getJSONObject("payments");
									if (paymentJsonObject.has("latest_payment_trxn_id") && paymentJsonObject.getInt("latest_payment_trxn_id") > 0) {

										boolean updatePaymentsDone = false;
										if (paymentJsonObject.has("payment_history")) {

											JSONArray jsonArray = paymentJsonObject.getJSONArray("payment_history");
											if (jsonArray.length() > 0) {
												updatePaymentsDone = PaymentHistoryFragment.appendPaymenttToDisk(ctxt, jsonArray);
												if (PAYMENT_HISTORY_ROLE_OBSERVER) {
													LocalBroadcastManager.getInstance(ctxt).sendBroadcast(new Intent(INTENT_FILTER_PAYMENT_HISTORY_UPDATE_ACTION));
												}

											}
										}

										if (updatePaymentsDone) {
											cv.put(UserDB.COL_FIELD_OWN_PAYMENT_CHANGESET,
													Integer.parseInt(paymentJsonObject.getString("latest_payment_trxn_id")));
											userDetailsDataSource.updateUserDetails(cv);
										}
									}
								}
							}

							// checking new messages
							if (StaticConfig.FEATURE_MESSAGES_ENABLED) {
								if (newJSONObj.has("new_messages")) {
									JSONArray newJSONArray = newJSONObj.getJSONArray("new_messages");
									JSONArray finalJSONArray = new JSONArray();

									int i, newCount = newJSONArray.length();
									String textDisp = "";
									if (newCount > 0) {
										String url, imgNam;
										Bitmap bitmap;
										for (i = 0; i < newCount; i++) {
											tempObj = new JSONObject();
											tempObj = newJSONArray.getJSONObject(i);
											tempObj.put("read", false);
											textDisp = tempObj.getString("subject");

											/*imgNam = tempObj.getString("img").trim();
											if (!imgNam.equalsIgnoreCase("") && !imgNam.contains(".pdf")) {
												String urlString = StaticConfig.API_MESSAGE_IMAGES_BASE + imgNam;
												// System.out.println("urlString: "+urlString);
												url = urlString.replaceAll(" ", "%20");
												bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());

												FileOutputStream fimOut = ctxt.openFileOutput(imgNam, MODE_PRIVATE);
												Boolean b = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fimOut);
												fimOut.close();
												bitmap = null;
											} else if (!imgNam.equalsIgnoreCase("") && imgNam.contains(".pdf")) {
												// saving pdf into file
												String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
												File folder = new File(extStorageDirectory, "pdf");
												folder.mkdir();
												File file = new File(folder, imgNam);
												try {
													file.createNewFile();
												} catch (IOException e1) {
													e1.printStackTrace();
												}
												url = StaticConfig.API_MESSAGE_IMAGES_BASE + imgNam;
												Downloader.DownloadFile(url, file);
											}
*/
											finalJSONArray.put(tempObj);
										}
										MessagesData.appendMessages(ctxt, finalJSONArray);

										if (INBOX_MESSAGE_ROLE_OBSERVER) {
											LocalBroadcastManager.getInstance(ctxt).sendBroadcast(
													new Intent(INTENT_FILTER_INBOX_MESSAGE_UPDATE_ACTION));
										}

										// raise notification
										Notification.Builder builder = new Notification.Builder(ctxt);
										//Notification noti = new Notification(R.drawable.messages, textDisp, System.currentTimeMillis());
										Intent iin = new Intent(ctxt, MainActivity.class);
										iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_NEW_MESSAGE_NOTIFICATION);

										PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin,
												Intent.FLAG_ACTIVITY_NEW_TASK);

										builder.setSmallIcon(R.drawable.messages)
									       .setContentTitle("New Messages")
									       .setContentText(textDisp)
									       .setContentIntent(pendIntent);
									    Notification noti = builder.getNotification();   
										noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
										noti.number = newCount;
										noti.flags = Notification.FLAG_AUTO_CANCEL;

										long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
										noti.vibrate = x;

										notifyManager.notify("NewMessages", 1, noti);
										
										tracker.send(GATrackerMaps.getBackgroundNotifyEvent("New Messages", (long) newCount));
									}
								}
							}

							// checking tooltips
							if (StaticConfig.FEATURE_TOOLTIPS_ENABLED) {
								cv = new ContentValues();
								if (newJSONObj.has("tooltips")) {
									// save file
									JSONObject ttObj = newJSONObj.getJSONObject("tooltips");

									JSONArray ttArr = ttObj.getJSONArray("arr");

									JSONObject finalJSONObj = new JSONObject();
									finalJSONObj.put("tooltips_list", ttArr);

									// Write the string to the file
									boolean b = MyIO.writeStringToFile(ctxt, TooltipsListFragment.TOOLTIPS_DATA_FILENAME, finalJSONObj.toString());
									// once writing is done, NOW update
									// changeset
									// number on disk
									if (b && ttArr.length() > 0) {
										cv.put(UserDB.COL_FIELD_OWN_TOOLTIPS_CHANGESET, Integer.parseInt(ttObj.getString("changeset").trim()));
										userDetailsDataSource.updateUserDetails(cv);
										// raise notification
										String textDisp = StaticConfig.DEALER_FULL_NAME + " has updated new tips for you and your car!";
										Notification.Builder builder = new Notification.Builder(ctxt);
										//Notification noti = new Notification(R.drawable.burger_tooltips, textDisp, System.currentTimeMillis());
										Intent iin = new Intent(ctxt, MainActivity.class);
										iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_NEW_TOOLTIPS_NOTIFICATION);

										PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin,
												Intent.FLAG_ACTIVITY_NEW_TASK);
										builder.setSmallIcon(R.drawable.burger_tooltips)
											   .setContentTitle("Tooltips")
										       .setContentText(textDisp)
										       .setContentIntent(pendIntent);
										Notification noti = builder.getNotification();
										noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
										noti.flags = Notification.FLAG_AUTO_CANCEL;

										long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
										noti.vibrate = x;

										notifyManager.notify("TooltipsUpdate", 1, noti);
										tracker.send(GATrackerMaps.getBackgroundNotifyEvent("Tooltips Updated", (Long) null));
									}
								}
							}

							// checking chat messages
							if (StaticConfig.FEATURE_CHAT_ENABLED) {

								sendQueuedChatMessages();

								if (newJSONObj.has("chat")) {
									JSONObject chatObj = newJSONObj.getJSONObject("chat");
									if (chatObj.has("chat_data")) {
										JSONArray chatArray = chatObj.getJSONArray("chat_data");
										if (chatArray.length() > 0) {
											// found incoming chat messages!!!
											JSONObject chatMsgObj;
											ContentValues cv_msg;
											int[] senderMsgCount = {0, 0};// 0th position is for Advisor, 1st is for CEO

											for (int t = 0; t < chatArray.length(); t++) {
												chatMsgObj = chatArray.getJSONObject(t);

												cv_msg = new ContentValues();
												cv_msg.put(ChatDB.COL_CHAT_SERVER_MSG_ID, chatMsgObj.getInt("msgId"));// server
																														// msg
																														// id
												cv_msg.put(ChatDB.COL_CHAT_MSG, chatMsgObj.getString("msg"));// message
																												// text
												cv_msg.put(ChatDB.COL_CHAT_SENDER, chatMsgObj.getInt("msgSender"));// message
																													// sender
												cv_msg.put(ChatDB.COL_CHAT_RECEIVER, ChatMessages.SENDER_USER);
												cv_msg.put(ChatDB.COL_CHAT_READ_FLAG, 0);
												cv_msg.put(ChatDB.COL_CHAT_TYPE, 0);
												cv_msg.put(ChatDB.COL_CHAT_DELIVERY_FLAG, 1);// message
												cv_msg.put(ChatDB.COL_CHAT_TIMESTAMP,
														chatDataSource.getServerTimeString(chatMsgObj.getString("msgTime")));// message
																																// sent
																																// time
												chatDataSource.insertServerMessages(cv_msg);

												senderMsgCount[chatMsgObj.getInt("msgSender") + 2]++;
											}

											if (CHAT_ROLE_OBSERVER < 0) {
												LocalBroadcastManager.getInstance(ctxt).sendBroadcast(new Intent(INTENT_FILTER_CHAT_UPDATE_ACTION));
											} else {
												String textDisp = "", notificationIdentifier = "";
												Notification.Builder builder = new Notification.Builder(ctxt);
												//Notification noti = new Notification();
												Intent iin = new Intent(ctxt, MainActivity.class);
												if (senderMsgCount[0] > 0) {// service Advisor has a message (Higher Priority)
													textDisp = "New live quote response from " + StaticConfig.DEALER_FULL_NAME + " Service Advisor!";
													//noti = new Notification(R.drawable.service_quote, textDisp, System.currentTimeMillis());
													iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY,
															MainActivity.EXTRA_NEW_ADVISOR_CHAT_NOTIFICATION);
													builder.setSmallIcon(R.drawable.service_quote);
													notificationIdentifier = ChatAdvisorFragment.CHAT_NOTIFICATION_IDENTIFIER;
													builder.setContentTitle(notificationIdentifier)
															.setContentText(textDisp);
												} else if (senderMsgCount[1] > 0) {// ceo has a msg (Lower priority)
													textDisp = StaticConfig.DEALER_FULL_NAME + " CEO has replied to your feedback!";
													//noti = new Notification(R.drawable.you, textDisp, System.currentTimeMillis());
													iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY,
															MainActivity.EXTRA_NEW_CEO_CHAT_NOTIFICATION);
													builder.setSmallIcon(R.drawable.you);
													notificationIdentifier = ChatCEOFragment.CHAT_NOTIFICATION_IDENTIFIER;
													builder.setContentTitle(notificationIdentifier)
															.setContentText(textDisp);
												} else {
													// Log.e(TAG,
													// "Should not happen!");
												}

												// Common code
												PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin,
														Intent.FLAG_ACTIVITY_NEW_TASK);
												builder.setContentIntent(pendIntent);

												/*noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp
														+ " Click here to read more.", pendIntent);*/
												Notification noti = builder.getNotification();
												noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
												noti.flags = Notification.FLAG_AUTO_CANCEL;

												long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
												noti.vibrate = x;
												notifyManager.notify(notificationIdentifier, 1, noti);
												// end of common code
											}
										}
									}
								}
							}
							// checking offers
							if (StaticConfig.FEATURE_OFFERS_ENABLED) {
								cv = new ContentValues();
								if (newJSONObj.has("offers")) {
									int i;
									ArrayList<String> oldOfferImgs = new ArrayList<String>();
									Offers[] ofrs = OffersListFragment.getOffersData(ctxt);
									// Log.d(TAG, "Existing No. of Offers: " +
									// ofrs.length);
									for (i = 0; i < ofrs.length; i++) {
										if (!ofrs[i].getImage().trim().equalsIgnoreCase("")) {
											oldOfferImgs.add(ofrs[i].getImage().trim());
										}
									}
									JSONObject ttObj = newJSONObj.getJSONObject("offers");

									JSONArray ttArr = ttObj.getJSONArray("arr");

									// download new images
									/*String url, imgNam = "";
									File imgFile;
									JSONObject tttJso;
									Bitmap bitmap = null;

									for (i = 0; i < ttArr.length(); i++) {
										// Log.d(TAG, "Offer Index: " + i);
										tttJso = ttArr.getJSONObject(i);
										imgNam = tttJso.getString("img");
										if (!imgNam.trim().equalsIgnoreCase("") && !imgNam.contains(".pdf")) {
											imgFile = ctxt.getFileStreamPath(imgNam);

											if (!imgFile.exists()) {
												// url =
												// StaticConfig.API_OFFER_IMAGES_BASE
												// + imgNam;
												String urlString = StaticConfig.API_OFFER_IMAGES_BASE + imgNam;
												// System.out.println("urlString: "+urlString);
												url = urlString.replaceAll(" ", "%20");
												bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());

												FileOutputStream fimOut = ctxt.openFileOutput(imgNam, MODE_PRIVATE);
												Boolean b1 = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fimOut);
												fimOut.close();
												bitmap = null;
											} else {
												oldOfferImgs.remove(imgNam); // remove from list of old names of images to be deleted.
											}
										} else if (!imgNam.trim().equalsIgnoreCase("") && imgNam.contains(".pdf")) {
											// saving pdf into file
											String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
											File folder = new File(extStorageDirectory, "OffersPdf");
											folder.mkdir();
											File file = new File(folder, imgNam);
											try {
												file.createNewFile();
											} catch (IOException e1) {
												e1.printStackTrace();
											}
											url = StaticConfig.API_OFFER_IMAGES_BASE + imgNam;
											//Downloader.DownloadFile(url, ""+file);
										}
									}*/

									// save file
									JSONObject finalJSONObj = new JSONObject();
									finalJSONObj.put("offers_list", ttArr);

									// Write the string to the file
									boolean b = MyIO.writeStringToFile(ctxt, OffersListFragment.OFFERS_DATA_FILENAME, finalJSONObj.toString());
									// file write to disk done, NOW update
									// internal
									// changeset number
									if (b) {
										for (String t : oldOfferImgs) {
											b = ctxt.deleteFile(t);
										}

										cv.put(UserDB.COL_FIELD_OWN_OFFERS_CHANGESET, Integer.parseInt(ttObj.getString("changeset").trim()));
										userDetailsDataSource.updateUserDetails(cv);
										if (ttArr.length() > 0) {
											// raise notification
											String textDisp = StaticConfig.DEALER_FULL_NAME + " has new Offers for you and your friends!";
											Notification.Builder builder = new Notification.Builder(ctxt);
											//Notification noti = new Notification(R.drawable.offers, textDisp, System.currentTimeMillis());
											Intent iin = new Intent(ctxt, MainActivity.class);
											iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_NEW_OFFERS_NOTIFICATION);

											PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin,
													Intent.FLAG_ACTIVITY_NEW_TASK);

											builder.setSmallIcon(R.drawable.offers)
										       .setContentTitle("Offers Updated")
										       .setContentText(textDisp)
										       .setContentIntent(pendIntent);
											Notification noti = builder.getNotification();
											/*noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp
													+ " Click here to read more.", pendIntent);*/
											noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
											noti.flags = Notification.FLAG_AUTO_CANCEL;

											long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
											noti.vibrate = x;

											notifyManager.notify("NewOffers", 1, noti);
											tracker.send(GATrackerMaps.getBackgroundNotifyEvent("Offers Updated", (Long) null));
										}
									}
								}
							}

							if (StaticConfig.FEATURE_DOCUMENTS_ENABLED) {
								// add code later
							}

							// unused image files cleanup code
							if (ownData.fetchData("unused_image_cleanup").trim().equalsIgnoreCase("")) {
								int i;
								ArrayList<String> inUseImgNams = new ArrayList<String>();
								// get message images in use
								Messages[] msgs = MessagesData.getMessagesFromFile(ctxt);
								for (i = 0; i < msgs.length; i++) {
									if (!msgs[i].getImage().trim().equalsIgnoreCase("")) {
										inUseImgNams.add(msgs[i].getImage().trim());
									}
								}
								Offers[] ofrs = OffersListFragment.getOffersData(ctxt);
								for (i = 0; i < ofrs.length; i++) {
									if (!ofrs[i].getImage().trim().equalsIgnoreCase("")) {
										inUseImgNams.add(ofrs[i].getImage().trim());
									}
								}

								ArrayList<String> s = new ArrayList<String>(Arrays.asList(ctxt.fileList()));
								// Log.d(TAG, s.size() +
								// " Files to be assessed!");
								i = 0;
								long a = 0, b = 0;
								for (String chkFile : s) {
									if (chkFile.substring(chkFile.length() - 3, chkFile.length()).equalsIgnoreCase("txt")
											|| inUseImgNams.contains(chkFile)) {
										a += ctxt.getFileStreamPath(chkFile).length();
									} else {
										ctxt.deleteFile(chkFile);
										i++;
										b += ctxt.getFileStreamPath(chkFile).length();
									}
								}

								if (MyJSONData.dataFileExists(ctxt, MyJSONData.TYPE_OWN)) {
									MyJSONData.editMyData(ctxt, new String[]{"unused_image_cleanup"}, new String[]{"done"}, MyJSONData.TYPE_OWN);
								}else{
									MyJSONData.editMyData(ctxt, new String[]{"unused_image_cleanup"}, new String[]{"done"}, MyJSONData.TYPE_OWN);
								}
							}
						}
					} catch (JSONException e) {
						// Log.e(TAG, "JSONException: " + e.toString());
					} catch (IOException e) {
						// Log.e(TAG, "IOException: " + e.toString());
					} catch (NullPointerException e) {
						// Log.e(TAG, "NullPointerException: " + e.toString() +
						// "||" + e.getMessage());
					} catch (ParseException e) {
						// Log.e(TAG, "ParseException: " + e.toString());
					}
				}
				// sleep
				try {
					// Log.d(TAG, "Sleeping for: " + (DELAY / 1000) +
					// " seconds...");
					threadIsSleeping = true;
					Thread.sleep(DELAY);
					threadIsSleeping = false;
				} catch (InterruptedException e) {
					// use 'internalInterrupt' here to make sure WHILE carries
					// on when necessary
					usrvs.runFlag = internalInterrupt;
					internalInterrupt = false;// reset this to false
					threadIsSleeping = false;
				}
			}
		}
	}

	private void checkAPImyCarUpdate() {
		String android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
		String mycarURL = StaticConfig.API_FETCH_DEALER_DATA + "?phone=" + mobile;
		mycarURL += "&otp=" + otp;
		mycarURL += "&device_type=" + "android";
		if(!gcmRegId.equalsIgnoreCase("")){
			mycarURL += "&device_token=" + gcmRegId;
			}
		mycarURL += "&" + StaticConfig.getCMSPass();

		if (StaticConfig.FEATURE_REGISTRATION_NUMBER_LOGIN && !registration.equalsIgnoreCase("")) {
			mycarURL += "&reg=" + registration;
		}
		 System.out.println("URL FOR APP BUFFER:" + mycarURL);
		JSONObject myCarResult = RestClient.connectJSON(mycarURL);
		parseTheCarJson(myCarResult);
		/*ServiceLocations.fetchLocations(getApplicationContext());
		HotlinesFragment temp = new HotlinesFragment();
		temp.fetchHotlinesFromServer(getApplicationContext());*/

	}

	private void parseTheCarJson(JSONObject myCarResult) {
		try {
			// StaticConfig.myCar.setCarId(0);
			ContentValues cv;
			if (myCarResult.has("error") && myCarResult.getString("error").equalsIgnoreCase("true")) {
				cv = new ContentValues();
				cv.put(UserDB.COL_FIELD_JSON_TEXT, myCarResult.getString("text"));
				if (userDetailsDataSource.isPhoneNumberExist()) {
					userDetailsDataSource.updateUserDetails(cv);
				} else {
					cv.put(UserDB.COL_FIELD_OWN_NUMBER, StaticConfig.LOGGED_PHONE_NUMBER);
					userDetailsDataSource.insertUserDetails(cv);
				}
			} else {
				JSONArray jsonCarsArray = null;
				JSONObject jsonUserDetail = null;
				JSONObject jSonText = null;
				int fetchedChangeSet = 0;
				if (myCarResult.has("error") && myCarResult.getString("error").equalsIgnoreCase("false")) {
					jSonText = myCarResult.getJSONObject("text");
					if (myCarResult.has("changeset")) {
						fetchedChangeSet = Integer.parseInt(myCarResult.getString("changeset").trim());
						//System.out.println("fetchedChangeSet"+fetchedChangeSet);
					}
					cv = new ContentValues();
					cv.put(UserDB.COL_FIELD_JSON_TEXT, "Has Data");
					// System.out.println("REAL OBJCT: " + jSonText);
				}
				jsonCarsArray = jSonText.getJSONArray("car_details");
				jsonUserDetail = jSonText.getJSONObject("user_details");
				JSONArray JsonServiceHistoryArray = null;
				// System.out.println("DB DELETED NO: "+StaticConfig.IsFirstTime);
				//System.out.println("fetchedChangeSet DB"+userDetailsDataSource.getStoredCarChangeSet());
				if (fetchedChangeSet > userDetailsDataSource.getStoredCarChangeSet()) {
					if (jsonUserDetail.has("owner")) {
						cv = new ContentValues();
						cv.put(UserDB.COL_CAR_CHANGE_SET, fetchedChangeSet);
						userDetailsDataSource.updateUserDetails(cv);
						// System.out.println("DB DELETED");
						//fetchedChangeSet++;
						tracker.send(GATrackerMaps.getBackgroundAPIEvent("Car Data Update"));
						carDetailDataSource.deleteCarDB();
						servceHistryDataSource.deleteServiceHistory();
						for (int i = 0; i < jsonCarsArray.length(); i++) {
							JSONObject c = jsonCarsArray.getJSONObject(i);
							String latestService_Date = checkLatesDates(1, Integer.parseInt(c.optString("car_id")), c.optString("next_service_date")
									.trim());
							String latestInsuranceExp = checkLatesDates(2, Integer.parseInt(c.optString("car_id")), c.optString("insurance_expiry")
									.trim());
							String latestPucExp = checkLatesDates(3, Integer.parseInt(c.optString("car_id")), c.optString("puc_expiry").trim());
							JsonServiceHistoryArray = c.getJSONArray("service_history");
							handleServiceHistory(JsonServiceHistoryArray, Integer.parseInt(c.optString("car_id")));
							cv = new ContentValues();
							cv.put(CarDetailsDB.COL_CAR_ID, Integer.parseInt(c.optString("car_id")));
							cv.put(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE, latestService_Date);
							cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY, latestInsuranceExp);
							cv.put(CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT, c.optString("service_provider"));
							cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_PROVIDER, c.optString("insurance_provider"));
							cv.put(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY, latestPucExp);
							cv.put(CarDetailsDB.COL_FIELD_CAR_REGNUM, c.optString("reg_num"));
							cv.put(CarDetailsDB.COL_FIELD_CAR_MODEL, c.optString("model"));
							cv.put(CarDetailsDB.COL_CAR_OWNER, jsonUserDetail.optString("owner"));
							long r = carDetailDataSource.insertCarDetails(cv);
						}
						carDetailDataSource.getCarList();
						raiseNotificationForCarUpdate(jsonUserDetail);

					} else {
						// dbHelper.deleteCarDB();
						servceHistryDataSource.deleteServiceHistory();
					}
				}
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void raiseNotificationForCarUpdate(JSONObject myCarResult) {
		if (StaticConfig.TOTAL_NUMBER_OF_CARS != StaticConfig.myCarList.size()) {
			tracker.send(GATrackerMaps.getBackgroundAPIEvent("Car Data Found"));
			StaticConfig.TOTAL_NUMBER_OF_CARS = StaticConfig.myCarList.size();
			firstReceive = true;
			// raise notification
			String textDisp = "Your car data has been updated!";
			//Notification noti = new Notification(R.drawable.mycar, textDisp, System.currentTimeMillis());
			Notification.Builder builder = new Notification.Builder(ctxt);
			Intent iin = new Intent(ctxt, MainActivity.class);
			iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_MYCAR_RECEIVE_NOTIFICATION);

			PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin, Intent.FLAG_ACTIVITY_NEW_TASK);

			builder.setSmallIcon(R.drawable.mycar)
				.setContentTitle("MyCar")
			    .setContentText(textDisp)
		        .setContentIntent(pendIntent);
			Notification noti = builder.getNotification();
			//noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp + " Click here to view car details.", pendIntent);
			noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			noti.flags = Notification.FLAG_AUTO_CANCEL;
			long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
			noti.vibrate = x;

			notifyManager.notify("MyCarData", 1, noti);
			// end of notification

			try {
				myCarResult.put("phone", mobile);
				String jsonStrWithPhone = myCarResult.toString();

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("json", jsonStrWithPhone));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String checkLatesDates(int type, int carId, String match) {
		// keep largest next_service_date
		String Derived = "";
		String Received = match;
		switch (type) {
			case 1 :
				Derived = carDetailDataSource.getNextServiceDate(carId);
				break;
			case 2 :
				Derived = carDetailDataSource.getInsuranceExpiry(carId);
				break;
			case 3 :
				Derived = carDetailDataSource.getPucExpiry(carId);
				break;
			default :
				break;
		}
		if (Derived.equalsIgnoreCase("")) {
			// local value is blank, keep received value
			Derived = Received;
		} else if (Received.equalsIgnoreCase("")) {
		} else {
			// both not blank, so compare date objects
			Date currServDate = new Date();
			Date newServDate = new Date();
			try {
				currServDate = sdf.parse(Derived);
				newServDate = sdf.parse(Received);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (newServDate.after(currServDate)) {
				Derived = Received;
			}
			return Derived;
		}
		return Derived;
	}

	private void handleServiceHistory(JSONArray jsonServiceHistoryArray, int car_id) {
		ContentValues cv = new ContentValues();
		for (int i = 0; i < jsonServiceHistoryArray.length(); i++) {
			try {
				JSONObject c = jsonServiceHistoryArray.getJSONObject(i);
				cv.put(ServiceDetailsDB.COL_SERVICE_CAR_ID, car_id);
				cv.put(ServiceDetailsDB.COL_SERVICE_ADVISOR, c.optString("advisor"));
				cv.put(ServiceDetailsDB.COL_SERVICE_AMOUNT, c.optString("amount"));
				cv.put(ServiceDetailsDB.COL_SERVICE_DATE, c.optString("service_date"));
				cv.put(ServiceDetailsDB.COL_SERVICE_WORK_TYPE, c.optString("work_type"));
				cv.put(ServiceDetailsDB.COL_SERVICE_MILEAGE, c.optString("mileage"));
				cv.put(ServiceDetailsDB.COL_SERVICE_TECHNICIAN, c.optString("technician"));
				cv.put(ServiceDetailsDB.COL_SERVICE_OUTLET, c.optString("outlet"));
				servceHistryDataSource.insertServiceHistory(cv);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void reminderCheck(Calendar today, Date nowDate, String nowStr) throws ParseException {
		// dbHelper = new DatabaseHelper(ctxt);
		ContentValues cv;
		long diff;
		int days;
		int idxStr = userDetailsDataSource.getReminderMsgId();
		int idx;
		// if (idxStr.equalsIgnoreCase("") {
		if (idxStr == 0) {
			idx = -10;
		} else {
			idx = idxStr;
		}

		// check service
		Date lastServiceReminderChk;
		Date lastPucRemindercheck;
		Date lastInsuranceReminderChk;
		Iterator<MyCar> itr = StaticConfig.myCarList.iterator();
		while (itr.hasNext()) {
			MyCar temp = itr.next();
			String nxtServDateStr = carDetailDataSource.getNextServiceDate(temp.getCarId());
			try {
				lastServiceReminderChk = sdf.parse(carDetailDataSource.getLastServiceReminderCheck(temp.getCarId()));
			} catch (ParseException e) {
				lastServiceReminderChk = sdf.parse("01-01-2013");
			}
			try {
				lastInsuranceReminderChk = sdf.parse(carDetailDataSource.getLastInsuranceReminderCheck(temp.getCarId()));
			} catch (ParseException e) {
				lastInsuranceReminderChk = sdf.parse("01-01-2013");
			}
			try {
				lastPucRemindercheck = sdf.parse(carDetailDataSource.getLastPUCReminderCheck(temp.getCarId()));
			} catch (ParseException e) {
				lastPucRemindercheck = sdf.parse("01-01-2013");
			}
			if (!nxtServDateStr.equalsIgnoreCase("") && lastServiceReminderChk.before(nowDate)) {
				Calendar nxtServDate = Calendar.getInstance();
				nxtServDate.setTime(sdf.parse(nxtServDateStr));
				diff = nxtServDate.getTimeInMillis() - today.getTimeInMillis();
				days = Math.round(diff / (24 * 60 * 60 * 1000));

				if (!carDetailDataSource.getserviceAlarm(temp.getCarId()).equalsIgnoreCase(nxtServDateStr)
						&& (days == StaticConfig.SERVICE_INTERVAL_1 || days == StaticConfig.SERVICE_INTERVAL_2
								|| days == StaticConfig.SERVICE_INTERVAL_3 || days == StaticConfig.SERVICE_INTERVAL_Last)) {
					// create reminder message
					idx = idx - 1;// decrement message id
					String textDisp = "Your car service for " + temp.getModelName() + ", " + temp.getReg_num() + (diff > 0 ? " is" : " was")
							+ " due on " + nxtServDateStr + ".";

					JSONObject remMsgObj = new JSONObject();
					JSONArray remMsgArr = new JSONArray();
					getTheHotline(3);
					String serviceHotline = " ";
					if (!StaticConfig.SERVICE_HOTLINE_SELECTOR.isEmpty()) {
						serviceHotline = StaticConfig.SERVICE_HOTLINE_SELECTOR.get(0).getNumber();
					}
					try {
						remMsgObj
								.put("id", idx)
								.put("subject", textDisp)
								.put("text",
										"Dear Customer,\n\n" + textDisp
												+ "\n\nPlease call our Hotline number below to set up an appointment at the earliest.")
								.put("date", sdf2.format(today.getTime())).put("read", false).put("cfa_type", 1).put("cfa_details", serviceHotline);

						remMsgArr.put(remMsgObj);

						MessagesData.appendMessages(ctxt, remMsgArr);
						if (days == StaticConfig.SERVICE_INTERVAL_Last) {
							cv = new ContentValues();
							cv.put(CarDetailsDB.COL_ALARM_SERVICE, nxtServDateStr.trim());
							carDetailDataSource.updateCarDetails(cv, temp.getCarId());
						}

						tracker.send(GATrackerMaps.getBackgroundReminderEvent("Service Due"));

						// raise notification
						Notification.Builder builder = new Notification.Builder(ctxt);
						//Notification noti = new Notification(R.drawable.services, textDisp, System.currentTimeMillis());
						Intent iin = new Intent(ctxt, MainActivity.class);
						iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_SERVICE_REMINDER_NOTIFICATION);

						PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin, Intent.FLAG_ACTIVITY_NEW_TASK);

						builder.setSmallIcon(R.drawable.services)
							.setContentText(textDisp)
						   .setContentTitle("Service")
					       .setContentIntent(pendIntent);
						Notification noti = builder.getNotification();
						/*noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp + "  Call "
								+ StaticConfig.DEALER_FULL_NAME + " today and book your service!", pendIntent);*/
						noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						noti.flags = Notification.FLAG_AUTO_CANCEL;

						long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
						noti.vibrate = x;

						notifyManager.notify("ServiceDue", 1, noti);
					} catch (JSONException e) {
						// ignore
					}
				}
				cv = new ContentValues();
				cv.put(CarDetailsDB.COL_LAST_SERVICE_REMINDER_CHECK, nowStr);
				carDetailDataSource.updateCarDetails(cv, temp.getCarId());
			}

			// check insurance
			String insuExpDateStr = carDetailDataSource.getInsuranceExpiry(temp.getCarId());
			if (!insuExpDateStr.equalsIgnoreCase("") && lastInsuranceReminderChk.before(nowDate)) {
				Calendar insuExpDate = Calendar.getInstance();
				insuExpDate.setTime(sdf.parse(insuExpDateStr));
				diff = insuExpDate.getTimeInMillis() - today.getTimeInMillis();
				days = Math.round(diff / (24 * 60 * 60 * 1000));

				if (!carDetailDataSource.getInsuranceAlarm(temp.getCarId()).equalsIgnoreCase(insuExpDateStr)
						&& (days == StaticConfig.INSURANCE_INTERVAL_1 || days == StaticConfig.INSURANCE_INTERVAL_2
								|| days == StaticConfig.INSURANCE_INTERVAL_3 || days == StaticConfig.INSURANCE_INTERVAL_4 || days == StaticConfig.INSURANCE_INTERVAL_Last)) {
					// create reminder message
					idx = idx - 1;// decrement message id
					String textDisp = "Your Car Insurance for " + temp.getModelName() + ", " + temp.getReg_num()
							+ (days >= 0 ? " is about to expire" : " has expired") + " on " + insuExpDateStr + ".";

					JSONObject remMsgObj = new JSONObject();
					JSONArray remMsgArr = new JSONArray();
					String insuranceHotline = "";
					if (!StaticConfig.INSURANCE_HOTLINE_SELECTOR.isEmpty()) {
						insuranceHotline = StaticConfig.INSURANCE_HOTLINE_SELECTOR.get(0).getNumber();
					}
					try {
						remMsgObj
								.put("id", idx)
								.put("subject", textDisp)
								.put("text",
										"Dear Customer,\n\n" + textDisp
												+ "\n\nPlease call our Hotline number below to renew your Insurance at the earliest.")
								.put("date", sdf2.format(today.getTime())).put("read", false).put("cfa_type", 1).put("cfa_details", insuranceHotline);

						remMsgArr.put(remMsgObj);

						MessagesData.appendMessages(ctxt, remMsgArr);
							if (days == StaticConfig.INSURANCE_INTERVAL_Last) {
								cv = new ContentValues();
								cv.put(CarDetailsDB.COL_ALARM_INSURANCE, insuExpDateStr.trim());
								carDetailDataSource.updateCarDetails(cv, temp.getCarId());
							}
						tracker.send(GATrackerMaps.getBackgroundReminderEvent("Insurance Expiry"));

						// raise notification
						Notification.Builder builder = new Notification.Builder(ctxt);
						//Notification noti = new Notification(R.drawable.mycar, textDisp, System.currentTimeMillis());
						Intent iin = new Intent(ctxt, MainActivity.class);
						iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_INSURANCE_REMINDER_NOTIFICATION);

						PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin, Intent.FLAG_ACTIVITY_NEW_TASK);

						builder.setSmallIcon(R.drawable.mycar)
							.setContentTitle("Mycar")
						   .setContentText(textDisp)
					       .setContentIntent(pendIntent);
						Notification noti = builder.getNotification();
						/*noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp + "  Call "
								+ StaticConfig.DEALER_FULL_NAME + " today and renew your car insurance!", pendIntent);*/
						noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						noti.flags = Notification.FLAG_AUTO_CANCEL;

						long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
						noti.vibrate = x;

						notifyManager.notify("InsuranceDue", 1, noti);
					} catch (JSONException e) {
						// ignore
					}
				}
				cv = new ContentValues();
				cv.put(CarDetailsDB.COL_LAST_INSURANCE_REMINDER_CHECK, nowStr);
				carDetailDataSource.updateCarDetails(cv, temp.getCarId());
			}

			// check puc
			//String pucExpDateStr = carDetailDataSource.getPucExpiry(temp.getCarId());
	/*		if (!pucExpDateStr.equalsIgnoreCase("") && lastPucRemindercheck.before(nowDate)) {
				Calendar pucExpDate = Calendar.getInstance();
				pucExpDate.setTime(sdf.parse(pucExpDateStr));
				diff = pucExpDate.getTimeInMillis() - today.getTimeInMillis();
				days = Math.round(diff / (24 * 60 * 60 * 1000));

				if (!carDetailDataSource.getInsuranceAlarm(temp.getCarId()).equalsIgnoreCase(pucExpDateStr)
						&& (days == StaticConfig.INSURANCE_INTERVAL_1 || days == StaticConfig.INSURANCE_INTERVAL_2
								|| days == StaticConfig.INSURANCE_INTERVAL_3 || days == StaticConfig.INSURANCE_INTERVAL_4 || days == StaticConfig.INSURANCE_INTERVAL_Last)) {

					// create reminder message
					idx = idx - 1;// decrement message id
					String textDisp = "Your Car PUC Certificate for " + temp.getModelName() + ", " + temp.getReg_num()
							+ (days >= 0 ? "is about to expire" : "has expired") + " on " + pucExpDateStr + ".";

					JSONObject remMsgObj = new JSONObject();
					JSONArray remMsgArr = new JSONArray();
					String pucHotline = "";
					if (!StaticConfig.PUC_HOTLINE_SELECTOR.isEmpty()) {
						pucHotline = StaticConfig.PUC_HOTLINE_SELECTOR.get(0).getNumber();
					}
					try {
						remMsgObj
								.put("id", idx)
								.put("subject", textDisp)
								// .put("text", "Dear Customer,\n\n" + textDisp
								// +
								// "\n\nPlease call our Hotline number below to renew your Pollution Under Control (PUC) Certificate at the earliest.")
								.put("text",
										"Dear Customer,\n\n" + textDisp
												+ "\n\nPlease renew your Pollution Under Control (PUC) Certificate at the earliest.")
								.put("date", sdf2.format(today.getTime())).put("read", false);
						if (StaticConfig.PUC_HOTLINE_SELECTOR.size() > 0
								&& !StaticConfig.PUC_HOTLINE_SELECTOR.get(0).getNumber().equalsIgnoreCase("")) {
							remMsgObj.put("cfa_type", 1).put("cfa_details", pucHotline);
						} else {
							remMsgObj.put("cfa_type", 0);
						}

						remMsgArr.put(remMsgObj);

						MessagesData.appendMessages(ctxt, remMsgArr);
						if (days == StaticConfig.INSURANCE_INTERVAL_Last) {
								cv = new ContentValues();
								cv.put(CarDetailsDB.COL_ALARM_PUC, pucExpDateStr.trim());
								carDetailDataSource.updateCarDetails(cv, temp.getCarId());
						}
						// MyJSONData.editMyData(ctxt, new
						// String[]{MyJSONData.ALARM_PUC}, new
						// String[]{pucExpDateStr}, MyJSONData.TYPE_OWN);

						// mGaTracker.sendEvent(GATrackerActivity.CATEGORY_REMINDER,
						// GATrackerActivity.ACTION_NOTIFY, "PUC Expiry", null);
						tracker.send(GATrackerMaps.getBackgroundReminderEvent("PUC Expiry"));

						// raise notification
						Notification.Builder builder = new Notification.Builder(ctxt);
						//Notification noti = new Notification(R.drawable.mycar, textDisp, System.currentTimeMillis());
						Intent iin = new Intent(ctxt, MainActivity.class);
						iin.putExtra(MainActivity.EXTRA_NOTIFICATION_TYPE_KEY, MainActivity.EXTRA_PUC_REMINDER_NOTIFICATION);
						// Intent iin = new Intent(ctxt,
						// ContentWithTabsActivity.class);
						// iin.putExtra("tabIndex",
						// StaticConfig.getTabPositionFromName(StaticConfig.TAB_NAME_MYCAR));
						// iin.putExtra("puc_reminder", true);

						PendingIntent pendIntent = PendingIntent.getActivity(ctxt, new Random().nextInt(10000), iin, Intent.FLAG_ACTIVITY_NEW_TASK);

						builder.setSmallIcon(R.drawable.mycar)
							.setContentText(textDisp)
						   .setContentTitle("MyCar")
					       .setContentIntent(pendIntent);
						Notification noti = builder.getNotification();
						noti.setLatestEventInfo(getApplicationContext(), StaticConfig.DEALER_FULL_NAME, textDisp + "  Call "
								+ StaticConfig.DEALER_FULL_NAME + " today to renew your car PUC Certificate!", pendIntent);
						noti.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						noti.flags = Notification.FLAG_AUTO_CANCEL;

						long[] x = {0, 300, 150, 300, 750, 300, 150, 300};
						noti.vibrate = x;

						notifyManager.notify("PUCDue", 1, noti);
					} catch (JSONException e) {
						// ignore
					}
				}
				cv = new ContentValues();
				cv.put(CarDetailsDB.COL_LAST_PUC_REMINDER_CHECK, nowStr);
				carDetailDataSource.updateCarDetails(cv, temp.getCarId());
			}*/

		}
		/*cv = new ContentValues();
		cv.put(UserDB.COL_FIELD_LAST_REMINDER_MESSAGE_ID, idx);
		userDetailsDataSource.updateUserDetails(cv);*/
	}

	private void getTheHotline(int id) {
		Cursor c = hotlineDataSource.getHotlineFromId(id);

	}

	private void sendQueuedChatMessages() {
		// Get all queued chat messages to send and set-up post calls
		MultipartEntity mpEntity;
		JSONObject resultObj;
		// System.out.println("SENDING QUEUED MSGS");

		ArrayList<ChatMessages> cMsgs = chatDataSource.selectForSending();
		for (ChatMessages msg : cMsgs) {
			try {
				mpEntity = new MultipartEntity();

				mpEntity.addPart("reqId", new StringBody("" + msg.getUid()));
				mpEntity.addPart("sender", new StringBody("" + mobile));
				mpEntity.addPart("msgType", new StringBody("" + msg.getMessageType()));
				mpEntity.addPart("receiver", new StringBody("" + msg.getReceiverId()));
				mpEntity.addPart("city", new StringBody(msg.getCity()));
				if (msg.getMessageType() == 0) {
					mpEntity.addPart("message", new StringBody(msg.getMessage()));
				} else {
					File f = new File(ChatAdvisorFragment.getAlbumDir(), msg.getMessage());
					ContentBody cImage;
					if(null!=BitmapFactory.decodeFile(f.getAbsolutePath())){
						 cImage = new FileBody(f, "image/jpeg");
					}else{
						f = new File(msg.getMessage());
						 cImage = new FileBody(f, "image/jpeg");
					}
					mpEntity.addPart("img", cImage);
					// Log.d(TAG, "Added Image Part");
				}

				ContentValues cv = new ContentValues();
				cv.put(ChatDB.COL_CHAT_DELIVERY_FLAG, 0);
				cv.put(ChatDB.COL_CHAT_SENDING_STARTED_AT, chatDataSource.getCurrentServerTime());

				chatDataSource.update(msg.getUid(), cv);

				try {
					String executeURL = StaticConfig.API_POST_CHAT_MESSAGE + "?" + StaticConfig.getCMSPass();
					resultObj = RestClient.connectJSONbyPost(executeURL, mpEntity);
					// System.out.println("CHAT RESPONSE: "+resultObj);
					// Log.d(TAG, "ResultObj toString: " +
					// resultObj.toString());

					if (resultObj.has("error") && resultObj.getBoolean("error")) {
						int reqId = resultObj.getInt("reqId");
						cv = new ContentValues();
						cv.put(ChatDB.COL_CHAT_DELIVERY_FLAG, -1);
						chatDataSource.update(reqId, cv);
					} else if(resultObj.has("error") && !resultObj.getBoolean("error"))
							/*
							 * if (resultObj.has("error") &&
							 * !resultObj.getBoolean("error"))
							 */{
						int reqId = resultObj.getInt("reqId");
						int insertId = resultObj.getInt("msgInsertId");

						cv = new ContentValues();
						cv.put(ChatDB.COL_CHAT_SERVER_MSG_ID, insertId);
						cv.put(ChatDB.COL_CHAT_DELIVERY_FLAG, 1);
						cv.put(ChatDB.COL_CHAT_TIMESTAMP, chatDataSource.getCurrentTime());

						chatDataSource.update(reqId, cv);
					}else{
						int reqId = resultObj.getInt("reqId");
						cv = new ContentValues();
						cv.put(ChatDB.COL_CHAT_DELIVERY_FLAG, -1);	
						chatDataSource.update(reqId, cv);

					} /*
					 * else {
					 * 
					 * if (resultObj.has("success") &&
					 * !resultObj.getBoolean("success")) { //Mostly is a
					 * duplicate insert call so Ignore! //Log.e(TAG,
					 * resultObj.getString("response"); }
					 * 
					 * if (resultObj.has("response")) { // Log.e(TAG,
					 * resultObj.getString("response")); } }
					 */
				} catch (JSONException e) {
					// Log.e(TAG, "JSONException : " + e.getMessage());
				}

			} catch (UnsupportedEncodingException e) {
				// Log.e(TAG, "UnsupportedEncodingException: " +
				// e.getMessage());
			}
		}
		if (cMsgs.size() > 0) {// broadcast UI update if sending messages was
								// attempted!
			LocalBroadcastManager.getInstance(ctxt).sendBroadcast(new Intent(INTENT_FILTER_CHAT_UPDATE_ACTION));
		}
	}

	public static void setChatRoleObserver(int roleId) {
		CHAT_ROLE_OBSERVER = roleId;
	}

	public static void unsetChatRoleObserver() {
		setChatRoleObserver(0);
	}

	public static void setInboxMessageRoleObserver(boolean observerState) {
		INBOX_MESSAGE_ROLE_OBSERVER = observerState;
	}

	public static void setRewardHistoryRoleObserver(boolean rewardObserver) {
		REWARD_HISTORY_ROLE_OBSERVER = rewardObserver;
	}

	public static void setPaymentHistoryRoleObserver(boolean paymentObserver) {
		PAYMENT_HISTORY_ROLE_OBSERVER = paymentObserver;
	}
}
