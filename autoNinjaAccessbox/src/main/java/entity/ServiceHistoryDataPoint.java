package entity;

import myJsonData.MyJSONData;

public class ServiceHistoryDataPoint {
	private static final String MILEAGE_UNIT = " Kms";
	private static final String AMOUNT_UNIT = "Rs. ";
	
	private String _date;
	private String _serviceType;
	private String _mileage;
	private String _amount;
	
	public ServiceHistoryDataPoint(String date, String serviceType, String mileage) {
		this._date = date;
		this._serviceType = serviceType;
		this._mileage = mileage;
		this._amount = "";
	}
	
	public ServiceHistoryDataPoint(String date, String serviceType, String mileage, String amount) {
		this(date, serviceType, mileage);
		this._amount = amount;
	}
	
	public String getDate() {
		return _date;
	}
	public String getDisplayDate() {
		return MyJSONData.formatDateServerToDisplay(_date);
	}
	public void setDate(String _date) {
		this._date = _date;
	}
	
	public String getServiceType() {
		return _serviceType;
	}
	public void setServiceType(String _serviceType) {
		this._serviceType = _serviceType;
	}
	
	public String getMileage() {
		if (!_mileage.equalsIgnoreCase("")) {
			return _mileage + MILEAGE_UNIT;
		}
		else {
			return "";
		}
	}
	public void setMileage(String _mileage) {
		this._mileage = _mileage;
	}
	
	public String getAmount() {
		if (!_amount.equalsIgnoreCase("")) {
			return AMOUNT_UNIT + _amount;
		}
		else {
			return "";
		}
	}
	public void setAmount(String _amount) {
		this._amount = _amount;
	}
	
	public String getMileageOrAmount() {
		if (!getMileage().equalsIgnoreCase("")) {
			return getMileage();
		}
		else {
			return getAmount();
		}
	}
}
