package entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.drawable;

public class Offers implements Parcelable{
	
	private int _ID;
	private String _subject = "";
	private String _body = "";
	private String _type = "";
	private String _ribbonText = "";
	private String _teaser = "";
	private String _img = "";
	private ArrayList<CallForAction> _CFAList = new ArrayList<CallForAction>();
	
	public static final String TYPE_NEW_CAR = "sales";
	public static final String TYPE_INSURANCE = "insurance";
	public static final String TYPE_USED_CAR = "true_value";
	public static final String TYPE_SERVICE = "service";
	public static final String TYPE_DRIVING_SCHOOL = "school";
	public static final String TYPE_ACCESSORIES = "accessories";
	
	public static final Map<String, Integer> TYPE_RESOURCE_MAP = new HashMap<String, Integer>(){
		private static final long serialVersionUID = 1L;

		{
			put(TYPE_NEW_CAR, R.drawable.hotline_new_car);
			put(TYPE_DRIVING_SCHOOL, R.drawable.hotline_learn_driving);
			put(TYPE_INSURANCE, R.drawable.hotline_renew_insurance);
			put(TYPE_SERVICE, R.drawable.hotline_book_service);
			put(TYPE_USED_CAR, R.drawable.hotline_used_car);
			put(TYPE_ACCESSORIES, R.drawable.hotline_accessories);
		}
	};
	
	public Offers(int id, String sub, String body, String teaser, String type, String ribbonText, String imgName, ArrayList<CallForAction> cfaList) {
		this._ID = id;
		this._subject = sub;
		this._body = body;
		this._type = type;
		this._teaser = teaser;
		this._ribbonText = ribbonText;
		this._img = imgName;
		this._CFAList = cfaList;
	}
	
	private Offers(Parcel in) {
		this._ID = in.readInt();
		this._type = in.readString();
		this._subject = in.readString();
		this._teaser = in.readString();
		this._ribbonText = in.readString();
		this._body = in.readString();
		this._img = in.readString();
		this._CFAList = in.readArrayList(null);
	}
	
	public void setID (int id) {
		this._ID = id;
	}
	public int getID () {
		return this._ID;
	}

	public void setSubject (String sub) {
		this._subject = sub;
	}
	public String getSubject () {
		return this._subject;
	}

	public void setBody (String body) {
		this._body = body;
	}
	public String getBody () {
		return this._body;
	}

	public void setType (String type) {
		this._type = type;
	}
	public String getType () {
		return this._type;
	}

	public void setTeaser (String teaser) {
		this._teaser = teaser;
	}
	public String getTeaser () {
		return this._teaser;
	}
	
	public void setRibbonText (String ribbon) {
		this._ribbonText = ribbon;
	}
	public String getRibbonText () {
		return this._ribbonText;
	}
	
	public void setImage (String name) {
		this._img = name;
	}
	public String getImage () {
		return this._img;
	}
	
	public void setCFAList (ArrayList<CallForAction> cfaList) {
		this._CFAList = cfaList;
	}
	public ArrayList<CallForAction> getCFAList() {
		return this._CFAList;
	}
	public int getCFACount() {
		return this._CFAList.size();
	}
	public CallForAction getCFAatIndex(int index) {
		if (index >= this.getCFACount()) {
			return new CallForAction(CallForAction.TYPE_INVALID, "", "");
		}
		else 
			return this._CFAList.get(index);
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_ID);
		dest.writeString(_type);
		dest.writeString(_subject);
		dest.writeString(_teaser);
		dest.writeString(_ribbonText);
		dest.writeString(_body);
		dest.writeString(_img);
		dest.writeList(_CFAList);
	}
	
	public static final Parcelable.Creator<Offers> CREATOR = new Parcelable.Creator<Offers>() {

		public Offers createFromParcel(Parcel source) {
			return new Offers(source);
		}

		public Offers[] newArray(int size) {
			return new Offers[size];
		}
	};
}
