package entity;


import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.drawable;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;

public class CallForAction implements Parcelable {

	public static final int TYPE_INVALID = 0;
	public static final int TYPE_CALL = 1;
	public static final int TYPE_WEB = 2;
	public static final int TYPE_REFER = 3;
	public static final int TYPE_PAY_NOW = 4;
	public static final int TYPE_PAY_CITRUS = 5;
	public static final int TYPE_PSF_RATE = 6;
	public static final int TYPE_CANCEL_SERVICE = 7;
	
	private int _type;
	private String _label;
	private String _details;
	
	public CallForAction() {
		this._type = TYPE_INVALID;
		this._label = "";
		this._details = "";
	}
	
	public CallForAction(int type, String label, String details) {
		switch (type) {
			case TYPE_CALL:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Call Now";
				else 
					this._label = label;
				this._details = details;
				break;
			case TYPE_WEB:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Visit Now";
				else 
					this._label = label;
				this._details = details;
				break;
			case TYPE_REFER:
				this._type = type;
				this._label = "Refer a Friend";
				this._details = "";
				break;
			case TYPE_PAY_NOW:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Pay Now";
				else 
					this._label = label;
				this._details = details;
				break;
			case TYPE_PAY_CITRUS:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Pay Now";
				else 
					this._label = label;
				this._details = details;
				break;	
			case TYPE_PSF_RATE:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Rate";
				else 
					this._label = label;
				this._details = details;
				break;	
			case TYPE_CANCEL_SERVICE:
				this._type = type;
				if (label.trim().equalsIgnoreCase("")) 
					this._label = "Cancel Service";
				else 
					this._label = label;
				this._details = details;
				break;
			default:
				this._type = TYPE_INVALID;
				this._label = "";
				this._details = "";
		}
	}

	public CallForAction(Parcel p) {
		this._type = p.readInt();
		this._label = p.readString();
		this._details = p.readString();
	}
	
	public void setType(int type) {
		switch (type) {
			case TYPE_CALL:
			case TYPE_WEB:
			case TYPE_REFER:
			case TYPE_PAY_NOW:
			case TYPE_PAY_CITRUS:	
			case TYPE_PSF_RATE:	
			case TYPE_CANCEL_SERVICE:
				this._type = type;
				break;
			default:
				this._type = TYPE_INVALID;
		}
	}
	public int getType() {
		return this._type;
	}
	
	public void setLabel(String label) {
		this._label = label;
	}
	public String getLabel() {
		return this._label;
	}
	
	public void setDetails(String details) {
		this._details = details;
	}
	public String getDetails() {
		return this._details;
	}
	
	public int getButtonDrawableId() {
		switch(this._type){
			case TYPE_CALL:
				return R.drawable.phone_icon_small;
			case TYPE_WEB:
				return R.drawable.web_icon_small;
			case TYPE_PAY_NOW:
				return R.drawable.pay_icon_small;
			case TYPE_PAY_CITRUS:
				return R.drawable.pay_icon_small;	
			case TYPE_REFER:
				return R.drawable.raf_icon_small;
			case TYPE_PSF_RATE:
				return R.drawable.web_icon_small;	
			default:
				return android.R.color.transparent;
		}
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_type);
		dest.writeString(_label);
		dest.writeString(_details);
	}

	public static final Parcelable.Creator<CallForAction> CREATOR = new Parcelable.Creator<CallForAction>() {

		public CallForAction createFromParcel(Parcel source) {
			return new CallForAction(source);
		}

		public CallForAction[] newArray(int size) {
			return new CallForAction[size];
		}
	};
	public static ContactHolder getContactDetails(ContentResolver cr, Uri dataUri) {
		Cursor c = cr.query(dataUri, null, null, null, null);
		if (c.moveToFirst()) {
			String contactID = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
			String contactName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
			String contactNumber = "";
			Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, 
						ContactsContract.Contacts._ID + " = ?", new String[] {contactID}, null);
				
			if (pCur.moveToFirst()) {
				contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				if (contactName != "" && contactNumber != "") {
					return new ContactHolder(contactID, contactName, contactNumber);
				}
				else {
					//return null;
				}
			}
			else {
				//return null;
			}
		}

		return null;
	}
	
	public static AlertDialog getContactNotFoundDialog (FragmentActivity FragmentActivity, 
			String positiveButtonLabel, DialogInterface.OnClickListener positiveClickHandler, 
			String negativeButtonLabel, DialogInterface.OnClickListener negativeClickHandler) {
		
		AlertDialog.Builder bldr = new AlertDialog.Builder(FragmentActivity);
		bldr.setTitle("Restricted Contact!");
		bldr.setMessage("This contact is protected by Facebook's Privacy Policy.\n\nChoose another contact?");
		
		bldr.setPositiveButton(positiveButtonLabel, positiveClickHandler);
		bldr.setNegativeButton(negativeButtonLabel, negativeClickHandler);
		
		return bldr.show();
	}
}
