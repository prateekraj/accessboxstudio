package entity;


import config.StaticConfig;
import android.content.Context;

public class ChatMessages {
	
	public static final int SENDER_USER = 1; 
	public static final int SENDER_CEO = -1; 
	public static final int SENDER_SERVICE_ADVISOR = -2; 
	
	public static final int DELIVERY_SUCCESS = 1; 
	public static final int DELIVERY_PENDING = 0; 
	public static final int DELIVERY_FAILED = -1; 
	
	private int _uid;
	private boolean _read = false;
	private int _sender = 0;
	private int _receiver = 0;
	private int _type = -1;
	private String _msg = "";
	private String _date = "";
	private String _city = "";
	private int _deliveryStatus;
	
	public ChatMessages(int uid, int senderId, int receiverId, int msgType, String message, String date, boolean read, int deliveryStatus, String city) {
		this._uid = uid;
		this._sender = senderId;
		this._receiver = receiverId;
		this._type = msgType;
		this._msg = message;
		this._date = date;
		this._read = read;
		this._deliveryStatus = deliveryStatus;
		this._city = city;
	}
	public ChatMessages(int uid, int senderId, int receiverId, int msgType, String message, String date, boolean read, int deliveryStatus) {
		this(uid, senderId, receiverId, msgType, message, date, read, deliveryStatus, "");
	}
	public ChatMessages(int uid, int senderId, int receiverId, int msgType, String message, String date) {
		this(uid, senderId, receiverId, msgType, message, date, false, DELIVERY_PENDING, "");
	}
	
	public void setUid (int newUid) {
		this._uid = newUid;
	}
	public int getUid () {
		return this._uid;
	}
	
	public int getSenderId () {
		return this._sender;
	}
	
	public int getReceiverId () {
		return this._receiver;
	}
	
	public int getMessageType () {
		return this._type;
	}
	
	public String getCity () {
		return this._city;
	}
	public void setCity (String city) {
		this._city = city;
	}
	
	public void setReadFlag (boolean readFlag) {
		this._read = readFlag;
	}
	public boolean isRead () {
		return this._read;
	}
	
	public String getMessage () {
		return this._msg;
	}

	public String getSenderName () {
		switch (this.getSenderId()) {
		case SENDER_USER:
			return "You";
		case SENDER_CEO:
			return StaticConfig.DEALER_FIRST_NAME + " CEO";
		case SENDER_SERVICE_ADVISOR:
			return "Service\nAdvisor";
		}
		return "Unknown";
	}

	public void setDate (String date) {
		this._date = date;
	}
	public String getDate () {
		if (this._deliveryStatus == DELIVERY_SUCCESS) {
			return this._date;
		}
		else if (this._deliveryStatus == DELIVERY_FAILED) {
			return "Failed!";
		}
		else {
			return "Pending";
		}
	}
	
	public void setDeliveryStatus (int deliveryStatus) {
		this._deliveryStatus = deliveryStatus;
	}
	public int getDeliveryStatus () {
		return this._deliveryStatus;
	}
	
	public void markAsRead(Context ctxt) {
		/*
		int id = this.getUid();
		if (id != -1 && !this.isRead()) {
			MessagesData.markAsRead(ctxt, id);
			this.setReadFlag(true);
		}
		*/
	}
}
