package entity;

import db.TableContract.UserDB;

public class UserDetails {
	private String otp = "";
	private long own_number = 0;
	private String error = "";
	private String last_reminder_check = "";
	private int random_hour = 0;
	private int last_reminder_msg_id = 0;
	private String second_check = "";
	private int tooltips_change_set = 0;
	private int offers_change_set = 0;
	private int latest_version = 0;
	private String update_reminded_for = "";
	private String chat_city_name = "";
	private String referral_code = "";
	private String reg_num = "";
	private String rewards_point_total = "";
	private int rewards_change_set = 0;
	private String message_reply_ceo = "";
	private String emergency_contact_name = "";
	private String emergency_contact_number = "";
	
	public long getOwnNumber() {
		return own_number;
	}
	public void setOwnNumber(long own_number) {
		this.own_number = own_number;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getLast_reminder_check() {
		return last_reminder_check;
	}
	public void setLast_reminder_check(String last_reminder_check) {
		this.last_reminder_check = last_reminder_check;
	}
	public int getRandom_hour() {
		return random_hour;
	}
	public void setRandom_hour(int random_hour) {
		this.random_hour = random_hour;
	}
	public int getLast_reminder_msg_id() {
		return last_reminder_msg_id;
	}
	public void setLast_reminder_msg_id(int last_reminder_msg_id) {
		this.last_reminder_msg_id = last_reminder_msg_id;
	}
	public String getSecond_check() {
		return second_check;
	}
	public void setSecond_check(String second_check) {
		this.second_check = second_check;
	}
	public int getTooltips_change_set() {
		return tooltips_change_set;
	}
	public void setTooltips_change_set(int tooltips_change_set) {
		this.tooltips_change_set = tooltips_change_set;
	}
	public int getOffers_change_set() {
		return offers_change_set;
	}
	public void setOffers_change_set(int offers_change_set) {
		this.offers_change_set = offers_change_set;
	}
	public int getLatest_version() {
		return latest_version;
	}
	public void setLatest_version(int latest_version) {
		this.latest_version = latest_version;
	}
	public String getUpdate_reminded_for() {
		return update_reminded_for;
	}
	public void setUpdate_reminded_for(String update_reminded_for) {
		this.update_reminded_for = update_reminded_for;
	}
	public String getChat_city_name() {
		return chat_city_name;
	}
	public void setChat_city_name(String chat_city_name) {
		this.chat_city_name = chat_city_name;
	}
	public String getReferral_code() {
		return referral_code;
	}
	public void setReferral_code(String referral_code) {
		this.referral_code = referral_code;
	}
	public String getReg_num() {
		return reg_num;
	}
	public void setReg_num(String reg_num) {
		this.reg_num = reg_num;
	}
	public String getRewards_point_total() {
		return rewards_point_total;
	}
	public void setRewards_point_total(String rewards_point_total) {
		this.rewards_point_total = rewards_point_total;
	}
	public int getRewards_change_set() {
		return rewards_change_set;
	}
	public void setRewards_change_set(int rewards_change_set) {
		this.rewards_change_set = rewards_change_set;
	}
	public String getMessage_reply_ceo() {
		return message_reply_ceo;
	}
	public void setMessage_reply_ceo(String message_reply_ceo) {
		this.message_reply_ceo = message_reply_ceo;
	}
	public String getEmergency_contact_name() {
		return emergency_contact_name;
	}
	public void setEmergency_contact_name(String emergency_contact_name) {
		this.emergency_contact_name = emergency_contact_name;
	}
	public String getEmergency_contact_number() {
		return emergency_contact_number;
	}
	public void setEmergency_contact_number(String emergency_contact_number) {
		this.emergency_contact_number = emergency_contact_number;
	}
	
	
}
