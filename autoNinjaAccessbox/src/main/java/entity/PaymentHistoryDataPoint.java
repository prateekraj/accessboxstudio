package entity;


public class PaymentHistoryDataPoint
{
    private String transaction_types;

    private String transaction_amounts;

    private String remarks;

    private String transaction_timestamps;

    private String transaction_id;

    private String invoice_num;
    
    
    public PaymentHistoryDataPoint(String transactiontypes, String transactionamounts, String transactiontimestamps) {
		this.transaction_types = transactiontypes;
		this.transaction_amounts = transactionamounts;
		this.transaction_timestamps = transactiontimestamps;
	}

	public String getTransaction_types ()
    {
        return transaction_types;
    }

    public void setTransaction_types (String transaction_types)
    {
        this.transaction_types = transaction_types;
    }

    public String getTransaction_amounts ()
    {
        return transaction_amounts;
    }

    public void setTransaction_amounts (String transaction_amounts)
    {
        this.transaction_amounts = transaction_amounts;
    }

    public String getRemarks ()
    {
        return remarks;
    }

    public void setRemarks (String remarks)
    {
        this.remarks = remarks;
    }

    public String getTransaction_timestamps ()
    {
        return transaction_timestamps;
    }

    public void setTransaction_timestamps (String transaction_timestamps)
    {
        this.transaction_timestamps = transaction_timestamps;
    }

    public String getTransaction_id ()
    {
        return transaction_id;
    }

    public void setTransaction_id (String transaction_id)
    {
        this.transaction_id = transaction_id;
    }

    public String getInvoice_num ()
    {
        return invoice_num;
    }

    public void setInvoice_num (String invoice_num)
    {
        this.invoice_num = invoice_num;
    }
}
