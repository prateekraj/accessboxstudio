package activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.myaccessbox.appcore.R;

import fragment.LoginBaseFragment;
import fragment.LoginMobileFragment;
import fragment.LoginBaseFragment.LoginReplaceListener;

public class LoginActivityNew extends MyFragmentedActivityBase implements LoginBaseFragment.LoginReplaceListener {
	 
	android.support.v7.app.ActionBar actionBar;
	RelativeLayout actionBarView;
	Toolbar toolbar;
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
       /* actionBar = getSupportActionBar();
		
		actionBarView = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_home, null);
		((ImageView) actionBarView.findViewById(R.id.burgerButton)).setVisibility(View.INVISIBLE);

		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			actionBarView.setBackgroundDrawable(background);
		} else {
			actionBarView.setBackground(background);
		}*/
		//actionBar.hide();

		
		//actionBar.setCustomView(actionBarView);
		
		setContentView(R.layout.activity_frag_content_only);
		toolbar = (Toolbar)findViewById(R.id.toolbar_inner);
		toolbar.setVisibility(View.GONE);
		
		replaceFragment(new LoginMobileFragment(), false);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	protected void onResume() {
		super.onResume();
		
		/*if (MyJSONData.dataFileExists(this, MyJSONData.TYPE_OWN)) {
			MyJSONData ownData = new MyJSONData(this, MyJSONData.TYPE_OWN);
			editMobileNo.setText(ownData.fetchData(MyJSONData.FIELD_OWN_NUMBER));
		}*/
		
		
		
		
	}

	@Override
	public void onFragmentReplace(Fragment fragment, Boolean storeStack) {
		replaceFragment(fragment, storeStack);
	}

	/*@Override
	
	public void OnClick(View v) {
		
		switch(v.getId()) {
		case R.id.login_submit:
			
			//hide keypad
			imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

			String mobileNoInput = editMobileNo.getText().toString().trim();
			
			if (mobileNoInput.length() == 10) {
				try {
					Boolean goFlag = false;
					if (MyJSONData.dataFileExists(getApplicationContext(), MyJSONData.TYPE_OWN)) {
						goFlag = MyJSONData.editMyData(getApplicationContext(), new String[] {"own_num", "error"}, new String[] {mobileNoInput, null}, MyJSONData.TYPE_OWN);
					}
					else {
						JSONObject ownData = new JSONObject();
						ownData.put(MyJSONData.FIELD_OWN_NUMBER, mobileNoInput);
						ownData.put(MyJSONData.FIELD_LAST_REMIND_CHECK, "01-01-2013");
						ownData.put(MyJSONData.FIELD_LAST_API_CHECK, "01-01-2013:P"); //the ':A' or ':P' indicate the am/pm update checks to enable 2 checks per day
						ownData.put(MyJSONData.FIELD_RANDOM_HOUR, new Random().nextInt(12));
						ownData.put(MyJSONData.FIELD_SECOND_CHECK, false); //no clue why this is here - looks like legacy (need to verify)
						
						goFlag = MyJSONData.createMyData(getApplicationContext(), ownData.toString(), MyJSONData.TYPE_OWN);
					}
					
					if (goFlag) {
						Toast.makeText(getApplicationContext(), "Fetching car details matching your credentials...", Toast.LENGTH_LONG).show();
						
						stopService(new Intent(this, BackgroundService.class));

						Intent i = new Intent();
						if (fromMyCar) {
							i = new Intent(this, ContentWithTabsActivity.class);
							i.putExtra(ContentWithTabsActivity.EXTRA_TAB_INDEX_KEY, 
									StaticConfig.getTabPositionFromName(StaticConfig.TAB_NAME_MYCAR));
						}
						else {
							i = new Intent(this, MainActivity.class);
						}
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
						finish();
					}
				} catch (JSONException e) {
					//Log.e(TAG, "JSON Exception: " + e.toString());
				}
			}
			else {
				Toast.makeText(this, "Please enter a valid 10-digit mobile number!", Toast.LENGTH_LONG).show();
			}
			
			replaceFragment(new LoginPasswordFragment(), true);
			
			break;
		//case R.id.login_previous:
		}
	}
*/
}
