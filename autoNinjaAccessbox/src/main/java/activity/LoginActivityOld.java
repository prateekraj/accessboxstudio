package activity;

import googleAnalytics.GATrackerMaps;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import myJsonData.MyJSONData;

import org.json.JSONException;
import org.json.JSONObject;

import service.BackgroundService;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.myaccessbox.appcore.R;

import config.StaticConfig;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import entity.UserDetails;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivityOld extends MyFragmentedActivityBase implements View.OnClickListener {

	android.support.v7.app.ActionBar actionBar;
	RelativeLayout actionBarView;
	boolean fromMyCar;

	// to control the keypad
	InputMethodManager imm;

	TextView title;
	TextView loginButton;
	TextView prevButton;
	EditText editMobileNo;
	EditText editReferralCode;
	EditText editRegNumber;
	UserDetailsDataSource userDetailDataSource;
	Toolbar toolbar;
	CleverTapAPI cleverTap = null; 

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//actionBar = getSupportActionBar();
		/*actionBarView = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_home, null);
		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			actionBarView.setBackgroundDrawable(background);
		} else {
			actionBarView.setBackground(background);
		}
		actionBarView.findViewById(R.id.burgerButton).setVisibility(View.INVISIBLE);*/
		//actionBar.setCustomView(actionBarView);

		setContentView(R.layout.fragment_login);
		//toolbar = (Toolbar)findViewById(R.id.toolbar_inner);
		findViewById(R.id.burgerButton).setVisibility(View.INVISIBLE);
		try {
			cleverTap = CleverTapAPI.getInstance(getApplicationContext());
			cleverTap.event.push("Clicked Login without OTP - Internet Actvie");
		} catch (CleverTapMetaDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//setSupportActionBar(toolbar);
		//actionBarView.findViewById(R.id.burgerButton);

		editMobileNo = (EditText) findViewById(R.id.login_mobile_no);
		editMobileNo.setHint("Your mobile number");
		editMobileNo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
		editMobileNo.setInputType(InputType.TYPE_CLASS_PHONE);

		if (StaticConfig.FEATURE_REFERRAL_CODE) {
			editReferralCode = (EditText) findViewById(R.id.login_referral_code);
			editReferralCode.setVisibility(View.VISIBLE);
			editReferralCode.setHint("Referral Code (optional)");
		}

		if (StaticConfig.FEATURE_REGISTRATION_NUMBER_LOGIN) {
			editRegNumber = (EditText) findViewById(R.id.login_reg_number);
			editRegNumber.setVisibility(View.VISIBLE);
			editRegNumber.setHint("Registration No. (Optional)");
			editRegNumber.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		}

		prevButton = (TextView) findViewById(R.id.login_previous);
		prevButton.setVisibility(View.GONE);

		title = (TextView) findViewById(R.id.title_text);
		title.setText("Enter your mobile number");

		loginButton = (TextView) findViewById(R.id.login_submit);
		loginButton.setOnClickListener(this);

		fromMyCar = getIntent().getBooleanExtra("editMobile", false);

		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onResume() {
		super.onResume();
	    userDetailDataSource = new UserDetailsDataSource(this);
		/*
		 * if (MyJSONData.dataFileExists(this, MyJSONData.TYPE_OWN)) {
		 * MyJSONData ownData = new MyJSONData(this, MyJSONData.TYPE_OWN);
		 */
		/* editMobileNo.setText(ownData.fetchData(MyJSONData.FIELD_OWN_NUMBER)); */
		if (userDetailDataSource.getUserOwnNumber() != 0) {
			editMobileNo.setText("" + userDetailDataSource.getUserOwnNumber());
		}

		if (StaticConfig.FEATURE_REFERRAL_CODE) {
			/*
			 * String refCd =
			 * ownData.fetchData(MyJSONData.FIELD_OWN_REFERRAL_CODE);
			 */
			String refCd = userDetailDataSource.getReferralCode();
			if (!refCd.equalsIgnoreCase("")) {
				editReferralCode.setText(refCd);
			}
		}
		if (StaticConfig.FEATURE_REGISTRATION_NUMBER_LOGIN) {
			/*
			 * String regNumb =
			 * ownData.fetchData(MyJSONData.FIELD_OWN_REG_NUMBER);
			 */
			String regNumb = userDetailDataSource.getRegNum();
			if (!regNumb.equalsIgnoreCase("")) {
				editRegNumber.setText(regNumb);
			}
		}

		if (fromMyCar) {
			loginButton.setText("Submit");
			tracker.send(GATrackerMaps.VIEW_LOGIN_EDIT);

			// MyJSONData ownData = new MyJSONData(this, MyJSONData.TYPE_OWN);
			if (StaticConfig.FEATURE_REFERRAL_CODE) {
				/*
				 * String refCd =
				 * ownData.fetchData(MyJSONData.FIELD_OWN_REFERRAL_CODE);
				 */
				String refCd = userDetailDataSource.getReferralCode();
				if (!refCd.equalsIgnoreCase("")) {
					editReferralCode.setFocusable(false);
					editReferralCode.setFocusableInTouchMode(false);
					editReferralCode.setClickable(false);
					editReferralCode.setEnabled(false);
				} else {
					editReferralCode.setFocusable(true);
					editReferralCode.setFocusableInTouchMode(true);
					editReferralCode.setClickable(true);
					editReferralCode.setEnabled(true);
				}
			}
		} else {
			loginButton.setText("Login");
			tracker.send(GATrackerMaps.VIEW_LOGIN);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.login_submit :
				// newfragment=new LoginPasswordFragment();
				// hide keypad
				imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

				Pattern mobilePattern = Pattern.compile("[0-9]{10}");

				String mobileNoInput = editMobileNo.getText().toString().trim();

				Matcher mobileMatcher = mobilePattern.matcher(mobileNoInput);

				if (mobileMatcher.matches()) {
					boolean toBeContinued = true;
					String refCodeInput = "";
					if (StaticConfig.FEATURE_REFERRAL_CODE) {
						refCodeInput = editReferralCode.getText().toString().trim();
					}

					String regNumbInput = "";
					if (StaticConfig.FEATURE_REGISTRATION_NUMBER_LOGIN) {
						regNumbInput = editRegNumber.getText().toString().trim();
						if (regNumbInput.length() == 0 || (regNumbInput.length() > 4 && isRegNumValid(regNumbInput))) {
							toBeContinued = true;
						} else {
							toBeContinued = false;
							String toastMessage = "";
							if (regNumbInput.length() < 5) {
								toastMessage = "Registration number is too short";
							} else {
								toastMessage = "Invalid Characters in registration number!";
							}
							Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show();
						}
					}// end of if(StaticConfig.FEATURE_REG_NUMBER)

					if (toBeContinued) {
						try {
							Boolean goFlag = false;
							// if (MyJSONData.dataFileExists(this,
							// MyJSONData.TYPE_OWN)) {

							/*
							 * goFlag = MyJSONData.editMyData(this, new String[]
							 * {MyJSONData.FIELD_OWN_NUMBER,
							 * MyJSONData.FIELD_ERROR,
							 * MyJSONData.FIELD_OWN_REFERRAL_CODE,
							 * MyJSONData.FIELD_OWN_REG_NUMBER}, new String[]
							 * {mobileNoInput, null, refCodeInput,
							 * regNumbInput}, MyJSONData.TYPE_OWN);
							 */
							long phoneNumber = Long.parseLong(mobileNoInput.trim());
							ContentValues cv = new ContentValues();
							cv.put(UserDB.COL_FIELD_OWN_NUMBER, phoneNumber);
							cv.put(UserDB.COL_FIELD_ERROR, " ");
							cv.put(UserDB.COL_FIELD_OWN_REFERRAL_CODE, refCodeInput.trim());
							cv.put(UserDB.COL_FIELD_OWN_REG_NUMBER, regNumbInput.trim());
							if (userDetailDataSource.isPhoneNumberExist()) {
								tracker.send(GATrackerMaps.EVENT_LOGIN_EDIT_MOBILE);
								if (userDetailDataSource.updateUserDetails(cv)) {
									goFlag = true;
									StaticConfig.LOGGED_PHONE_NUMBER = phoneNumber;
								}
							} else {
								tracker.send(GATrackerMaps.EVENT_LOGIN_SUBMIT_MOBILE);
								cv.put(UserDB.COL_FIELD_LAST_API_CHECK, "01-01-2013:P");
								cv.put(UserDB.COL_FIELD_RANDOM_HOUR, new Random().nextInt(12));
								cv.put(UserDB.COL_FIELD_OWN_OTP, "AAAAA");
								cv.put(UserDB.COL_FIELD_SECOND_CHECK, false);
								if (userDetailDataSource.insertUserDetails(cv)) {
									goFlag = true;
									StaticConfig.LOGGED_PHONE_NUMBER = phoneNumber;
								}

							}

							// }
							/*
							 * else {
							 * tracker.send(GATrackerMaps.EVENT_LOGIN_SUBMIT_MOBILE
							 * );
							 * 
							 * JSONObject ownData = new JSONObject();
							 * ownData.put(MyJSONData.FIELD_OWN_NUMBER,
							 * mobileNoInput);
							 * ownData.put(MyJSONData.FIELD_LAST_REMIND_CHECK,
							 * "01-01-2013");
							 * ownData.put(MyJSONData.FIELD_LAST_API_CHECK,
							 * "01-01-2013:P"); //the ':A' or ':P' indicate the
							 * am/pm update checks to enable 2 checks per day
							 * ownData.put(MyJSONData.FIELD_RANDOM_HOUR, "" +
							 * new Random().nextInt(12)); //as string
							 * ownData.put(MyJSONData.FIELD_OWN_OTP, "AAAAA");
							 * //as string
							 * ownData.put(MyJSONData.FIELD_SECOND_CHECK,
							 * false); //no clue why this is here - looks like
							 * legacy (need to verify) //ignore
							 * ownData.put(MyJSONData.FIELD_OWN_REFERRAL_CODE,
							 * refCodeInput);
							 * ownData.put(MyJSONData.FIELD_OWN_REG_NUMBER,
							 * regNumbInput); // registration number
							 * 
							 * goFlag = MyJSONData.createMyData(this,
							 * ownData.toString(), MyJSONData.TYPE_OWN); }
							 */
							if (goFlag) {
								Toast.makeText(this, "Fetching car details matching your credentials...", Toast.LENGTH_LONG).show();
								stopService(new Intent(this, BackgroundService.class));

								Intent i = new Intent();
								if (fromMyCar) {
									// assuming myCar is always one of the tabs
									i = new Intent(this, ContentWithTabsActivity.class);
									i.putExtra(ContentWithTabsActivity.EXTRA_TAB_INDEX_KEY,
											StaticConfig.getTabPositionFromName(StaticConfig.TAB_NAME_MYCAR));
								} else {
									i = new Intent(this, MainActivity.class);
								}
								i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(i);
								finish();
							}
						} catch (Exception e) {
							// Log.e(TAG, "JSON Exception: " + e.toString());
						}
					}
				} else {
					// else of mobile matcher
					Toast.makeText(this, "Please enter a valid 10-digit mobile number!", Toast.LENGTH_LONG).show();
				}
				break;
		}
	}

	private static boolean isRegNumValid(String str) {
		boolean isValid = false;
		String expression = "^[a-zA-Z0-9]+$";
		CharSequence input = str;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(input);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}
}
