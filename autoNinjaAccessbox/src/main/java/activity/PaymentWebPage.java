package activity;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import myJsonData.MyJSONData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Preference;

import com.myaccessbox.appcore.R;

import config.StaticConfig;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
public class PaymentWebPage extends Activity {
	WebView webView;

    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_webview);
        //getActionBar().hide();
        
        String url = getIntent().getStringExtra("url");

        webView = (WebView) this.findViewById(R.id.webview);

        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        webView.addJavascriptInterface(new JsInterface(), "CitrusResponse");

        webView.setWebViewClient(new WebViewClient() {
        	ProgressDialog progressDialog;
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);

                return false;
            }
            
            //Show loader on url load
	        public void onLoadResource (WebView view, String url) {
	            if (progressDialog == null) {
	                // in standard case YourActivity.this
	                progressDialog = new ProgressDialog(PaymentWebPage.this);
	                progressDialog.setMessage("Loading...");
	                progressDialog.setCancelable(false);
	                progressDialog.show();
	            }
	        }
	        public void onPageFinished(WebView view, String url) {
	            try{
	            if (progressDialog.isShowing()) {
	                progressDialog.dismiss();
	                progressDialog = null;
	            }
	            }catch(Exception exception){
	                exception.printStackTrace();
	            }
	        }
        });
        
        webView.setWebChromeClient(new WebChromeClient());

        webView.loadUrl(url);
	}


    private class JsInterface {

        @JavascriptInterface
        public void pgResponse(String response) {
			
        	try {
				JSONObject jObj = new JSONObject(response);
		        // kill this activity once the transaction is done
		        finish();
				Toast.makeText(getApplicationContext(), "Your transaction has been "+jObj.getString("TxStatus"), Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				//ignore
			}
            
        }
      
    }
	@Override
	public void onBackPressed() {
		this.finish();
		super.onBackPressed();
	}
}
