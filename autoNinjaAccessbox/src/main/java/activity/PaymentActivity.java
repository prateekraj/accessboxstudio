package activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.Environment;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.logger.CitrusLogger;
import com.citruspay.sdkui.ui.utils.CitrusFlowManager;
import com.citruspay.sdkui.ui.utils.PPConfig;
import com.citruspay.sdkui.ui.utils.ResultModel;
import com.myaccessbox.appcore.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import config.StaticConfig;
import db.UserDetailsDataSource;
import fragment.MessageDetailFragment;
import myJsonData.MyJSONData;

/**
 * Created by prateek on 4/2/17.
 */

public class PaymentActivity  extends AppCompatActivity{

    EditText edtEmail, edtAmount;
    TextView edtMobile, tvAmountInvoice;
    Button btnBind;
    MyJSONData myOwn;
    SharedPreferences userDetailsPreference;
    private static String USER_DETAILS = "user_details";
    public static final String EMAIL = "user_email";
    public static final String TAG = "Payment Activity";
    private String paymentId = "-1";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_payment_citrus);
        UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(this);
        long mobile = userDetailDataSource.getUserOwnNumber();
        edtEmail = (EditText)findViewById(R.id.editTextemail);
        edtMobile = (TextView)findViewById(R.id.editTextmobile);
        btnBind = (Button)findViewById(R.id.buttonbinduser);
        edtAmount = (EditText)findViewById(R.id.editTextAmount);
        edtAmount.requestFocus();
        tvAmountInvoice = (TextView)findViewById(R.id.editTextAmountInvoice);
        userDetailsPreference = getSharedPreferences(USER_DETAILS, MODE_PRIVATE);

        myOwn = new MyJSONData(this, MyJSONData.TYPE_OWN);
        edtMobile.setText("" + mobile);

        btnBind.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String eEmail = edtEmail.getText().toString().trim();
                String editedAmount = edtAmount.getText().toString().trim();
                if (!edtAmount.getText().toString().trim().equalsIgnoreCase("")) {
                    if (isEmailValid(eEmail)) {

                        SharedPreferences.Editor editor = userDetailsPreference.edit();
                        editor.putString(EMAIL, eEmail);
                        editor.commit();
                        //parentActivity.onFragmentReplaceRequest(new PaymentOptionsFragment(), false);
                        initListener();
                        initQuickPayFlow(editedAmount, eEmail);
                        PaymentActivity.this.finish();
                    } else {
                        Toast.makeText(PaymentActivity.this, "Please enter correct email ID", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PaymentActivity.this, "Please enter amount", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void initQuickPayFlow(String editedAmount, String eEmail) {
        UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(PaymentActivity.this);
        //String emailId = myOwn.fetchData(MyJSONData.FIELD_OWN_EMAIL_ID);
        String mobile = userDetailDataSource.getUserOwnNumber()+"";
        //String amount = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT);
        CitrusFlowManager.startShoppingFlowStyle(PaymentActivity.this,
                eEmail, mobile,
                editedAmount, R.style.AppTheme_Citrus, false);
    }

    private void initListener() {
        PPConfig.getInstance().disableSavedCards(false);
        PPConfig.getInstance().disableNetBanking(false);
        PPConfig.getInstance().disableWallet(true);
        selectProductionEnv();
    }

    private void selectProductionEnv() {

        //To Set the Log Level of Core SDK & Plug & Play
        PPConfig.getInstance().setLogLevel(PaymentActivity.this, CitrusLogger.LogLevel.DEBUG);

        Log.d("PaymentID", paymentId);
        CitrusFlowManager.initCitrusConfig("dl9kdnqnut-signup",
                "3dbd0b468da7a1e222e8bdc43ad74fb8", "dl9kdnqnut-signin",
                "4008a122f48b4bd47b369b9fba1ec330", getResources().getColor(R.color.citrus_white),
                PaymentActivity.this, Environment.PRODUCTION, "autoninja", "https://dashboard.accessbox.in/pratham/apis/v2/billgeneratorcitrusplugandplay" + "?MerchantName="+ StaticConfig.DEALER_CUSTOM_PARAMETER_FOR_CITRUS+"&PaymentId="+paymentId,
                "https://dashboard.accessbox.in/pratham/apis/v2/redirecturlcitrus");

        //To Set the User details
        CitrusUser.Address customAddress = new CitrusUser.Address("80 ft road", "koramangala", "Bangalore", "Karnataka", "India", "560035");
        PPConfig.getInstance().setUserDetails("Accessbox", "Autoninja", customAddress);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myOwn = new MyJSONData(PaymentActivity.this, MyJSONData.TYPE_OWN);
        if (!userDetailsPreference.getString(EMAIL, "").equalsIgnoreCase("")) {
            edtEmail.setText(userDetailsPreference.getString(EMAIL, ""));
            // parentActivity.onFragmentReplaceRequest(new
            // PaymentOptionsFragment(), false);
        }
        if (MessageDetailFragment.payAmountCheck) {
            MessageDetailFragment.payAmountCheck = false;
            tvAmountInvoice.setVisibility(View.VISIBLE);
            edtAmount.setVisibility(View.GONE);
            tvAmountInvoice.setText(myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT));
            edtAmount.setText(myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT));
            paymentId = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_ID);
        } else {
            paymentId = "-1";
            tvAmountInvoice.setVisibility(View.GONE);
            edtAmount.setVisibility(View.VISIBLE);
            if (MyJSONData.dataFileExists(PaymentActivity.this, MyJSONData.TYPE_OWN)) {

                MyJSONData.editMyData(PaymentActivity.this,

                        new String[]{MyJSONData.FIELD_OWN_PAYMENT_ID},

                        new String[]{null}, MyJSONData.TYPE_OWN);

            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CitrusFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=  null) {

            // You will get data here if transaction flow is started through pay options other than wallet
            TransactionResponse transactionResponse = data.getParcelableExtra(CitrusFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            // You will get data here if transaction flow is started through wallet
            ResultModel resultModel = data.getParcelableExtra(CitrusFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getJsonResponse() != null) {
                // Decide what to do with this data
                Log.d(TAG, "Transaction response : " + transactionResponse.getJsonResponse());
            }
            else if (resultModel != null && resultModel.getTransactionResponse() != null) {
                // Decide what to do with this data
                Log.d(TAG, "Result response : " + resultModel.getTransactionResponse().getTransactionId());
            }
            else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(TAG, "Both objects are null!");
            }
        }
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PaymentActivity.this.finish();
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        //startActivity(new Intent(PaymentActivity.this, MainActivity.class));
    }
}
