package activity;

import entity.ChatMessages;
import entity.Tooltips;
import fragment.HotlinesFragment;
import fragment.ServiceBaseFragment;
import fragment.TooltipsListFragment;
import googleAnalytics.GATrackerMaps;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import service.BackgroundService;
import service.ServiceLocations;
import utils.Preference;
import utils.Utils;
import myJsonData.MessagesData;
import myJsonData.MyJSONData;

import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.Tracker;
//import com.google.android.gcm.GCMRegistrar;
import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.drawable;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.ConfigInfo;
import config.StaticConfig;
import config.ConfigInfo.Launcher;
import config.ConfigInfo.Hotline.GroupEntity;

import db.CarDetailsDatasource;
import db.ChatDataSource;
import db.DatabaseHelper;
import db.HotlinesDataSource;
import db.TableContract.HotlinesDB;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import dialog.MyPickFromActDialogFrag;
import dialog.MyPickFromActDialogFrag.onSendResultListener;
import adapter.SlidingMenuRowAdapter;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.CommonDataKinds.Event;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, onSendResultListener {

	public static final String EXTRA_NOTIFICATION_TYPE_KEY = "notification_type";
	public static final int EXTRA_NEW_MESSAGE_NOTIFICATION = 11;
	public static final int EXTRA_MYCAR_RECEIVE_NOTIFICATION = 12;
	public static final int EXTRA_NEW_TOOLTIPS_NOTIFICATION = 13;
	public static final int EXTRA_NEW_OFFERS_NOTIFICATION = 14;
	public static final int EXTRA_SERVICE_REMINDER_NOTIFICATION = 15;
	public static final int EXTRA_INSURANCE_REMINDER_NOTIFICATION = 16;
	public static final int EXTRA_PUC_REMINDER_NOTIFICATION = 17;
	public static final int EXTRA_NEW_ADVISOR_CHAT_NOTIFICATION = 18;
	public static final int EXTRA_NEW_CEO_CHAT_NOTIFICATION = 19;

	private static final String TAG = "MainActivity";
	UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(this);
	CarDetailsDatasource carDetailDataSource = new CarDetailsDatasource(this);
	ContentValues cv = new ContentValues();

	Tracker tracker;
	//android.support.v7.app.ActionBar actionBar;
	RelativeLayout view;
	ImageView burgerButt;
	ImageView logoButt;
	ListView menuList;

	TextView tipBoxTitle;
	TextView tipBoxSubtitle;
	TextView tipBoxContent;
	TextView tipBoxButton1;
	TextView tipBoxButton2;

	LinearLayout tipHolderLL;

	AlertDialog editProfileDialog;

	Tooltips[] tips;
	int tipOnDisplayIndex;
	CleverTapAPI cleverTap = null;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	Toolbar toolbar;

	List<RelativeLayout> launcherButtons = new ArrayList<RelativeLayout>();
	List<Integer> _launcherButtonIds = new ArrayList<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(R.id.butt_launch1);
			add(R.id.butt_launch2);
			add(R.id.butt_launch3);
			add(R.id.butt_launch4);
			add(R.id.butt_launch5);
			add(R.id.butt_launch6);
			add(R.id.butt_launch7);
			add(R.id.butt_launch8);
			add(R.id.butt_launch9);
		}
	};

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		//ActivityLifecycleCallback.register(getApplication());
		super.onCreate(savedInstanceState);
		tracker = EasyTracker.getInstance(this);
		try {
			cleverTap = CleverTapAPI.getInstance(getApplicationContext());
		} catch (CleverTapMetaDataNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Actionbar customization starts
		//actionBar = getSupportActionBar();

		view = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_home, null);
		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackgroundDrawable(background);
		} else {
			view.setBackground(background);
		}

		/*// Register the device to GCM
				try{
				registerToGcm();
				}catch(UnsupportedOperationException e){
					e.printStackTrace();
				}*/
				
		// Main content view setting starts
		setContentView(R.layout.activity_main);
		toolbar = (Toolbar)findViewById(R.id.toolbar_home);
		setSupportActionBar(toolbar);
		userDetailDataSource.setLoggedInNumber();
		// Set this true for calling car update API
		Preference.setBooleanPreference(getApplicationContext(), Preference.CHECK_CAR_API_BOOLEAN, true);
		
		HotlinesFragment temp = new HotlinesFragment();
		ServiceBaseFragment serviceTemp = new ServiceBaseFragment();
		if(Utils.isNetworkConnected(getApplicationContext())){
			serviceTemp.fetchLocations(getApplicationContext());
			temp.fetchHotlinesFromServer(getApplicationContext());
		}else{
			temp.update9Hotlines(getApplicationContext());
			temp.updateOtherHotlines(getApplicationContext());
			ServiceLocations.setLocations(getApplicationContext());
		}
		
		burgerButt = (ImageView) toolbar.findViewById(R.id.burgerButton);
		burgerButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Toast.makeText(getApplicationContext(), "Burger!",
				// Toast.LENGTH_LONG).show();
				tracker.send(GATrackerMaps.EVENT_HOME_BURGER_BUTTON);
				cleverTap.event.push("Clicked Burger Menu");
				//Toast.makeText(getApplicationContext(), "Burger Clicked", 0).show();

				if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT)){
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
				}else{
					mDrawerLayout.openDrawer(Gravity.RIGHT);
				}
				//toggle();
			}
		});
		// findViewById(R.id.layout_launcher_buttons).setOnClickListener(this);

		int ctr = 0;
		for (int x : _launcherButtonIds) {
			RelativeLayout tRelLay = (RelativeLayout) findViewById(R.id.layout_launcher_buttons).findViewById(x);
			launcherButtons.add(tRelLay);

			if (ctr < StaticConfig.getLaunchersCount()) {
				// tRelLay.setOnClickListener(this);

				TextView b = (TextView) tRelLay.findViewById(R.id.launch_button_view);
				Launcher lInfo = StaticConfig.getLaunchInfoAtPosition(ctr);

				b.setText(lInfo.getLabel());
				b.setCompoundDrawablesWithIntrinsicBounds(0, lInfo.getDrawableResID(), 0, 0);

				if (lInfo.hasCounter())
					tRelLay.findViewById(R.id.tview_notify).setVisibility(View.VISIBLE);
				else
					tRelLay.findViewById(R.id.tview_notify).setVisibility(View.GONE);

				tRelLay.setVisibility(View.VISIBLE);

				// launcherButtons.get(launcherButtons.size() -
				// 1).setOnClickListener(this);
			} else {
				tRelLay.setVisibility(View.INVISIBLE);
			}
			ctr++;
		}

		for (RelativeLayout relLay : launcherButtons) {
			relLay.setOnClickListener(this);
		}

		tipHolderLL = (LinearLayout) findViewById(R.id.home_tips_holder);
		tipBoxTitle = (TextView) findViewById(R.id.tip_box_title_text);
		tipBoxSubtitle = (TextView) findViewById(R.id.tip_box_subtitle_text);
		tipBoxContent = (TextView) findViewById(R.id.tip_box_detail_text);
		tipBoxButton1 = (TextView) findViewById(R.id.tip_box_button1);
		tipBoxButton2 = (TextView) findViewById(R.id.tip_box_button2);

		tipBoxButton1.setOnClickListener(this);
		tipBoxButton2.setOnClickListener(this);
		
		// Navigation Draw starts
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.right_drawer);
        ArrayList<ConfigInfo.Launcher> menuItems = StaticConfig.getMenuItemsList();
        mDrawerList.setAdapter(new SlidingMenuRowAdapter(this, R.layout.row_sliding_menu, menuItems));
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// Toast.makeText(getApplicationContext(),
				// StaticConfig.getMenuItemAtPosition(arg2).getName(),
				// Toast.LENGTH_LONG).show();
				handleLauncherClick(StaticConfig.getMenuItemAtPosition(arg2), true);
			}
		});
        
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        
        //Nevigation Draw Ends
		// Main content view setting ends

	}
	
	/*private void registerToGcm() {
		SharedPreferences prefs =PreferenceManager.getDefaultSharedPreferences(this);
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		final String regId = GCMRegistrar.getRegistrationId(this);
        if(prefs.getString("regId", "").equalsIgnoreCase("")){
            GCMRegistrar.register(this, StaticConfig.SENDER_ID);
        } else{
            Log.d("info", "already registered as" + regId);
        }
	}*/

	@Override
	protected void onResume() {
		super.onResume();
		cleverTap.event.push("Opened Home Screen");
		userDetailDataSource.setLoggedInNumber();
		StaticConfig.position = userDetailDataSource.getLastVisitedCarPosition();
		carDetailDataSource.setMyCarDetails();
		checkforHotlineLogSync();
		checkForAppUpdate();
		/*if(Utils.isNetworkConnected(getApplicationContext())){
			ServiceLocations.fetchLocations(getApplicationContext());
			temp.fetchHotlinesFromServer(getApplicationContext());
		}else{
			temp.update9Hotlines(getApplicationContext());
			temp.updateOtherHotlines(getApplicationContext());
			ServiceLocations.setLocations(getApplicationContext());
		}*/
		
		Intent i;
		if (StaticConfig.FEATURE_SPLASH_SCREEN_ENABLED) {
			StaticConfig.FEATURE_SPLASH_SCREEN_ENABLED = false;
			i = new Intent(this, SplashScreenActivity.class);
			startActivity(i);
			finish();
			return;
		}

		/* if (!MyJSONData.dataFileExists(this, MyJSONData.TYPE_OWN)) { */
		if (!userDetailDataSource.isPhoneNumberExist()) {
			if (StaticConfig.FEATURE_OTP_LOGIN_ENABLED) {
				i = new Intent(this, LoginActivityNew.class);
			} else {
				i = new Intent(this, LoginActivityOld.class);
			}
			i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(i);
			finish();
			return;
		} else if (StaticConfig.FEATURE_OTP_LOGIN_ENABLED) {
			String otp = "AAAAA";
			try {
				otp = URLEncoder.encode(userDetailDataSource.getUserOtp(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// problem reading OTP - treat it as dummy OTP case
			}

			if (otp.equalsIgnoreCase("AAAAA")) {
				// OTP checking is enabled but OTP on record is dummy or not set
				// so - enforce OTP check
				i = new Intent(this, LoginActivityNew.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(i);
				finish();
				return;
			}
		}

		// Toast.makeText(this, (new MyJSONData(this,
		// MyJSONData.TYPE_OWN)).fetchData(MyJSONData.FIELD_OWN_NUMBER),
		// 0).show();
		i = new Intent(this, BackgroundService.class);
		i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_MEDIUM);
		startService(i);

		// notification management center!
		String toastString = "", notificationGALabel = "";
		boolean notificationRedirect = false;
		int notificationType = getIntent().getIntExtra(EXTRA_NOTIFICATION_TYPE_KEY, 0);
		switch (notificationType) {
			case EXTRA_MYCAR_RECEIVE_NOTIFICATION :
				toastString = "Please verify car details received from " + StaticConfig.DEALER_FULL_NAME;

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_MYCAR);
				notificationRedirect = true;
				notificationGALabel = "My-Car Data Received";
				break;
			case EXTRA_SERVICE_REMINDER_NOTIFICATION :
				toastString = "Your car is due for service!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_SERVICE);
				notificationRedirect = true;
				notificationGALabel = "Service Reminder Notification";
				break;
			case EXTRA_INSURANCE_REMINDER_NOTIFICATION :
				toastString = "Time to renew your car insurance!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_MESSAGES);
				notificationRedirect = true;
				notificationGALabel = "Insurance Reminder Notification";
				break;
			case EXTRA_NEW_MESSAGE_NOTIFICATION :
				toastString = "You have new message(s)!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_MESSAGES);
				notificationRedirect = true;
				notificationGALabel = "New Message Received";
				break;
			case EXTRA_NEW_ADVISOR_CHAT_NOTIFICATION :
				toastString = "Your Service Advisor has a message for you!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_LIVE_QUOTE);
				notificationRedirect = true;
				notificationGALabel = "New Live Quote Received";
				break;
			case EXTRA_NEW_CEO_CHAT_NOTIFICATION :
				toastString = StaticConfig.DEALER_FULL_NAME + " CEO has a message for you!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_CEO_CHAT);
				notificationRedirect = true;
				notificationGALabel = "New CEO Reply Received";
				break;
			case EXTRA_NEW_OFFERS_NOTIFICATION :
				toastString = "New and exciting offers, only for you!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_OFFERS);
				notificationRedirect = true;
				notificationGALabel = "New Offer(s) Received";
				break;
			case EXTRA_NEW_TOOLTIPS_NOTIFICATION :
				toastString = "New handy tooltips have been updated!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_TOOLTIPS);
				notificationRedirect = true;
				notificationGALabel = "New Tooltip(s) Received";
				break;
			case EXTRA_PUC_REMINDER_NOTIFICATION :
				toastString = "Time to renew your Pollution Under Control certificate!";

				i = getIntentForLauncherName(StaticConfig.LAUNCH_NAME_MESSAGES);
				notificationRedirect = true;
				notificationGALabel = "PUC Reminder Notification";
				break;
			default :
				notificationRedirect = false;
		}

		if (notificationRedirect) {
			tracker.send(GATrackerMaps.getNotificationViewEvent(notificationGALabel));

			Toast.makeText(this, toastString, Toast.LENGTH_LONG).show();
			getIntent().removeExtra(EXTRA_NOTIFICATION_TYPE_KEY);
			i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(i);
			return;
		}
		// end of notification center!

		// actual home opens here
		tracker.send(GATrackerMaps.VIEW_HOME);

		int pos;
		// Unread count on Messages Launcher button
		pos = StaticConfig.getLaunchButtonPositionFromName(StaticConfig.LAUNCH_NAME_MESSAGES);
		if (pos != -1) {
			int numUnread = MessagesData.countUnread(this);
			if (StaticConfig.FEATURE_CHAT_ENABLED) {
				ChatDataSource chatDataSource = new ChatDataSource(this);

				numUnread += chatDataSource.getUnreadCountForRole(ChatMessages.SENDER_CEO)
						+ chatDataSource.getUnreadCountForRole(ChatMessages.SENDER_SERVICE_ADVISOR);
			}
			TextView t = (TextView) launcherButtons.get(pos).findViewById(R.id.tview_notify);

			if (numUnread > 0) {
				t.setText("" + numUnread);
				t.setVisibility(View.VISIBLE);
			} else {
				t.setVisibility(View.GONE);
			}
		}

		// Error alert on MyCar Launcher button
		pos = StaticConfig.getLaunchButtonPositionFromName(StaticConfig.LAUNCH_NAME_MYCAR);
		if (pos != -1) {
			TextView t = (TextView) launcherButtons.get(pos).findViewById(R.id.tview_notify);
			if (userDetailDataSource.getJsonText().equalsIgnoreCase("No details found for the phone number.")
					&& StaticConfig.myCar.getModelName().equalsIgnoreCase("")) {
				t.setTextSize(14);
				t.setText("!");
				t.setVisibility(View.VISIBLE);
			} else {
				t.setVisibility(View.GONE);
			}
		}

		if (StaticConfig.FEATURE_REWARD_POINT) {
			// total rewards in notification counter
			pos = StaticConfig.getLaunchButtonPositionFromName(StaticConfig.LAUNCH_NAME_REWARD_HISTORY);
			if (pos != -1) {
				TextView t = (TextView) launcherButtons.get(pos).findViewById(R.id.tview_notify);
				t.setBackgroundResource(R.drawable.notification_bg_rewards);
				// MyJSONData myOwn = new MyJSONData(this, MyJSONData.TYPE_OWN);
				/*
				 * String totalRewardsString =
				 * myOwn.fetchData(MyJSONData.FIELD_OWN_REWARD_POINTS_TOTAL);
				 */
				String totalRewardsString = userDetailDataSource.getTotalRewards().trim();
				// int totalRewardsValue = Integer.parseInt(totalRewardsString);
				if (totalRewardsString.equalsIgnoreCase("") || totalRewardsString.equalsIgnoreCase("0")) {
					t.setVisibility(View.GONE);
				} else {
					t.setTextSize(8);
					t.setTextColor(Color.BLACK);
					t.setText(totalRewardsString);
					t.setVisibility(View.VISIBLE);

				}
			}
		}

		if (StaticConfig.FEATURE_CHAT_ENABLED) {
			// Notification counter for Live Quote
			pos = StaticConfig.getLaunchButtonPositionFromName(StaticConfig.LAUNCH_NAME_LIVE_QUOTE);
			if (pos != -1) {
				TextView t = (TextView) launcherButtons.get(pos).findViewById(R.id.tview_notify);
				int x = new ChatDataSource(this).getUnreadCountForRole(ChatMessages.SENDER_SERVICE_ADVISOR);
				if (x > 0) {

					t.setText("" + x);
					t.setVisibility(View.VISIBLE);
				} else {
					t.setVisibility(View.GONE);
				}
			}
		}

		tips = TooltipsListFragment.getTipsData(this);

		if (tips.length > 0) {
			tipHolderLL.setVisibility(View.VISIBLE);
			// code to handle data in bottom tip-box
			tipBoxTitle.setText("Tip of the Day");
			// tipBoxTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tooltips_smaller_icon,
			// 0, 0, 0);
			tipBoxButton1.setText("Read Further");
			tipBoxButton2.setText("More Tips");

			tipOnDisplayIndex = new Random().nextInt(tips.length);
			tipBoxSubtitle.setText(tips[tipOnDisplayIndex].getSubject());
			tipBoxContent.setText(tips[tipOnDisplayIndex].getBody());

		} else {
			tipHolderLL.setVisibility(View.INVISIBLE);
		}

		// Toast.makeText(this, "CID: " + tracker.get(Fields.CLIENT_ID),
		// 0).show();
		Log.d(TAG, tracker.get(Fields.CLIENT_ID));
		// Toast.makeText(this, getPackageName(), 0).show();
		
	}

	private void checkforHotlineLogSync() {
		HotlinesDataSource hotlineDS = new HotlinesDataSource(this);
		int log_count=0;
		Cursor c = hotlineDS.getDataFromHelper();
		if (c.moveToFirst()) {
			do {
				String deptName = c.getString(c.getColumnIndex(HotlinesDB.COL_HELPER_HOTLINE_DEPARTMENT_NAME));
				GroupEntity obj = new GroupEntity(deptName, "", "", c.getInt(c.getColumnIndex(HotlinesDB.COL_HELPER_HOTLINE_ID)));
				log_count = c.getInt(c.getColumnIndex(HotlinesDB.COL_LOG_COUNT));
				ArrayList<GroupEntity> numberList = new ArrayList<ConfigInfo.Hotline.GroupEntity>();
				numberList.add(obj);
				for(int i = log_count; i>0; i--){
					HotlinesFragment hotlineObj = new HotlinesFragment();
					if(Utils.isNetworkConnected(getApplicationContext())){
						hotlineObj.sendToServer(0, numberList, getApplicationContext());
					}	
				}
			} while (c.moveToNext());
			
		} else {
			
			/*hotlineSelector.clear();
			System.out.println("HOTLINE SELECTOR SIZE: "+hotlineSelector.size());*/
		}
		hotlineDS.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	private int launcherButtonsIdToIndex(int buttonID) {
		return _launcherButtonIds.indexOf(buttonID);
	}

	@Override
	public void onClick(View v) {
		// Log.d(TAG, "OnClick");
		int x = launcherButtonsIdToIndex(v.getId());
		// Log.d(TAG, "Launcher Button Index: " + x);
		if (x != -1) {
			handleLauncherClick(StaticConfig.getLaunchInfoAtPosition(x), false);
		} else {// one of the non-launcher buttons has been clicked
			Intent iin;
			switch (v.getId()) {
				case R.id.tip_box_button1 :
					tracker.send(GATrackerMaps.EVENT_HOME_READ_FURTHER);

					// Open Tooltips Detail with the 'on-display' tip
					// with making sure that the list->detail flow is honored
					iin = new Intent(this, ContentOnlyFragActivity.class);
					iin.putExtra(ContentOnlyFragActivity.EXTRA_LAUNCHER_DATA_KEY,
							StaticConfig.getLauncherInfoFromName(StaticConfig.LAUNCH_NAME_TOOLTIPS));
					iin.putExtra(TooltipsListFragment.INTENT_EXTRA_KEY_TIP_INDEX, tipOnDisplayIndex);
					startActivity(iin);
					break;
				case R.id.tip_box_button2 :
					tracker.send(GATrackerMaps.EVENT_HOME_MORE_TIPS);

					// Open Tooltips list
					iin = new Intent(this, ContentOnlyFragActivity.class);
					iin.putExtra(ContentOnlyFragActivity.EXTRA_LAUNCHER_DATA_KEY,
							StaticConfig.getLauncherInfoFromName(StaticConfig.LAUNCH_NAME_TOOLTIPS));
					startActivity(iin);
					break;
			}
		}
	}
	public void checkForAppUpdate(){
		PackageInfo pInfo;
		try {
			// MyJSONData ownData = new MyJSONData(this, MyJSONData.TYPE_OWN);
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			int versionCode = pInfo.versionCode;
			// NOTE: latest_version field is updated by bkgrnd service
			final String latestVersionFlag = userDetailDataSource.getLatestVersion().trim();
			System.out.println("APP VERSION FETCHED"+latestVersionFlag);
			System.out.println("APP VERSION INSTALLED"+versionCode);
			if (latestVersionFlag.equalsIgnoreCase("0")) {
				// flag was never set, so set it now and forget about it
				cv.put(UserDB.COL_FIELD_OWN_LATEST_VERSION, versionCode);
				userDetailDataSource.updateUserDetails(cv);
			}
			/*else if (!latestVersionFlag.equalsIgnoreCase("" + versionCode)
					&& !userDetailDataSource.getLastRemindedFor().trim().equalsIgnoreCase(latestVersionFlag)) {*/
			else if (!latestVersionFlag.equalsIgnoreCase("" + versionCode)) {

				Builder bldr;
				Dialog dlg;
				cv = new ContentValues();
				bldr = new Builder(this);
				bldr.setTitle("New App Version Available");
				bldr.setPositiveButton("Update", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						tracker.send(GATrackerMaps.EVENT_HOME_UPDATE_APP);
						cv.put(UserDB.COL_FIELD_OWN_UPDATE_REMINDED_FOR, latestVersionFlag);
						userDetailDataSource.updateUserDetails(cv);
						try {
							startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
						} catch (ActivityNotFoundException e) {
							Toast.makeText(getApplicationContext(), "Please install the Google Play Store and try again!", Toast.LENGTH_LONG).show();
						}
					}
				});
				/*bldr.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						tracker.send(GATrackerMaps.EVENT_HOME_UPDATE_NOT_NOW);
					}
				});*/
				dlg = bldr.create();
				dlg.setCancelable(false);
				dlg.show();
			}
		} catch (NameNotFoundException e) {
			// ignore
		}
	}
	private void handleLauncherClick(Launcher launcher, boolean isMenuItem) {
		if (launcher == null)
			return;

		tracker.send(GATrackerMaps.getLaunchButtonEvent(launcher, isMenuItem));

		switch (launcher.getLaunchType()) {
			case Launcher.LAUNCH_TAB :
			case Launcher.LAUNCH_ACTIVITY :
			case Launcher.LAUNCH_FRAGMENT :
				startActivity(getIntentForLauncherName(launcher.getName()));
				break;
			case Launcher.LAUNCH_LOCATE :
				if (!MyPickFromActDialogFrag.isPickerDialogOpen()) {
					MyPickFromActDialogFrag pickerFragDialog = MyPickFromActDialogFrag.newInstance(MyPickFromActDialogFrag.TYPE_LOCATE_LIST_PICKER);
					pickerFragDialog.show(this.getSupportFragmentManager(), "locateListPicker");
				}
				break;
			default :
				// Log.d(TAG, launcher.getName());
				if (launcher.getName().equalsIgnoreCase(StaticConfig.LAUNCH_NAME_FACEBOOK)) {
					if (!StaticConfig.DEALER_FACEBOOK_PAGE.equalsIgnoreCase("")) {
						// Log.d(TAG, StaticConfig.DEALER_FACEBOOK_PAGE);
						Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(StaticConfig.DEALER_FACEBOOK_PAGE));
						startActivity(i);
					}
				} else if (launcher.getName().equalsIgnoreCase(StaticConfig.LAUNCH_NAME_SHARE_APP)) {
					// Toast.makeText(this, "Share App", 0).show();
					Intent sendIntent = new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);
					sendIntent.putExtra(Intent.EXTRA_TEXT, "Try the " + StaticConfig.DEALER_FULL_NAME
					// + " Mobile App! It is really cool.\n" +
					// "market://details?id=" + getPackageName());
							+ " Mobile App! It is really cool.\n" + StaticConfig.DEALER_APPLINKS_PAGE);
					sendIntent.setType("text/plain");
					startActivity(sendIntent);
				} else if (launcher.getName().equalsIgnoreCase(StaticConfig.LAUNCH_NAME_RATEUS)) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
				}
		}
	}

	@Override
	public void onSendResult(String result, int requestCode) {
		switch (requestCode) {
			case MyPickFromActDialogFrag.TYPE_LOCATE_LIST_PICKER :
				String uri = "";
				if (result.equalsIgnoreCase("" + StaticConfig.LOCATOR_SERVICE_CENTERS)) {
					tracker.send(GATrackerMaps.EVENT_HOME_LOCATE_SERVICE);
					uri = StaticConfig.getDealerLocationMapLink();
				} else if (result.equalsIgnoreCase("" + StaticConfig.LOCATOR_PETROL_PUMPS)) {
					tracker.send(GATrackerMaps.EVENT_HOME_LOCATE_FUEL);
					uri = "geo:0,0?q=Petrol+Pump";
				} else if (result.equalsIgnoreCase("" + StaticConfig.LOCATOR_POLICE_STATION)) {
					tracker.send(GATrackerMaps.EVENT_HOME_LOCATE_POLICE);
					uri = "geo:0,0?q=Police+Station";
				}

				if (!uri.equalsIgnoreCase("")) {
					try {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
					} catch (ActivityNotFoundException e) {
						// Log.e(TAG, "ActivityNotFound: " + e.getMessage());
						Toast.makeText(this, "Please install Google Maps and try again!", Toast.LENGTH_LONG).show();
					}
				}
				break;
			default :
				// ignore
		}
	}

	private Intent getIntentForLauncherName(String launcherName) {
		Launcher l = StaticConfig.getLauncherInfoFromName(launcherName);

		if (l == null)
			return null;
		else {
			Intent i;
			switch (l.getLaunchType()) {
				case (Launcher.LAUNCH_TAB) :
					i = new Intent(getApplicationContext(), ContentWithTabsActivity.class);
					i.putExtra(ContentWithTabsActivity.EXTRA_TAB_INDEX_KEY, l.getAssociatedTabID());
					i.putExtra(ContentWithTabsActivity.EXTRA_TAB_FRAGMENT_INIT_EXTRA_KEY, l.getExtraFragmentInitValue());
					// Log.d(TAG, "Frag init extra: " +
					// l.getExtraFragmentInitValue());
					return i;
				case (Launcher.LAUNCH_ACTIVITY) :
					i = new Intent(getApplicationContext(), l.getAssociatedActivity());
					return i;
				case (Launcher.LAUNCH_FRAGMENT) :
					i = new Intent(getApplicationContext(), ContentOnlyFragActivity.class);
					i.putExtra(ContentOnlyFragActivity.EXTRA_LAUNCHER_DATA_KEY, l);
					return i;
				default :
					return null;
			}
		}
	}
}
