package activity;

import entity.ChatMessages;
import fragment.ChatAdvisorFragment;
import fragment.MessagesHomeFragment;
import fragment.MyCarAddFragment;
import fragment.MyFragment;
import fragment.TabBarFragment;
import fragment.MyFragment.MyOnFragmentReplaceListener;
import fragment.MyFragment.ToolbarListener;
import fragment.TabBarFragment.MyOnTabChangeListener;
import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import myJsonData.MyJSONData;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.ConfigInfo;
import config.StaticConfig;

import db.ChatDataSource;
import db.DatabaseHelper;
import db.TableContract.ChatDB;

public class ContentWithTabsActivity extends MyFragmentedActivityBase 
	implements MyOnTabChangeListener, MyOnFragmentReplaceListener,ToolbarListener {
	
	private static final String TAG = "ContentWithTabsActivity";
	public static final String EXTRA_TAB_INDEX_KEY = "tabIndex";
	public static final String EXTRA_TAB_FRAGMENT_INIT_EXTRA_KEY = "tabFragInitKey";
	
	private int FRAGMENT_INIT_EXTRA_VALUE = 0;
	
	View tabBarLayout;
	Toolbar toolbar;
	TextView actionBarTitleView;
	ImageView homeButtMycar;
	ImageView homeButt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_frag_content_with_tabbar);
		tabBarLayout = findViewById(R.id.tabbar_fragment);
		// hide below tab
		tabBarLayout.setVisibility(View.GONE);
		toolbar = (Toolbar)findViewById(R.id.toolbar_inner);
		actionBarTitleView = (TextView)toolbar.findViewById(R.id.titleText);
		setSupportActionBar(toolbar);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		//getSupportActionBar().setHomeAsUpIndicator(R.drawable.right_arrow);
		//getSupportActionBar().setHomeButtonEnabled(true);
		//getTheme().applyStyle(R.style.Your_Theme, true);
		int x = getIntent().getIntExtra(EXTRA_TAB_INDEX_KEY, 0); 
		FRAGMENT_INIT_EXTRA_VALUE = getIntent().getIntExtra(EXTRA_TAB_FRAGMENT_INIT_EXTRA_KEY, 0);
		onTabChangeRequest(x);

		homeButt = (ImageView)findViewById(R.id.homeButton);
		homeButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		
		homeButtMycar = (ImageView)findViewById(R.id.addCar);
		homeButtMycar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cleverTap.event.push("Clicked Add New Car button- My Car Screen");
				replaceFragment(new MyCarAddFragment(), true);
			}
		});
		
		ImageView backButt = (ImageView)findViewById(R.id.backButton);
		backButt.setEnabled(true);
		backButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		//Log.d(TAG, "onCreate");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//Log.d(TAG, "onResume");
	}
	
	@Override
	public void onTabChanged(ConfigInfo.Tab selectedTab) {
		//Log.d("TabActivityBase", "onTabChanged");
		//Log.d("TabActivitybase", selectedTab.getTabName());

		Fragment tfg;
		try {
			MyFragment fgmt = (MyFragment) selectedTab.getbaseFragmentClass().newInstance();
			fgmt.initExtraInt = FRAGMENT_INIT_EXTRA_VALUE;
			tfg = (Fragment) fgmt;
			replaceFragment(tfg, false);
		} catch (InstantiationException e) {
			//Log.e("InnerAct", "InstantionException: " + e.getMessage());
		} catch (IllegalAccessException e) {
			//Log.e("InnerAct", "IllegalAccessException: " + e.getMessage());
		}
	}
	
	@Override
	public void onFragmentReplaceRequest(Fragment sFgmt, boolean addToBackStack) {
		replaceFragment(sFgmt, addToBackStack);
	}

	@Override
	public void onTabChangeRequest(int newTabID) {
		((TabBarFragment) getSupportFragmentManager().findFragmentById(R.id.tabbar_fragment)).setActiveTab(newTabID);
		
		onTabChanged(StaticConfig.getTabInfoAtPosition(newTabID));
	}

	@Override
	public void onTabBarVisiblityChangeRequest(boolean showTabBar) {
		if (showTabBar) {
			tabBarLayout.setVisibility(View.VISIBLE);
		}
		else {
			tabBarLayout.setVisibility(View.GONE);
		}
	}
	
	private void storeChatImage(String attemptPath,int type){
		Bitmap b = BitmapFactory.decodeFile(attemptPath);
		String fName = "";
		if (b != null) {
			switch(type){
				case 0:
					fName = attemptPath;
					break;
				case 1:
					 fName = (new File(attemptPath)).getName();
					break;
			}
			
			try {
				ExifInterface exif = new ExifInterface(attemptPath);
				String orient = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
				//Log.d(TAG, "Orientation: " + orient);
				Matrix m = new Matrix();
				
				int orientInt = 0;
				if (orient != null) {
					orientInt = Integer.parseInt(orient);
				}

				switch (orientInt) {
				case 6:
					m.postRotate(90);
					break;
				case 3:
					m.postRotate(180);
					break;
				case 8:
					m.postRotate(270);
					break;
				default:
					m.postRotate(0);
					break;
				}
				m.postScale(0.5f, 0.5f);
				//Log.d(TAG, "oldHeight: " + b.getHeight() + "\noldWidth: " + b.getWidth());
				b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
				//Log.d(TAG, "newHeight: " + b.getHeight() + "\nnewWidth: " + b.getWidth());

				FileOutputStream imgFOS = new FileOutputStream(attemptPath);
				b.compress(Bitmap.CompressFormat.JPEG, 75, imgFOS);
				imgFOS.close();
			}
			catch (FileNotFoundException e) {
				//Log.e(TAG, "FileNotFoundException: " + e.getMessage());
			}
			catch (IOException e) {
				//Log.e(TAG, "IOException: " + e.getMessage());
			}
			catch (NumberFormatException e) {
				//Log.e(TAG, "NumberFormatException: " + e.getMessage());
			}
			
			tracker.send(GATrackerMaps.EVENT_LIVE_QUOTE_SEND_SNAP);
			
			ContentValues cv = new ContentValues();
			cv.put(ChatDB.COL_CHAT_MSG, fName);
			cv.put(ChatDB.COL_CHAT_SENDER, ChatMessages.SENDER_USER);
			cv.put(ChatDB.COL_CHAT_RECEIVER, ChatMessages.SENDER_SERVICE_ADVISOR);
			cv.put(ChatDB.COL_CHAT_READ_FLAG, 1); //user sent message is always read by user!
			cv.put(ChatDB.COL_CHAT_TYPE, 1);

			long reqId = new ChatDataSource(this).insert(cv);
		}				
		else {
			//Log.e(TAG, "Bitmap is Null!");
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Log.d(TAG, "onActivityResult Called!");
		//Log.d(TAG, "Request Code: " + requestCode);
		String attemptPath = "";
		if (requestCode == ChatAdvisorFragment.REQUEST_CODE_CLICK_PHOTO) {
			
			//MyJSONData tOwn = new MyJSONData(this, MyJSONData.TYPE_OWN);
			//String attemptPath = tOwn.fetchData(MyJSONData.FIELD_QUOTE_PHOTO_ATTEMPT_PATH);
			
			if(!StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH.equals("")){
			   attemptPath = StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH;
			}
			
			//Log.d(TAG, "AttemptPath: " + attemptPath);
			
			if (resultCode == Activity.RESULT_OK) {
				storeChatImage(attemptPath,1);
			}
		}
			else if (requestCode == ChatAdvisorFragment.REQUEST_CODE_GALLERY_PHOTO) {
				tracker.send(GATrackerMaps.EVENT_CAR_SET_PIC_FROM_GALLERY);
				try{
				Uri selectedImageUri = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getApplicationContext().getContentResolver().query(
						selectedImageUri, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String finalPhotoPath = cursor.getString(columnIndex);
				StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH = finalPhotoPath;
				cursor.close();
				if(!StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH.equals("")){
					   attemptPath = StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH;
					}
				if (resultCode == Activity.RESULT_OK) {
					storeChatImage(attemptPath,0);
			}else{
				//delete temp holder file that was created
				new File(attemptPath).delete();
			}
				}catch(Exception e){
					e.printStackTrace();
				}
			Fragment currFrag = (Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
			if (!currFrag.getClass().getSimpleName().equalsIgnoreCase(ChatAdvisorFragment.class.getSimpleName())) {

				FRAGMENT_INIT_EXTRA_VALUE = MessagesHomeFragment.REDIRECT_TO_ADVISOR;
				onTabChangeRequest(StaticConfig.getTabPositionFromName(StaticConfig.TAB_NAME_MESSAGES));
			}
		}
		else {
			//pass this on to the child fragment
			((Fragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container)).onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void setText(String title) {
		actionBarTitleView.setText(title);
		
	}

	@Override
	public void setAddCarButton(boolean addCar) {
		if(addCar == true){
			homeButtMycar.setVisibility(View.VISIBLE);
			homeButt.setVisibility(View.GONE);
		}else{
			homeButtMycar.setVisibility(View.GONE);
			homeButt.setVisibility(View.VISIBLE);
		}
		
	}

}
