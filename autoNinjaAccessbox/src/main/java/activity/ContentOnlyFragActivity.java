package activity;

import com.myaccessbox.appcore.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import config.StaticConfig;
import config.ConfigInfo.Launcher;

import fragment.TooltipsListFragment;
import fragment.MyCarAddFragment;
import fragment.MyFragment.MyOnFragmentReplaceListener;
import fragment.MyFragment.ToolbarListener;

public class ContentOnlyFragActivity extends MyFragmentedActivityBase implements
	MyOnFragmentReplaceListener, ToolbarListener {
	
	public static final String EXTRA_LAUNCHER_DATA_KEY = "fragmentLauncher";
	Toolbar toolbar;
	TextView actionBarTitleView;
	ImageView homeButtMycar;
	ImageView homeButt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_frag_content_only);
		toolbar = (Toolbar)findViewById(R.id.toolbar_inner);
		actionBarTitleView = (TextView)toolbar.findViewById(R.id.titleText);
		setSupportActionBar(toolbar);
		//getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		//getSupportActionBar().setHomeButtonEnabled(true);
				
		//replaceFragment(new MyCarBaseFragment(), false);
		
		homeButt = (ImageView)findViewById(R.id.homeButton);
		homeButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		
		homeButtMycar = (ImageView)findViewById(R.id.addCar);
		homeButtMycar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cleverTap.event.push("Clicked Add New Car button- My Car Screen");
				replaceFragment(new MyCarAddFragment(), true);
			}
		});
		
		ImageView backButt = (ImageView)findViewById(R.id.backButton);
		backButt.setEnabled(true);
		backButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		
		Launcher l = (Launcher) getIntent().getSerializableExtra(EXTRA_LAUNCHER_DATA_KEY);
		
		try {
			replaceFragment((Fragment) l.getAssociatedFragment().newInstance(), false);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		//Log.d("ContentOnlyFragActivity", "onCreate");
	}
	
	@Override
	protected void onResume() {
		getIntent().getParcelableExtra(TooltipsListFragment.INTENT_EXTRA_KEY_TIP_INDEX);
		
		super.onResume();
	}

	@Override
	public void onFragmentReplaceRequest(Fragment sFgmt, boolean addToBackStack) {
		replaceFragment(sFgmt, addToBackStack);
	}

	@Override
	public void onTabChangeRequest(int newTabID) {
		try {
			replaceFragment((Fragment) StaticConfig.getTabInfoAtPosition(newTabID).getbaseFragmentClass().newInstance(), false);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTabBarVisiblityChangeRequest(boolean showTabBar) {
		//do nothing as there is no tabBar 
	}

	@Override
	public void setText(String title) {
		actionBarTitleView.setText(title);
		
	}

	@Override
	public void setAddCarButton(boolean addCar) {
		if(addCar == true){
			homeButtMycar.setVisibility(View.VISIBLE);
			homeButt.setVisibility(View.GONE);
		}else{
			homeButtMycar.setVisibility(View.GONE);
			homeButt.setVisibility(View.VISIBLE);
		}
		
	}

}
