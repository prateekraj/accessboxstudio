package activity;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.layout;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity{
	
	// Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onCreate(savedInstanceState);
    	
    	setContentView(R.layout.activity_splash_screen);
    	//getSupportActionBar().hide();
    	new Handler().postDelayed(new Runnable() {
			
    		/*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
    		
			@Override
			public void run() {
				// This method will be executed once the timer is over
                // Start your app main activity
				Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
				startActivity(i);
				finish();
				
			}
		}, SPLASH_TIME_OUT);
    }
}
