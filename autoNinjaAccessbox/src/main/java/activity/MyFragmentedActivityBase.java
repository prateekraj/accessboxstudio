package activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.Tracker;
import com.myaccessbox.appcore.R;

import fragment.MyCarAddFragment;

public class MyFragmentedActivityBase extends AppCompatActivity {
	
	protected Tracker tracker;
	protected CleverTapAPI cleverTap = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//setActionBarCustomView();
		//setToolBarCustomView();
		tracker = EasyTracker.getInstance(this);
		try {
			cleverTap = CleverTapAPI.getInstance(getApplicationContext());
		} catch (CleverTapMetaDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Log.d("MyFragmentedActivityBase", "onCreate");
	}
	
	protected void replaceFragment(Fragment sFgmt, boolean addToBackStack) {
		//Log.d("TabActivityBase", "replaceFragment");
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction fTransaction = fm.beginTransaction();
		
		fTransaction.setCustomAnimations(R.anim.zoom_in, R.anim.fade_out, R.anim.fade_in, R.anim.zoom_out);
		
		fTransaction.replace(R.id.fragment_container, sFgmt);
		if (addToBackStack) {
			fTransaction.addToBackStack(null);
		}
		else {
			fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
		fTransaction.commitAllowingStateLoss();
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public void setActionBarCustomView() {
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		
		RelativeLayout view = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_inner, null);
		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
		    view.setBackgroundDrawable(background);
		} else {
		    view.setBackground(background);
		}
		
		actionBar.setCustomView(view);

		ImageView homeButt = (ImageView) view.findViewById(R.id.homeButton);
		homeButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), MainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			}
		});
		
		ImageView backButt = (ImageView) view.findViewById(R.id.backButton);
		backButt.setEnabled(true);
		backButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		
	}
	
	@SuppressLint("NewApi")
	public void setMyCarActionBarCustomView() {
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		
		RelativeLayout view = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_inner_mycar, null);
		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
		    view.setBackgroundDrawable(background);
		} else {
		    view.setBackground(background);
		}
		
		actionBar.setCustomView(view);

		ImageView homeButt = (ImageView) view.findViewById(R.id.homeButton);
		homeButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				replaceFragment(new MyCarAddFragment(), true);
			}
		});
		
		/*TextView tv = (TextView) view.findViewById(R.id.titleText);
		tv.setText("MyCar");*/
		
		ImageView backButt = (ImageView) view.findViewById(R.id.backButton);
		backButt.setEnabled(true);
		backButt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		
	}
	
	/*public void setMyCarActionBarCustomView() {
		ActionBar actionBar = getSupportActionBar();
		
		RelativeLayout vw = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_inner_mycar, null);
		actionBar.setCustomView(vw);

		TextView bacK = (TextView) vw.findViewById(R.id.titleleft);
		bacK.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		
		TextView addCar = (TextView) vw.findViewById(R.id.titleRight);
		addCar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				replaceFragment(new MyCarAddFragment(), true);
			}
		});
		
	}*/
	
	/*public android.support.v7.app.ActionBar getSupportActionBar() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	protected void onStart() {
		super.onStart();
		//EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		//EasyTracker.getInstance(this).activityStop(this);
	}
}
