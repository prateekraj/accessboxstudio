package fragment;

import googleAnalytics.GATrackerMaps;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adapter.ServiceHistoryListAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.StaticConfig;

import db.DatabaseHelper;
import db.ServiceHistoryDataSource;
import db.UserDetailsDataSource;
import db.TableContract.ServiceDetailsDB;
import db.TableContract.UserDB;
import entity.ServiceHistoryDataPoint;

public class ServiceHistoryFragment extends MyFragment {

	public static final String INTENT_EXTRA_KEY_TIP_INDEX = "open_detail_tip_index";

	ServiceHistoryDataPoint historyData[];
	ListView historyListView;
	TextView tvExtra;
	TextView tvListEmptyNote;
	Spinner carList;
	LinearLayout carSpinnerLL;
	ArrayAdapter<String> dataAdapter;

	protected Dialog dlgChangeCar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Log.d("TestFragment2", "OnCreateView Called!");

		historyData = new ServiceHistoryDataPoint[0];

		View v = inflater.inflate(R.layout.frag_service_history, container, false);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		historyListView = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		carList = (Spinner) v.findViewById(R.id.car_list);
		carSpinnerLL = (LinearLayout) v.findViewById(R.id.select_car_layout);
		dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_multiplecar, StaticConfig.myCarListNames);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		carList.setAdapter(dataAdapter);
		carList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String chosenModelName = parent.getItemAtPosition(position).toString();
				StringTokenizer st = new StringTokenizer(chosenModelName, ",");

				String model = st.nextToken();
				String regNum = st.nextToken().trim();
				for (int i = 0; i < StaticConfig.myCarList.size(); i++) {

					if (StaticConfig.myCarList.get(i).getModelName().equalsIgnoreCase(model)
							&& StaticConfig.myCarList.get(i).getReg_num().equalsIgnoreCase(regNum)) {
						StaticConfig.myCar = StaticConfig.myCarList.get(i);
						StaticConfig.position = position;
						UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
						ContentValues cv = new ContentValues();
						cv.put(UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION, position);
						userDetailDataSource.updateUserDetails(cv);
						// dbhelper.getCarList();
					}
				}
				setSpinnerData();
				populateData();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		return v;
	}
	public void setSpinnerData() {
		if (StaticConfig.myCarListNames.size() <= 0) {
			carList.setVisibility(View.GONE);
			carSpinnerLL.setVisibility(View.GONE);
		} else {
			dataAdapter.notifyDataSetChanged();
			carList.setSelection(StaticConfig.position);
		}
	}
	@Override
	public void onAttach(Activity activity) {
		// first thing to do is to call super!
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		super.onResume();
		toolbarListener.setText("Service History");
		setSpinnerData();
		tracker.send(GATrackerMaps.VIEW_MYCAR_SERVICE_HISTORY);
		cleverTap.event.push("Opened Service History Screen");
		populateData();
	}
	private void populateData() {

		historyData = getHistoryFromDB();
		System.out.println("HISTORY FOR CAR: " + StaticConfig.myCar.getModelName() + "CAR ID: " + StaticConfig.myCar.getCarId());
		if (historyData.length > 0) {
			historyListView.setVisibility(View.VISIBLE);
			tvListEmptyNote.setVisibility(View.GONE);

			ServiceHistoryListAdapter histAdptr = new ServiceHistoryListAdapter(getActivity(), R.layout.row_service_history_list, historyData);
			historyListView.setAdapter(histAdptr);

			tvExtra.setText("Serviced " + historyData.length + " times");
		} else {
			nothingToDisplay();
		}

	}
	private ServiceHistoryDataPoint[] getHistoryFromDB() {

		ServiceHistoryDataSource serviceHistoryDS = new ServiceHistoryDataSource(getActivity());
		Cursor c = serviceHistoryDS.getAllServiceHistoryOfTheCarId();
		ArrayList<ServiceHistoryDataPoint> returnArrayList = new ArrayList<ServiceHistoryDataPoint>();
		ServiceHistoryDataPoint returnData[] = new ServiceHistoryDataPoint[0];
		int i = 0;
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(ServiceDetailsDB.COL_SERVICE_CAR_ID));
				if (id == StaticConfig.myCar.getCarId()) {
					String date = c.getString(c.getColumnIndex(ServiceDetailsDB.COL_SERVICE_DATE));
					String serviceType = c.getString(c.getColumnIndex(ServiceDetailsDB.COL_SERVICE_WORK_TYPE));
					String mileage = c.getString(c.getColumnIndex(ServiceDetailsDB.COL_SERVICE_MILEAGE));
					String amount = c.getString(c.getColumnIndex(ServiceDetailsDB.COL_SERVICE_AMOUNT));
					ServiceHistoryDataPoint tempObj = new ServiceHistoryDataPoint(date, serviceType, mileage, amount);
					returnArrayList.add(tempObj);
					System.out.println("SERVICE HISTORY:" + date + ":" + serviceType + ":" + mileage + ":" + amount);
				}

			} while (c.moveToNext());
		} else {

		}
		returnData = new ServiceHistoryDataPoint[returnArrayList.size()];
		for (int j = 0; j < returnArrayList.size(); j++) {
			returnData[j] = returnArrayList.get(j);
		}
		serviceHistoryDS.close();
		return returnData;
	}

	private void nothingToDisplay() {
		historyListView.setVisibility(View.GONE);
		tvListEmptyNote.setVisibility(View.VISIBLE);
		tvListEmptyNote.setText("No service history data to display!");
		tvExtra.setText("");
	}
}
