package fragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import myJsonData.MyJSONData;

import service.AsyncInvokeURLTask;
import utils.Utils;
import android.app.AlertDialog.Builder;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import config.ConfigInfo;
import config.StaticConfig;
import config.ConfigInfo.Hotline.GroupEntity;
import db.HotlinesDataSource;
import db.TableContract.HotlinesDB;


public class HotlinePickerDialogFrag extends DialogFragment implements
		DialogInterface.OnClickListener {

	public interface onSendResultListener {
		public void onSendResult(String result, int requestCode);
	}

	private static final String KEY_DATA = "data";
	private static final String KEY_TITLE = "title";

	private static boolean _current_dialog_is_open = false;

	private String _title = "";
	private ArrayList<GroupEntity> _numbersData = new ArrayList<ConfigInfo.Hotline.GroupEntity>();

	@Override
	public void onCancel(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onCancel(dialog);
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		_current_dialog_is_open = false;
		super.onDismiss(dialog);
	}

	public static HotlinePickerDialogFrag newInstance(String title,
			ArrayList<GroupEntity> numbersData) {
		Bundle bundle = new Bundle();
		bundle.putParcelableArrayList(KEY_DATA, numbersData);
		bundle.putString(KEY_TITLE, title);

		HotlinePickerDialogFrag dFrag = new HotlinePickerDialogFrag();
		dFrag.setArguments(bundle);
		return dFrag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		savedInstanceState = getArguments();
		_numbersData = savedInstanceState.getParcelableArrayList(KEY_DATA);
		_title = savedInstanceState.getString(KEY_TITLE);

		_current_dialog_is_open = true;

		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setTitle(_title);

		String[] displayLabels = new String[_numbersData.size()];
		int p = 0;
		for (GroupEntity number : _numbersData) {
			displayLabels[p] = number.getName();
			p++;
		}

		builder.setItems(displayLabels, this);

		return builder.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		HotlinesFragment object = new HotlinesFragment();
		HotlinesDataSource hotlineDS = new HotlinesDataSource(getActivity());
		ContentValues cv = new ContentValues();
		System.out.println("CLICKED");
		int log_count=hotlineDS.getStoredflag(_numbersData.get(0).getLabel());
		if(Utils.isNetworkConnected(getActivity())){
			object.sendToServer(which,_numbersData,this.getActivity() );
		}else{
			log_count++;
			cv.put(HotlinesDB.COL_LOG_COUNT,log_count );
			hotlineDS.updateHotlinesHelper(cv,_numbersData.get(0).getLabel());
		}
		sendResultToTarget(_numbersData.get(which).getNumber());
	}

	private void sendResultToTarget(String str) {
		_current_dialog_is_open = false;
		((onSendResultListener) getTargetFragment()).onSendResult(str,
				getTargetRequestCode());
	}

	public static boolean isPickerDialogOpen() {
		return _current_dialog_is_open;

	}
}
