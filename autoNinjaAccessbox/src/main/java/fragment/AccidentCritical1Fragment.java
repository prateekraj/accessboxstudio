package fragment;

import googleAnalytics.GATrackerMaps;

import com.myaccessbox.appcore.R;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import config.StaticConfig;

public class AccidentCritical1Fragment extends MyFragment implements View.OnClickListener {
	
	TextView buttonCallPolice;
	TextView buttonLocatePolice;
	TextView buttonNext;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_accident_crit1, container, false);
		
		buttonCallPolice = (TextView) v.findViewById(R.id.acc_crit1_button_call_police);
		buttonLocatePolice = (TextView) v.findViewById(R.id.acc_crit1_button_locate_police);
		buttonNext = (TextView) v.findViewById(R.id.button_next);
		
		buttonCallPolice.setOnClickListener(this);
		buttonLocatePolice.setOnClickListener(this);
		buttonNext.setOnClickListener(this);
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity); //call super first before doing anything else!
		
		toolbarListener.setText("Critical Situation");
		//setActionBarBackEnabled(true);
		
		//Log.d("AccidentBaseFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		tracker.send(GATrackerMaps.VIEW_ACCIDENT_CRITICAL1);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.acc_crit1_button_call_police:
			tracker.send(GATrackerMaps.EVENT_ACCIDENT1_CALL_POLICE);
			
			Intent i = new Intent(Intent.ACTION_CALL);
			i.setData(Uri.parse("tel:" + StaticConfig.CALL_POLICE));
			startActivity(i);
			//Toast.makeText(getActivity(), "Call Police!", 0).show();
			break;
		case R.id.acc_crit1_button_locate_police:
			//Toast.makeText(getActivity(), "Find Police!", 0).show();
			tracker.send(GATrackerMaps.EVENT_ACCIDENT1_LOCATE_POLICE);

			String uri = "geo:0,0?q=Police+Station";
			try {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
			}
			catch (ActivityNotFoundException e) {
				//Log.e(TAG, "ActivityNotFound: " + e.getMessage());
				Toast.makeText(getActivity(), "Please install Google Maps and try again!", Toast.LENGTH_LONG).show();
			}

			break;
		case R.id.button_next:
			parentActivity.onFragmentReplaceRequest(new AccidentCritical2Fragment(), true);
			break;
		}
	}
}
