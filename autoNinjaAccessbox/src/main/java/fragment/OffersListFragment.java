package fragment;

import entity.CallForAction;
import entity.Offers;
import googleAnalytics.GATrackerMaps;
import utils.MyIO;

import java.util.ArrayList;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adapter.OffersAdapter;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.drawable;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

public class OffersListFragment extends MyFragment {
	
	public static final String OFFERS_DATA_FILENAME = "offers_data.txt";
	
	TextView tvTitle;
	TextView tvExtra;
	Offers offersData[];
	ListView offersListView;
	TextView offersFetchingTxt;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		
		View v = inflater.inflate(R.layout.frag_mylist, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		offersListView = (ListView) v.findViewById(R.id.list);
		offersFetchingTxt = (TextView) v.findViewById(R.id.textListEmptyNote);
		
		offersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				parentActivity.onFragmentReplaceRequest(OfferDetailFragment.newInstance(offersData[arg2]), true);
			}
		});
		
		offersListView.setSelector(R.drawable.hotline_caller_press);
		
		tvTitle.setText("Offers");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		//Log.d("OffersListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_OFFERS);
		cleverTap.event.push("Opened Offers List Screen");
		offersData = getOffersData(getActivity());
		
		if (offersData.length > 0) {
			offersListView.setVisibility(View.VISIBLE);
			offersFetchingTxt.setVisibility(View.GONE);
			OffersAdapter mListAdapter = new OffersAdapter(getActivity(), R.layout.row_offers, offersData);
			offersListView.setAdapter(mListAdapter);
			
			tvExtra.setText("" + offersData.length);
			String actionBarTitle = "Offers";
			actionBarTitle += " (" + offersData.length + ")"; 
			toolbarListener.setText(actionBarTitle);

			NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
			nm.cancel("NewOffers", 1);
		}
		else {
			tvExtra.setText(""); 
			toolbarListener.setText("Offers");

			offersListView.setVisibility(View.GONE);
			offersFetchingTxt.setText("Fetching Offers...");
			offersFetchingTxt.setVisibility(View.VISIBLE);
		}
	}
	
	public static Offers[] getOffersData(Context ctxt) {
		Offers returnOffers[] = new Offers[0];
		
		String readString = MyIO.fileToString(ctxt, OFFERS_DATA_FILENAME);
		
		if (!readString.trim().equalsIgnoreCase("")) {
			try {
				JSONObject jso = new JSONObject(readString);
	
				//count = Integer.parseInt(new String(inputBuffer, 0, x));
				JSONArray jsoArr = jso.getJSONArray("offers_list");
	
				int i = 0, j = 0;
				JSONObject obj, obj2;
				JSONArray jArr2;
				ArrayList<CallForAction> cfaList;
				returnOffers = new Offers[jsoArr.length()];
				for (i = 0; i < jsoArr.length(); i ++) {
					obj = jsoArr.getJSONObject(i);
					
					cfaList = new ArrayList<CallForAction>();
					jArr2 = obj.getJSONArray("cfa");
					for (j = 0; j < jArr2.length(); j ++) {
						obj2 = jArr2.getJSONObject(j);
						cfaList.add(new CallForAction(obj2.getInt("type"), obj2.getString("title"), obj2.getString("details")));
					}
	
					returnOffers[i] = new Offers(obj.getInt("id"), 
							obj.getString("title"), 
							obj.getString("body"), 
							obj.getString("teaser"),
							obj.getString("type"),
							obj.getString("ribbon"),
							obj.getString("img"),
							cfaList);
				}
	
				//Toast.makeText(this, "Old: " + count, Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				//Log.e("TooltipsList", "JSONException: " + e.toString());
			}
		}
		
		return returnOffers;
	}

}
