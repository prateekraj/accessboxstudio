package fragment;

import java.net.URLEncoder;

import myJsonData.MyJSONData;

import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Utils;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.myaccessbox.appcore.R;

import config.StaticConfig;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoginMobileFragment extends LoginBaseFragment implements View.OnClickListener {

	InputMethodManager imm;
	View view;

	MyJSONData loginData;

	Boolean goFlag = false;
	//Boolean fileExists = false;
	String mobileOnFile = "";
	EditText txtField;
	EditText editReferralCode;

	String urlData;

	ProgressDialog progressDialog;
	Toolbar toolbar;

	private static final String TAG = LoginMobileFragment.class.getSimpleName();

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.fragment_login, container, false);
		//toolbar = (Toolbar)view.findViewById(R.layout.toolbar_login);
		view.findViewById(R.id.burgerButton).setVisibility(View.INVISIBLE);
		cleverTap.event.push("Login Screen Opened");
		TextView titleText = (TextView) view.findViewById(R.id.title_text);
		titleText.setText("Enter your mobile number");

		TextView bckText = (TextView) view.findViewById(R.id.login_previous);
		bckText.setVisibility(View.GONE);

		TextView lgnText = (TextView) view.findViewById(R.id.login_submit);

		txtField = (EditText) view.findViewById(R.id.login_mobile_no);
		txtField.setHint("Enter mobile number");
		txtField.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
		//txtField.setInputType(InputType.TYPE_CLASS_PHONE);

		if (StaticConfig.FEATURE_REFERRAL_CODE) {
			editReferralCode = (EditText) view.findViewById(R.id.login_referral_code);
			editReferralCode.setVisibility(View.VISIBLE);
			editReferralCode.setHint("Referral Code (optional)");
		}
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

		lgnText.setOnClickListener(this);
		return view;
	}
	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.login_submit) {
			if (Utils.isNetworkConnected(getActivity())) {

				// hide keypad
				imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);

				String mobileNoInput = ((EditText) (view.findViewById(R.id.login_mobile_no))).getText().toString().trim();

				if (mobileNoInput.matches("^[0-9]+")) {
					if (mobileNoInput.length() == 10) {

						goFlag = false;
						String refCodeInput = "";
						if (StaticConfig.FEATURE_REFERRAL_CODE) {
							refCodeInput = editReferralCode.getText().toString().trim();
						}
							if (!mobileNoInput.equals(mobileOnFile)) {
								StaticConfig.FIELD_LOGIN_OTP = "";
								StaticConfig.FIELD_LOGIN_NUMBER = mobileNoInput;
								StaticConfig.FIELD_REF_CODE = refCodeInput;
								goFlag = true;
							}

							else {
								goFlag = true;
							}


						if (goFlag) {
							StaticConfig.OTP_PROGRESS_BAR_RUNNING = true;
							progressDialog = ProgressDialog.show(getActivity(), "Requesting password", "Please wait for a moment");

							try {
								urlData = StaticConfig.API_REQUEST_REGISTER_FOR_OTP;
								urlData += "?phone=" + URLEncoder.encode(mobileNoInput, "UTF-8");

								AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
									public void onPostExecute(String result) {
										JSONObject returnMsg;
										try {
											returnMsg = new JSONObject(result);
											if (returnMsg.has("success")) {
												/*Toast.makeText(getSherlockActivity(), "Please check your SMS for the generated one time password",
														Toast.LENGTH_LONG).show();*/
												loginListener.onFragmentReplace((Fragment) new LoginPasswordFragment(), true);
											} else {
												Toast.makeText(getActivity(), returnMsg.getString("error"), Toast.LENGTH_LONG).show();
											}
										} catch (JSONException e) {
											// Log.e(TAG, "Json exception: " +
											// e.getMessage());
										}
										progressDialog.dismiss();
									}
								}, AsyncInvokeURLTask.REQUEST_TYPE_GET);

								task.execute(urlData);
							} catch (Exception e) {
								// Log.e(TAG, "Default exectpion: " +
								// e.getMessage());

							}

						} else
							Toast.makeText(getActivity(), "Problem saving mobile no. please try again", Toast.LENGTH_LONG).show();
					} else

						Toast.makeText(getActivity(), "Please enter a valid 10-digit mobile number!", Toast.LENGTH_LONG).show();

				} else {
					Toast.makeText(getActivity(), "Please enter numbers only", Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
			}
		}

	}

	@Override
	public void onResume() {
		super.onResume();

		/*if (MyJSONData.dataFileExists(getActivity(), MyJSONData.TYPE_LOGIN)) {

			fileExists = true;*/
			//loginData = new MyJSONData(getActivity(), MyJSONData.TYPE_LOGIN);

			/*mobileOnFile = loginData.fetchData(MyJSONData.FIELD_LOGIN_NUMBER);*/
			mobileOnFile = StaticConfig.FIELD_LOGIN_NUMBER;
			EditText mobileInputText = (EditText) view.findViewById(R.id.login_mobile_no);
			mobileInputText.setText(mobileOnFile);
			StaticConfig.FIELD_LOGIN_OTP = "";

		//}

	}

}
