package fragment;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.StaticConfig;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutDealerFragment extends MyFragment {
	
	TextView tvAddshowroom, tvAddworkshop;
	TextView tvTelephoneshowroom, tvTelephoneworkshop;
	TextView tvFaxshowroom, tvFaxworkshop;
	TextView tvEmailshowroom, tvEmailworkshop;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_about_dealer, container, false);
		tvAddshowroom = (TextView) v.findViewById(R.id.about_dealer_address_showroom);
		tvAddworkshop = (TextView) v.findViewById(R.id.about_dealer_address_workshop);
		tvTelephoneshowroom = (TextView) v.findViewById(R.id.about_dealer_telephone_showroom);
		tvTelephoneworkshop = (TextView) v.findViewById(R.id.about_dealer_telephone_workshop);
		tvFaxshowroom = (TextView) v.findViewById(R.id.about_dealer_fax_showroom);
		tvFaxworkshop= (TextView) v.findViewById(R.id.about_dealer_fax_workshop);
		tvEmailshowroom = (TextView) v.findViewById(R.id.about_dealer_email_showroom);
		tvEmailworkshop = (TextView) v.findViewById(R.id.about_dealer_email_workshop);
		
		tvAddshowroom.setText(StaticConfig.ADDRESS_SHOWROOM);
		tvAddworkshop.setText(StaticConfig.ADDRESS_WORSHOP);
		tvTelephoneshowroom.setText(StaticConfig.TELEPHONE_SHOWROOM);
		tvTelephoneworkshop.setText(StaticConfig.TELEPHONE_WORKSHOP);
		tvFaxshowroom.setText(StaticConfig.FAX_SHOWROOM);
		tvFaxworkshop.setText(StaticConfig.FAX_WORKSHOP);
		tvEmailshowroom.setText(StaticConfig.EMAIL_SHOWROOM);
		tvEmailworkshop.setText(StaticConfig.EMAIL_WORKSHOP);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		toolbarListener.setText("Contact Us");
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

}
