package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import myJsonData.MyJSONData;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myaccessbox.appcore.R;

import config.ConfigInfo;
import config.StaticConfig;
import config.ConfigInfo.Hotline;
import config.ConfigInfo.Hotline.GroupEntity;

import db.DatabaseHelper;
import db.HotlinesDataSource;
import db.TableContract.HotlinesDB;
import db.TableContract.UserDB;

public class HotlinesFragment extends MyHotlineMultiSelectFragment implements View.OnClickListener {

	TextView tvTitle;
	TextView tvExtra;
	HotlinesDataSource hotlineDatasource;
	View v;
	Context context;

	ProgressDialog progressDialog;


	List<RelativeLayout> hotlineButtons = new ArrayList<RelativeLayout>();
	List<Integer> _hotlineButtonIds = new ArrayList<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(R.id.hotline_1);
			add(R.id.hotline_2);
			add(R.id.hotline_3);
			add(R.id.hotline_4);
			add(R.id.hotline_5);
			add(R.id.hotline_6);
			add(R.id.hotline_7);
			add(R.id.hotline_8);
			add(R.id.hotline_9);
		}
	};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		tracker.send(GATrackerMaps.VIEW_HOTLINES);

		v = inflater.inflate(R.layout.frag_hotlines, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);

		tvExtra.setVisibility(View.GONE);
		tvTitle.setText("Hotlines");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hotlines, 0, 0, 0);
		context = this.getActivity();
		if(Utils.isNetworkConnected(context)){
		progressDialog = ProgressDialog.show(getActivity(), "Fetching Hotlines", "Please wait for a moment");
		fetchHotlinesFromServer(context);
		}else{
		 update9Hotlines(context);
		 setView();
		 updateOtherHotlines(context);
		}
		
		
		return v;
	}

	private void setView() {

		int ctr = 0;
		for (int x : _hotlineButtonIds) {
			RelativeLayout tRelLay = (RelativeLayout) v.findViewById(x);

			if (ctr < StaticConfig.getHotlinesCount()) {
				hotlineButtons.add(tRelLay); // add only if it is on display

				TextView lab = (TextView) tRelLay.findViewById(R.id.hotline_label);
				ImageView icon = (ImageView) tRelLay.findViewById(R.id.hotline_icon);
				Hotline hInfo = StaticConfig.getHotlineAtPosition(ctr);

				lab.setText(hInfo.getLabel());
				icon.setImageDrawable(getResources().getDrawable(hInfo.getIconDrawableId()));

				tRelLay.setVisibility(View.VISIBLE);
			} else {
				tRelLay.setVisibility(View.INVISIBLE);
			}
			ctr++;
		}
		if(ctr ==0){
			Toast.makeText(this.getActivity(), "Sorry! No hotlines for this dealer", Toast.LENGTH_SHORT).show();
		}
		for (RelativeLayout relLay : hotlineButtons) {
			relLay.setOnClickListener(this);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		// first thing to do is to call super!
		super.onAttach(activity);
		toolbarListener.setText("Hotlines");
	}

	// @Override
	public void onResume() {
		super.onResume();
		cleverTap.event.push("Opened  Hotlines Screen");
		this.getView().refreshDrawableState();
	}

	@Override
	public void onClick(final View v) {
		Hotline htLine = getHotlineFromViewId(v.getId());
		Intent i = new Intent(Intent.ACTION_CALL);
		DialogFragment pickerFragDialog;
		HotlinesDataSource hotlineDS = new HotlinesDataSource(getActivity());
		ContentValues cv = new ContentValues();
		System.out.println("CLICKED");
		if (htLine != null) {
			tracker.send(GATrackerMaps.getHotlineCallEvent(htLine));
			ArrayList<GroupEntity> numberList = htLine.getNumbers();
			if (numberList != null && numberList.size() > 0) {
				if (numberList.size() == 1) {
					v.setEnabled(false);
					int log_count=hotlineDS.getStoredflag(numberList.get(0).getLabel());
					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							v.setEnabled(true);	
						}
					}, 1000);
					if(Utils.isNetworkConnected(getActivity())){
						sendToServer(0, numberList, this.getActivity());
					}else{
						log_count++;
						cv.put(HotlinesDB.COL_LOG_COUNT,log_count );
						hotlineDS.updateHotlinesHelper(cv,numberList.get(0).getLabel());
					}
					i.setData(Uri.parse("tel:" + numberList.get(0).getNumber()));
					startActivity(i);
				} else {
					if (!HotlinePickerDialogFrag.isPickerDialogOpen()) {
						pickerFragDialog = HotlinePickerDialogFrag.newInstance(htLine.getLabel(), numberList);
						pickerFragDialog.setTargetFragment(this, REQUEST_CODE_HOTLINE_SUB_PICKER);
						pickerFragDialog.show(getActivity().getSupportFragmentManager(), "hotlinePicker");
					}
				}
			} else {
				// number not found
			}
		}
	}

	private Hotline getHotlineFromViewId(int id) {
		return StaticConfig.getHotlineAtPosition(_hotlineButtonIds.indexOf(id));
	}

	/**
	 * Method to fetch hotlines from server and store in sqlite database and
	 * reset the hotlines arraylist in static config class
	 */

	public void fetchHotlinesFromServer(final Context cxt) {
		String executeURL = StaticConfig.API_FETCH_HOTLINES ;
				executeURL += "?" + StaticConfig.getCMSPass();
				hotlineDatasource = new HotlinesDataSource(cxt);
		try {
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {

				public void onPostExecute(String result) {
					JSONObject jsonObj, jSonText = null;
					JSONArray JsonHotlinesArray = null;
					ContentValues cv, cvHelper;
					int tempId = 0;
					String tempDeptName = "";
					// String messageFromServer = "";
					int new_change_set = 0;

					if (null != result) {
						try {
							// getting the json object from the response
							// string
							jsonObj = new JSONObject(result);
							if (jsonObj.has("error") && jsonObj.getString("error").equalsIgnoreCase("false")) {
								jSonText = jsonObj.getJSONObject("text");
								new_change_set = Integer.parseInt(jSonText.optString("change_set"));
								JsonHotlinesArray = jSonText.getJSONArray("hotlines");
							} else {
								jSonText = jsonObj.getJSONObject("text");
							}
							// JSONObject check =
							// jsonObj.getJSONObject("change_set");
							if (jsonObj.has("error")//|| StaticConfig.masterHotlineSelector.size() == 0)
									&& jsonObj.getString("error").equalsIgnoreCase("false")
									&& (hotlineDatasource.getStoredChangeSet() < new_change_set) ){

								hotlineDatasource.deleteFromHotlines();
								hotlineDatasource.deleteHelper();
								// looping through All Contacts
								for (int i = 0; i < JsonHotlinesArray.length(); i++) {
									JSONObject c = JsonHotlinesArray.getJSONObject(i);

									//cv = new ContentValues();
									cvHelper = new ContentValues();

									// flag
									int flag = Integer.parseInt(c.optString("flag"));
									  cvHelper.put(HotlinesDB.COL_HELPER_HOTLINE_FLAG, Integer.parseInt(c.optString("flag")));

									// id
									tempId = Integer.parseInt(c.optString("id"));// id
									cvHelper.put(HotlinesDB.COL_HELPER_HOTLINE_ID, Integer.parseInt(c.optString("id")));// id

									// department name
									tempDeptName = c.optString("department_name").toString();
									cvHelper.put(HotlinesDB.COL_HELPER_HOTLINE_DEPARTMENT_NAME, c.optString("department_name").toString());
									
									// inserting into helper
									long inserIntoHelper = hotlineDatasource.insertIntoHelperHotline(cvHelper);
									//System.out.println("INSERTED HELPER HOTLINES: id: " + tempId+", tempDeptname:"+tempDeptName + ", flag:"+ flag);
									
									//looping through each dept numbers
									JSONArray numbersArray = c.getJSONArray("numbers");
									for (int j = 0; j < numbersArray.length(); j++) {
										try{
										cv = new ContentValues();
										JSONObject numberJson = numbersArray.getJSONObject(j);
										cv.put(HotlinesDB.COL_HOTLINE_ID, tempId);
										cv.put(HotlinesDB.COL_HOTLINE_DEPARTMENT_NAME, tempDeptName);
										cv.put(HotlinesDB.COL_NAME, numberJson.optString("name").toString().trim());
										cv.put(HotlinesDB.COL_NUMBER, numberJson.optString("number").toString().trim());// number
										cv.put(HotlinesDB.COL_CHANGE_SET, new_change_set);
										long r = hotlineDatasource.insertIntoHotlines(cv);
										}catch(SQLException e){
											
										}
										//System.out.println("INSERTED HOTLINES: id: " + tempId+", tempDeptname:"+tempDeptName + ", name:"+ numberJson.optString("name").toString().trim() +", number: "+ numberJson.optString("number").toString().trim());
									}
								}
							}else{
							}
							update9Hotlines(cxt);
							if(cxt == context ){
							setView();
							progressDialog.dismiss();
							}
							updateOtherHotlines(cxt);
							StaticConfig.HOTLINE_FETCHED = true;
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}


			}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
			task.execute(executeURL);
		} catch (Exception e1) {
			// Log.e(TAG, "Exception: " + e1.toString());
		}
	}
	/**
	 * getting the flagged deparments in the 9 grid hotline list
	 * @param cxt
	 */
	public void update9Hotlines(Context cxt) {
		hotlineDatasource = new HotlinesDataSource(cxt);
		ArrayList<Integer> flaggedDeptIds = hotlineDatasource.getFlaggedDepartmentIds(1);
		Iterator itr = flaggedDeptIds.iterator();
		StaticConfig.masterHotlineSelector.clear();
		while(itr.hasNext()){
			int deptId = (Integer) itr.next();
			Cursor c = hotlineDatasource.getHotlineFromId(deptId);
			hotlineDatasource.setNumbersFromCursor(c,1);
			
		}
		hotlineDatasource.close();
		StaticConfig.masterHotlineSelectorSize = StaticConfig.masterHotlineSelector.size();
		StaticConfig._hotlinesButtonInfoList.clear();
		for (int i = 0; i < StaticConfig.masterHotlineSelectorSize; i++) {
			addToHotlineFinalList(i);
		}
		
		// TODO Auto-generated method stub
		
	}
	/**
	 * getting departments which will not be there in the 9 grid hotline list
	 * @param cxt
	 */
	public void updateOtherHotlines(Context cxt) {
		hotlineDatasource = new HotlinesDataSource(cxt);
		ArrayList<Integer> flaggedDeptIds = hotlineDatasource.getFlaggedDepartmentIds(0);
		Iterator itr = flaggedDeptIds.iterator();
		while(itr.hasNext()){
			int deptId = (Integer) itr.next();
			Cursor c = hotlineDatasource.getHotlineFromId(deptId);
			hotlineDatasource.setNumbersFromCursor(c,0);
			
		}
		hotlineDatasource.close();
		// TODO Auto-generated method stub
	}


	/**
	 * setting hotlines which are not in the hotlinelist
	 * 
	 * @param tempHotline
	 */
	public static void setOtherHotlines(ArrayList<GroupEntity> tempHotline) {

		switch (tempHotline.get(0).getId()) {
			case 3 :
				StaticConfig.SERVICE_HOTLINE_SELECTOR.clear();
				StaticConfig.SERVICE_HOTLINE_SELECTOR.addAll(tempHotline);
				//System.out.println("SERVICE SELECTOR" + StaticConfig.SERVICE_HOTLINE_SELECTOR.size());
				break;
			case 6 :
				StaticConfig.INSURANCE_HOTLINE_SELECTOR.clear();
				StaticConfig.INSURANCE_HOTLINE_SELECTOR.addAll(tempHotline);
				break;
			case 9 :
				StaticConfig.PUC_HOTLINE_SELECTOR.clear();
				StaticConfig.PUC_HOTLINE_SELECTOR.addAll(tempHotline);
				break;
			case 11 :
				StaticConfig.ONROAD_SERVICE_SELECTOR.clear();
				StaticConfig.ONROAD_SERVICE_SELECTOR.addAll(tempHotline);
				break;
			case 12 :
				StaticConfig.TOWING_SELECTOR.clear();
				StaticConfig.TOWING_SELECTOR.addAll(tempHotline);
				break;
			default :
				break;
		}
	}

	/**
	 * building the final list of hotlines with the corresponding icons
	 * 
	 * @param index
	 */
	public static void addToHotlineFinalList(int index) {
		StaticConfig._hotlinesButtonInfoList.add(new ConfigInfo.Hotline(StaticConfig.masterHotlineSelector.get(index).get(0).getLabel(),
				StaticConfig.masterHotlineSelector.get(index).get(0).getName(), StaticConfig._hotlinesIconsList
						.get(StaticConfig.masterHotlineSelector.get(index).get(0).getId() - 1), StaticConfig.masterHotlineSelector.get(index).get(0)
						.getId(), StaticConfig.masterHotlineSelector.get(index)));
		//System.out.println("FINAL TO SHOW LIST SIZE:"+ StaticConfig._hotlinesButtonInfoList.size());
	}

	public void sendToServer(int which, ArrayList<GroupEntity> numberList, final Context cxt) {
		System.out.println("SEND TO SERVER CALLED");
		 SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
		 String strDate  = "";
		 Calendar c = Calendar.getInstance();
		 c.setTime(new Date());
		 Random r = new Random();
		 int random = r.nextInt(100);
		 c.add(Calendar.SECOND, random);
		 strDate = sdfDate.format(c.getTime());
		 System.out.println("CURRENT TIME RANDOM NUMBER"+random);
		 System.out.println("CURRENT TIME"+strDate);
		 
		final String hotline_type = numberList.get(which).getLabel();
		try {
			MultipartEntity mpEntity = new MultipartEntity();

			try {
				mpEntity.addPart("mobile", new StringBody(""+StaticConfig.LOGGED_PHONE_NUMBER));
				mpEntity.addPart("hotline_type", new StringBody(hotline_type.trim()));
				mpEntity.addPart("os", new StringBody("android"));
				mpEntity.addPart("model", new StringBody(StaticConfig.myCar.getModelName().trim()));
				mpEntity.addPart("owner", new StringBody(StaticConfig.myCar.getOwner().trim()));
				mpEntity.addPart("reg_num", new StringBody(StaticConfig.myCar.getReg_num().trim()));
				mpEntity.addPart("generated_timestamp", new StringBody(strDate));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
			}
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(
					new AsyncInvokeURLTask.OnPostExecuteListener() {

						@Override
						public void onPostExecute(String result) {
							System.out.println("LOG HOTLINE:"+result);
							JSONObject jsonObj;
							try {
								jsonObj = new JSONObject(result);
								if (jsonObj.has("error") && jsonObj.getString("error").equalsIgnoreCase("false")) {
									updateHelperDB(hotline_type.trim(),cxt);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
			String executeURL = StaticConfig.API_SEND_HOTLINE_DETAIL + "?" + StaticConfig.getCMSPass();
			task.execute(executeURL);
		} catch (Exception e1) {
		}
	}
	private void updateHelperDB(String hotline_type,Context cxt) {
		HotlinesDataSource hotlineDS = new HotlinesDataSource(cxt);
		ContentValues cv = new ContentValues();
		int log_count = hotlineDS.getStoredflag(hotline_type.trim());
		if(log_count>0){
			log_count--;
			cv.put(HotlinesDB.COL_LOG_COUNT, log_count);
			hotlineDS.updateHotlinesHelper(cv, hotline_type.trim());
		}
		
	}
}