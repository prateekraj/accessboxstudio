package fragment;

import entity.Tooltips;
import googleAnalytics.GATrackerMaps;
import utils.MyIO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adapter.TooltipsAdapter;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.myaccessbox.appcore.R;

public class TooltipsListFragment extends MyFragment {
	
	public static final String INTENT_EXTRA_KEY_TIP_INDEX = "open_detail_tip_index";
	public static final String TOOLTIPS_DATA_FILENAME = "tooltips_data.txt";
	
	Tooltips tipsData[];
	ListView tooltipsListView;
	TextView tvTitle;
	TextView tvExtra;
	TextView tvListEmptyNote;

	private static Tooltips[] DEFAULT_TIPS = new Tooltips[] {
		new Tooltips("Car engine, your car's heart.",
				"Checking your car engine's vital signs periodically is really very important. Never ignore it!", 
				"Keep an eye on that engine light!"),
		new Tooltips("Be aware of your car's running-in period.",
				"Your car is your baby, and similarly you need to teach it to walk before you can expect it to run.\n\nDo not exceed 55-60 KMPH during your car's running-in period (first 1000 Kms).",
				"Learn to walk before you run!"),
		new Tooltips("Battery usage and maintenance tip.", 
				"To avoid an electrical shock or mishap, always remember to disconnect the negative/ground terminal FIRST while disconnecting the battery.",
				"Read more for a no-shock recipe!"),
		new Tooltips("Detecting an electrical bug!", 
				"When one of the blinkers/indicators flashes quicker than the other, it means one of the bulbs/fuses has blown. Get it fixed as soon as possible.",
				"Ever noticed one of the blinkers flashing faster than usual?"),
		new Tooltips("Release the Handbrake before you go!", 
				"Never drive your car with the Handbrake half-engaged. It damages both your engine as well as the brake-pads.", 
				"Notice the exclamation mark Handbrake symbol on your dashboard."),
		new Tooltips("Slower air-flow is an indicator of blockage.", 
				"If the air-flow seems slower than usual, check the AC Unit's air-filter or take your car to the nearest service station.", 
				"Keep yourself and your car cool at all times!"),
		new Tooltips("Trivia from the world of cars.", 
				"It is a good thing most of us were not born before 1916, because that is when rear-view-mirrors became standard car equipment!", 
				"Would you be able to drive without a rear-view mirror? Think again...!"),
		new Tooltips("Tyre health management tip #27", 
				"Rotate the car's tyres, including the spare one, after every 7,500 Kms of running.", 
				"Ensure an even wear-n-tear on your cars for a long run!"),
		new Tooltips("If it says 'Regular', use 'Regular'.", 
				"It is a myth that higher Octane fuel gives greater mileage or power. If your manual says 'regular' use only regular unleaded fuel.", 
				"High octane fuel myth busted...")
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		
		//Log.d("TestFragment2", "OnCreateView Called!");
		
		tipsData = new Tooltips[0];
		
		View v = inflater.inflate(R.layout.frag_mylist, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		tooltipsListView = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		
		tooltipsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				launchDetailTipFragment(arg2);
			}
		});
		
		tooltipsListView.setVisibility(View.VISIBLE);
		tvListEmptyNote.setVisibility(View.GONE);

		tvTitle.setText("Tooltips");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tooltips, 0, 0, 0);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_TOOLTIPS);
		cleverTap.event.push("Opened Tooltips Listing Screen");
		
		tipsData = getTipsData(getActivity());
		
		TooltipsAdapter tipsAdptr = new TooltipsAdapter(getActivity(), R.layout.row_mylist, tipsData);
		tooltipsListView.setAdapter(tipsAdptr);
		
		tvExtra.setText(tipsData.length + " Tips");
		toolbarListener.setText("Handy Tooltips");
		
		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel("TooltipsUpdate", 1);
		
		int directOpenTipIndex = getActivity().getIntent().getIntExtra(INTENT_EXTRA_KEY_TIP_INDEX, -1);
		if (directOpenTipIndex != -1) {
			getActivity().getIntent().removeExtra(INTENT_EXTRA_KEY_TIP_INDEX);
			launchDetailTipFragment(directOpenTipIndex);
		}
		
		//setActionBarBackEnabled(false);
	}

	public static Tooltips[] getTipsData(Context ctxt) {
		Tooltips returnTips[] = new Tooltips[0];
		
		String readString = MyIO.fileToString(ctxt, TOOLTIPS_DATA_FILENAME);
		
		if (!readString.trim().equalsIgnoreCase("")) {

			try {
				JSONObject jso = new JSONObject(readString);

				//count = Integer.parseInt(new String(inputBuffer, 0, x));
				JSONArray jsoArr = jso.getJSONArray("tooltips_list");

				int i = 0;
				JSONObject obj;
				returnTips = new Tooltips[jsoArr.length()];
				for (i = 0; i < jsoArr.length(); i ++) {
					obj = jsoArr.getJSONObject(i);
					JSONArray imgNamesJso = obj.getJSONArray("imgs");
					String temp[] = new String[imgNamesJso.length()];
					for (int j = 0; j < imgNamesJso.length(); j ++) {
						temp[j] = imgNamesJso.getString(j);
					}
					returnTips[i] = new Tooltips(obj.getString("title"), 
							obj.getString("body"), 
							obj.getString("teaser"), temp);
				}
				return returnTips;
				//Toast.makeText(this, "Old: " + count, Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				//Log.e("TooltipsList", "JSONException: " + e.toString());
			}
		}
		
		return DEFAULT_TIPS;
	}
	
	private void launchDetailTipFragment(int tipIndex) {
		if (tipIndex >= 0 && tipIndex < tipsData.length) {
			parentActivity.onFragmentReplaceRequest(TooltipsDetailFragment.newInstance(tipsData[tipIndex]), true);
		}
	}
}
