package fragment;

import googleAnalytics.GATrackerMaps;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myaccessbox.appcore.R;

public class AccidentCritical3Fragment extends MyFragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_accident_crit3, container, false);
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity); //call super first before doing anything else!
		
		toolbarListener.setText("Critical Situation");
		//setActionBarBackEnabled(true);
		
		//Log.d("AccidentBaseFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		tracker.send(GATrackerMaps.VIEW_ACCIDENT_CRITICAL3);
	}
}
