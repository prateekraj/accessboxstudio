package fragment;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import activity.PaymentActivity;
import config.StaticConfig;
import myJsonData.MyJSONData;

/*import com.citrus.asynch.Binduser;
import com.citrus.mobile.Callback;
import com.citrus.mobile.Config;*/
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.Environment;
import com.citrus.sdk.logger.CitrusLogger;
import com.citruspay.sdkui.ui.utils.CitrusFlowManager;
import com.citruspay.sdkui.ui.utils.PPConfig;
import com.myaccessbox.appcore.R;

import db.UserDetailsDataSource;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class PaymentFragmentCitrus extends MyFragment {

	EditText edtEmail, edtAmount;
	TextView edtMobile, tvAmountInvoice;
	Button btnBind;
	MyJSONData myOwn;
	SharedPreferences userDetailsPreference;
	private static String USER_DETAILS = "user_details";
	public static final String EMAIL = "user_email";
	public static final String TAG = "Payment Activity";
	private String paymentId = "-1";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.frag_payment_citrus, container, false);
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		long mobile = userDetailDataSource.getUserOwnNumber();
		edtEmail = (EditText)v.findViewById(R.id.editTextemail);
		edtMobile = (TextView)v.findViewById(R.id.editTextmobile);
		btnBind = (Button)v.findViewById(R.id.buttonbinduser);
		edtAmount = (EditText)v.findViewById(R.id.editTextAmount);
		edtAmount.requestFocus();
		tvAmountInvoice = (TextView)v.findViewById(R.id.editTextAmountInvoice);
		userDetailsPreference = getActivity().getSharedPreferences(USER_DETAILS, MODE_PRIVATE);

		myOwn = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		edtMobile.setText("" + mobile);

		btnBind.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String eEmail = edtEmail.getText().toString().trim();
				String editedAmount = edtAmount.getText().toString().trim();
				if (!edtAmount.getText().toString().trim().equalsIgnoreCase("")) {
					if (isEmailValid(eEmail)) {

						SharedPreferences.Editor editor = userDetailsPreference.edit();
						editor.putString(EMAIL, eEmail);
						editor.commit();
						//parentActivity.onFragmentReplaceRequest(new PaymentOptionsFragment(), false);
						initListener();
						initQuickPayFlow(editedAmount, eEmail);
						getActivity().finish();
					} else {
						Toast.makeText(getActivity(), "Please enter correct email ID", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), "Please enter amount", Toast.LENGTH_SHORT).show();
				}

			}
		});

		return v;
	}

	private void initQuickPayFlow(String editedAmount, String eEmail) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		//String emailId = myOwn.fetchData(MyJSONData.FIELD_OWN_EMAIL_ID);
		String mobile = userDetailDataSource.getUserOwnNumber()+"";
		//String amount = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT);
		CitrusFlowManager.startShoppingFlowStyle(getActivity(),
				eEmail, mobile,
				editedAmount, R.style.AppTheme_Citrus, false);
	}

	private void initListener() {
		PPConfig.getInstance().disableSavedCards(false);
		PPConfig.getInstance().disableNetBanking(false);
		PPConfig.getInstance().disableWallet(true);
		selectProductionEnv();
	}

	private void selectProductionEnv() {

		//To Set the Log Level of Core SDK & Plug & Play
		PPConfig.getInstance().setLogLevel(getActivity(), CitrusLogger.LogLevel.DEBUG);

		CitrusFlowManager.initCitrusConfig("dl9kdnqnut-signup",
				"3dbd0b468da7a1e222e8bdc43ad74fb8", "dl9kdnqnut-signin",
				"4008a122f48b4bd47b369b9fba1ec330", getResources().getColor(R.color.citrus_white),
				getActivity(), Environment.PRODUCTION, "autoninja", "https://dashboard.accessbox.in/pratham/apis/v2/billgeneratorcitrusplugandplay" + "?MerchantName="+ StaticConfig.DEALER_CUSTOM_PARAMETER_FOR_CITRUS+"&PaymentId="+paymentId,
				"https://dashboard.accessbox.in/pratham/apis/v2/redirecturlcitrus");

		//To Set the User details
		CitrusUser.Address customAddress = new CitrusUser.Address("80 ft road", "koramangala", "Bangalore", "Karnataka", "India", "560035");
		PPConfig.getInstance().setUserDetails("Accessbox", "Autoninja", customAddress);
	}


	@Override
	public void onResume() {
		super.onResume();

		toolbarListener.setText("Register");
		myOwn = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		if (!userDetailsPreference.getString(EMAIL, "").equalsIgnoreCase("")) {
			edtEmail.setText(userDetailsPreference.getString(EMAIL, ""));
			// parentActivity.onFragmentReplaceRequest(new
			// PaymentOptionsFragment(), false);
		}
		if (MessageDetailFragment.payAmountCheck) {
			MessageDetailFragment.payAmountCheck = false;
			tvAmountInvoice.setVisibility(View.VISIBLE);
			edtAmount.setVisibility(View.GONE);
			tvAmountInvoice.setText(myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT));
			edtAmount.setText(myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT));
			paymentId = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_ID);
		} else {
			paymentId = "-1";
			tvAmountInvoice.setVisibility(View.GONE);
			edtAmount.setVisibility(View.VISIBLE);
			if (MyJSONData.dataFileExists(getActivity(), MyJSONData.TYPE_OWN)) {

				MyJSONData.editMyData(getActivity(),

						new String[]{MyJSONData.FIELD_OWN_PAYMENT_ID},

						new String[]{null}, MyJSONData.TYPE_OWN);

			}

		}
	}

	/**
	 * method is used for checking valid email id format.
	 * 
	 * @param email
	 * @return boolean true for valid false for invalid
	 */
	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	private void showToast(String message, String error) {
		if (!TextUtils.isEmpty(message))
			Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

		if (!TextUtils.isEmpty(error))
			Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
	}
}
