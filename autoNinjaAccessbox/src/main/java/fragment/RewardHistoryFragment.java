package fragment;

import db.UserDetailsDataSource;
import entity.RewardHistoryDataPoint;
import googleAnalytics.GATrackerMaps;

import myJsonData.MyJSONData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import service.BackgroundService;
import utils.MyIO;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.StaticConfig;
import adapter.RewardHistoryListAdapter;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class RewardHistoryFragment extends MyFragment {
	
	public static final String REWARDS_DATA_FILE = "rewards_data.txt";
	private static final String REWARDS_JSON_ARRAY_KEY_NAME = "rewards_history";
	
	
	ListView historyListView;
	TextView tvListEmptyNote;
	TextView tvTotal;
	ImageView ivImageView;
	
	LinearLayout llLayout;
	
	RewardHistoryDataPoint rewardData[];
	

	private static final int ROLE_ID = -15;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tracker.send(GATrackerMaps.VIEW_REWARD);
		
		View v = inflater.inflate(R.layout.frag_reward_history, container, false);
		rewardData  = new RewardHistoryDataPoint[0];
		historyListView = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		tvTotal = (TextView) v.findViewById(R.id.reward_total);
		ivImageView = (ImageView) v.findViewById(R.id.frag_list_separator);
		llLayout = (LinearLayout) v.findViewById(R.id.reward_total_container);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}
	
	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiver);
		
		BackgroundService.setRewardHistoryRoleObserver(false);
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		LocalBroadcastManager.getInstance(getActivity())
		        .registerReceiver(broadcastReceiver, 
					new IntentFilter(BackgroundService.INTENT_FILTER_REWARD_HISTORY_UPDATE_ACTION));

       BackgroundService.setRewardHistoryRoleObserver(true);

       Intent i = new Intent(getActivity(), BackgroundService.class);
       i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_EXPRESS);
       getActivity().startService(i);
		
		refreshRewardListView();

}
	
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(getActivity(), "Broadcastrecceiver Called", 0).show();
			
			refreshRewardListView();
		}
	};
	
		private void refreshRewardListView() {
			
			toolbarListener.setText("Reward History");
	       UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
			
			if (userDetailDataSource.isPhoneNumberExist()) {
				
				//MyJSONData myOwn = new MyJSONData(getActivity(),MyJSONData.TYPE_OWN);
				
			/*	JSONObject totalRewardJsonObject = myOwn.fetchJSONObject(MyJSONData.FIELD_OWN_REWARD_POINTS_TOTAL);*/
							
				rewardData = getRewardHistoryData(getActivity());
				
				if (rewardData.length > 0) {
					historyListView.setVisibility(View.VISIBLE);
					tvListEmptyNote.setVisibility(View.GONE);
					ivImageView.setVisibility(View.VISIBLE);
					llLayout.setVisibility(View.VISIBLE);
					//tvTotal.setText(myOwn.fetchData(MyJSONData.FIELD_OWN_REWARD_POINTS_TOTAL));
					tvTotal.setText(userDetailDataSource.getTotalRewards());
					RewardHistoryListAdapter rListAdapter = new RewardHistoryListAdapter(getActivity(), R.layout.row_reward_history, rewardData);
					historyListView.setAdapter(rListAdapter);
				} 
				else {
					nothingToDisplay();
				}
		}
			else {
				nothingToDisplay();
			}
		}
	
	private void nothingToDisplay() {
		historyListView.setVisibility(View.GONE);
		tvListEmptyNote.setVisibility(View.VISIBLE);
		ivImageView.setVisibility(View.GONE);
		llLayout.setVisibility(View.GONE);
		tvListEmptyNote.setText("No reward history data to display!");
	}

	private RewardHistoryDataPoint[] getRewardHistoryData(Context ctxt) {
		RewardHistoryDataPoint returnData[] = new RewardHistoryDataPoint[0];
				
		JSONArray histArray = MyIO.readJSONArrayFromFile(getActivity(), REWARDS_DATA_FILE, REWARDS_JSON_ARRAY_KEY_NAME);
		
		if (histArray == null) {//history empty or unavailable
			return returnData;
		}
		
		JSONObject histObj;
		
		try {
			returnData = new RewardHistoryDataPoint[histArray.length()];
			for (int i = 0; i < histArray.length(); i ++) {
				histObj = histArray.getJSONObject(i);
				
				returnData[i] = new RewardHistoryDataPoint(histObj.getString("points"), histObj.getString("type") , 
						histObj.getString("scheme_offered_on"), histObj.getString("remarks"));
				
			}
		} catch (JSONException e) {
			//will eventually return empty array
		}
		return returnData;
	}
	
	public static boolean appendRewardsToDisk(Context ctxt, JSONArray newRewardsArray) {
		JSONArray rewardsOnDisk = MyIO.readJSONArrayFromFile(ctxt, REWARDS_DATA_FILE, REWARDS_JSON_ARRAY_KEY_NAME);
		JSONArray resultArray = new JSONArray();

		try {
			for (int i = 0; i < newRewardsArray.length(); i ++) {
				resultArray.put(newRewardsArray.get(i));
					
			}
			for (int i = 0; i < rewardsOnDisk.length(); i ++) {
				resultArray.put(rewardsOnDisk.get(i));
					
			}
		} catch (JSONException e) {
			return false;
		}
		
		return MyIO.writeJSONArrayToFile(ctxt, REWARDS_DATA_FILE, resultArray, REWARDS_JSON_ARRAY_KEY_NAME);
	}
	
  // Class for Sorting List 	
}