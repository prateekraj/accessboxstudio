package fragment;

import service.BackgroundService;
import myJsonData.MessagesData;
import entity.Messages;
import googleAnalytics.GATrackerMaps;
import adapter.MessageAdapter;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import com.myaccessbox.appcore.R;

public class MessageListFragment extends MyFragment {
	
	TextView tvTitle;
	TextView tvExtra;
	ListView listMessages;
	TextView tvListEmptyNote;
	
	Messages msgData[];
	/*
	Messages msgData[] = new Messages[] {
			new Messages(1, "Subject1", "Your next service is due in the next month. Please contact our helpline to set up an appointment.", "25 Nov 2012"),
			new Messages(2, "Subject2", "Attractive discounts on all accessories for Maruti Swift and Dezire!", "25 Oct 2012", true, new CallForAction(CallForAction.TYPE_CALL, "Call Me", "9341459665")),
			new Messages(3, "Subject3", "Refer a friend and get 10% off on your next paid service.", "25 Aug 2012", true, new CallForAction(CallForAction.TYPE_WEB, "Open Me", "http://www.myaccessbox.com")),
			new Messages(4, "Subject4", "Wish you and your family a very happy Independence Day!", "15 Aug 2012", true, new CallForAction(CallForAction.TYPE_REFER, "Refer Me", "")),
			new Messages(5, "Subject5", "Your car is fully serviced and ready for pick-up.", "25 Jan 2012", true, new CallForAction(CallForAction.TYPE_CALL, "Call Us", "9986311645")),
			new Messages(6, "Subject6", "Your next service is due in 2 days. Please contact our helpline to set up an appointment.", "5 Jan 2012"),
			new Messages(7, "Subject7", "Attractive discounts on all accessories for Maruti Swift and Dezire!", "25 Dec 2011"),
			new Messages(8, "Subject8", "Refer a friend and get 10% off on your next paid service.", "25 Nov 2011"),
			new Messages(9, "Subject9", "Wish you and your family a very happy Festive Season!", "15 Oct 2011"),
			new Messages(10, "Subject10", "Your car is fully serviced and ready for pick-up.", "25 Sep 2011"),
			new Messages(11, "Subject11", "Your car is currently at the washing bay, 5th in queue.", "17 Aug 2011"),
			new Messages(12, "Subject12", "Attractive discounts on all accessories for Maruti Omni and Alto!", "3 Jul 2011"),
			new Messages(13, "Subject13", "Refer a friend and get 10% off on your next paid service.", "29 Jun 2011"),
			new Messages(14, "Subject14", "Wish you and your family a very happy Republic Day!", "26 Jan 2011"),
			new Messages(15, "Subject15", "Your car is fully serviced and ready for pick-up.", "25 Jan 2011"),
			new Messages(16, "Subject16", "Your next service is due in 2 days. Please contact our helpline to set up an appointment.", "5 Jan 2011"),
			new Messages(17, "Subject17", "Happy New Year! Usher in the happiness, drive home a Maruti vehicle.", "1 Jan 2011")
	};
	*/
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		
		View v = inflater.inflate(R.layout.frag_mylist, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		listMessages = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		
		listMessages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				parentActivity.onFragmentReplaceRequest(MessageDetailFragment.newInstance(msgData[arg2]), true);
			}
		});
		
		tvTitle.setText("Inbox");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.inbox, 0, 0, 0);
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiver);
		
		BackgroundService.setInboxMessageRoleObserver(false);
		super.onPause();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_INBOX);
		cleverTap.event.push("Opened Messages List Screen");

		/*
		msgData[2].setReadFlag(true);
		msgData[4].setReadFlag(true);
		msgData[5].setReadFlag(true);
		msgData[6].setReadFlag(true);
		msgData[7].setReadFlag(true);
		msgData[8].setReadFlag(true);
		msgData[13].setReadFlag(true);
		msgData[14].setReadFlag(true);
		
		int numUnread = 1 + (int) (Math.random() * 19);
		*/
		
		LocalBroadcastManager.getInstance(getActivity())
				.registerReceiver(broadcastReceiver, 
							new IntentFilter(BackgroundService.INTENT_FILTER_INBOX_MESSAGE_UPDATE_ACTION));

		BackgroundService.setInboxMessageRoleObserver(true);
		
		Intent i = new Intent(getActivity(), BackgroundService.class);
		i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_EXPRESS);
		getActivity().startService(i);
		
		parentActivity.onTabBarVisiblityChangeRequest(false);
		
		refreshMessageListView();

		
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(getActivity(), "Broadcastrecceiver Called", 0).show();
			
			refreshMessageListView();
		}
	};

	private void refreshMessageListView() {
        msgData = MessagesData.getMessagesFromFile(getActivity());
		
		if (msgData.length > 0) {
			listMessages.setVisibility(View.VISIBLE);
			tvListEmptyNote.setVisibility(View.GONE);
			MessageAdapter mListAdapter = new MessageAdapter(getActivity(), R.layout.row_mylist, msgData);
			listMessages.setAdapter(mListAdapter);
			
			int numUnread = MessagesData.countUnread(getActivity());
			tvExtra.setText(numUnread + " New");
			String actionBarTitle = "Inbox";
			if (numUnread > 0) actionBarTitle += " (" + numUnread + ")"; 
			toolbarListener.setText(actionBarTitle);

			NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
			nm.cancel("NewMessages", 1);
		}
		else {
			tvExtra.setText(""); 
			toolbarListener.setText("Inbox");

			listMessages.setVisibility(View.GONE);
			tvListEmptyNote.setText("You have no messages!");
			tvListEmptyNote.setVisibility(View.VISIBLE);
		}
		
	}
}
