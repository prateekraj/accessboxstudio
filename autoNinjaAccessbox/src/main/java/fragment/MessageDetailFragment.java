package fragment;

import activity.PaymentActivity;
import db.DatabaseHelper;
import db.PSFDataSource;
import db.ServiceBookingDataSource;
import db.TableContract.PSFTable;
import db.TableContract.ServiceBookingDB;
import db.UserDetailsDataSource;
import entity.CallForAction;
import entity.ContactHolder;
import entity.Messages;
import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import myJsonData.MessagesData;
import myJsonData.MyJSONData;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Downloader;
import utils.UICallback;
import utils.Utils;

import com.myaccessbox.appcore.R;
import com.squareup.picasso.Picasso;

import config.StaticConfig;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class MessageDetailFragment extends MyFragment implements View.OnClickListener, OnRatingBarChangeListener {
	
	Messages thisMessage;
	TextView subjectTv;
	TextView dateTv;
	TextView bodyTv;
	Button delButt;
	Button CFAButton;
	Button replyButton;
	ImageView imgMsgDetail, pdfMsgDetail;
	AlertDialog confirmDeleteDlg;
	AlertDialog contactPickFBIssueTryAgainDialog;
	PSFDataSource psfDatasource;
	//public static boolean imageDownloaded=false;
	UICallback callback;
	private ProgressBar progressBar;
	TextView downloading_image;
	AlertDialog cancelService;
	
	
	LinearLayout msgll;
	WebView msgWebview;
	
	String url ="";
	public static boolean payAmountCheck = false;
	public static boolean msgCopyToCEO = false;
	RatingBar ratingBar;
	TextView tvRating;
	EditText edtRateRemark;
	public float starNumber = 0;
	ProgressDialog progressDialog;
	boolean success;
	private float _userRating = 0;
	
	public static MessageDetailFragment newInstance(Messages msg) {
		Bundle init = new Bundle();
		init.putParcelable("message", msg);
		MessageDetailFragment frag = new MessageDetailFragment();
		frag.setArguments(init);
		return frag;
	}
	
	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		psfDatasource = new PSFDataSource(getActivity());
		state = getArguments();
		thisMessage = (Messages) state.getParcelable("message");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_msgdetail, container, false);
		
		subjectTv = (TextView) v.findViewById(R.id.textMessageDetailSubject);
		dateTv = (TextView) v.findViewById(R.id.textMsgDetailDate);
		bodyTv = (TextView) v.findViewById(R.id.textMessageDetailBody);
		
		msgll = (LinearLayout) v.findViewById(R.id.message_layout);
		msgWebview = (WebView) v.findViewById(R.id.message_webView);
		
		subjectTv.setText(thisMessage.getSubject());
		dateTv.setText(thisMessage.getDate());
		bodyTv.setText(thisMessage.getBody());
		
		pdfMsgDetail = (ImageView) v.findViewById(R.id.pdfMessageDetailImg);
		pdfMsgDetail.setOnClickListener(this);

		delButt = (Button) v.findViewById(R.id.buttMsgDelete);
		delButt.setOnClickListener(this);
		
		ratingBar = (RatingBar) v.findViewById(R.id.ratingBar);
		ratingBar.setOnRatingBarChangeListener(this);
		
		edtRateRemark = (EditText) v.findViewById(R.id.edtRateRemark);
		tvRating = (TextView) v.findViewById(R.id.txtRateString);
		
		progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
		downloading_image = (TextView) v.findViewById(R.id.downloading_image);
		
		if (!thisMessage.isRead()) {
			thisMessage.markAsRead(getActivity());
			/*
			 * Code for GA to report auto-gen reminder messages
			 */ 
			if (thisMessage.getUid() < 0) {
				String temp = thisMessage.getSubject(); 
				temp = temp.substring(9, temp.indexOf(" ", 9));

				tracker.send(GATrackerMaps.getReminderViewEvent(temp));
			}
		}

		CFAButton = (Button) v.findViewById(R.id.buttCFAction);
		CFAButton.setOnClickListener(this);
		CFAButton.setCompoundDrawablesWithIntrinsicBounds(thisMessage.getCFA().getButtonDrawableId(), 0, 0, 0);
		CFAButton.setText(thisMessage.getCFA().getLabel());
		replyButton = (Button) v.findViewById(R.id.buttTalkToCEO);
		replyButton.setOnClickListener(this);
		
		switch (thisMessage.getCFA().getType()) {
		case CallForAction.TYPE_CALL:
		case CallForAction.TYPE_WEB:
		case CallForAction.TYPE_REFER:
		case CallForAction.TYPE_PAY_NOW:
		case CallForAction.TYPE_PAY_CITRUS:	
			CFAButton.setVisibility(View.VISIBLE);
			ratingBar.setVisibility(View.GONE);
			edtRateRemark.setVisibility(View.GONE);
			break;
		case CallForAction.TYPE_CANCEL_SERVICE:
			ServiceBookingDataSource serviceDS = new ServiceBookingDataSource(getActivity());
			if(!StaticConfig.FEATURE_CANCEL_SERVICE_ENABLED){
				CFAButton.setVisibility(View.GONE);
			}else{
			if(serviceDS.getCancelledFlag(""+thisMessage.getUid())==1){
				CFAButton.setVisibility(View.GONE);
				ratingBar.setVisibility(View.GONE);
				edtRateRemark.setVisibility(View.GONE);
			}else{
				CFAButton.setVisibility(View.VISIBLE);
				ratingBar.setVisibility(View.GONE);
				edtRateRemark.setVisibility(View.GONE);
			}
			}
			break;
		case CallForAction.TYPE_PSF_RATE:
			ratingBar.setVisibility(View.VISIBLE);
			edtRateRemark.setVisibility(View.VISIBLE);
			CFAButton.setVisibility(View.VISIBLE);
			break;
		default:
			CFAButton.setVisibility(View.GONE);
			ratingBar.setVisibility(View.GONE);
			edtRateRemark.setVisibility(View.GONE);
			break;
		}
		
		imgMsgDetail = (ImageView) v.findViewById(R.id.imgMessageDetailImg);
		imgMsgDetail.setOnClickListener(this);
		if (!thisMessage.getImage().trim().equalsIgnoreCase("") && thisMessage.getImage().contains(".pdf")) {
			pdfMsgDetail.setVisibility(View.GONE);
			imgMsgDetail.setVisibility(View.GONE);
			checkBeforeDownload();
		} else if (!thisMessage.getImage().trim().equalsIgnoreCase("") && !thisMessage.getImage().contains(".pdf")) {
			imgMsgDetail.setVisibility(View.VISIBLE);
			pdfMsgDetail.setVisibility(View.GONE);
			if(!Utils.isNetworkConnected(getActivity())){
				Toast.makeText(getActivity(), R.string.network_msg_img, Toast.LENGTH_LONG).show();
			}
			String urlString = StaticConfig.API_MESSAGE_IMAGES_BASE + thisMessage.getImage().trim();
			//System.out.println("urlString: "+urlString);
			String url = urlString.replaceAll(" ", "%20");
			
			Picasso.with(getActivity())
			.load(url)
		    .placeholder(R.drawable.default_image) 
		    .error(R.drawable.default_image)         // optional
		    .into(imgMsgDetail);
		}
		else {
			imgMsgDetail.setVisibility(View.GONE);
		}

		return v;
	}
	
	private void checkBeforeDownload() {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		File folder = new File(extStorageDirectory, "pdf");
		if(folder.length()==0){
			folder.mkdir();
		}
		File file = new File(folder, thisMessage.getImage().trim());
		if(file.length()==0){
			try {
				file.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}	
		}
		if(Utils.isNetworkConnected(getActivity()) && file.length()==0){
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(100);
			downloading_image.setVisibility(View.VISIBLE);
			downlodPDF(thisMessage.getImage().trim(), file);
		}else if(!Utils.isNetworkConnected(getActivity()) && file.length()==0){
			pdfMsgDetail.setImageResource(R.drawable.pdf_error);
			pdfMsgDetail.setVisibility(View.VISIBLE);
			Toast.makeText(getActivity(), R.string.network_msg_img, Toast.LENGTH_LONG).show();
		}else if(file.length()!=0){
			pdfMsgDetail.setImageResource(R.drawable.pdf_view);
			pdfMsgDetail.setVisibility(View.VISIBLE);
		}
	}

	private void downlodPDF(String imgNam, File file) {
		// saving pdf into file
					url = StaticConfig.API_MESSAGE_IMAGES_BASE + imgNam;
					
					new Downloader(new UICallback() {

						@Override
						public void OnUICallback(Boolean fetched) {
							File file = new File(Environment.getExternalStorageDirectory()+"/pdf/"+thisMessage.getImage());
							progressBar.setVisibility(View.GONE);
							downloading_image.setVisibility(View.GONE);
							pdfMsgDetail.setVisibility(View.VISIBLE);
							if(fetched && file.exists()){
									//System.out.println("FILE LENGTH after downloading: "+file.length());
									//imgMsgDetail.setImageBitmap(BitmapFactory.decodeFile(t.getAbsolutePath()));
									pdfMsgDetail.setImageResource(R.drawable.pdf_view);
							}else{
								pdfMsgDetail.setImageResource(R.drawable.pdf_error);
								Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
							}	// TODO Auto-generated method stub
							
						}
					}).execute(url,""+file);									
	}

	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_INBOX_DETAIL);
		cleverTap.event.push("Opened Message Detail Screen");

		if(psfDatasource.getPsfRating(thisMessage.getUid())>0){
			ratingBar.setIsIndicator(true);
			ratingBar.setFocusable(false);
			CFAButton.setVisibility(View.GONE);
			edtRateRemark.setVisibility(View.GONE);
			ratingBar.setRating(psfDatasource.getPsfRating(thisMessage.getUid()));
		}
		toolbarListener.setText(thisMessage.getSubject());
		//setActionBarBackEnabled(true);
	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.pdfMessageDetailImg:
			if(thisMessage.getImage().contains(".pdf")) {
				i = new Intent(Intent.ACTION_VIEW);
				File file = new File(Environment.getExternalStorageDirectory()+"/pdf/"+thisMessage.getImage());
				i.setDataAndType(Uri.fromFile(file), "application/pdf");
				try {
					startActivity(i);
				}
				catch (ActivityNotFoundException e) {
					//Log.e(TAG, "No PDF reader! " + e.getMessage());
					Toast.makeText(getActivity(), "Please install a PDF reader to view this file!", Toast.LENGTH_LONG).show();
				}
			}
			break;
		case R.id.buttMsgDelete:
			handleDeleteMsg();
			break;
			
		case R.id.buttTalkToCEO :
			callChatCEO();
			break;
			
		case R.id.buttCFAction: 
			boolean isReminder = false;
			String gaLabel = "Message ID:" + thisMessage.getUid();
			switch (thisMessage.getCFA().getType()) {
			case CallForAction.TYPE_CALL:
				if (thisMessage.getUid() < 0) {
					isReminder = true;
					gaLabel = thisMessage.getSubject(); 
					gaLabel = gaLabel.substring(9, gaLabel.indexOf(" ", 9));
				} 
				
				i = new Intent(Intent.ACTION_CALL);
				i.setData(Uri.parse("tel:" + thisMessage.getCFA().getDetails()));
				startActivity(i);
				break;
			case CallForAction.TYPE_WEB:
			case CallForAction.TYPE_PAY_NOW:
				msgll.setVisibility(View.GONE);
				url = thisMessage.getCFA().getDetails();
				try {
					startWebView(url);
					msgWebview.setVisibility(View.VISIBLE);
					/*i = new Intent(Intent.ACTION_VIEW, Uri.parse(thisMessage.getCFA().getDetails()));
					startActivity(i);*/
				}
				catch (ActivityNotFoundException e) {
					Toast.makeText((Context) parentActivity, "Unable to open weblink!", Toast.LENGTH_SHORT).show();
				}
				break;
			
			case CallForAction.TYPE_PAY_CITRUS:
				handleCitrusPayment();
				break;
			case CallForAction.TYPE_REFER:
				startContactPicker();
				break;
			case CallForAction.TYPE_PSF_RATE:
				handlePSFRating();
				break;
			case CallForAction.TYPE_CANCEL_SERVICE:
				AlertDialog.Builder bldr = new AlertDialog.Builder(getActivity());
				bldr.setMessage("Are you sure?");
				bldr.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(Utils.isNetworkConnected(getActivity())){
							handleCancelService();
						}else{
							Toast.makeText(getActivity(), R.string.network_msg, Toast.LENGTH_SHORT).show();
						}
						

					}
				});
				bldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						cancelService.dismiss();

					}
				});
				cancelService = bldr.show();
				break;
			default:
				//Log.e("MessageDetailActivity", "This should never occour!");
				return;
			}
			
			tracker.send(GATrackerMaps.getInboxCFAEvent(thisMessage.getCFA().getType(), gaLabel, isReminder));
			break;
		}
	}
	
	private void handleCancelService() {
		progressDialog = ProgressDialog.show(getActivity(),
				"Cancelling your service......", "Please wait for a moment");
		progressDialog.setCancelable(true);
		ServiceBookingDataSource serviceBookingDS = new ServiceBookingDataSource(getActivity());
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(
				getActivity());
		Cursor c = serviceBookingDS.getDetails(""+thisMessage.getUid());
		String car_id = "",phone="",service_booking_id="";
		if(c.moveToFirst()){
			do{
				car_id = c.getString(c.getColumnIndex(ServiceBookingDB.COL_CAR_ID));
				service_booking_id = c.getString(c.getColumnIndex(ServiceBookingDB.COL_BOOKING_ID));
				
			}while(c.moveToNext());
		}else{
			Toast.makeText(getActivity(), "Error Occured. Please write to CEO through message", Toast.LENGTH_SHORT).show();
		}
		serviceBookingDS.close();
		if((!car_id.equalsIgnoreCase("") || !service_booking_id.equalsIgnoreCase("")) && Utils.isNetworkConnected(getActivity())){
			postToserver(car_id,service_booking_id);
		}else if(!Utils.isNetworkConnected(getActivity())){
			if(null!=progressDialog && progressDialog.isShowing()){
				progressDialog.dismiss();
			}
			Toast.makeText(getActivity(), R.string.network_msg, Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(getActivity(), "Error Occured. Please report to CEO through message", Toast.LENGTH_SHORT).show();
		}
		
		
	}

	private void postToserver(String car_id, String service_booking_id) {
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		final ContentValues cv = new ContentValues();
		final ServiceBookingDataSource serviceDS = new ServiceBookingDataSource(getActivity());
		MultipartEntity mpEntity = new MultipartEntity();
		try {
			mpEntity.addPart("phone", new StringBody("" + userDetailDataSource.getUserOwnNumber()));
			mpEntity.addPart("car_id", new StringBody("" + car_id.trim()));
			mpEntity.addPart("service_booking_id",new StringBody("" + service_booking_id.trim()));
			} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			AsyncInvokeURLTask task;
			task = new AsyncInvokeURLTask(
					new AsyncInvokeURLTask.OnPostExecuteListener() {
						public void onPostExecute(String result) {
							progressDialog.dismiss();
							//System.out.println("SERVICE RESPONSE:"+ result);
							try {
								JSONObject response = new JSONObject(result);
								if(null!= response && response.has("text")){
								Toast.makeText(getActivity(), response.getString("text"), Toast.LENGTH_LONG).show();
								}
								if(null!=response && response.has("error") && response.getString("error").equalsIgnoreCase("false")){
									cv.put(ServiceBookingDB.COL_CANCELLED_FLAG, 1);
									serviceDS.updateServiceDetails(cv, ""+thisMessage.getUid());
									CFAButton.setVisibility(View.GONE);	
								}
								/*if(response.has("error") && response.getString("error").equalsIgnoreCase("false")){
									Toast.makeText(
											getActivity(),
											"Your service booking request has been received. We will get back to you with the confirmation shortly.",
											Toast.LENGTH_LONG).show();
								}else if(response.has("error") && response.getString("error").equalsIgnoreCase("true")){*/
									
								//}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
			String executeURL = StaticConfig.API_CANCEL_SERVICE + "?"
					+ StaticConfig.getCMSPass();
			task.execute(executeURL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	private void handleDeleteMsg() {
		//mGaTracker.sendEvent(CATEGORY_MESSAGES, ACTION_CLICK, "Delete Message", null);
		AlertDialog.Builder bldr = new AlertDialog.Builder(getActivity());
		bldr.setMessage("This message will be deleted!");
		bldr.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				tracker.send(GATrackerMaps.EVENT_DELETE_MESSAGE);
				
				MessagesData.deleteMessage(getActivity(), thisMessage.getUid());
				confirmDeleteDlg.dismiss();
				getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
			}
		});
		
		bldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				confirmDeleteDlg.dismiss();
			}
		});
		
		confirmDeleteDlg = bldr.show();
		//Toast.makeText((Context) parentActivity, "Delete Clicked", Toast.LENGTH_LONG).show();
			
	}

	private void handlePSFRating() {
		ContentValues cv = new ContentValues();
		if(psfDatasource.getPsfRating(thisMessage.getUid())>0){
			ratingBar.setIsIndicator(true);
			ratingBar.setFocusable(false);
			CFAButton.setVisibility(View.GONE);
			edtRateRemark.setVisibility(View.GONE);
		}else{
			if(get_userRating() > 0){
			boolean b = SendRatingToServer();
			if(b == false && psfDatasource.getPsfRating(thisMessage.getUid())==0 ){
				cv.put(PSFTable.COL_PSF_MSG_ID, thisMessage.getUid());
				cv.put(PSFTable.COL_PSF_RATING, get_userRating());
				psfDatasource.insertPSF(cv);
				ratingBar.setIsIndicator(true);
				ratingBar.setFocusable(false);
				CFAButton.setVisibility(View.GONE);
				edtRateRemark.setVisibility(View.GONE);
			}else if(b==false){
				
				set_userRating(_userRating);
				//make everythyng visible
			}
		} else 
			Toast.makeText(getActivity(), "Please rate before you proceed", Toast.LENGTH_SHORT).show();
		}
	}

	private void handleCitrusPayment() {
		if(!MyJSONData.dataFileExists(getActivity(), MyJSONData.TYPE_OWN)){

			new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);

		}

		payAmountCheck = true;
		msgll.setVisibility(View.GONE);
		try {
		url = thisMessage.getCFA().getDetails();
		String temp[] = url.split(",");
		for(int t=0;t<temp.length;t++)
		{
			System.out.println(temp[t]);
		}
		String paymentId = temp[0];
		String paymentAmount = temp[1];
		String tempId[] = paymentId.split("=");
		String tempAmount[] = paymentAmount.split("=");
		String finalId = "";
		String finalAmount = "";
		for(int j=0;j<tempId.length;j++)
		{
			finalId = tempId[1];
		}
		
		for(int k=0;k<tempAmount.length;k++)
		{
			finalAmount = tempAmount[1];
		}
		if (MyJSONData.dataFileExists(getActivity(), MyJSONData.TYPE_OWN)) {

			MyJSONData.editMyData(getActivity(), 

					new String[] {MyJSONData.FIELD_OWN_PAYMENT_ID, MyJSONData.FIELD_OWN_PAYMENT_AMOUNT}, 

					new String[] {finalId, finalAmount}, MyJSONData.TYPE_OWN);

		}else{

			MyJSONData.createMyData(getActivity(), 

					new String[] {MyJSONData.FIELD_OWN_PAYMENT_ID, MyJSONData.FIELD_OWN_PAYMENT_AMOUNT}, 

					new String[] {finalId, finalAmount}, MyJSONData.TYPE_OWN);

		}

		parentActivity.onFragmentReplaceRequest(new fragment.PaymentFragmentCitrus(), false);
			//startActivity(new Intent(getActivity(), PaymentActivity.class));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private boolean SendRatingToServer() {
		url = thisMessage.getCFA().getDetails();
		if (Utils.isNetworkConnected(getActivity())) {

			progressDialog = ProgressDialog.show(getActivity(),
					"Sending......", "Please wait for a moment");
			progressDialog.setCancelable(true);

			try {
				String params = "?phone="+ StaticConfig.LOGGED_PHONE_NUMBER;
				params += "&remarks="+ URLEncoder.encode(edtRateRemark.getText().toString(), "UTF-8");
				params += "&rating="+ URLEncoder.encode(""+get_userRating(), "UTF-8");
				params += "&psf_log_id="+ URLEncoder.encode(url, "UTF-8");;  
				params += "&" + StaticConfig.getCMSPass();
				//System.out.println("**********"+StaticConfig.API_PSF_RATING + params);
				//Log.i("----------"+params);

				AsyncInvokeURLTask task = new AsyncInvokeURLTask(
						new AsyncInvokeURLTask.OnPostExecuteListener() {
							public void onPostExecute(String result) {
							String response = "";	
							try {
								JSONObject jResult = new JSONObject(result);
								success = jResult.getBoolean("error");
								response = jResult.getString("text");
								
							//	Log.i("~~~~~~~~~~~"+success);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}							
							progressDialog.dismiss();
							Toast.makeText(getActivity(),
								""+response,Toast.LENGTH_LONG).show();
							}
						}, AsyncInvokeURLTask.REQUEST_TYPE_GET);

				task.execute(StaticConfig.API_PSF_RATING + params);

			} catch (UnsupportedEncodingException e) {
				// ignore
			} catch (Exception e) {
				// ignore
			}
		} else {
			Toast.makeText(getActivity(),
					"Please check your internet connection.",
					Toast.LENGTH_LONG).show();
		}
		return success;
		
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ContactHolder contact;
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		if (resultCode == Activity.RESULT_OK) {
			contact = CallForAction.getContactDetails(getActivity().getContentResolver(), data.getData());
			
			if (contact != null) {
				try {
					AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
						public void onPostExecute(String result) {
							try {
								JSONObject response = new JSONObject(result);
								if(response.has("success")&& response.getString("success").equalsIgnoreCase("true")){
									Toast.makeText(getActivity(), "Friend referal request successfully sent... Thanks!", Toast.LENGTH_LONG).show();
								}else{
									Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
					//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);

					String executeURL = StaticConfig.API_REFER_A_FRIEND + "?referer=" + userDetailDataSource.getUserOwnNumber();
					executeURL += "&referee_name=" + URLEncoder.encode(contact.getName(), "UTF-8");
					executeURL += "&referee_num=" + URLEncoder.encode(contact.getNumber(), "UTF-8");
					executeURL += "&source_id=" + thisMessage.getUid();
					executeURL += "&source=1"; // for messages

					task.execute(executeURL);
					//Log.d("MessagesDetailFrag", "execute called with url: " + executeURL);
				} catch (Exception e1) {
					//Log.e("MessagesDetailFrag", "Exception: " + e1.toString());
				}
			}
			else {
				contactPickFBIssueTryAgainDialog = CallForAction.getContactNotFoundDialog(getActivity(), 
						"Pick Again", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
								startContactPicker();
							}
						},
						
						"Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
							}
						}
				);
			}
		}
	}
	
	private void startContactPicker() {
		Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
		getActivity().startActivityForResult(i, 1);
	}
	
	private void callChatCEO() {
		//if (MyJSONData.dataFileExists(getActivity(), MyJSONData.TYPE_OWN)) {
		msgCopyToCEO = true;
		StaticConfig.FIELD_OWN_MESSAGE_REPLY_TO_CEO = thisMessage.getSubject();
		/*MyJSONData.editMyData(getActivity(), 
				new String[] {MyJSONData.FIELD_OWN_MESSAGE_REPLY_TO_CEO}, 
				new String[] {thisMessage.getSubject()}, 
				MyJSONData.TYPE_OWN);*/
		//}
		parentActivity.onFragmentReplaceRequest(new ChatCEOFragment(), true);

	}
	
	private void startWebView(String url2) {
        msgWebview.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url link will be  open in new browser not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
             //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }
        });

        // Javascript enabled on webview
        msgWebview.getSettings().setJavaScriptEnabled(true);
        msgWebview.getSettings().setLoadWithOverviewMode(true);
        msgWebview.getSettings().setUseWideViewPort(true);
        msgWebview.getSettings().setBuiltInZoomControls(true);
        msgWebview.getSettings().setSupportZoom(true);
        msgWebview.loadUrl(url);

    }

    public void onBackPressed() {
        if(msgWebview.canGoBack()) {
        	msgWebview.goBack();
        } else {
            // Let the system handle the back button
        }
    }

	@Override
	public void onRatingChanged(RatingBar ratingBar, float rating,
			boolean fromUser) {
		set_userRating(rating);
		
	}

	public float get_userRating() {
		return _userRating;
	}

	public void set_userRating(float _userRating) {
		this._userRating = _userRating;
	}
}
