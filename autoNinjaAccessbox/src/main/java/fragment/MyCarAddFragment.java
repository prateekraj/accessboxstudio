package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import myJsonData.MyJSONData;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Utils;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.myaccessbox.appcore.R;

import config.StaticConfig;

import db.CarDetailsDatasource;
import db.DatabaseHelper;
import db.TableContract.CarDetailsDB;
import db.UserDetailsDataSource;
import entity.MyCar;
import android.R.bool;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MyCarAddFragment extends MyFragment implements View.OnClickListener {

	EditText edtCarName;
	EditText edtOwnerName;
	EditText edtRegNumber;
	TextView tvCancelButton;
	TextView tvSubmitButton;
	MyJSONData ownData;
	int car_id = -1;
	Boolean added = false;
	ProgressDialog progressDialog;
	String carName, ownerName, regNum;
	CarDetailsDatasource carDetailDataSource;
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		super.onResume();
	    carDetailDataSource= new CarDetailsDatasource(getActivity());
		tracker.send(GATrackerMaps.VIEW_CAR_ADD);
		toolbarListener.setText("Add Car");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_addcar, container, false);
		edtCarName = (EditText) v.findViewById(R.id.addcar_edtcarname);
		edtOwnerName = (EditText) v.findViewById(R.id.addcar_edtownername);
		edtRegNumber = (EditText) v.findViewById(R.id.addcar_edtregnumber);
		tvCancelButton = (TextView) v.findViewById(R.id.addcar_tvcancel_button);
		tvSubmitButton = (TextView) v.findViewById(R.id.addcar_tvsubmit_button);

		tvCancelButton.setOnClickListener(this);
		tvSubmitButton.setOnClickListener(this);
		return v;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public void onClick(View v) {
		//DatabaseHelper dbhelper = new DatabaseHelper(getActivity());
		carName = edtCarName.getText().toString().trim();
		ownerName = edtOwnerName.getText().toString().trim();
		regNum = edtRegNumber.getText().toString().trim().replaceAll("\\s+", "").toUpperCase();
		String[] vals = {regNum, ownerName, carName, "false"};
		switch (v.getId()) {
			case R.id.addcar_tvsubmit_button :
				tracker.send(GATrackerMaps.EVENT_CAR_ADD_SUBMIT);
				if (carName.equalsIgnoreCase("")) {
					Toast.makeText(getActivity(), "Please enter your car name", 0).show();
				} else if (ownerName.equalsIgnoreCase("")) {
					Toast.makeText(getActivity(), "Please enter your name", 0).show();
				} else if (regNum.equalsIgnoreCase("")) {
					Toast.makeText(getActivity(), "Please enter your car registration number", 0).show();
				} else if (!regNum.equalsIgnoreCase("") && carDetailDataSource.regNumMathched(regNum)) {
					Toast.makeText(getActivity(), "Car with same registration no. exists!", Toast.LENGTH_LONG).show();
				} /*else if(!Utils.isRegNumValid(regNum)){
					Toast.makeText(getActivity(), "Please enter your car registration number in correct format!", Toast.LENGTH_LONG).show();
				}*/ else if (Utils.isNetworkConnected(getActivity())) {
					addNewCar(vals, true, getActivity(), parentActivity);
				}else{
					Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
				}
				break;
			case R.id.addcar_tvcancel_button :
				tracker.send(GATrackerMaps.EVENT_CAR_ADD_CANCEL);
				parentActivity.onFragmentReplaceRequest(new MyCarBaseFragment(), false);
				break;
		}

	}
	/**
	 * 
	 * @param vals
	 * @param moreCar
	 *            : to determine the action after car is added new/more new
	 * @return
	 */
	public boolean addNewCar(final String[] vals, final boolean moreCar, final Context cxt, final MyOnFragmentReplaceListener parentActivity) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(cxt);
	    carDetailDataSource = new CarDetailsDatasource(cxt);
	    try {
			cleverTap = CleverTapAPI.getInstance(getActivity());
		} catch (CleverTapMetaDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			//if (moreCar) {
				progressDialog = ProgressDialog.show(cxt, "Adding Car", "Please wait for a moment");
			//}
			MultipartEntity mpEntity = new MultipartEntity();
			//ownData = new MyJSONData(cxt, MyJSONData.TYPE_OWN);

			try {
				mpEntity.addPart("phone", new StringBody("" + userDetailDataSource.getUserOwnNumber()));
				mpEntity.addPart("car_id", new StringBody("" + car_id));
				mpEntity.addPart("reg_num", new StringBody("" + vals[0]));
				mpEntity.addPart("owner_name", new StringBody("" + vals[1]));
				mpEntity.addPart("model", new StringBody("" + vals[2]));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
				private JSONObject newAddedCar = null; 

				public void onPostExecute(String result) {
					try {
						MyCar myCAR = new MyCar();
						//System.out.println("POST ADD NNEW CAR"+result);
						//if (moreCar && progressDialog != null) {
							progressDialog.dismiss();
						//}
						newAddedCar = new JSONObject(result);
						if (newAddedCar.has("error") && newAddedCar.getString("error").equalsIgnoreCase("false")) {
							cleverTap.event.push("Succesfully added a car");
							if (newAddedCar.has("car_id")) {
								car_id = newAddedCar.getInt("car_id");
								if (car_id > 0) {
									
									myCAR.setCarId(car_id);
									myCAR.setModelName(vals[2]);
									myCAR.setOwner(vals[1]);
									myCAR.setReg_num(vals[0]);
									if (vals[3].equalsIgnoreCase("false")) {
										myCAR.setData_verified(false);
									} else {
										myCAR.setData_verified(true);
									}

									ContentValues cv = addCarDetails(myCAR);
									long r = carDetailDataSource.insertCarDetails(cv);
									if (r > 0) {
										carDetailDataSource.getCarList();
										added = true;
									}
								} else {
									added = false;
								}
								// action to be taken according to new car added
								// or more car added based on more car boolean
								// variable value
								if (added == true) {
									progressDialog.dismiss();
									StaticConfig.position = StaticConfig.myCarList.size()-1;
									Toast.makeText(cxt, "Car Added Successfully", 0).show();
									//System.out.println("CAR ID ADDED:"+car_id);
									parentActivity.onFragmentReplaceRequest(new MyCarBaseFragment(), false);
								} 
							}
						} else {
							added = false;
							Toast.makeText(cxt, newAddedCar.getString("text"), 0).show();
							parentActivity.onFragmentReplaceRequest(new MyCarBaseFragment(), false);
						}
					} catch (JSONException e) { // TODO Auto-generated catch
												// block
						e.printStackTrace();
					}
				}
			}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
			String executeURL = StaticConfig.API_POST_ADD_CAR + "?" + StaticConfig.getCMSPass();
			Log.d("ADD CAR", executeURL);
			task.execute(executeURL);
			// Log.d(TAG,"execute called with url: " + executeURL);
		} catch (Exception e1) { //
			System.out.println("Exception HIT SERVER: " + e1.toString());
		}
		return added;
	}

	private ContentValues addCarDetails(MyCar myCar) {

		ContentValues cv = new ContentValues();
		cv.put(CarDetailsDB.COL_CAR_ID, myCar.getCarId());
		cv.put(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE, myCar.getNext_service_date());
		cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY, myCar.getInsurance_expiry());
		cv.put(CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT, myCar.getService_provider());
		cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_PROVIDER, myCar.getInsurance_provider());
		cv.put(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY, myCar.getPuc_expiry());
		cv.put(CarDetailsDB.COL_FIELD_CAR_REGNUM, myCar.getReg_num());
		cv.put(CarDetailsDB.COL_FIELD_CAR_MODEL, myCar.getModelName());
		cv.put(CarDetailsDB.COL_CAR_OWNER, myCar.getOwner());
		cv.put(CarDetailsDB.COL_FIELD_CAR_DATA_VERIFIED, myCar.isData_verified());
		cv.put(CarDetailsDB.COL_FIELD_CAR_PHOTO_PATH, myCar.getCar_photo_path());
		cv.put(CarDetailsDB.COL_FIELD_CAR_PHOTO_ATTEMPT_PATH, myCar.getCar_photo_attempt_path());
		cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_DOCUMENT, myCar.getInsurance_document_name());
		cv.put(CarDetailsDB.COL_FIELD_CAR_PUC_DOCUMENT, myCar.getPuc_document_name());
		return cv;
	}

	// end

}
