package fragment;

import activity.MyFragmentedActivityBase;
import android.app.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;

public class MyFragment extends Fragment {
	
	protected MyOnFragmentReplaceListener parentActivity;
	//protected TextView actionBarTitleView;
	//private ImageView actionBarBackButton;

	public int initExtraInt = 0;
	protected Tracker tracker;
	protected MyFragmentedActivityBase baseActivityObj;
	protected ToolbarListener toolbarListener;
	protected CleverTapAPI cleverTap = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return null;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
	    switch (itemId) {
	    case android.R.id.home:
	    	getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
	    	getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
	        break;
	    }
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			cleverTap = CleverTapAPI.getInstance(getActivity());
		} catch (CleverTapMetaDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setHasOptionsMenu(true);
		parentActivity = (MyOnFragmentReplaceListener) activity;
		toolbarListener =(ToolbarListener)activity;
		
		ActionBar abar = ((AppCompatActivity) activity).getSupportActionBar();
		View v = abar.getCustomView();
		if (v == null) {
			//actionbar.getCustomView may return null onResume -> go to Home Activity in this case
			
			((MyFragmentedActivityBase) activity).setActionBarCustomView();
			//startActivity(new Intent(activity, MainActivity.class));
			abar = ((AppCompatActivity) activity).getSupportActionBar();
			v = abar.getCustomView();
		}

		//actionBarTitleView = (TextView) v.findViewById(R.id.titleText);
		//re-set actionbar title to blank ""
		//child-fragments have the onus of defining the title subsequently
		//actionBarTitleView.setText("");
		//actionBarTitleView.setSelected(true);  //line to make maquee work if required

		//actionBarBackButton = (ImageView) v.findViewById(R.id.backButton);
		
		tracker = EasyTracker.getInstance(activity);
		baseActivityObj = (MyFragmentedActivityBase)activity;
		toolbarListener.setAddCarButton(false);
	}
	/*
	public void setActionBarBackEnabled(boolean enabled) {
		actionBarBackButton.setEnabled(enabled);
	}
	*/
	
	public interface ToolbarListener {
		
		public void setText(String title);
		
		public void setAddCarButton(boolean addCar);
	}
	
	public interface MyOnFragmentReplaceListener {
		/*
		 * This interface function is used to replace fragment in  
		 * parentActivity without changing the current-active tab
		 */
		public void onFragmentReplaceRequest(Fragment Fragment, boolean addToBackStack);
		
		/*
		 * This interface function is used to change the fragment in
		 * parentActivity by changing the tab-bar's current-active-tab
		 */
		public void onTabChangeRequest(int newTabID);
		
		/*
		 * This interface function is used to change the parent layout
		 * by showing/hiding the tabBarFragment if available
		 */
		public void onTabBarVisiblityChangeRequest(boolean showTabBar);
	}	
}
