package fragment;

import googleAnalytics.GATrackerMaps;

import java.util.ArrayList;


import service.BackgroundService;
import service.ServiceLocations;

import myJsonData.MyJSONData;

import adapter.ChatAdapter;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.myaccessbox.appcore.R;
import config.StaticConfig;
import db.ChatDataSource;
import db.TableContract.ChatDB;
import db.UserDetailsDataSource;
import db.TableContract.UserDB;
import dialog.MyPickerDialogFrag;
import entity.ChatMessages;
import android.support.v4.app.Fragment;

public class ChatCEOFragment extends MyFragment 
				implements View.OnClickListener, 
					MyPickerDialogFrag.onSendResultListener {
	
	public static final String CHAT_NOTIFICATION_IDENTIFIER = "NewChatByCEO";
	
	TextView tvTitle;
	TextView tvExtra;
	ListView listMessages;
	TextView tvListEmptyNote;
	
	ImageView takePhotoButton;
	EditText chatCompose;
	ImageView sendButton;
	
	LinearLayout chatCityChanger;
	TextView chatCityName;
	TextView chatCityChangeButton;

	ChatDataSource chatDatasource;
	
	private static final String TAG = "ChatCEOFragment";
	private static final int ROLE_ID = ChatMessages.SENDER_CEO;
	private static final int REQUEST_CODE_CHAT_CITY_CHOICE = 744; 

/*
	private ArrayList<ChatMessages> chatData = new ArrayList<ChatMessages>() { 
		private static final long serialVersionUID = 1L;
		{
			add(new ChatMessages(1, ChatMessages.SENDER_USER, "Hi,", "25 Nov 2012"));
			add(new ChatMessages(2, ChatMessages.SENDER_USER, "Attractive discounts on all accessories for Maruti Swift and Dezire!", "25 Oct 2012"));
			add(new ChatMessages(3, ChatMessages.SENDER_CEO, "Refer a friend and get 10% off on your next paid service.", "25 Aug 2012"));
			add(new ChatMessages(4, ChatMessages.SENDER_USER, "Wish you and your family a very happy Independence Day!", "15 Aug 2012"));
			add(new ChatMessages(5, ChatMessages.SENDER_CEO, "Your car is fully serviced and ready for pick-up.", "25 Jan 2012"));
			add(new ChatMessages(6, ChatMessages.SENDER_CEO, "Your next service is due in 2 days. Please contact our helpline to set up an appointment.", "5 Jan 2012"));
			add(new ChatMessages(7, ChatMessages.SENDER_USER, "Attractive discounts on all accessories for Maruti Swift and Dezire!", "25 Dec 2011"));
			add(new ChatMessages(8, ChatMessages.SENDER_USER, "Refer a friend and get 10% off on your next paid service.", "25 Nov 2011"));
			add(new ChatMessages(9, ChatMessages.SENDER_USER, "Wish you and your family a very happy Festive Season!", "15 Oct 2011"));
			add(new ChatMessages(10, ChatMessages.SENDER_CEO, "Your car is fully serviced and ready for pick-up.", "25 Sep 2011"));
			add(new ChatMessages(11, ChatMessages.SENDER_SERVICE_ADVISOR, "Your car is currently at the washing bay, 5th in queue.", "17 Aug 2011"));
			add(new ChatMessages(12, ChatMessages.SENDER_USER, "Attractive discounts on all accessories for Maruti Omni and Alto!", "3 Jul 2011"));
			add(new ChatMessages(13, ChatMessages.SENDER_USER, "Refer a friend and get 10% off on your next paid service.", "29 Jun 2011"));
			add(new ChatMessages(14, ChatMessages.SENDER_CEO, "Wish you and your family a very happy Republic Day!", "26 Jan 2011"));
			add(new ChatMessages(15, ChatMessages.SENDER_USER, "Your car is fully serviced and ready for pick-up.", "25 Jan 2011"));
			add(new ChatMessages(16, ChatMessages.SENDER_CEO, "Your next service is due in 2 days. Please contact our helpline to set up an appointment.", "5 Jan 2011"));
			add(new ChatMessages(17, ChatMessages.SENDER_USER, "Happy New Year! Usher in the happiness, drive home a Maruti vehicle.", "1 Jan 2011"));
		}
	};
*/	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		
		View v = inflater.inflate(R.layout.frag_chat, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		listMessages = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		
		takePhotoButton = (ImageView) v.findViewById(R.id.take_photo_button);
		chatCompose = (EditText) v.findViewById(R.id.chat_message_edit);
		sendButton = (ImageView) v.findViewById(R.id.chat_message_send);
		
		chatCityChanger = (LinearLayout) v.findViewById(R.id.chat_citi_pick_holder);
		chatCityName = (TextView) v.findViewById(R.id.chat_city_name);
		chatCityChangeButton = (TextView) v.findViewById(R.id.chat_city_change_button);
		if (null == StaticConfig.LOCS_CHAT_CEO || StaticConfig.LOCS_CHAT_CEO.length == 0) {
			 ServiceLocations.setLocations(getActivity());
		 }
		if(!StaticConfig.FIELD_OWN_MESSAGE_REPLY_TO_CEO.equals("") && MessageDetailFragment.msgCopyToCEO){
			chatCompose.setText(StaticConfig.FIELD_OWN_MESSAGE_REPLY_TO_CEO);
			MessageDetailFragment.msgCopyToCEO =false;
		}
		
		if (StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED) {
			chatCityChanger.setVisibility(View.VISIBLE);
			chatCityName.setText("");
			chatCityChangeButton.setOnClickListener(this);
		}
		else {
			chatCityChanger.setVisibility(View.GONE);
		}
		
		sendButton.setOnClickListener(this);
		
		listMessages.setDividerHeight(0);
		/*
		listMessages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				parentActivity.onFragmentReplaceRequest(MessageDetailFragment.newInstance(chatData[arg2]), true);
			}
		});
		*/
		
		tvTitle.setText("Talk to the CEO");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.talk_ceo, 0, 0, 0);
		toolbarListener.setText("Feedback");
		
		tvExtra.setVisibility(View.GONE);
		takePhotoButton.setVisibility(View.GONE);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		chatDatasource = new ChatDataSource(getActivity());
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_CEO_CHAT);
		cleverTap.event.push("Opened Talk to CEO Screen");
		
		LocalBroadcastManager.getInstance(getActivity())
				.registerReceiver(broadcastReceiver, 
								new IntentFilter(BackgroundService.INTENT_FILTER_CHAT_UPDATE_ACTION));
		
		BackgroundService.setChatRoleObserver(ROLE_ID);
		
		//getActivity().stopService(new Intent(getActivity(), BackgroundService.class));
		
		Intent i = new Intent(getActivity(), BackgroundService.class);
		i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_EXPRESS);
		getActivity().startService(i);
		
		/*
		msgData[2].setReadFlag(true);
		msgData[4].setReadFlag(true);
		msgData[5].setReadFlag(true);
		msgData[6].setReadFlag(true);
		msgData[7].setReadFlag(true);
		msgData[8].setReadFlag(true);
		msgData[13].setReadFlag(true);
		msgData[14].setReadFlag(true);
		
		int numUnread = 1 + (int) (Math.random() * 19);
		*/
		
		//chatData = MessagesData.getMessagesFromFile(getActivity());
		
		parentActivity.onTabBarVisiblityChangeRequest(false);
		
		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity)
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(CHAT_NOTIFICATION_IDENTIFIER, 1);
		
		refreshListView();
	}
	
	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiver);
		
		BackgroundService.unsetChatRoleObserver();
		
		//getActivity().stopService(new Intent(getActivity(), BackgroundService.class));
		getActivity().startService(new Intent(getActivity(), BackgroundService.class));

		super.onPause();
	}

	@Override
	public void onClick(View v) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		if (v.getId() == R.id.chat_message_send) {
			String msg = chatCompose.getText().toString().trim();
			chatCompose.setText("");//reset textbox

			if (!msg.equalsIgnoreCase("")) {
				boolean doSendMessage = true;
				String city = "";

				if (StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED) {
					//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN); 
					//city = ownData.fetchData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME);
					city = userDetailDataSource.getCEOChatCityName().trim();
					if (city.equalsIgnoreCase("")) {
						Toast.makeText(getActivity(), "Please set a city of your choice and then send this message!", Toast.LENGTH_SHORT).show();
						chatCompose.setText(msg);
						doSendMessage = false;
					}
				}

				if (doSendMessage) {
					tracker.send(GATrackerMaps.EVENT_CEO_CHAT_SEND);
					
					ContentValues cv = new ContentValues();
					cv.put(ChatDB.COL_CHAT_MSG, msg);
					cv.put(ChatDB.COL_CHAT_SENDER, ChatMessages.SENDER_USER);
					cv.put(ChatDB.COL_CHAT_RECEIVER, ROLE_ID);
					cv.put(ChatDB.COL_CHAT_READ_FLAG, 1); //user sent message is always read by user!
					cv.put(ChatDB.COL_CHAT_TYPE, 0);
					cv.put(ChatDB.COL_CHAT_CITY, city);
					
					long reqId = chatDatasource.insert(cv);
				}				
				refreshListView();
			}
			else {
				Toast.makeText(getActivity(), "Please enter your message to send!", Toast.LENGTH_SHORT).show();
			}
		}
		else if (v.getId() == R.id.chat_city_change_button) {
			if(!MyPickerDialogFrag.isPickerDialogOpen() && null!=StaticConfig.LOCS_CHAT_CEO && StaticConfig.LOCS_CHAT_CEO.length>0) {
			DialogFragment pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_CHAT_CEO_CITY_LIST_PICKER);
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_CHAT_CITY_CHOICE);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "listPicker");
			}else if(StaticConfig.LOCS_CHAT_CEO==null || StaticConfig.LOCS_CHAT_CEO.length==0){
				Toast.makeText(this.getActivity(), "Fetching locations..Please wait for sometime", Toast.LENGTH_SHORT).show();
				
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void refreshListView() {
		//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		/*if (!StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED 
				|| (ownData.keyNameExistsInData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME) 
					&& !ownData.fetchData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME).equalsIgnoreCase(""))) {*/
		if (!StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED 
				|| (!userDetailDataSource.getCEOChatCityName().trim().equalsIgnoreCase(""))) {
			//chatCityName.setText(ownData.fetchData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME));
			chatCityName.setText(userDetailDataSource.getCEOChatCityName().trim());
			chatCityChangeButton.setText("Change Location");
		
			ArrayList<ChatMessages> chatData = chatDatasource.selectForRole(ROLE_ID);
		
			if (chatData.size() > 0) {
				listMessages.setVisibility(View.VISIBLE);
				tvListEmptyNote.setVisibility(View.GONE);
				ChatAdapter mListAdapter = new ChatAdapter(getActivity(),getActivity().getLayoutInflater(), 
						R.layout.chat_row_left, R.layout.chat_row_right, chatData.toArray(new ChatMessages[chatData.size()]), false);
				
				listMessages.setAdapter(mListAdapter);
				listMessages.setSelection(chatData.size() - 1);
			}
			else {
				listMessages.setVisibility(View.GONE);
				tvListEmptyNote.setText("Send your feedback directly to the CEO!");
				tvListEmptyNote.setVisibility(View.VISIBLE);
			}

			chatDatasource.markAllReadForRole(ROLE_ID);
		}
		else {
			chatCityName.setText("--");
			chatCityChangeButton.setText("Set Location");
			
			DialogFragment pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_CHAT_CEO_CITY_LIST_PICKER);
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_CHAT_CITY_CHOICE);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "listPicker");
		}
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(getActivity(), "Broadcastrecceiver Called", 0).show();
			
			refreshListView();
		}
	};

	@Override
	public void onSendResult(String result, int requestCode) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		if (requestCode == REQUEST_CODE_CHAT_CITY_CHOICE) {
			/*MyJSONData.editMyData(getActivity(), 
					new String[] {MyJSONData.FIELD_OWN_CHAT_CITY_NAME}, 
					new String[] {result}, MyJSONData.TYPE_OWN);*/
			ContentValues cv = new ContentValues();
			cv.put(UserDB.COL_FIELD_OWN_CEO_CHAT_CITY_NAME, result.trim());
			userDetailDataSource.updateUserDetails(cv);
			
			
			refreshListView();
		}
	}
}
