package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import myJsonData.MyJSONData;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myaccessbox.appcore.R;

import config.StaticConfig;
import db.UserDetailsDataSource;

public class PaymentFragment extends MyFragment implements View.OnClickListener {

	EditText paymentEditText;
	TextView chooseBrand;
	TextView payButton;
	EditText regNumber;
	
	String choosenBrandName = "";
	
	WebView webView;
	
	LinearLayout payButtonll;
	
	LinearLayout webViewll;
	
	String url = "";

	public static final String API_PAY_NOW_BASE = "http://www.5shells.com/cms/";
	public static final String API_PAY_NOW_ENDPOINT = "/gateway.php?nb44m0=nw&gb2o09=0";
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tracker.send(GATrackerMaps.VIEW_PAYMENT);
		
		View v = inflater.inflate(R.layout.frag_payment, container, false);
		
		paymentEditText = (EditText) v.findViewById(R.id.pay_amount_edit);
		
		chooseBrand = (TextView) v.findViewById(R.id.select_brand);
		
		payButton = (TextView) v.findViewById(R.id.pay_now_button);
		
		regNumber = (EditText) v.findViewById(R.id.reg_number);
		
		webView = (WebView) v.findViewById(R.id.webView1);
		
		payButtonll = (LinearLayout) v.findViewById(R.id.pay_button_layout);
		webViewll = (LinearLayout) v.findViewById(R.id.webview_layout);
		payButton.setOnClickListener(this);
		
		if(StaticConfig.FEATURE_MULTI_BRAND_ENABLED) {
			chooseBrand.setVisibility(View.VISIBLE);
			chooseBrand.setOnClickListener(this);
		}
		else {
			chooseBrand.setVisibility(View.GONE);
		}
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		super.onResume();
		//MyJSONData own = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		String reg_num = userDetailDataSource.getRegNum().trim();
		if(!reg_num.equalsIgnoreCase("")) {
			regNumber.setText(reg_num);
		}
		
		choosenBrandName = StaticConfig.DEALER_CMS_VALUE;
		/*
		String brand = own.fetchData(MyJSONData.FIELD_OWN_BRAND_NAME);
		StaticMultiBrandConfig.BrandObject obj = StaticMultiBrandConfig.getBrandObjectByName(brand);
		if (obj != null) {
			chooseBrand.setText(obj.getDisplayName());
			choosenBrandName = obj.getBrandEndpoint();
		}
		*/
		toolbarListener.setText("Pay Now");
	}

	@Override
	public void onClick(View v) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		switch(v.getId()) {	
		case R.id.pay_now_button:
			
			if (choosenBrandName.trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please choose a brand name!", Toast.LENGTH_LONG).show();
				return;
			} 
			if (paymentEditText.getText().toString().trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please enter the payment amount", Toast.LENGTH_LONG).show();
				return;
			} 
			
			try {
				float amountInFloat = Float.parseFloat(paymentEditText.getText().toString().trim());
				if ((amountInFloat) < 1) {
	                Toast.makeText(getActivity(), "Amount should not be less than Rs 1.00", Toast.LENGTH_LONG).show();
	                return;
	            }
			}
			catch (NumberFormatException e){
				Toast.makeText(getActivity(), "Please enter a valid payment amount!", Toast.LENGTH_LONG).show();
				return;
			}
			
			if (regNumber.getText().toString().trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please enter the registration Number ", Toast.LENGTH_LONG).show();
				return;
			}
			else if(regNumber.length() < 4){
				Toast.makeText(getActivity(), "Registration Number too short!", Toast.LENGTH_LONG).show();
				return;
			}
				
			try {
				MyJSONData own = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);

				url = API_PAY_NOW_BASE + choosenBrandName.trim() + API_PAY_NOW_ENDPOINT;
				url += "&fgb3x4=" + URLEncoder.encode(paymentEditText.getText().toString(), "UTF-8");   
				url += "&rsl32v=" + URLEncoder.encode(regNumber.getText().toString(), "UTF-8");
				url += "&m43nfg=" + userDetailDataSource.getUserOwnNumber();
			} catch (UnsupportedEncodingException e) {
				Log.e("PaymentFragment", e.getMessage());
			}
					
			payButtonll.setVisibility(View.GONE);
			startWebView(url);
			webView.setVisibility(View.VISIBLE);
			webViewll.setVisibility(View.VISIBLE);
			
		    break;
		/*
		case R.id.select_brand:
			ArrayList<String> items = new ArrayList<String>();
			for (int i = 0; i < StaticMultiBrandConfig.BRAND_NAMES.size(); i ++) {
				items.add(StaticMultiBrandConfig.BRAND_NAMES.get(i).getDisplayName());
			}
			String [] itemsArr = new String[items.size()];
			itemsArr = items.toArray(itemsArr);

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Choose Brand");
			builder.setItems(itemsArr, new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int id) {
			    	choosenBrandName = StaticMultiBrandConfig.BRAND_NAMES.get(id).getBrandEndpoint();
			    	Log.d("MyTag", choosenBrandName);
			    	
			    	chooseBrand.setText(StaticMultiBrandConfig.BRAND_NAMES.get(id).getDisplayName());
			    }
			});
			builder.show();
			break;
		*/
		}
	}
	
	private void startWebView(String url2) {
		webView.setWebViewClient(new WebViewClient() {
			ProgressDialog progressDialog;
			
			//If you will not use this method url link will be  open in new browser not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {              
	            view.loadUrl(url);
	            return true;
	        }
			 //Show loader on url load
	        public void onLoadResource (WebView view, String url) {
	            if (progressDialog == null) {
	                // in standard case YourActivity.this
	                progressDialog = new ProgressDialog(getActivity());
	                progressDialog.setMessage("Loading...");
	                progressDialog.show();
	            }
	        }
	        public void onPageFinished(WebView view, String url) {
	            try{
	            if (progressDialog.isShowing()) {
	                progressDialog.dismiss();
	                progressDialog = null;
	            }
	            }catch(Exception exception){
	                exception.printStackTrace();
	            }
	        }
		});	

		// Javascript enabled on webview  
	    webView.getSettings().setJavaScriptEnabled(true); 
	    webView.getSettings().setLoadWithOverviewMode(true);
	    webView.getSettings().setUseWideViewPort(true);
	    webView.loadUrl(url);
			
	}	

	public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
        }
    }
}