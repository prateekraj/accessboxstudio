package fragment;

import entity.PaymentHistoryDataPoint;
import googleAnalytics.GATrackerMaps;
import service.BackgroundService;
import utils.MyIO;
import utils.Preference;

import java.util.Iterator;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.myaccessbox.appcore.R;

import activity.PaymentWebPage;
import adapter.PaymentHistoryListAdapter;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import db.UserDetailsDataSource;

public class PaymentHistoryFragment extends MyFragment{
	
	public static final String PAYMENT_DATA_FILE = "payments_data.txt";
	public static final String PAYMENT_JSON_ARRAY_KEY_NAME = "payments_history";
	ListView listView;
	TextView tvListEmptyNote;
	
	PaymentHistoryDataPoint paymentData[];
	JSONObject jObj;
	JSONArray jArray;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tracker.send(GATrackerMaps.VIEW_PAYMENT_HISTORY);
		View v = inflater.inflate(R.layout.frag_payment_history, container, false);
		paymentData = new PaymentHistoryDataPoint[0];
		listView = (ListView) v.findViewById(R.id.listpayment);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}
	
	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiver);
		
		BackgroundService.setPaymentHistoryRoleObserver(false);
		super.onPause();
	}
	
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(getActivity(), "Broadcastrecceiver Called", 0).show();
			
			refreshRewardListView();
		}
	};
	
	@Override
	public void onResume() {
		
		LocalBroadcastManager.getInstance(getActivity())
		        .registerReceiver(broadcastReceiver, 
					new IntentFilter(BackgroundService.INTENT_FILTER_PAYMENT_HISTORY_UPDATE_ACTION));
		
		BackgroundService.setPaymentHistoryRoleObserver(true);
		
		Intent i = new Intent(getActivity(), BackgroundService.class);
		i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_EXPRESS);
		getActivity().startService(i);
		
		refreshRewardListView();
		super.onResume();
	}
	
	protected void refreshRewardListView() {
		toolbarListener.setText("Payment History");
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		paymentData = getPaymentHistoryData(getActivity());
		
		if(userDetailDataSource.isPhoneNumberExist()){
		if (paymentData.length > 0) {
			listView.setVisibility(View.VISIBLE);
			tvListEmptyNote.setVisibility(View.GONE);
			PaymentHistoryListAdapter rListAdapter = new PaymentHistoryListAdapter(getActivity(), R.layout.row_payment_history, paymentData);
			listView.setAdapter(rListAdapter);
		}else {
			nothingToDisplay();
		}} 
		else {
			nothingToDisplay();
		}
		
	}

	private void nothingToDisplay() {
		listView.setVisibility(View.GONE);
		tvListEmptyNote.setVisibility(View.VISIBLE);
		tvListEmptyNote.setText("No payment history data to display!");
		
	}

	private PaymentHistoryDataPoint[] getPaymentHistoryData(Context ctxt) {
		PaymentHistoryDataPoint paymentData[] = new PaymentHistoryDataPoint[0];
				
		JSONArray histArray = MyIO.readJSONArrayFromFile(getActivity(), PAYMENT_DATA_FILE, PAYMENT_JSON_ARRAY_KEY_NAME);
		
		if (histArray == null) {//history empty or unavailable
			return paymentData;
		}
		
		JSONObject histObj;
		
		try {
			paymentData = new PaymentHistoryDataPoint[histArray.length()];
			for (int i = 0; i < histArray.length(); i ++) {
				histObj = histArray.getJSONObject(i);
				
				paymentData[i] = new PaymentHistoryDataPoint(histObj.getString("transaction_types"), histObj.getString("transaction_amounts") , 
						histObj.getString("transaction_timestamps"));
				
			}
		} catch (JSONException e) {
			//will eventually return empty array
		}
		return paymentData;
	}
	
	/*private PaymentHistoryDataPoint[] getPaymentHistoryData(Context ctxt) {
		PaymentHistoryDataPoint paymentData[] = new PaymentHistoryDataPoint[0];
		try {
			JSONObject histObj = new JSONObject(resp);
			JSONArray histArray = new JSONArray();
			histArray.put(histObj);
			Iterator x = histObj.keys();
			while (x.hasNext()) {
				String key = (String) x.next();
				histArray.put(histObj.get(key));
				
			}
			paymentData = new PaymentHistoryDataPoint[histArray.length()];
			for (int i = 0; i < histArray.length(); i ++) {
				histObj = histArray.getJSONObject(i);
				
				paymentData[i] = new PaymentHistoryDataPoint(histObj.getString("TxMsg"), histObj.getString("amount") , 
						histObj.getString("txnDateTime"));
				
			}
		} catch (JSONException e) {
			//will eventually return empty array
		}
		return paymentData;
	}*/
	
	public static boolean appendPaymenttToDisk(Context ctxt, JSONArray newPaymentArray) {
		JSONArray paymentOnDisk = MyIO.readJSONArrayFromFile(ctxt, PAYMENT_DATA_FILE, PAYMENT_JSON_ARRAY_KEY_NAME);
		JSONArray resultArray = new JSONArray();

		try {
			for (int i = 0; i < newPaymentArray.length(); i ++) {
				resultArray.put(newPaymentArray.get(i));
					
			}
			for (int i = 0; i < paymentOnDisk.length(); i ++) {
				resultArray.put(paymentOnDisk.get(i));
					
			}
		} catch (JSONException e) {
			return false;
		}
		
		return MyIO.writeJSONArrayToFile(ctxt, PAYMENT_DATA_FILE, resultArray, PAYMENT_JSON_ARRAY_KEY_NAME);
	}
}
