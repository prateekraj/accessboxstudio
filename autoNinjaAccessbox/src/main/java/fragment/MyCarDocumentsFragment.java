package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import utils.Utils;
import myJsonData.MyJSONData;

import com.myaccessbox.appcore.R;

import config.StaticConfig;
import config.ConfigInfo.Hotline.GroupEntity;

import db.CarDetailsDatasource;
import db.DatabaseHelper;
import db.UserDetailsDataSource;
import db.TableContract.CarDetailsDB;
import db.TableContract.UserDB;
import dialog.MyPickerDialogFrag;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class MyCarDocumentsFragment extends MyHotlineMultiSelectFragment
		implements View.OnClickListener,
		MyPickerDialogFrag.onSendResultListener {

	public static final int REQUEST_CODE_INSURANCE_SET_DATE = 17;
	public static final int REQUEST_CODE_PUC_SET_DATE = 23;
	public static final int REQUEST_CODE_CLICK_PHOTO = 7;
	public static final int REQUEST_CODE_GALLERY_PHOTO = 8;
	ProgressDialog progressDialog;

	private static String TAG = MyCarDocumentsFragment.class.getSimpleName();
	private static String DOC_TYPE_KEY = "document_type";

	public static final int DOCUMENT_TYPE_INSURANCE = 234;
	
	public static final int DOCUMENT_TYPE_PUC = 567;

	public CarDetailsDatasource carDetailSource;

	private int currentDocument = 0;
	protected Dialog dlgChangeCar;

	TextView documentTitle;
	TextView documentTeaser;
	TextView expiryRemark;
	TextView expiryDate;
	TextView editExpiryButton;
	TextView renewButton,textNoCar;
	ImageView documentImage;
	// TextView car_chosen;
	// TextView car_choose;
	Spinner carList;
	LinearLayout carSpinnerLL,carDocsLLyt;
	ArrayAdapter<String> dataAdapter;
	TextView take_photo;
	MyJSONData ownData;

	String documentBaseName = "";
	private String pucPhotoPath = "";
	private String insPhotoPath = "";

	public static MyCarDocumentsFragment newInstance(int docType) {
		MyCarDocumentsFragment frag = new MyCarDocumentsFragment();
		Bundle b = new Bundle();
		b.putInt(DOC_TYPE_KEY, docType);

		frag.setArguments(b);
		// Log.d(TAG, "custom newInstance called with doctype: " + docType);
		return frag;
	}

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		state = getArguments();

		if (state != null) {
			currentDocument = state.getInt(DOC_TYPE_KEY);
		} else {
			// Log.e(TAG, "Wrong instance!");
		}
		// Log.d(TAG, "currentDocument: " + currentDocument);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);

		// set title
		// actionBarTitleView.setText("My Car");

		// enable/disable backbutton
		// setActionBarBackEnabled(true);

		// Log.d(TAG, "OnAttach Called! Now");
		
	}
	// UnComment this only if you want extra menu option on toolbar
	/*@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}*/
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itme_id = item.getItemId();
		switch(itme_id){
		case R.id.driver_license:
			Toast.makeText(getActivity(), "You Clicked Driving License", 0).show();
            return true;

        case R.id.insurance_policy:
            // User chose the "Favorite" action, mark the current item
            // as a favorite...
        	Toast.makeText(getActivity(), "You Clicked Insurance Policy", 0).show();
            return true;
            
        case R.id.rc_book:
        	Toast.makeText(getActivity(), "You Clicked RC Book", 0).show();
        	return true;
        	
        case R.id.puc_overflow:
        	Toast.makeText(getActivity(), "You Clicked PUC", 0).show();
        	return true;
        
        case R.id.others_overflow:
        	Toast.makeText(getActivity(), "You Clicked Others", 0).show();
        	return true;	
        	
        case R.id.action_delete:
        	Toast.makeText(getActivity(), "Do you really want to delete??", 0).show();
        	return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Log.d(TAG, "OnCreateView Called! Now!!!");
		View v = inflater.inflate(R.layout.frag_mycar_documents, container,
				false);

		documentImage = (ImageView) v.findViewById(R.id.document_img);

		documentTitle = (TextView) v.findViewById(R.id.document_title);
		documentTeaser = (TextView) v.findViewById(R.id.document_teaser);

		expiryDate = (TextView) v.findViewById(R.id.date_tv);
		expiryRemark = (TextView) v.findViewById(R.id.remark_tv);

		editExpiryButton = (TextView) v.findViewById(R.id.edit_expiry_date);
		renewButton = (TextView) v.findViewById(R.id.renew_button);

		take_photo = (TextView) v.findViewById(R.id.take_photo_textview);
		carList = (Spinner) v.findViewById(R.id.car_list);
		carSpinnerLL = (LinearLayout) v.findViewById(R.id.select_car_layout);
		textNoCar = (TextView) v.findViewById(R.id.textNoCar);
		carDocsLLyt = (LinearLayout) v.findViewById(R.id.carDocsLLyt);
		
		//checking for empty car list
		if (StaticConfig.myCarList.isEmpty()) {
			carDetailSource = new CarDetailsDatasource(
					getActivity());
			if (carDetailSource.getCarList() > 0) {
				carDocsLLyt.setVisibility(View.VISIBLE);
				textNoCar.setVisibility(View.GONE);
			} else {
				carDocsLLyt.setVisibility(View.GONE);
				textNoCar.setVisibility(View.VISIBLE);
				textNoCar.setText(R.string.no_car_added);
			}
		} 
		dataAdapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_spinner_multiplecar,
				StaticConfig.myCarListNames);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		carList.setAdapter(dataAdapter);
		carList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String chosenModelName = parent.getItemAtPosition(position)
						.toString();
				StringTokenizer st = new StringTokenizer(chosenModelName, ",");

				String model = st.nextToken();
				String regNum = st.nextToken().trim();
				for (int i = 0; i < StaticConfig.myCarList.size(); i++) {

					if (StaticConfig.myCarList.get(i).getModelName()
							.equalsIgnoreCase(model)
							&& StaticConfig.myCarList.get(i).getReg_num()
									.equalsIgnoreCase(regNum)) {
						StaticConfig.myCar = StaticConfig.myCarList.get(i);
						StaticConfig.position = position;
						UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(
	      						getActivity());
	            		  ContentValues cv = new ContentValues();
	            		  cv.put(UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION, position);
	            		  userDetailDataSource.updateUserDetails(cv);
						// dbhelper.getCarList();
					}
				}
				setSpinnerData();
				populateData();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		take_photo.setOnClickListener(this);
		editExpiryButton.setOnClickListener(this);
		documentImage.setOnClickListener(this);
		// car_choose.setOnClickListener(this);

		boolean hideRenewButton = true;
		switch (currentDocument) {
		case DOCUMENT_TYPE_INSURANCE:
			if (!StaticConfig.INSURANCE_HOTLINE_SELECTOR.isEmpty()) {
				hideRenewButton = StaticConfig.INSURANCE_HOTLINE_SELECTOR
						.get(0).getNumber().equalsIgnoreCase("");
			}
			break;
		case DOCUMENT_TYPE_PUC:
			if (!StaticConfig.PUC_HOTLINE_SELECTOR.isEmpty()) {
				hideRenewButton = StaticConfig.PUC_HOTLINE_SELECTOR.get(0)
						.getNumber().equalsIgnoreCase("");
			}
			break;
		}

		renewButton.setVisibility(View.VISIBLE);
		renewButton.setOnClickListener(this);
		/*
		 * if (!hideRenewButton) {//Call_number exists!
		 * renewButton.setVisibility(View.VISIBLE);
		 * renewButton.setOnClickListener(this); } else {
		 * renewButton.setVisibility(View.GONE); }
		 */

		return v;
	}

	public void setSpinnerData() {
		if (StaticConfig.myCarListNames.size() <= 0) {
			carList.setVisibility(View.GONE);
			carSpinnerLL.setVisibility(View.GONE);
		} else {
			dataAdapter.notifyDataSetChanged();
			carList.setSelection(StaticConfig.position);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		cleverTap.event.push("Opened Go Paperless Screen");
		getActivity().getTheme().applyStyle(R.style.ToolbarOverflow_Theme, true);
		setSpinnerData();
		// ownData = new MyJSONData (getActivity(),
		// MyJSONData.TYPE_OWN);
		carDetailSource = new CarDetailsDatasource(this.getActivity());
		switch (currentDocument) {
		case DOCUMENT_TYPE_INSURANCE:
			tracker.send(GATrackerMaps.VIEW_MYCAR_DOCS_INSURANCE);
			StaticConfig.TYPE_OF_DOC = "Insurance";
			break;
		case DOCUMENT_TYPE_PUC:
			tracker.send(GATrackerMaps.VIEW_MYCAR_DOCS_PUC);
			StaticConfig.TYPE_OF_DOC = "PUC";
			break;
		}
		// TODO: cancel notification once Background Service starts creating
		// one!
		// NotificationManager nm = (NotificationManager)
		// ((FragmentActivity)
		// parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
		// nm.cancel("MyCarDocuments", 1);

		// first, check if there exists any car file or not
		// (missing car file can occur when 'go-paperless' launcher is clicked
		// from home
		// even when car data was not found or still searching
		if (StaticConfig.myCar.getCarId() > 0) {
			// car data file exists, so display set date button here
			editExpiryButton.setVisibility(View.VISIBLE);
		} else {
			// no car data file, so no point asking to set date here
			editExpiryButton.setVisibility(View.GONE);
		}

		populateData();
	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.edit_expiry_date:
			MyPickerDialogFrag pickerFragDialog = MyPickerDialogFrag
					.newInstance(MyPickerDialogFrag.TYPE_DATE_PICKER);
			switch (currentDocument) {
			case DOCUMENT_TYPE_INSURANCE:
				tracker.send(GATrackerMaps.EVENT_CAR_DOCS_EDIT_DATE_INSURANCE);
				pickerFragDialog.setTargetFragment(this,
						REQUEST_CODE_INSURANCE_SET_DATE);
				if (!MyPickerDialogFrag.isPickerDialogOpen()) {
					pickerFragDialog.show(getActivity()
							.getSupportFragmentManager(), "insuDatePicker");
				}
				break;
			case DOCUMENT_TYPE_PUC:
				tracker.send(GATrackerMaps.EVENT_CAR_DOCS_EDIT_DATE_PUC);
				pickerFragDialog.setTargetFragment(this,
						REQUEST_CODE_PUC_SET_DATE);
				if (!MyPickerDialogFrag.isPickerDialogOpen()) {
					pickerFragDialog.show(getActivity()
							.getSupportFragmentManager(), "pucDatePicker");
				}
				break;
			}
			break;
		case R.id.renew_button:
			final DatabaseHelper dbHelper = new DatabaseHelper(
					getActivity());
			DialogFragment hotlinePickerFragDialog;
			i = null;
			ArrayList<GroupEntity> numberList = null;
			String pickerLabel = "";
			String expiryDate = "";
			switch (currentDocument) {
			case DOCUMENT_TYPE_INSURANCE:
				tracker.send(GATrackerMaps.EVENT_CAR_INSURANCE_CALL);

				numberList = StaticConfig.INSURANCE_HOTLINE_SELECTOR;
				pickerLabel = "Renew Insurance";
				String insuranceExp = carDetailSource.getInsuranceExpiry(
						StaticConfig.myCar.getCarId());
				if (!insuranceExp.equalsIgnoreCase("") || !insuranceExp.equalsIgnoreCase("0000-00-00")) {
					expiryDate = insuranceExp;
				} else {
					Toast.makeText(getActivity(),
							"you can't proceed without expiry date", 0).show();
					return;
				}
				break;
			case DOCUMENT_TYPE_PUC:
				tracker.send(GATrackerMaps.EVENT_CAR_PUC_CALL);

				numberList = StaticConfig.PUC_HOTLINE_SELECTOR;
				pickerLabel = "Renew PUC";
				String pucExp = carDetailSource
						.getPucExpiry(StaticConfig.myCar.getCarId());
				if (!pucExp.equalsIgnoreCase("") || !pucExp.equalsIgnoreCase("0000-00-00")) {
					/*expiryDate = MyJSONData
							.getServerFormateDate(pucExp);*/
					expiryDate = pucExp;
				} else {
					Toast.makeText(getActivity(),
							"you can't proceed without expiry date", 0).show();
					return;
				}
				break;
			}

			if (Utils.isNetworkConnected(getActivity())) {
				renewDocument(pickerLabel, expiryDate);
			} else {
				Toast.makeText(getActivity(),
						getResources().getString(R.string.network_msg), 0)
						.show();
			}
			break;
		case R.id.document_img:
			switch (currentDocument) {
			case DOCUMENT_TYPE_INSURANCE:
				tracker.send(GATrackerMaps.EVENT_CAR_DOCS_INSURANCE_VIEW_DOC);
				// to view the Insurance image if it exists
				if (!insPhotoPath.equalsIgnoreCase("")) {
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_VIEW);
					Uri imgUri = Uri.parse("file://" + insPhotoPath);
					intent.setDataAndType(imgUri, "image/*");
					startActivity(intent);
				}

				break;
			case DOCUMENT_TYPE_PUC:
				tracker.send(GATrackerMaps.EVENT_CAR_DOCS_PUC_VIEW_DOC);
				if (!pucPhotoPath.equalsIgnoreCase("")) {
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_VIEW);
					Uri imgUri = Uri.parse("file://" + pucPhotoPath);
					intent.setDataAndType(imgUri, "image/*");
					startActivity(intent);
				}
				break;
			}
			if (!documentBaseName.equalsIgnoreCase("")) {
				i = new Intent(Intent.ACTION_VIEW);
				// File f =
				// getActivity().getFileStreamPath(documentBaseName +
				// ".pdf");
				File f = new File(MyCarDocumentsFragment.getFilesDir(),
						documentBaseName + ".pdf");
				i.setDataAndType(Uri.fromFile(f), "application/pdf");
				try {
					startActivity(i);
				} catch (ActivityNotFoundException e) {
					// Log.e(TAG, "No PDF reader! " + e.getMessage());
					Toast.makeText(getActivity(),
							"Please install a PDF reader to view this file!",
							Toast.LENGTH_LONG).show();
				}
			} else {
				// TODO: prompt for renewal?!?
			}
			break;
		case R.id.take_photo_textview:
			if (Utils.isNetworkConnected(getActivity())) {
				switch (currentDocument) {
				case DOCUMENT_TYPE_INSURANCE:

					selectImage();

					break;
				case DOCUMENT_TYPE_PUC:

					selectImage();

					break;
				}
			} else {
				Toast.makeText(getActivity(),
						"Please check your internet connection.",
						Toast.LENGTH_LONG).show();
			}
			break;

		}
	}

	// this method will give option to select image from gallery or take a new
	// pic from camera
	private void selectImage() {
		final CharSequence imageArray[] = { "Take Photo", "From Gallery" };

		AlertDialog.Builder builder = new AlertDialog.Builder(
				getActivity());
		builder.setTitle("Choose Picture:");
		builder.setItems(imageArray, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {

				if (imageArray[item].equals("Take Photo")) {
					// Toast.makeText(getActivity(), "Take Photo!",
					// 0).show();
					Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					try {
						File photofile = createImageFile();
						i.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(photofile));
						StaticConfig.FIELD_OWN_DOC_PHOTO_ATTEMPT_PATH = photofile
								.getAbsolutePath();
						getActivity().startActivityForResult(i,
								REQUEST_CODE_CLICK_PHOTO);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(getActivity(),
								"Sorry! Camera not available.",
								Toast.LENGTH_LONG).show();
					} catch (IOException e) {
						Toast.makeText(getActivity(),
								"Problem creating image location!",
								Toast.LENGTH_LONG).show();
						// Log.d(TAG, e.getMessage());
					}
				} else if (imageArray[item].equals("From Gallery")) {

					Intent i = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

					getActivity().startActivityForResult(i,
							REQUEST_CODE_GALLERY_PHOTO);

				}
			}
		});
		builder.show();

	}

	/*
	 * Alert Dialog to renew Insurance/PUC
	 */
	private void renewDocument(String pickerLabel, final String expiryDate2) {
		AlertDialog.Builder docAlertDialog = new AlertDialog.Builder(
				getActivity());
		// insAlertDialog.setTitle("");
		docAlertDialog
				.setMessage("Do you want to " + pickerLabel)
				.setCancelable(false)
				.setPositiveButton(pickerLabel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								progressDialog = ProgressDialog.show(getActivity(), "Registering your request", "Please wait for a moment");
								// Toast.makeText(getActivity(), "Yes",
								// 0).show();
								String renewFlag = "1";
								callPostService(renewFlag, expiryDate2);
							}
						})
				.setNegativeButton("Not Interested",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// Toast.makeText(getActivity(), "No",
								// 0).show();
								String renewFlag = "0";
								callPostService(renewFlag, expiryDate2);
							}
						});
		// create alert dailog
		AlertDialog alrtDialog = docAlertDialog.create();
		alrtDialog.show();
	}

	/*
	 * Posting the details to renew Insurance/PUC
	 */
	protected void callPostService(final String renewFlag, String expiryDate2) {
		MultipartEntity mpEntity = new MultipartEntity();
		// MyJSONData own = new MyJSONData(getActivity(),
		// MyJSONData.TYPE_OWN);
		try {
			int carID = StaticConfig.myCar.getCarId();
			mpEntity.addPart("phone", new StringBody(""
					+ StaticConfig.LOGGED_PHONE_NUMBER));
			mpEntity.addPart("car_id", new StringBody("" + carID));
			mpEntity.addPart("insurance_expiry_date", new StringBody(""
					+ expiryDate2));
			mpEntity.addPart("interested_in_renewal", new StringBody(""
					+ renewFlag));
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(
					new AsyncInvokeURLTask.OnPostExecuteListener() {
						public void onPostExecute(String result) {
							try {
								JSONObject jo = new JSONObject(result);
								if(renewFlag.equalsIgnoreCase("1")){
									progressDialog.dismiss();
								Toast.makeText(getActivity(),
										jo.getString("text"), Toast.LENGTH_LONG)
										.show();
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							// System.out.println(result);
						}
					}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);

			String URL = StaticConfig.API_INSURANCE_RENEWAL + "?"
					+ StaticConfig.getCMSPass();
			task.execute(URL);
		} catch (Exception e) {
			System.out.println("Big Exception Bro");
			e.printStackTrace();
		}

	}

	private void populateData() {
		// check which role this frg is opened and read/update accordingly,
		// final DatabaseHelper dbhedlper = new
		// DatabaseHelper(getActivity());

		String expDateStr = "";
		Date expDate, today = new Date();
		File f;

		switch (currentDocument) {
		case DOCUMENT_TYPE_INSURANCE:
			toolbarListener.setText("Car Insurance");

			// expDateStr =
			// car.fetchData((MyJSONData.FIELD_CAR_INSURANCE_EXPIRY));//insurance_expiry
			expDateStr = carDetailSource.getInsuranceExpiry(StaticConfig.myCar
					.getCarId());

			documentTitle.setText("Car Insurance Policy");
			documentTeaser.setText("Renew your Insurance with "
					+ StaticConfig.DEALER_FULL_NAME
					+ " and see your policy document here!");
			renewButton.setText("Renew Insurance");
			expiryRemark.setText("Insurance expires on");
			expiryRemark.setTypeface(null, Typeface.NORMAL);
			// documentBaseName =
			// car.fetchData(MyJSONData.FIELD_CAR_INSURANCE_DOCUMENT);
			insPhotoPath = carDetailSource.getInsuranceDocument();
			documentBaseName = ""; // setting this to blank explicitly to not
									// invoke the pdf viewer code on click
			if (!insPhotoPath.equalsIgnoreCase("")) {
				documentImage.setImageBitmap(BitmapFactory
						.decodeFile(insPhotoPath));
				documentTeaser.setVisibility(View.GONE);
			} else {
				documentImage.setImageDrawable(getResources().getDrawable(
						R.drawable.dummy_document));
				documentTeaser.setVisibility(View.VISIBLE);
			}

			if (expDateStr.equalsIgnoreCase("") || expDateStr.equalsIgnoreCase("0000-00-00")) {// data not present, ask for
													// input
				editExpiryButton.setText("Set Expiry Date");
				expiryDate.setText("---");
			} else {
				editExpiryButton.setText("Edit Expiry Date");
				expiryDate.setText(MyJSONData
						.formatDateServerToDisplay(expDateStr));

				expDate = MyJSONData.parseFileDate(expDateStr);
				if (expDate != null) {
					if (expDate.before(today)) {
						expiryRemark.setText("Insurance expired on");
						expiryRemark.setTypeface(null, Typeface.BOLD);
					}
				} else {
					// Log.e(TAG, "ParseException on Insurance Date");
				}
			}

			break;
		case DOCUMENT_TYPE_PUC:
			toolbarListener.setText("Car PUC");

			// expDateStr =
			// car.fetchData((MyJSONData.FIELD_CAR_PUC_EXPIRY));//puc_expiry
			expDateStr = carDetailSource.getPucExpiry(StaticConfig.myCar
					.getCarId());
			documentTitle.setText("Car PUC Certificate");
			documentTeaser.setText("Renew your PUC with "
					+ StaticConfig.DEALER_FULL_NAME
					+ " and see your PUC Certificate here!");
			renewButton.setText("Renew PUC");
			expiryRemark.setText("PUC expires on");
			expiryRemark.setTypeface(null, Typeface.NORMAL);

			// documentBaseName =
			// car.fetchData(MyJSONData.FIELD_CAR_PUC_DOCUMENT);
			pucPhotoPath = carDetailSource.getPUCDocument();
			documentBaseName = "";
			if (!pucPhotoPath.equalsIgnoreCase("")) {
				documentImage.setImageBitmap(BitmapFactory
						.decodeFile(pucPhotoPath));
				documentTeaser.setVisibility(View.GONE);
			} else {
				documentImage.setImageDrawable(getResources().getDrawable(
						R.drawable.dummy_document));
				documentTeaser.setVisibility(View.VISIBLE);
			}

			if (expDateStr.equalsIgnoreCase("") || expDateStr.equalsIgnoreCase("0000-00-00")) {// data not present, ask for
													// input
				editExpiryButton.setText("Set Expiry Date");
				expiryDate.setText("---");
			} else {
				editExpiryButton.setText("Edit Expiry Date");
				expiryDate.setText(MyJSONData
						.formatDateServerToDisplay(expDateStr));

				expDate = MyJSONData.parseFileDate(expDateStr);
				if (expDate != null) {
					if (expDate.before(today)) {
						expiryRemark.setText("PUC expired on");
						expiryRemark.setTypeface(null, Typeface.BOLD);
					}
				} else {
					// Log.e(TAG, "ParseException on Insurance Date");
				}
			}

			break;
		}
	}

	@Override
	public void onSendResult(String result, int requestCode) {
		if (requestCode == REQUEST_CODE_HOTLINE_SUB_PICKER
				&& !result.trim().equalsIgnoreCase("")) {
			super.onSendResult(result, requestCode);
		} else {
			if (MyCarBaseFragment.processSendResult(getActivity(),
					result, requestCode, ownData)) {

				switch (requestCode) {
				case (REQUEST_CODE_INSURANCE_SET_DATE):
					tracker.send(GATrackerMaps.EVENT_CAR_SET_DATE_INSURANCE);
					break;
				case (REQUEST_CODE_PUC_SET_DATE):
					tracker.send(GATrackerMaps.EVENT_CAR_SET_DATE_PUC);
					break;
				}

				populateData();
			}
		}
	}

	public static File getFilesDir() {
		File ret = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"/Accessbox/Documents");
		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}

	public static File getFileThumbsDir() {
		File ret = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"/Accessbox/Documents/Thumbs");
		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			MyJSONData myOwn = new MyJSONData(getActivity(),
					MyJSONData.TYPE_OWN);
			String android_id = Secure.getString(getActivity()
					.getContentResolver(), Secure.ANDROID_ID);
			String mobile = "" + StaticConfig.LOGGED_PHONE_NUMBER;
			MultipartEntity mpEntity;
			String responseMessage = "";
			String finalPhotoPath = "";

			switch (requestCode) {
			case REQUEST_CODE_CLICK_PHOTO:
				finalPhotoPath = StaticConfig.FIELD_OWN_DOC_PHOTO_ATTEMPT_PATH;
				break;
			case REQUEST_CODE_GALLERY_PHOTO:
				Uri selectedImageUri = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						selectedImageUri, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				finalPhotoPath = cursor.getString(columnIndex);
				cursor.close();
				break;
			}

			if (!finalPhotoPath.equalsIgnoreCase("")) {
				boolean b = false;
				ContentValues cv = new ContentValues();
				if (StaticConfig.TYPE_OF_DOC.equalsIgnoreCase("Insurance")) {
					cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_DOCUMENT,
							finalPhotoPath);
				} else if (StaticConfig.TYPE_OF_DOC.equalsIgnoreCase("PUC")) {
					cv.put(CarDetailsDB.COL_FIELD_CAR_PUC_DOCUMENT,
							finalPhotoPath);
				}
				if (carDetailSource.updateCarDetails(cv,
						StaticConfig.myCar.getCarId()) > 0) {
					b = true;
				}
				if (b) {
					try {
						mpEntity = new MultipartEntity();
						File path = new File(finalPhotoPath);
						System.out.println(" DOCS FOR CAR: "
								+ StaticConfig.myCar.getModelName()
								+ "CAR ID: " + StaticConfig.myCar.getCarId());
						mpEntity.addPart("MOBILE_NUMBER",
								new StringBody(mobile));
						mpEntity.addPart("UPLOADED_FILE", new FileBody(path));
						mpEntity.addPart("FILE_TYPE", new StringBody(
								StaticConfig.TYPE_OF_DOC.trim()));
						mpEntity.addPart("DEVICE_ID",
								new StringBody(android_id));
						mpEntity.addPart("car_id", new StringBody(""
								+ StaticConfig.myCar.getCarId()));
						// Log.d("", "----------android_id----------" +
						// android_id+ "---path---"
						// + path + "--mobile--" + mobile +
						// StaticConfig.API_GO_PAPERLESS + "" + mpEntity);

						AsyncInvokeURLTask task = new AsyncInvokeURLTask(
								new AsyncInvokeURLTask.OnPostExecuteListener() {
									public void onPostExecute(String result) {
										System.out.println("DOC RESULT:"
												+ result);
										JSONObject docJsonObject;
										try {
											docJsonObject = new JSONObject(
													result);
											Toast.makeText(
													getActivity(),
													docJsonObject
															.getString("text"),
													Toast.LENGTH_LONG).show();
										} catch (JSONException e) {
											// TODO Auto-generated catch block
										}
									}
								}, AsyncInvokeURLTask.REQUEST_TYPE_POST,
								mpEntity);

						// Log.d(TAG, executeURL);
						String executeURL = StaticConfig.API_GO_PAPERLESS + "?"
								+ StaticConfig.getCMSPass();
						task.execute(executeURL);
						// Log.d(TAG, "execute called with url: " + executeURL);
					} catch (Exception e) {
						// TODO Auto-generated catch block
					}

					populateData();
				}
			}
		}
	}

	protected File createImageFile() throws IOException {
		// unique filename based on timestamp
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());

		String imageFileName = StaticConfig.TYPE_OF_DOC + "_" + timeStamp + "_";
		File image = File.createTempFile(imageFileName, ".jpg", getAlbumDir());
		return image;
	}

	public static File getAlbumDir() {
		File ret = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				File.separator + "Accessbox" + File.separator
						+ StaticConfig.TYPE_OF_DOC);

		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}
}
