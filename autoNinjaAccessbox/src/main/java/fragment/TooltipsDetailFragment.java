package fragment;

import entity.Tooltips;
import googleAnalytics.GATrackerMaps;
import android.app.Activity;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.widget.ImageView;
import android.widget.TextView;

import com.myaccessbox.appcore.R;
import com.squareup.picasso.Picasso;

import config.StaticConfig;

public class TooltipsDetailFragment extends MyFragment {
	
	Tooltips thisTip;
	
	TextView tvSubject;
	TextView tvTeaser;
	TextView tvBody;

	//ImageView imgViews[];
	ImageView imgViews[];
	String imgNames[];
	boolean [] errorRecvd = {false, false, false};

	public static TooltipsDetailFragment newInstance(Tooltips tip) {
		Bundle init = new Bundle();
		init.putParcelable("tooltip", tip);
		TooltipsDetailFragment frag = new TooltipsDetailFragment();
		frag.setArguments(init);
		return frag;
	}
	
	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		
		state = getArguments();
		thisTip = (Tooltips) state.getParcelable("tooltip");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_tooltip_detail, container, false);
		tvSubject = (TextView) v.findViewById(R.id.textTipsDetailSubject);
		tvTeaser = (TextView) v.findViewById(R.id.textTipsDetailTeaser);
		tvBody = (TextView) v.findViewById(R.id.textTipsDetailBody);
		
		tvSubject.setText(thisTip.getSubject());
		tvTeaser.setText(thisTip.getTeaser());
		tvBody.setText(thisTip.getBody());

		imgNames = thisTip.getImgNames();
		
		imgViews = new ImageView[3];
		imgViews[0] = (ImageView) v.findViewById(R.id.imgTipsDetailImg1);
		imgViews[1] = (ImageView) v.findViewById(R.id.imgTipsDetailImg2);
		imgViews[2] = (ImageView) v.findViewById(R.id.imgTipsDetailImg3);
		
		for (int j = 0; j < imgNames.length; j ++) {
			String urlString = StaticConfig.API_TOOLTIP_IMAGES_BASE + imgNames[j];
			// System.out.println("urlString: "+urlString);
			String url = urlString.replaceAll(" ", "%20");
			Picasso.with(getActivity()).load(url).into(imgViews[j]);
		}


		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_TOOLTIPS_DETAIL);
		cleverTap.event.push("Opened Tooltips Detail Screen");
		toolbarListener.setText(thisTip.getSubject());
		//setActionBarBackEnabled(true);
	}
}
