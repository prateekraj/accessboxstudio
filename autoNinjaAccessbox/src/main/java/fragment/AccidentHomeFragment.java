package fragment;

import myJsonData.MyJSONData;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import entity.CallForAction;
import entity.ContactHolder;
import googleAnalytics.GATrackerMaps;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myaccessbox.appcore.R;

import config.StaticConfig;

public class AccidentHomeFragment extends MyFragment implements View.OnClickListener {
	
	private static String TAG = "AccidentHomeFragment";
	
	RelativeLayout acciCriticalButton;
	RelativeLayout acciNonCriticalButton;
	RelativeLayout acciEmgContactButton;
	TextView changeEmgContactHint;
	
	AlertDialog contactPickFBIssueTryAgainDialog;
	AlertDialog changeEmgContactConfirmDialog;
	long emergencyContactNumer = 0;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_accident_home, container, false); 
		acciCriticalButton = (RelativeLayout) v.findViewById(R.id.acci_critical_launch);
		acciNonCriticalButton = (RelativeLayout) v.findViewById(R.id.acci_noncritical_launch);
		acciEmgContactButton = (RelativeLayout) v.findViewById(R.id.acci_emg_contact);
		changeEmgContactHint = (TextView) v.findViewById(R.id.change_emg_contact_hint);
		
		styleLauncherButton(acciCriticalButton, "Critical Situation", "Someone is seriously injured or car has major damage", R.drawable.goahead);
		styleLauncherButton(acciNonCriticalButton, "Non Critical Situation", "Car has minor damage", R.drawable.goahead);
		setEmgContactButton();
		
		acciCriticalButton.setOnClickListener(this);
		acciNonCriticalButton.setOnClickListener(this);
		acciEmgContactButton.setOnClickListener(this);
		acciEmgContactButton.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				if (emergencyContactNumer!=0) {					
					AlertDialog.Builder bldr = new AlertDialog.Builder(getActivity());
					bldr.setMessage("Change emergency contact?");
					bldr.setPositiveButton("Change", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							tracker.send(GATrackerMaps.EVENT_ACCIDENT_CHANGE_EMG_CONTACT);

							changeEmgContactConfirmDialog.dismiss();
							pickContact();
						}
					});
					bldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							changeEmgContactConfirmDialog.dismiss();
						}
					});
					changeEmgContactConfirmDialog = bldr.show();
					return true;
				}
				else {
					return false;
				}
				
			}
		});
		
		/*
		TextView tvTitle = (TextView) v.findViewById(R.id.frag_accident_title).findViewById(R.id._title);
		tvTitle.setText("Accident Helper");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.help, 0, 0, 0);
		*/
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_ACCIDENT_HOME);
		
		toolbarListener.setText("Accident Helper");
		//setActionBarBackEnabled(false);

	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity); //call super first before doing anything else!
		
		
		//Log.d("AccidentBaseFragment", "OnAttach Called!");
	}
	
	private void styleLauncherButton(RelativeLayout button, String title, String subTitle, int iconDrawableID) {
		((TextView) button.findViewById(R.id.title_text)).setText(title);
		((TextView) button.findViewById(R.id.subtitle_text)).setText(subTitle);
		((ImageView) button.findViewById(R.id.my_icon)).setImageDrawable(getResources().getDrawable(iconDrawableID));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.acci_critical_launch:
			//Toast.makeText(getActivity(), "Critical", 0).show();
			parentActivity.onFragmentReplaceRequest(new AccidentCritical1Fragment(), true);

			break;
		case R.id.acci_noncritical_launch:
			parentActivity.onFragmentReplaceRequest(new AccidentNonCriticalFragment(), true);

			break;
		case R.id.acci_emg_contact:
			if (emergencyContactNumer!=0) {
				tracker.send(GATrackerMaps.EVENT_ACCIDENT_CALL_EMG_CONTACT);
				
				Intent i = new Intent(Intent.ACTION_CALL);
				i.setData(Uri.parse("tel:" + emergencyContactNumer));
				startActivity(i);
			}
			else {
				tracker.send(GATrackerMaps.EVENT_ACCIDENT_SET_EMG_CONTACT);
				pickContact();
			}
			//Toast.makeText(getActivity(), "Emergency", 0).show();
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		ContentValues cv = new ContentValues();
		//Log.d(TAG, "onActivityResult Called with request code: " + requestCode);
		super.onActivityResult(requestCode, resultCode, data);
		
		ContactHolder contact;
		
		if (resultCode == Activity.RESULT_OK) {
			contact = CallForAction.getContactDetails(getActivity().getContentResolver(), data.getData());
			
			if (contact != null) {
				//Boolean done = MyJSONData.changeEmergencyContact(getActivity(), contact.getName(), contact.getNumber());
				cv.put(UserDB.COL_FIELD_EMERGENCY_CONTACT_NAME, contact.getName());
				cv.put(UserDB.COL_FIELD_EMERGENCY_CONTACT_NUMBER, contact.getNumber());
				Boolean done = userDetailDataSource.updateUserDetails(cv);
				if (done) {
					setEmgContactButton();
				}
			}
			else {
				contactPickFBIssueTryAgainDialog = CallForAction.getContactNotFoundDialog(getActivity(), 
						"Pick Again", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
								pickContact();
							}
						},
						
						"Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
							}
						}
				);
			}
		}
	}
	
	private void pickContact() {
		Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
		getActivity().startActivityForResult(i, 1);
	}
	
	private void setEmgContactButton() {
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		if (userDetailDataSource.isEmergencyContactExist()) {
			//MyJSONData emgData = new MyJSONData(getActivity(), MyJSONData.TYPE_EMERGENCY);
			
			//emergencyContactNumer = emgData.fetchData(MyJSONData.FIELD_EMERGENCY_CONTACT_NUMBER);
			emergencyContactNumer = userDetailDataSource.getEmergencyNumber();
			/*styleLauncherButton(acciEmgContactButton, 
					"Emergency Contact Person", 
					 emgData.fetchData(MyJSONData.FIELD_EMERGENCY_CONTACT_NAME) 
							  + ": " + emergencyContactNumer, 
					 R.drawable.phone_icon_small);*/
			styleLauncherButton(acciEmgContactButton, 
					"Emergency Contact Person", 
					 userDetailDataSource.getEmergencyContactName().trim()
							  + ": " + emergencyContactNumer, 
					 R.drawable.phone_icon_small);
			changeEmgContactHint.setVisibility(View.VISIBLE);
		}
		else {
			emergencyContactNumer = 0;
			styleLauncherButton(acciEmgContactButton, "Set Emergency Contact", "Someone you can rely upon!", R.drawable.raf_icon_small);
			changeEmgContactHint.setVisibility(View.GONE);
		}
	}
}
