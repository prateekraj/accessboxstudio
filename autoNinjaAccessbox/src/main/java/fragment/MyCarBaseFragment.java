package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import myJsonData.MyJSONData;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;


import service.AsyncInvokeURLTask;
import utils.Utils;

import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.myaccessbox.appcore.R;

import config.StaticConfig;

import db.CarDetailsDatasource;
import db.DatabaseHelper;
import db.TableContract.CarDetailsDB;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import dialog.MyPickerDialogFrag;
import dialog.MyPickerDialogFrag.onSendResultListener;
import activity.LoginActivityOld;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MyCarBaseFragment extends MyFragment implements View.OnClickListener,
		onSendResultListener {
	
	public static final int REQUEST_CODE_INSURANCE_SET_DATE = 17;
	public static final int REQUEST_CODE_PUC_SET_DATE = 23;
	public static final int REDIRECT_TO_DOCUMENT_INSURANCE = 374;
	public static final int REDIRECT_TO_DOCUMENT_PUC = 834;
	public static final int REDIRECT_TO_SERVICE_HISTORY = 259;

	private static final int REQUEST_CODE_CLICK_MYCAR_PIC = 37;
	private static final int REQUEST_CODE_GALLERY_MYCAR_PIC = 38;
	private static String TAG = MyCarBaseFragment.class.getSimpleName(); 
	
	ScrollView mycarHasDataView;
	ScrollView mycarNoDataView;
	
	//this button's display will be toggled based on StaticConfig.FEATURE_OTP_LOGIN_ENABLED
	TextView buttonEditMobileNo; 
	RelativeLayout orLineHolderLayout;
	//to control the keypad
	InputMethodManager imm;
	ProgressDialog progressDialog;

	TextView noDataOwnNumberTv;
	TextView noDataSubmitCarData;
	EditText noDataEditCarName; 
	EditText noDataEditOwnerName; 
	EditText noDataEditRegNo; 
	
	ImageView carPic;
	ImageView mycarClickIcon;
	ImageView editButton;
	ImageView takePhotoButton;
	View line2;
	ImageView viewServiceHistoryButton;
	TextView carName;
	TextView ownerName;
	TextView mobileNo;
	TextView regNo;
	
	LinearLayout serviceHolder;
	LinearLayout insuranceHolder;
	LinearLayout pucHolder;
	TextView serviceDate;
	TextView insuranceDate;
	TextView pucDate;
	TextView serviceRemark;
	TextView insuranceRemark;
	TextView pucRemark;
	
	AlertDialog editDialog;
	
	String strMobileNo = "";
	String strCarName = "";
	String strOwnerName = "";
	String strRegno = "";
	
	String mCurrentPhotoPath = "";
	
	LinearLayout manualFormHolder;
	MyJSONData ownData;
	
	Spinner carList;	
	LinearLayout carSpinnerLL;
	TextView deleteCar;		
	AlertDialog dlgDeleteCar;		
	//ImageView addCarr;		
	int car_id = -1;		
	boolean updated = false;		
	ArrayAdapter<String> dataAdapter;
	DatabaseHelper dbhelp;
	ContentValues cvv;
	static CarDetailsDatasource carDetailDataSource ;
	UserDetailsDataSource userDetailDataSource;
	
	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		
		//set title
		//actionBarTitleView.setText("My Car");
		
		//enable/disable backbutton
		//setActionBarBackEnabled(true);
		
		//Log.d(TAG, "OnAttach Called! Now");
	}
	
	@Override
	public void onPause() {
		super.onPause();
		baseActivityObj.setActionBarCustomView();
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (StaticConfig.FEATURE_DOCUMENTS_ENABLED) {
			switch(initExtraInt) {
			case REDIRECT_TO_DOCUMENT_INSURANCE:
				parentActivity.onFragmentReplaceRequest(MyCarDocumentsFragment.newInstance(
						MyCarDocumentsFragment.DOCUMENT_TYPE_INSURANCE), false);
				break;
			case REDIRECT_TO_DOCUMENT_PUC:
				parentActivity.onFragmentReplaceRequest(MyCarDocumentsFragment.newInstance(
						MyCarDocumentsFragment.DOCUMENT_TYPE_PUC), true);
				break;
			case REDIRECT_TO_SERVICE_HISTORY:
				parentActivity.onFragmentReplaceRequest(new ServiceHistoryFragment(), false);
				break;
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final DatabaseHelper dbhelper = new DatabaseHelper(getActivity());
		//Log.d(TAG, "OnCreateView Called! Now!!!");
		View v = inflater.inflate(R.layout.frag_mycar, container, false); 
		
		mycarHasDataView = (ScrollView) v.findViewById(R.id.mycar_has_data_holder);
		mycarNoDataView = (ScrollView) v.findViewById(R.id.mycar_nodata_holder);
		manualFormHolder = (LinearLayout) v.findViewById(R.id.nodata_or_manual_form_holder);
		
		noDataOwnNumberTv = (TextView) v.findViewById(R.id.nodata_mobile_no);
		noDataSubmitCarData = (TextView) v.findViewById(R.id.submit_edit_data);
		noDataEditCarName = (EditText) v.findViewById(R.id.edit_car_name); 
		noDataEditOwnerName = (EditText) v.findViewById(R.id.edit_owner_name); 
		noDataEditRegNo = (EditText) v.findViewById(R.id.edit_reg_no); 
		
		buttonEditMobileNo = (TextView) v.findViewById(R.id.button_edit_mobno);
		orLineHolderLayout = (RelativeLayout) v.findViewById(R.id.or_line_holder);
		if (StaticConfig.FEATURE_OTP_LOGIN_ENABLED) {
			buttonEditMobileNo.setVisibility(View.GONE);
			orLineHolderLayout.setVisibility(View.GONE);
		}
		else {
			buttonEditMobileNo.setVisibility(View.VISIBLE);
			orLineHolderLayout.setVisibility(View.VISIBLE);
			buttonEditMobileNo.setOnClickListener(this);
		}
		
		serviceHolder = (LinearLayout) v.findViewById(R.id.service_holder);
		insuranceHolder = (LinearLayout) v.findViewById(R.id.insurance_holder);
		pucHolder = (LinearLayout) v.findViewById(R.id.puc_holder);
		
		carPic = (ImageView) v.findViewById(R.id.mycar_pic);
		mycarClickIcon = (ImageView) v.findViewById(R.id.mycar_click_pic_icon);
		editButton = (ImageView) v.findViewById(R.id.edit);
		takePhotoButton = (ImageView) v.findViewById(R.id.take_photo);
		line2 = (View) v.findViewById(R.id.line2);
		viewServiceHistoryButton = (ImageView) v.findViewById(R.id.view_history);
		carName = (TextView) v.findViewById(R.id.mycar_name);
		ownerName =(TextView) v.findViewById(R.id.owner_name);
		mobileNo = (TextView) v.findViewById(R.id.mobile_number);
		regNo = (TextView) v.findViewById(R.id.reg_num);
		
		TextView tvTitle;
		tvTitle = (TextView) serviceHolder.findViewById(R.id.title_tv);
		tvTitle.setText("Next Service");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		
		int docsIcon = StaticConfig.FEATURE_DOCUMENTS_ENABLED ? R.drawable.attachment : 0;
		
		tvTitle = (TextView) insuranceHolder.findViewById(R.id.title_tv);
		tvTitle.setText("Insurance");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

		tvTitle = (TextView) pucHolder.findViewById(R.id.title_tv);
		tvTitle.setText("PUC");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		
		serviceDate = (TextView) serviceHolder.findViewById(R.id.date_tv);
		serviceRemark = (TextView) serviceHolder.findViewById(R.id.remark_tv);
		insuranceDate = (TextView) insuranceHolder.findViewById(R.id.date_tv);
		insuranceRemark = (TextView) insuranceHolder.findViewById(R.id.remark_tv);
		pucDate = (TextView) pucHolder.findViewById(R.id.date_tv);
		pucRemark = (TextView) pucHolder.findViewById(R.id.remark_tv);
		
		noDataSubmitCarData.setOnClickListener(this);
		carPic.setOnClickListener(this);
		editButton.setOnClickListener(this);
		takePhotoButton.setOnClickListener(this);
		viewServiceHistoryButton.setOnClickListener(this);
		serviceHolder.setOnClickListener(this);
		insuranceHolder.setOnClickListener(this);
		pucHolder.setOnClickListener(this);
		
		carList = (Spinner) v.findViewById(R.id.car_list);	
		carSpinnerLL = (LinearLayout) v.findViewById(R.id.select_car_layout);
		deleteCar = (TextView) v.findViewById(R.id.delete_car);		
		// carList.setOnClickListener(this);		
		deleteCar.setOnClickListener(this);		
		dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_multiplecar,		
				StaticConfig.myCarListNames);		
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		carList.setAdapter(dataAdapter);
		carList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				String chosenModelName = parent.getItemAtPosition(position).toString();		
				StringTokenizer st = new StringTokenizer(chosenModelName, ",");		
						
					String model = st.nextToken();		
					String regNum = st.nextToken().trim();		
	              for(int i=0;i<StaticConfig.myCarList.size();i++){		
	            	  		
	            	  if(StaticConfig.myCarList.get(i).getModelName().equalsIgnoreCase(model) && StaticConfig.myCarList.get(i).getReg_num().equalsIgnoreCase(regNum) ){		
	            		  StaticConfig.myCar = StaticConfig.myCarList.get(i);		
	            		  StaticConfig.position=position;	
	            		  UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(
		      						getActivity());
		            		  ContentValues cv = new ContentValues();
		            		  cv.put(UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION, position);
		            		  userDetailDataSource.updateUserDetails(cv);
	            		 // dbhelper.getCarList();		
	            	  }		
	              }		
				re_createLayout();		
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		return v;
	}
	
	private void setSpinnerData() {		
		if (StaticConfig.myCarListNames.size() <= 0) {		
			carList.setVisibility(View.GONE);	
			carSpinnerLL.setVisibility(View.GONE);
		} else {		
			dataAdapter.notifyDataSetChanged();		
			carList.setSelection(StaticConfig.position);		
		}		
	}		
	private void re_createLayout() {		
		//obj.setMyCarActionBarCustomView();		
		//addCarr = (TextView) getActivity().findViewById(R.id.titleRight);		
		setSpinnerData();		
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);		
		ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);		
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);		
		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);		
		nm.cancel("MyCarData", 1);		
		nm.cancel("InsuranceDue", 1);		
		nm.cancel("PUCDue", 1);		
		reconfigureLayoutView();		
	}

	@Override
	public void onResume() {
		super.onResume();
		carDetailDataSource = new CarDetailsDatasource(getActivity());
		userDetailDataSource = new UserDetailsDataSource(getActivity());
		baseActivityObj.setMyCarActionBarCustomView();		
		//addCarr = (ImageView) getActivity().findViewById(R.id.homeButton);		
		setSpinnerData();		
		DatabaseHelper dbhelper = new DatabaseHelper(getActivity());		
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		ownData = new MyJSONData (getActivity(), MyJSONData.TYPE_OWN); 
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel("MyCarData", 1);
		nm.cancel("InsuranceDue", 1);
		nm.cancel("PUCDue", 1);

		reconfigureLayoutView();
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public void onClick(View v) {
		carDetailDataSource = new CarDetailsDatasource(getActivity());		
		ContentValues cv = new ContentValues();
		Intent i;
		MyPickerDialogFrag pickerFragDialog;
		switch (v.getId()) {
		case R.id.mycar_pic:
			mCurrentPhotoPath = carDetailDataSource.getPhotoPath();
			//System.out.println("My photo path :"+mCurrentPhotoPath);
			if(!mCurrentPhotoPath.equalsIgnoreCase("")) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri imgUriToOpen = Uri.parse("file://" + mCurrentPhotoPath);
				intent.setDataAndType(imgUriToOpen, "image/*");
				startActivity(intent);
			} else {
				selectImage();
			}
			break;
		case R.id.take_photo:
			tracker.send(GATrackerMaps.EVENT_CAR_CLICK_PIC);
			selectImage();
			break;
		case R.id.edit:
			tracker.send(GATrackerMaps.EVENT_EDIT_DATA);
			tracker.send(GATrackerMaps.VIEW_MYCAR_REPORT);
			cleverTap.event.push("Clicked Edit Car Details button- My Car Screen");

			AlertDialog.Builder build = new AlertDialog.Builder(getActivity());
			ScrollView view = (ScrollView) getActivity().getLayoutInflater().inflate(R.layout.dialog_dataentry, null);
			
			final TextView tvMobileNo = (TextView) view.findViewById(R.id.mobile_no); 
			final EditText edCarName = (EditText) view.findViewById(R.id.edit_car_name); 
			final EditText edOwnerName = (EditText) view.findViewById(R.id.edit_owner_name); 
			final EditText edRegNo = (EditText) view.findViewById(R.id.edit_reg_no); 
			
			tvMobileNo.setText(strMobileNo);
			edCarName.setText(strCarName);
			edOwnerName.setText(strOwnerName);
			edRegNo.setText(strRegno);
			
			((TextView) view.findViewById(R.id.submit_edit_data)).setOnClickListener(new View.OnClickListener() {
				
				@SuppressLint("DefaultLocale")
				@Override
				public void onClick(View v) {
					String newCarName = edCarName.getText().toString().trim();
					String newOwner = edOwnerName.getText().toString().trim();
					String newRegnum = edRegNo.getText().toString().trim().replaceAll("\\s+", "").toUpperCase();
					if (newCarName.equalsIgnoreCase("")) {
						Toast.makeText(getActivity(), "Please enter your car model name!", Toast.LENGTH_LONG).show();
					}
					else if (newOwner.equalsIgnoreCase("")) {
						Toast.makeText(getActivity(), "Please enter your name!", Toast.LENGTH_LONG).show();
					}else if (newRegnum.equalsIgnoreCase("")) {
						Toast.makeText(getActivity(), "Please enter your car registration number!", Toast.LENGTH_LONG).show();
					}/*else if(!Utils.isRegNumValid(newRegnum)){
						Toast.makeText(getActivity(), "Please enter your car registration number in correct format!", Toast.LENGTH_LONG).show();
					}*/
					else {
						tracker.send(GATrackerMaps.EVENT_EDIT_DATA_SUBMIT);

						String[] vals = {newRegnum, newOwner, newCarName, "true"};
						
						if(Utils.isNetworkConnected(getActivity())){
							if (carUpdate(vals, false)) {
								// carUpdateToDB(vals, false);// for now
								Toast.makeText(getActivity(), "Car" + car_id + "Updated Successfully", 0).show();
								// editDialog.dismiss();
								// populateData();
							}
							}else{
								Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
							}
						
					}
				}

			});
			build.setTitle("Edit your car details");
			build.setView(view);
			editDialog = build.show();
			break;
		case R.id.service_holder:
			tracker.send(GATrackerMaps.EVENT_CAR_NEXT_SERVICE);
			cleverTap.event.push("Clicked Service button- My Car Screen");
			parentActivity.onTabChangeRequest(StaticConfig.getTabPositionFromName(StaticConfig.TAB_NAME_SERVICE));
			break;
		case R.id.insurance_holder:
			cleverTap.event.push("Clicked Insurance button- My Car Screen");
			if (StaticConfig.FEATURE_DOCUMENTS_ENABLED) {
				tracker.send(GATrackerMaps.EVENT_CAR_INSURANCE_VIEW_DOC);

				parentActivity.onFragmentReplaceRequest(MyCarDocumentsFragment.newInstance(
						MyCarDocumentsFragment.DOCUMENT_TYPE_INSURANCE), true);
			}
			else {
				tracker.send(GATrackerMaps.EVENT_CAR_INSURANCE);

				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_DATE_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_INSURANCE_SET_DATE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "insuDatePicker");
			}
			break;
		case R.id.puc_holder:
			if (StaticConfig.FEATURE_DOCUMENTS_ENABLED) {
				tracker.send(GATrackerMaps.EVENT_CAR_PUC_VIEW_DOC);
				
				parentActivity.onFragmentReplaceRequest(MyCarDocumentsFragment.newInstance(
						MyCarDocumentsFragment.DOCUMENT_TYPE_PUC), true);
			}
			else {
				tracker.send(GATrackerMaps.EVENT_CAR_PUC);

				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_DATE_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_PUC_SET_DATE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "pucDatePicker");
			}
			break;
		case R.id.button_edit_mobno:
			if (!StaticConfig.FEATURE_OTP_LOGIN_ENABLED) {
				tracker.send(GATrackerMaps.EVENT_EDIT_MOBILE_NUMBER);
				
				i = new Intent(getActivity(), LoginActivityOld.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				i.putExtra("editMobile", true);
				startActivity(i);
			}
			else {
				//Log.e(TAG, "This should not have occoured! Trying to edit mobile number with OTP enabled!");
			}
			break;
		case R.id.view_history:
			tracker.send(GATrackerMaps.EVENT_CAR_VIEW_HISTORY);
			parentActivity.onFragmentReplaceRequest(new ServiceHistoryFragment(), true);
			break;
		case R.id.submit_edit_data:
			//hide the keyboard
			if (Utils.isNetworkConnected(getActivity())) {
			imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
			
			String carName = noDataEditCarName.getText().toString().trim();
			String owner = noDataEditOwnerName.getText().toString().trim();
			String regnum = noDataEditRegNo.getText().toString().trim().replaceAll("\\s+", "").toUpperCase();
			
			if (carName.equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please enter your car model name!", Toast.LENGTH_LONG).show();
			}
			else if (owner.equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please enter your name!", Toast.LENGTH_LONG).show();
			}else if (regnum.equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please enter your car registration number!", Toast.LENGTH_LONG).show();
			}
			else {
				tracker.send(GATrackerMaps.EVENT_REPORT_PROBLEM_SUBMIT);
				
				String[] vals = {regnum, owner, carName, "true"};
				
				if (carUpdate(vals, true)) {
					//Toast.makeText(getActivity(), "Car" + car_id + "Updated Successfully", 0).show();
					// editDialog.dismiss();
					// reconfigureLayoutView();
				}
			}
		} else {

			Toast.makeText(getActivity(), "Please check your internet connection.", Toast.LENGTH_LONG).show();
		}
			break;
		case R.id.delete_car :
			tracker.send(GATrackerMaps.EVENT_MY_CAR_DELETE_DIALOG);
			cleverTap.event.push("Clicked Delete Car button- My Car Screen");
			AlertDialog.Builder bldr = new AlertDialog.Builder(getActivity());
			bldr.setMessage("This Car will be deleted!");
			bldr.setPositiveButton("Delete", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					dlgDeleteCar.dismiss();
					if(Utils.isNetworkConnected(getActivity())){
					tracker.send(GATrackerMaps.EVENT_MY_CAR_ADD);
					deleteCar();
					}else{
						Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
					}
					/*
					 * \dbhedlper.deleteACar(StaticConfig.myCar.getCarId());
					 * dbhedlper.getCarList();
					 * Toast.makeText(getActivity(), "Not Now", 1)
					 * .show();
					 */

				}
			});
			bldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dlgDeleteCar.dismiss();

				}
			});
			dlgDeleteCar = bldr.show();
			break;
		}
	}
	
	protected boolean deleteCar() {
		progressDialog = ProgressDialog.show(getActivity(), "Deleting Car", "Please wait for a moment");
		if (StaticConfig.myCar.getCarId() > 0) {
			car_id = StaticConfig.myCar.getCarId();
		}
		try {
			MultipartEntity mpEntity = new MultipartEntity();

			try {
				mpEntity.addPart("car_id", new StringBody("" + car_id));
				mpEntity.addPart("phone", new StringBody("" + StaticConfig.LOGGED_PHONE_NUMBER));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
				private JSONObject deletedCar = null;

				public void onPostExecute(String result) {
					try {
						progressDialog.dismiss();
						final DatabaseHelper dbhedlper = new DatabaseHelper(getActivity());
						deletedCar = new JSONObject(result);
						if (deletedCar.has("error") && deletedCar.getString("error").equalsIgnoreCase("false")) {
							cleverTap.event.push("Succesfully deleted a car");
							carDetailDataSource.deleteACarFromDb(StaticConfig.myCar.getCarId());
							Toast.makeText(getActivity(), "" + deletedCar.getString("text"), 1).show();
							updated = true;
							if(carDetailDataSource.getCarList()==0){
								mycarNoDataView.setVisibility(View.VISIBLE);
								mycarHasDataView.setVisibility(View.GONE);
								carSpinnerLL.setVisibility(View.GONE);
								
							}
							parentActivity.onFragmentReplaceRequest(new MyCarBaseFragment(), false);
							// onResume();
						} else {
							Toast.makeText(getActivity(), "" + deletedCar.getString("text"), 1).show();
							updated = false;
						}
					} catch (JSONException e) { // TODO Auto-generated catch
												// block
						e.printStackTrace();
					}
				}
			}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
			String executeURL = StaticConfig.API_POST_DELETE_CAR + "?" + StaticConfig.getCMSPass();
			//Log.d("UPDATE CAR", executeURL);
			task.execute(executeURL);
			// Log.d(TAG,"execute called with url: " + executeURL);
		} catch (Exception e1) { //
			System.out.println("Exception HIT SERVER: " + e1.toString());
		}
		return updated;
	}

			protected boolean carUpdateToDB(String[] vals, boolean doCreate) {
				carDetailDataSource = new CarDetailsDatasource(getActivity());
				ContentValues cv = new ContentValues();
				cv.put(CarDetailsDB.COL_FIELD_CAR_MODEL, vals[2]);
				cv.put(CarDetailsDB.COL_CAR_OWNER, vals[1]);
				cv.put(CarDetailsDB.COL_FIELD_CAR_REGNUM, vals[0]);
				cv.put(CarDetailsDB.COL_FIELD_CAR_DATA_VERIFIED, vals[3]);
				if (!doCreate && StaticConfig.myCar.getCarId() > 0) {
					if (carDetailDataSource.updateCarDetails(cv,StaticConfig.myCar.getCarId()) > 0) {
						carDetailDataSource.getCarList();
						return true;
					}
				} else if (doCreate) {
					if (carDetailDataSource.insertCarDetails(cv) > 0) {
						carDetailDataSource.getCarList();
						return true;
					}
				}
				return false;
			}

			// this method will give option to select image from gallery or take a new
			// pic from camera for Mycar
			private void selectImage() {
				final CharSequence imageArray[] = { "Take Photo", "From Gallery" };

				carDetailDataSource = new CarDetailsDatasource(getActivity());		
				cvv = new ContentValues();
				AlertDialog.Builder builder = new AlertDialog.Builder(
						getActivity());
				builder.setTitle("Choose Picture:");
				builder.setItems(imageArray, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int item) {

						if (imageArray[item].equals("Take Photo")) {
							// Toast.makeText(getActivity(), "Take Photo!",
							// 0).show();
							tracker.send(GATrackerMaps.EVENT_CAR_CLICK_PIC);
							Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							try {
								File f = createImageFile();
								/*i.putExtra("crop", "true");
								i.putExtra("aspectX", 12);
								i.putExtra("aspectY", 5);
								i.putExtra("outputX", 480);
								i.putExtra("outputY", 200);
								i.putExtra("scale", true);*/
								i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
								
								cvv.put(CarDetailsDB.COL_FIELD_CAR_PHOTO_ATTEMPT_PATH, mCurrentPhotoPath);
								carDetailDataSource.updateCarDetails(cvv,StaticConfig.myCar.getCarId());
								
								getActivity().startActivityForResult(i, REQUEST_CODE_CLICK_MYCAR_PIC);
								//Toast.makeText(getActivity(), "Temp file location: " + mCurrentPhotoPath, Toast.LENGTH_LONG).show();
							}
							catch (ActivityNotFoundException e) {
								Toast.makeText(getActivity(), "Sorry! Camera not available.", Toast.LENGTH_LONG).show();
							} catch (IOException e) {
								Toast.makeText(getActivity(), "Problem creating image location!", Toast.LENGTH_LONG).show();
								//Log.d(TAG, e.getMessage());
							}
						} else if (imageArray[item].equals("From Gallery")) {

							Intent i = new Intent(Intent.ACTION_PICK,
									android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
							getActivity().startActivityForResult(i,
									REQUEST_CODE_GALLERY_MYCAR_PIC);

						}
					}
				});
				builder.show();

			}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//Log.d(TAG, "onActivityResult Called!");
		carDetailDataSource = new CarDetailsDatasource(getActivity());
		cvv = new ContentValues();
		String finalPhotoPath = "";
		if (resultCode == Activity.RESULT_OK) {
			//Log.d(TAG, "result OK!");
			//Log.d(TAG, "Request Code: " + requestCode);
			switch(requestCode) {
			case REQUEST_CODE_CLICK_MYCAR_PIC:
				tracker.send(GATrackerMaps.EVENT_CAR_SET_PIC);
				finalPhotoPath = carDetailDataSource.getPhotoAttemptPath();
				//System.out.println("MY CAR PIC: "+StaticConfig.myCar.getCarId());
				break;
			case REQUEST_CODE_GALLERY_MYCAR_PIC:
				tracker.send(GATrackerMaps.EVENT_CAR_SET_PIC_FROM_GALLERY);
				Uri selectedImageUri = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						selectedImageUri, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				finalPhotoPath = cursor.getString(columnIndex);
				cursor.close();
				break;	
			}
			if(!finalPhotoPath.equalsIgnoreCase("")) {
				cvv.put(CarDetailsDB.COL_FIELD_CAR_PHOTO_PATH, finalPhotoPath);
				carDetailDataSource.updateCarDetails(cvv,StaticConfig.myCar.getCarId());
				setMycarPic();
			}
		}
		else {
			//delete temp holder file that was created
			new File(finalPhotoPath).delete();
		}
	}
	
	private boolean carUpdate(final String[] vals, final boolean doCreate) {
		DatabaseHelper dbhelper = new DatabaseHelper(getActivity());
		progressDialog = ProgressDialog.show(getActivity(), "Updating Car", "Please wait for a moment");
		car_id = StaticConfig.myCar.getCarId();
		try {
			MultipartEntity mpEntity = new MultipartEntity();

			try {
				mpEntity.addPart("car_id", new StringBody("" + car_id));
				mpEntity.addPart("reg_num", new StringBody("" + vals[0]));
				mpEntity.addPart("owner_name", new StringBody("" + vals[1]));
				mpEntity.addPart("model", new StringBody("" + vals[2]));
				mpEntity.addPart("phone", new StringBody("" + StaticConfig.LOGGED_PHONE_NUMBER));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (!doCreate) {
				AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
					private JSONObject updatedCar = null;

					public void onPostExecute(String result) {
						// progressDialog.dismiss();
						progressDialog.dismiss();
						try {
							updatedCar = new JSONObject(result);
							if (updatedCar.has("error") && updatedCar.getString("error").equalsIgnoreCase("false")) {
								if (updated = carUpdateToDB(vals, doCreate)) {
									editDialog.dismiss();
									re_createLayout();
									//onResume();
									//parentActivity.onFragmentReplaceRequest(new MyCarBaseFragment(), false);
								}

							} //else {
								Toast.makeText(getActivity(), updatedCar.getString("text"), 0).show();
							//}
						} catch (JSONException e) { // TODO Auto-generated catch
													// block
							e.printStackTrace();
						}
					}
				}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
				String executeURL = StaticConfig.API_POST_UPDATE_CAR + "?" + StaticConfig.getCMSPass();
				Log.d("UPDATE CAR", executeURL);
				task.execute(executeURL);
			} else {
				progressDialog.dismiss();
				MyCarAddFragment obj = new MyCarAddFragment();
				updated = obj.addNewCar(vals, false, getActivity(), parentActivity);
			}
			// Log.d(TAG,"execute called with url: " + executeURL);
		} catch (Exception e1) { //
			System.out.println("Exception HIT SERVER: " + e1.toString());
		}
		return updated;
	}
	
	private void populateData() {
		 carDetailDataSource = new CarDetailsDatasource(getActivity());
		mobileNo.setText(strMobileNo = ""+StaticConfig.LOGGED_PHONE_NUMBER); 

		carName.setText(strCarName = StaticConfig.myCar.getModelName());// model
		ownerName.setText(strOwnerName = StaticConfig.myCar.getOwner());// owner
		
		strRegno = StaticConfig.myCar.getReg_num();// reg_num
		if (strRegno.trim().equalsIgnoreCase("")) {
			regNo.setText("---");
		}
		else {
			regNo.setText(strRegno);
		}
		
		String servDue = carDetailDataSource.getNextServiceDate(StaticConfig.myCar.getCarId());//next_service_date
		String insExp = carDetailDataSource.getInsuranceExpiry(StaticConfig.myCar.getCarId());//insurance_expiry
		String pucExp = carDetailDataSource.getPucExpiry(StaticConfig.myCar.getCarId());//puc_expiry
		
		Date servDueDate, insExpDate, PUCExpDate, today = new Date();
		
		if (servDue.equalsIgnoreCase("") || servDue.equalsIgnoreCase("0000-00-00")) {//data not present, ask for input
			serviceRemark.setText("Set next service date");
			serviceDate.setText("---");
		}
		else {
			serviceRemark.setText("Due date:");
			serviceRemark.setTypeface(null, Typeface.NORMAL);
			serviceDate.setText(MyJSONData.formatDateServerToDisplay(servDue));

			servDueDate = MyJSONData.parseFileDate(servDue);
			if (servDueDate != null) {
				if (servDueDate.before(today)) {
					serviceRemark.setText("Service Overdue!");
					serviceRemark.setTypeface(null, Typeface.BOLD);
				}
			}
			else {
				//Log.e("MyCar", "ParseException on Service Date");
			}
		}

		if (insExp.equalsIgnoreCase("") || insExp.equalsIgnoreCase("0000-00-00")) {//data not present, ask for input
			insuranceRemark.setText("Set expiry date");
			insuranceDate.setText("---");
		}
		else {
			insuranceRemark.setText("Expires on:");
			insuranceRemark.setTypeface(null, Typeface.NORMAL);
			insuranceDate.setText(MyJSONData.formatDateServerToDisplay(insExp));
			
			insExpDate = MyJSONData.parseFileDate(insExp);
			if (insExpDate != null) {
				if (insExpDate.before(today)) {
					insuranceRemark.setText("Insurance Expired!");
					insuranceRemark.setTypeface(null, Typeface.BOLD);
				}
			}
			else {
				//Log.e("MyCar", "ParseException on Insurance Date");
			}
		}

		if (pucExp.equalsIgnoreCase("")|| pucExp.equalsIgnoreCase("0000-00-00")) {//data not present, ask for input
			pucRemark.setText("Set expiry date");
			pucDate.setText("---");
		}
		else {
			pucRemark.setText("Expires on:");
			pucRemark.setTypeface(null, Typeface.NORMAL);
			pucDate.setText(MyJSONData.formatDateServerToDisplay(pucExp));

			PUCExpDate = MyJSONData.parseFileDate(pucExp);
			if (PUCExpDate != null) {
				if (PUCExpDate.before(today)) {
  					pucRemark.setText("PUC Expired!");
					pucRemark.setTypeface(null, Typeface.BOLD);
				}
			} 
			else {
				//Log.e("MyCar", "ParseException on PUC Date");
			}
		}
	}

	@Override
	public void onSendResult(String result, int requestCode) {
		if (processSendResult(getActivity(), result, requestCode, ownData)) {
			
			switch (requestCode) {
			case (REQUEST_CODE_INSURANCE_SET_DATE):
				tracker.send(GATrackerMaps.EVENT_CAR_SET_DATE_INSURANCE);
				break;
			case (REQUEST_CODE_PUC_SET_DATE):
				tracker.send(GATrackerMaps.EVENT_CAR_SET_DATE_PUC);
				break;
			}
			
			populateData();
		}
	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
	    // unique filename based on timestamp
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    
	    String imageFileName = "MyCar_" + timeStamp + "_";
	    File image = File.createTempFile(imageFileName, ".jpg", getAlbumDir());
	    mCurrentPhotoPath = image.getAbsolutePath();
	    return image;
	}
	
	private File getAlbumDir() {
		File ret = new File(
				Environment.getExternalStoragePublicDirectory(
						Environment.DIRECTORY_PICTURES), 
				"/Accessbox/MyCar"
		);
		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}
	
	private void setMycarPic() {
		carDetailDataSource = new CarDetailsDatasource(getActivity());
		cvv = new ContentValues();
		String carPicPath = carDetailDataSource.getPhotoPath();
		Bitmap b = BitmapFactory.decodeFile(carPicPath);
		if (b != null) {
			carPic.setImageBitmap(b);
			mycarClickIcon.setVisibility(View.GONE);
			takePhotoButton.setVisibility(View.VISIBLE);
		}
		else {
			carPic.setImageResource(R.drawable.mycar_silhouette1);
			mycarClickIcon.setVisibility(View.VISIBLE);
			takePhotoButton.setVisibility(View.GONE);
		}
		//carPic.setBackgroundDrawable(new BitmapDrawable(getResources(), (Bitmap) data.getExtras().get("data")));
	}
	
	
	private void reconfigureLayoutView() {
		if (!StaticConfig.myCar.getModelName().equalsIgnoreCase("")) {
			tracker.send(GATrackerMaps.VIEW_MYCAR);
			cleverTap.event.push("Opened My Car with details Screen");

			toolbarListener.setAddCarButton(true);
			mycarHasDataView.setVisibility(View.VISIBLE);
			mycarNoDataView.setVisibility(View.GONE);
			toolbarListener.setText("My Car");
			deleteCar.setVisibility(View.VISIBLE);
			if (getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
				// enable camera features and click buttons
				carPic.setVisibility(View.VISIBLE);
				mycarClickIcon.setVisibility(View.VISIBLE);
				line2.setVisibility(View.VISIBLE);
				takePhotoButton.setVisibility(View.INVISIBLE);

				setMycarPic();
			} else {// disable camera features and hide cover photo
				carPic.setVisibility(View.GONE);
				mycarClickIcon.setVisibility(View.GONE);
				line2.setVisibility(View.GONE);
				takePhotoButton.setVisibility(View.GONE);
			}
			populateData();
		} else {
			mycarNoDataView.setVisibility(View.VISIBLE);
			mycarHasDataView.setVisibility(View.GONE);
			//addCarr.setVisibility(View.INVISIBLE);
			deleteCar.setVisibility(View.GONE);
			 long temp =  StaticConfig.LOGGED_PHONE_NUMBER;
			  noDataOwnNumberTv.setText(""+userDetailDataSource.getUserOwnNumber());
			//noDataOwnNumberTv.setText(new MyJSONData(getActivity(), MyJSONData.TYPE_LOGIN).fetchData(MyJSONData.FIELD_LOGIN_NUMBER));

			  if(userDetailDataSource.getJsonText().equalsIgnoreCase("No details found for the phone number.")){
				  cleverTap.event.push("Opened My Car without details Screen");
				  toolbarListener.setText("Data not found!");
				manualFormHolder.setVisibility(View.VISIBLE);
				carSpinnerLL.setVisibility(View.GONE);
				deleteCar.setVisibility(View.GONE);
			//	addCarr.setVisibility(View.GONE);
				tracker.send(GATrackerMaps.VIEW_MYCAR_REPORT);
			} else if (StaticConfig.myCar.getCarId() == 0) {
				toolbarListener.setText("Searching for your data!");
				manualFormHolder.setVisibility(View.GONE);
				tracker.send(GATrackerMaps.VIEW_MYCAR_SEARCHING);
				Toast.makeText(getActivity().getApplicationContext(),
						"Searching for car data matching your login credentials. Please wait...", Toast.LENGTH_LONG).show();
			}
		}
	}
	public static boolean processSendResult(Context ctxt, String result, int requestCode, MyJSONData own) {
		boolean ret = false;
		 carDetailDataSource = new CarDetailsDatasource(ctxt);
		ContentValues cv = new ContentValues();
		switch (requestCode) {
			case (REQUEST_CODE_INSURANCE_SET_DATE) :

				cv.put(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY, MyJSONData.getServerFormatFromChosen(result));
				if (carDetailDataSource.updateCarDetails(cv,StaticConfig.myCar.getCarId()) > 0) {
					ret = true;
					Cursor c = carDetailDataSource.getDetailsOfCarId();
					pingInsuPUCUpdateToServer(c,ctxt);
				}
				break;
			case (REQUEST_CODE_PUC_SET_DATE) :

				cv.put(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY, MyJSONData.getServerFormatFromChosen(result));
				if (carDetailDataSource.updateCarDetails(cv,StaticConfig.myCar.getCarId()) > 0) {
					ret = true;
					Cursor c = carDetailDataSource.getDetailsOfCarId();
					pingInsuPUCUpdateToServer(c,ctxt);
				}
				break;
		}
		return ret;
	}
	
	// add car id
	public static void pingInsuPUCUpdateToServer(Cursor c, final Context cxt) {
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(cxt);
		carDetailDataSource = new CarDetailsDatasource(cxt);
		String date_serv, date_insu, date_puc, insurance_provider, service_provider;
		date_serv = date_insu = date_puc = insurance_provider = service_provider = "";
		try {
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
				public void onPostExecute(String result) {
					// ignore result as of now
					try {
						JSONObject response = new JSONObject(result);
						if(response.has("error")&& response.getString("error").equalsIgnoreCase("false")){
							Toast.makeText(cxt, "Details Updated Successfully", Toast.LENGTH_LONG).show();
						}else if(response.has("text")){
							Toast.makeText(cxt, response.getString("text"), Toast.LENGTH_LONG).show();
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					carDetailDataSource.close();
				}
			}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
			if (c.moveToFirst()) {
				date_serv =c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE)));
				date_insu =c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY)));
				date_puc = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY)));
				/*date_serv = MyJSONData.getServerFormateDate(c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE))));
				date_insu = MyJSONData.getServerFormateDate(c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY))));
				date_puc = MyJSONData.getServerFormateDate(c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY))));*/
				insurance_provider = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_PROVIDER)));
				service_provider = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT)));
			}
			String executeURL = StaticConfig.API_DATES_RECORD + "?phone=" + userDetailDataSource.getUserOwnNumber();
			executeURL += "&car_id=" + URLEncoder.encode("" + StaticConfig.myCar.getCarId(), "UTF-8");
			executeURL += "&date_serv=" + date_serv.trim();
			executeURL += "&date_insu=" + date_insu.trim();
			executeURL += "&date_puc=" + date_puc.trim();
			executeURL += "&insurance_provider=" + URLEncoder.encode(insurance_provider, "UTF-8");
			executeURL += "&service_provider=" + URLEncoder.encode(service_provider, "UTF-8");
			executeURL += "&" + StaticConfig.getCMSPass();
			// }
			task.execute(executeURL);
			//System.out.println("DATES REORD: " + executeURL);
		} catch (Exception e1) {
			// Log.e("MessagesList", "Exception: " + e1.toString());
		}
	}
	}
