package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import service.BackgroundService;
import service.ServiceLocations;

import myJsonData.MyJSONData;

import adapter.ChatAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.myaccessbox.appcore.R;

import config.StaticConfig;

import db.CarDetailsDatasource;
import db.ChatDataSource;
import db.DatabaseHelper;
import db.TableContract.CarDetailsDB;
import db.TableContract.ChatDB;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import dialog.MyPickerDialogFrag;
import entity.ChatMessages;

public class ChatAdvisorFragment extends MyFragment 
			implements View.OnClickListener, 
				MyPickerDialogFrag.onSendResultListener, ChatAdapter.BookServiceListner {
	
	public static final String CHAT_NOTIFICATION_IDENTIFIER = "NewChatByAdvisor";

	TextView tvTitle;
	TextView tvExtra;
	ListView listMessages;
	TextView tvListEmptyNote;
	
	ImageView takePhotoButton;
	EditText chatCompose;
	ImageView sendButton;

	LinearLayout chatCityChanger;
	TextView chatCityName;
	TextView chatCityChangeButton;

	ChatDataSource chatDataSource;
	
	private static final String TAG = "ChatAdvisorFragment";
	private static final int ROLE_ID = ChatMessages.SENDER_SERVICE_ADVISOR;
	public static final int REQUEST_CODE_CLICK_PHOTO = 782;
	private static final int REQUEST_CODE_CHAT_CITY_CHOICE = 744; 
	public static final int REQUEST_CODE_GALLERY_PHOTO = 784;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = inflater.inflate(R.layout.frag_chat, container, false);
		tvTitle = (TextView) v.findViewById(R.id.frag_list_title);
		tvExtra = (TextView) v.findViewById(R.id.frag_list_extra);
		listMessages = (ListView) v.findViewById(R.id.list);
		tvListEmptyNote = (TextView) v.findViewById(R.id.textListEmptyNote);
		
		takePhotoButton = (ImageView) v.findViewById(R.id.take_photo_button);
		chatCompose = (EditText) v.findViewById(R.id.chat_message_edit);
		sendButton = (ImageView) v.findViewById(R.id.chat_message_send);
		
		chatCityChanger = (LinearLayout) v.findViewById(R.id.chat_citi_pick_holder);
		chatCityName = (TextView) v.findViewById(R.id.chat_city_name);
		chatCityChangeButton = (TextView) v.findViewById(R.id.chat_city_change_button);
		if (null == StaticConfig.LOCS_CHAT_ADVISOR || StaticConfig.LOCS_CHAT_ADVISOR.length == 0) {
			 ServiceLocations.setLocations(getActivity());
		 }
		if (StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED) {
			chatCityChanger.setVisibility(View.VISIBLE);
			chatCityName.setText("");
			chatCityChangeButton.setOnClickListener(this);
		}
		else {
			chatCityChanger.setVisibility(View.GONE);
		}
		
		sendButton.setOnClickListener(this);
		takePhotoButton.setOnClickListener(this);
		
		listMessages.setDividerHeight(0);
		
		tvTitle.setText("Chat with Service Advisor");
		tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.service_quote_list_logo, 0, 0, 0);
		toolbarListener.setText("Get a Service Quote");
		
		tvExtra.setVisibility(View.GONE);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		chatDataSource = new ChatDataSource(getActivity());
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_LIVE_QUOTE);
		cleverTap.event.push("Opened Live Quote Screen");
		
		LocalBroadcastManager.getInstance(getActivity())
				.registerReceiver(broadcastReceiver, 
								new IntentFilter(BackgroundService.INTENT_FILTER_CHAT_UPDATE_ACTION));
		
		LocalBroadcastManager.getInstance(getActivity())
        .registerReceiver(broadcastReceiverimage, 
			new IntentFilter("show image"));
		
		BackgroundService.setChatRoleObserver(ROLE_ID);
		
		//getActivity().stopService(new Intent(getActivity(), BackgroundService.class));
		
		Intent i = new Intent(getActivity(), BackgroundService.class);
		i.putExtra(BackgroundService.INTENT_EXTRA_NEW_DELAY_KEY, BackgroundService.DELAY_EXPRESS);
		getActivity().startService(i);
		
		parentActivity.onTabBarVisiblityChangeRequest(false);
		
		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity)
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(CHAT_NOTIFICATION_IDENTIFIER, 1);
		
		refreshListView();
	}
	
	@Override
	public void onPause() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiver);
		
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(broadcastReceiverimage);
		
		BackgroundService.unsetChatRoleObserver();
		
		//getActivity().stopService(new Intent(getActivity(), BackgroundService.class));
		getActivity().startService(new Intent(getActivity(), BackgroundService.class));

		super.onPause();
	}

	@Override
	public void onClick(View v) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		switch(v.getId()) {
		case R.id.chat_message_send:
			String msg = chatCompose.getText().toString().trim();
			chatCompose.setText("");//reset textbox

			if (!msg.equalsIgnoreCase("")) {
				boolean doSendMessage = true;
				String city = "";

				if (StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED) {
					//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN); 
					//city = ownData.fetchData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME);
					city = userDetailDataSource.getAdvisorChatCityName().trim();
					if (city.equalsIgnoreCase("")) {
						Toast.makeText(getActivity(), "Please set a city of your choice and then send this message!", Toast.LENGTH_SHORT).show();
						chatCompose.setText(msg);
						doSendMessage = false;
					}
				}
				
				if (doSendMessage) {
					tracker.send(GATrackerMaps.EVENT_LIVE_QUOTE_SEND_TEXT);
					
					ContentValues cv = new ContentValues();
					cv.put(ChatDB.COL_CHAT_MSG, msg);
					cv.put(ChatDB.COL_CHAT_SENDER, ChatMessages.SENDER_USER);
					cv.put(ChatDB.COL_CHAT_RECEIVER, ROLE_ID);
					cv.put(ChatDB.COL_CHAT_READ_FLAG, 1); //user sent message is always read by user!
					cv.put(ChatDB.COL_CHAT_TYPE, 0);
					cv.put(ChatDB.COL_CHAT_CITY, city);
					
					long reqId = chatDataSource.insert(cv);
				}
				refreshListView();
			}
			else {
				Toast.makeText(getActivity(), "Please enter your message to send!", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.take_photo_button:
			selectImage();
			tracker.send(GATrackerMaps.EVENT_LIVE_QUOTE_CLICK_SNAP);
/*
			//Toast.makeText(getActivity(), "Take Photo!", 0).show();
			Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			try {
				File f = createImageFile();
				i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
				
				StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH = f.getAbsolutePath();
				boolean b = MyJSONData.editMyData(getActivity(), 
						new String[] {MyJSONData.FIELD_QUOTE_PHOTO_ATTEMPT_PATH}, 
						new String[] {f.getAbsolutePath()}, 
						MyJSONData.TYPE_OWN);
				
				//Log.d(TAG, "Abs Path: " + f.getAbsolutePath() + "\nFileWrite: " + b);
				
				getActivity().startActivityForResult(i, REQUEST_CODE_CLICK_PHOTO);
			}
			catch (ActivityNotFoundException e) {
				Toast.makeText(getActivity(), "Sorry! Camera not available.", Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				Toast.makeText(getActivity(), "Problem creating image location!", Toast.LENGTH_LONG).show();
				//Log.d(TAG, e.getMessage());
			}
*/
			break;
		case (R.id.chat_city_change_button):
			if(!MyPickerDialogFrag.isPickerDialogOpen() && null!=StaticConfig.LOCS_CHAT_ADVISOR && StaticConfig.LOCS_CHAT_ADVISOR.length >0) {
				DialogFragment pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_CHAT_ADVISOR_CITY_LIST_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_CHAT_CITY_CHOICE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "listPicker");
			}else if(StaticConfig.LOCS_CHAT_ADVISOR==null || StaticConfig.LOCS_CHAT_ADVISOR.length==0){
				Toast.makeText(this.getActivity(), "Fetching locations..Please wait for sometime", Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}
	
	private void selectImage(){
			final CharSequence imageArray[] = { "Take Photo", "From Gallery" };
			//carDetailDataSource = new CarDetailsDatasource(getActivity());		
			//ContentValues cv = new ContentValues();
			AlertDialog.Builder builder = new AlertDialog.Builder(
					getActivity());
			builder.setTitle("Choose Picture:");
			builder.setItems(imageArray, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int item) {

					if (imageArray[item].equals("Take Photo")) {
						// Toast.makeText(getActivity(), "Take Photo!",
						// 0).show();
						tracker.send(GATrackerMaps.EVENT_CAR_CLICK_PIC);
						Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						try {
							File f = createImageFile();
							i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH = f.getAbsolutePath();
							getActivity().startActivityForResult(i, REQUEST_CODE_CLICK_PHOTO);
							//Toast.makeText(getActivity(), "Temp file location: " + mCurrentPhotoPath, Toast.LENGTH_LONG).show();
						}
						catch (ActivityNotFoundException e) {
							Toast.makeText(getActivity(), "Sorry! Camera not available.", Toast.LENGTH_LONG).show();
						} catch (IOException e) {
							Toast.makeText(getActivity(), "Problem creating image location!", Toast.LENGTH_LONG).show();
							//Log.d(TAG, e.getMessage());
						}
					} else if (imageArray[item].equals("From Gallery")) {
						Intent i = new Intent(Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						getActivity().startActivityForResult(i,
								REQUEST_CODE_GALLERY_PHOTO);

					}
				}
			});
			builder.show();

	}
	@SuppressWarnings("unused")
	private void refreshListView() {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		/*if (!StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED 
				|| (ownData.keyNameExistsInData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME) 
					&& !ownData.fetchData(MyJSONData.FIELD_OWN_CHAT_CITY_NAME).equalsIgnoreCase(""))) {*/
		
		if (!StaticConfig.FEATURE_CHAT_MULTI_CITY_ENABLED 
				|| (!userDetailDataSource.getAdvisorChatCityName().trim().equalsIgnoreCase(""))) {
			
			chatCityName.setText(userDetailDataSource.getAdvisorChatCityName().trim());
			chatCityChangeButton.setText("Change Location");
		
			ArrayList<ChatMessages> chatData = chatDataSource.selectForRole(ROLE_ID);
			
			if (chatData.size() > 0) {
				listMessages.setVisibility(View.VISIBLE);
				tvListEmptyNote.setVisibility(View.GONE);
				ChatAdapter mListAdapter = new ChatAdapter(getActivity(),getActivity().getLayoutInflater(), 
						R.layout.chat_row_left, R.layout.chat_row_right, chatData.toArray(new ChatMessages[chatData.size()]), this);
				
				listMessages.setAdapter(mListAdapter);
				listMessages.setSelection(chatData.size() - 1);
			}
			else {
				listMessages.setVisibility(View.GONE);
				tvListEmptyNote.setText("1. Click a Photo of your car damage\n\n" +
										"2. Send it to our Service Advisor\n\n" +
										"3. Get a service repair quote!");
				tvListEmptyNote.setVisibility(View.VISIBLE);
			}
			
			chatDataSource.markAllReadForRole(ROLE_ID);
		}
		else {
			chatCityName.setText("--");
			chatCityChangeButton.setText("Set Location");
			
			DialogFragment pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_CHAT_ADVISOR_CITY_LIST_PICKER);
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_CHAT_CITY_CHOICE);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "listPicker");
		}
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//Toast.makeText(getActivity(), "Broadcastrecceiver Called", 0).show();
			refreshListView();
		}
	};
	private BroadcastReceiver broadcastReceiverimage = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equalsIgnoreCase("show image")){
				String bitmap = intent.getExtras().getString("bitmap");
				parentActivity.onFragmentReplaceRequest(new ViewImageFragment(bitmap), true);	
			}
		}
	};

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
	    // unique filename based on timestamp
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    
	    String imageFileName = "LiveQuote_" + timeStamp + "_";
	    File image = File.createTempFile(imageFileName, ".jpg", getAlbumDir());
	    return image;
	}
	public static File getAlbumDir() {
		File ret = new File(
				Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), 
				"/Accessbox/LiveQuote");
		
		if (!ret.exists()) {
			ret.mkdirs();
		}
		return ret;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		String finalPhotoPath = "";
		if (resultCode == Activity.RESULT_OK) {
			switch(requestCode) {
				/**
				 * Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
						try {
							File f = createImageFile();
							i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
							StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH = f.getAbsolutePath();
							getActivity().startActivityForResult(i, REQUEST_CODE_CLICK_PHOTO);
							//Toast.makeText(getActivity(), "Temp file location: " + mCurrentPhotoPath, Toast.LENGTH_LONG).show();
						}
				 */
			case REQUEST_CODE_GALLERY_PHOTO:
				tracker.send(GATrackerMaps.EVENT_CAR_SET_PIC_FROM_GALLERY);
				Uri selectedImageUri = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = getActivity().getContentResolver().query(
						selectedImageUri, filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				finalPhotoPath = cursor.getString(columnIndex);
				StaticConfig.FIELD_QUOTE_PHOTO_ATTEMPT_PATH = finalPhotoPath;
				
				cursor.close();
				break;	
			}
		}
		else {
			//delete temp holder file that was created
			new File(finalPhotoPath).delete();
		}
	}

	@Override
	public void onSendResult(String result, int requestCode) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		if (requestCode == REQUEST_CODE_CHAT_CITY_CHOICE) {
			ContentValues cv = new ContentValues();
			cv.put(UserDB.COL_FIELD_OWN_ADVISOR_CHAT_CITY_NAME, result.trim());
			userDetailDataSource.updateUserDetails(cv);
			/*MyJSONData.editMyData(getActivity(), 
					new String[] {MyJSONData.FIELD_OWN_CHAT_CITY_NAME}, 
					new String[] {result}, MyJSONData.TYPE_OWN);*/
			
			refreshListView();
		}
	}

	// Call book service from chat row 
	@Override
	public void bookServiceClicked() {
		parentActivity.onFragmentReplaceRequest(new ServiceBaseFragment(), true);
		
	}
}
