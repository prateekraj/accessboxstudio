package fragment;

import db.UserDetailsDataSource;
import entity.CallForAction;
import entity.ContactHolder;
import entity.Offers;
import googleAnalytics.GATrackerMaps;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;


import service.AsyncInvokeURLTask;
import service.AsyncInvokeURLTask.OnPostExecuteListener;
import utils.Downloader;
import utils.UICallback;
import utils.Utils;
import myJsonData.MyJSONData;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;
import com.myaccessbox.appcore.R.string;
import com.squareup.picasso.Picasso;

import config.StaticConfig;

public class OfferDetailFragment extends MyFragment implements View.OnClickListener {
	
	Offers thisOffer;
	TextView tvTitle;
	ImageView imgType;
	TextView tvRibbonText;
	TextView tvTeaserText;

	TextView bodyTv;
	ImageView imgView, imgViewPdf;
	
	TextView[] cfaButtons;
	RelativeLayout cfaButtonHolder;
	AlertDialog contactPickFBIssueTryAgainDialog;
	TextView tvAvailOffer;
	ProgressDialog progressDialog;
	private ProgressBar progressBar;
	TextView downloading_image;
	public static OfferDetailFragment newInstance(Offers ofr) {
		Bundle init = new Bundle();
		init.putParcelable("offer", ofr);
		OfferDetailFragment frag = new OfferDetailFragment();
		frag.setArguments(init);
		return frag;
	}
	
	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		state = getArguments();
		thisOffer = (Offers) state.getParcelable("offer");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_offers_detail, container, false);
		
		tvTitle = (TextView) v.findViewById(R.id.textOffersTitle);
		imgType = (ImageView) v.findViewById(R.id.imgOffersType);
		tvRibbonText = (TextView) v.findViewById(R.id.textOffersRibbonTxt);
		tvTeaserText = (TextView) v.findViewById(R.id.textOffersTeaserText);
		progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
		downloading_image = (TextView) v.findViewById(R.id.downloading_image);
		bodyTv = (TextView) v.findViewById(R.id.textOffersDetailBody);
		imgView = (ImageView) v.findViewById(R.id.imgOffersDetail);
		imgView.setOnClickListener(this);
		
		imgViewPdf = (ImageView) v.findViewById(R.id.pdfOffersDetail);
		imgViewPdf.setOnClickListener(this);
		
		tvTitle.setText(thisOffer.getSubject());
		bodyTv.setText(thisOffer.getBody());
		tvTeaserText.setText(thisOffer.getTeaser());
		tvAvailOffer = (TextView) v.findViewById(R.id.buttOffersAvail);
		tvAvailOffer.setOnClickListener(this);
		
		tvTitle.setSelected(true);
		if (thisOffer.getRibbonText().trim().equalsIgnoreCase("")) {
			tvRibbonText.setVisibility(View.GONE);
		}
		else {
			tvRibbonText.setVisibility(View.VISIBLE);
			tvRibbonText.setText(thisOffer.getRibbonText());
		}

		
		if (!thisOffer.getImage().trim().equalsIgnoreCase("") && thisOffer.getImage().contains(".pdf")) {
			imgViewPdf.setVisibility(View.GONE);
			imgView.setVisibility(View.GONE);
			checkBeforeDownload();
		} else if (!thisOffer.getImage().trim().equalsIgnoreCase("") && !thisOffer.getImage().contains(".pdf")) {
			imgView.setVisibility(View.VISIBLE);
			imgViewPdf.setVisibility(View.GONE);
			if(!Utils.isNetworkConnected(getActivity())){
				Toast.makeText(getActivity(), R.string.network_msg_img, Toast.LENGTH_LONG).show();
			}
				String urlString = StaticConfig.API_OFFER_IMAGES_BASE + thisOffer.getImage().trim();
				System.out.println("urlString: "+urlString);
				String url = urlString.replaceAll(" ", "%20");
				Picasso.with(getActivity())

				.load(url)
			    .placeholder(R.drawable.default_image) // optional
			    .error(R.drawable.default_image)         // optional
			    .into(imgView);
			
		}
		else {
			imgView.setVisibility(View.GONE);
		}

		//set imgType src based on the type of this Offer use ofrs.getType
		Integer x = Offers.TYPE_RESOURCE_MAP.get(thisOffer.getType());
		if (x != null) {
			imgType.setImageResource(x);
		} else {
			imgType.setImageResource(android.R.color.transparent);
		}
		
		cfaButtons = new TextView[2];
		cfaButtons[0] = (TextView) v.findViewById(R.id.buttOffersCFA1);
		cfaButtons[0].setOnClickListener(this);
		cfaButtons[1] = (TextView) v.findViewById(R.id.buttOffersCFA2);
		cfaButtons[1].setOnClickListener(this);
		cfaButtonHolder = (RelativeLayout) v.findViewById(R.id.layCFAButtonHolder);

		
		//selectedOffer.getCFACount() => no of buttons & placement decided by this
		int index = 0;
		for (CallForAction cfa : thisOffer.getCFAList()) {
			cfaButtons[index].setText(cfa.getLabel());
			cfaButtons[index].setCompoundDrawablesWithIntrinsicBounds(cfa.getButtonDrawableId(), 0, 0, 0);
			
			index ++;
		}
		
		RelativeLayout.LayoutParams params;
		switch (index) {
			case 1:
				cfaButtonHolder.setVisibility(View.GONE);
				cfaButtons[0].setVisibility(View.VISIBLE);
				cfaButtons[1].setVisibility(View.GONE);

				params = (RelativeLayout.LayoutParams) cfaButtons[0].getLayoutParams();
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
				params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
				break;
			case 2:
				cfaButtonHolder.setVisibility(View.GONE);
				cfaButtons[0].setVisibility(View.VISIBLE);
				cfaButtons[1].setVisibility(View.VISIBLE);

				params = (RelativeLayout.LayoutParams) cfaButtons[0].getLayoutParams();
				params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
				params.addRule(RelativeLayout.CENTER_IN_PARENT, 0);
				break;
			case 0:
			default:
				cfaButtonHolder.setVisibility(View.GONE);
		}

		return v;
	}
	
	private void checkBeforeDownload() {
		String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		File folder = new File(extStorageDirectory, "OffersPdf");
		//System.out.println("FOLDER LENGTH: "+folder.length());
		if(folder.length()==0){
			folder.mkdir();
		}
		
		//System.out.println("FOLDER LENGTH after creating: "+folder.length());
		File file = new File(folder, thisOffer.getImage().trim());
		System.out.println("FILE LENGTH: "+file.length());
		if(file.length()==0){
			try {
				file.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}	
		}
		if(Utils.isNetworkConnected(getActivity()) && file.length()==0){
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(100);
			downloading_image.setVisibility(View.VISIBLE);
			downlodPDF(thisOffer.getImage().trim(), file);
		}else if(!Utils.isNetworkConnected(getActivity()) && file.length()==0){
			imgViewPdf.setImageResource(R.drawable.dummy_document);
			imgViewPdf.setVisibility(View.VISIBLE);
			Toast.makeText(getActivity(), R.string.network_msg_img, Toast.LENGTH_LONG).show();
		}	else if(file.length()!=0){
			imgViewPdf.setImageResource(R.drawable.dummy_document);
			imgViewPdf.setVisibility(View.VISIBLE);
		}	
	}

	private void downlodPDF(String imgNam, File file) {
		System.out.println("DOWNLOADING IMAGE");
		String url = StaticConfig.API_OFFER_IMAGES_BASE + imgNam;
		
		new Downloader(new UICallback() {

			@Override
			public void OnUICallback(Boolean fetched) {
				//String fileName = "/OffersPdf/"+thisOffer.getImage();
				progressBar.setVisibility(View.GONE);
				downloading_image.setVisibility(View.GONE);
				imgViewPdf.setVisibility(View.VISIBLE);
				File file = new File(Environment.getExternalStorageDirectory()+"/OffersPdf/"+thisOffer.getImage());
				if(fetched && file.exists()){
					imgViewPdf.setImageResource(R.drawable.pdf_view);
					System.out.println("FILE LENGTH after downloading: "+file.length());
				}else{
					imgViewPdf.setImageResource(R.drawable.pdf_error);
					Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
				}	// TODO Auto-generated method stub
				
			}
		}).execute(url,""+file);		
	}

	@Override
	public void onAttach(Activity activity) {
		//first thing to do is to call super!
		super.onAttach(activity); 
		
		//Log.d("MessageListFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_OFFERS_DETAIL);

		toolbarListener.setText(thisOffer.getSubject());
		//setActionBarBackEnabled(true);
	}

	@Override
	public void onClick(View v) {
		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		String offerId = String.valueOf(thisOffer.getID());
		switch (v.getId()) {
		case R.id.buttOffersCFA1:
			processCFA(0);
			break;
		case R.id.buttOffersCFA2:
			processCFA(1);
			break;
		case R.id.buttOffersAvail:
					if(Utils.isNetworkConnected(getActivity())){
				
						cleverTap.event.push("Clicked Avail Offer button");
						progressDialog = ProgressDialog.show(getActivity(),
								"Sending...", "Please wait for a moment");
						progressDialog.setCancelable(true);
				
						try {
							String params = "?phone="+ StaticConfig.LOGGED_PHONE_NUMBER;
							params += "&offer_id="+ URLEncoder.encode(offerId, "UTF-8");
							params += "&" + StaticConfig.getCMSPass();
				
							AsyncInvokeURLTask task = new AsyncInvokeURLTask(
									new AsyncInvokeURLTask.OnPostExecuteListener() {
										public void onPostExecute(String result) {
											progressDialog.dismiss();
											try {
												JSONObject response = new JSONObject(result);
												if(response.has("error")&& response.getString("error").equalsIgnoreCase("false")){
													Toast.makeText(
															getActivity(),
															"Your request has been received. We will get back to you shortly.",
															Toast.LENGTH_LONG).show();
													cleverTap.event.push("Availed Offer Successfully");
												}else if(response.has("text")){
													Toast.makeText(getActivity(), response.getString("text"), Toast.LENGTH_LONG).show();
												}
											} catch (JSONException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
										}
									}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
				
							task.execute(StaticConfig.API_OFFER_AVAIL + params);
				
						} catch (UnsupportedEncodingException e) {
							// ignore
						} catch (Exception e) {
							// ignore
						}
					} else {
						Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
					}
				break;
		case R.id.pdfOffersDetail:
			Intent i = new Intent(Intent.ACTION_VIEW);
			cleverTap.event.push("Opened Offers Detail Screen");
			if(thisOffer.getImage().contains(".pdf")) {
				i = new Intent(Intent.ACTION_VIEW);
				File file = new File(Environment.getExternalStorageDirectory()+"/OffersPdf/"+thisOffer.getImage());
				if(file.length()==0 && !Utils.isNetworkConnected(getActivity())){
					Toast.makeText(getActivity(), R.string.network_msg, Toast.LENGTH_LONG).show();
				}else if(file.length()==0 && Utils.isNetworkConnected(getActivity())){
					Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
				}else{
				i.setDataAndType(Uri.fromFile(file), "application/pdf");
				try {
					startActivity(i);
				}
				catch (ActivityNotFoundException e) {
					//Log.e(TAG, "No PDF reader! " + e.getMessage());
					Toast.makeText(getActivity(), "Please install a PDF reader to view this file!", Toast.LENGTH_LONG).show();
				}
				}
			}
			break;
				}
			}
	
	
	private void processCFA(int index) {
		CallForAction cfa = thisOffer.getCFAatIndex(index);
		Intent i;
		switch (cfa.getType()) {
		case CallForAction.TYPE_CALL:
			i = new Intent(Intent.ACTION_CALL);
			i.setData(Uri.parse("tel:" + cfa.getDetails()));
			startActivity(i);
			break;
		case CallForAction.TYPE_WEB:
			try {
				i = new Intent(Intent.ACTION_VIEW, Uri.parse(cfa.getDetails()));
				startActivity(i);
			}
			catch (ActivityNotFoundException e) {
				//Log.e("MessageDetailActivity", "Problem invoking URL: " + e.toString());
				Toast.makeText(getActivity(), "Unable to open weblink!", Toast.LENGTH_SHORT).show();
			}
			break;
		case CallForAction.TYPE_REFER:
			//mGaTracker.sendEvent(CATEGORY_OFFERS, ACTION_RAF, "Offer ID:" + selectedOffer.getID(), null);

			startContactPicker();
			break;
		default:
			//Log.e("MessageDetailActivity", "This should never occour!");
			return;
		}
		
		tracker.send(GATrackerMaps.getOffersCFAEvent(cfa.getType(), "Offer ID:" + thisOffer.getID()));
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		ContactHolder contact;
		if (resultCode == Activity.RESULT_OK) {
			contact = CallForAction.getContactDetails(getActivity().getContentResolver(), data.getData());
			
			if (contact != null) {
				try {
					AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
						public void onPostExecute(String result) {
							try {
								JSONObject response = new JSONObject(result);
								if(response.has("success")&& response.getString("success").equalsIgnoreCase("true")){
									Toast.makeText(getActivity(), "Friend referal request successfully sent... Thanks!", Toast.LENGTH_LONG).show();
								}else{
									Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}, AsyncInvokeURLTask.REQUEST_TYPE_GET);

					//MyJSONData ownData = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
					UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
					String executeURL = StaticConfig.API_REFER_A_FRIEND + "?referer=" + userDetailDataSource.getUserOwnNumber();
					executeURL += "&referee_name=" + URLEncoder.encode(contact.getName(), "UTF-8");
					executeURL += "&referee_num=" + URLEncoder.encode(contact.getNumber(), "UTF-8");
					executeURL += "&source_id=" + thisOffer.getID();
					executeURL += "&source=2"; // for offers-generated-referral

					task.execute(executeURL);
					//Log.d("MessagesDetailFrag", "execute called with url: " + executeURL);
				} catch (Exception e1) {
					//Log.e("MessagesDetailFrag", "Exception: " + e1.toString());
				}
			}
			else {
				contactPickFBIssueTryAgainDialog = CallForAction.getContactNotFoundDialog(getActivity(), 
						"Pick Again", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
								startContactPicker();
							}
						},
						
						"Cancel", new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								contactPickFBIssueTryAgainDialog.dismiss();
							}
						}
				);
			}
		}
	}
	
	private void startContactPicker() {
		Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
		getActivity().startActivityForResult(i, 1);
	}
}
