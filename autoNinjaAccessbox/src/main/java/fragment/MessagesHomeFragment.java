package fragment;

import googleAnalytics.GATrackerMaps;

import java.nio.channels.GatheringByteChannel;

import myJsonData.MessagesData;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myaccessbox.appcore.R;

import db.ChatDataSource;
import db.DatabaseHelper;
import entity.ChatMessages;

public class MessagesHomeFragment extends MyFragment implements View.OnClickListener {
	
	public static final int REDIRECT_TO_INBOX = 777;
	public static final int REDIRECT_TO_CEO = 555;
	public static final int REDIRECT_TO_ADVISOR = 333;
	
	RelativeLayout msgTalkToCEOButton;
	RelativeLayout msgTalkToSAButton; // SA = service advisor
	RelativeLayout msgViewInboxButton;
	
	private static final String DEFAULT_CEO_CHAT_SUBTITLE = "Send your feedback directly to the CEO!";
	private static final String DEFAULT_SA_CHAT_SUBTITLE = "Show us what's wrong and we'll tell you how much it would cost to fix it.";
	private static final String DEFAULT_INBOX_SUBTITLE = "Check your messages here.";
	
	ChatDataSource chatDatasource;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		switch(initExtraInt) {
		case REDIRECT_TO_INBOX:
			parentActivity.onFragmentReplaceRequest(new MessageListFragment(), true);

			break;
		case REDIRECT_TO_CEO:
			parentActivity.onFragmentReplaceRequest(new ChatCEOFragment(), true);
			
			break;
		case REDIRECT_TO_ADVISOR:
			parentActivity.onFragmentReplaceRequest(new ChatAdvisorFragment(), true);

			break;
		}
		
		chatDatasource = new ChatDataSource(getActivity());
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.frag_messages_home, container, false); 
		msgTalkToCEOButton = (RelativeLayout) v.findViewById(R.id.msg_ceo_chat_launch);
		msgTalkToSAButton = (RelativeLayout) v.findViewById(R.id.msg_sa_chat_launch);
		msgViewInboxButton = (RelativeLayout) v.findViewById(R.id.msg_inbox_launch);
		
		styleLauncherButton(msgTalkToCEOButton, "Talk to the CEO", DEFAULT_CEO_CHAT_SUBTITLE, false);
		styleLauncherButton(msgTalkToSAButton, "Get a Service Quote", DEFAULT_SA_CHAT_SUBTITLE, false);
		styleLauncherButton(msgViewInboxButton, "Inbox", DEFAULT_INBOX_SUBTITLE, false);
		
		msgTalkToCEOButton.setOnClickListener(this);
		msgTalkToSAButton.setOnClickListener(this);
		msgViewInboxButton.setOnClickListener(this);

		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_MESSAGES_HOME);
		cleverTap.event.push("Opened Messages Home Screen");
		
		toolbarListener.setText("Your Messages");
		parentActivity.onTabBarVisiblityChangeRequest(false);
		
		int cnt;
		//CEO chat unread counter code
		cnt = chatDatasource.getUnreadCountForRole(ChatMessages.SENDER_CEO);
		if (cnt > 0) {
			setLauncherButtonSubtitle(msgTalkToCEOButton, cnt + " New message" + (cnt == 1 ? "" : "s") + " from CEO", true);
		}
		else {
			setLauncherButtonSubtitle(msgTalkToCEOButton, DEFAULT_CEO_CHAT_SUBTITLE, false);
		}
		
		//Service Advisor chat unread counter code
		cnt = chatDatasource.getUnreadCountForRole(ChatMessages.SENDER_SERVICE_ADVISOR);
		if (cnt > 0) {
			setLauncherButtonSubtitle(msgTalkToSAButton, cnt + " Unread repair quote" + (cnt == 1 ? "" : "s") + " from Service Advisor", true);
		}
		else {
			setLauncherButtonSubtitle(msgTalkToSAButton, DEFAULT_SA_CHAT_SUBTITLE, false);
		}
		
		//Inbox unread counter code
		cnt = MessagesData.countUnread(getActivity());
		if (cnt > 0) {
			setLauncherButtonSubtitle(msgViewInboxButton, cnt + " Unread messages", true);
		}
		else {
			setLauncherButtonSubtitle(msgViewInboxButton, DEFAULT_INBOX_SUBTITLE, false);
		}
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity); //call super first before doing anything else!
	}
	
	private void styleLauncherButton(RelativeLayout button, String title, String subTitle, boolean makeSubtitleBold) {
		setLauncherButtonTitle(button, title);
		setLauncherButtonSubtitle(button, subTitle, makeSubtitleBold);
	}

	private void setLauncherButtonSubtitle(RelativeLayout button, String subTitle, boolean makeBold) {
		TextView tmp = (TextView) button.findViewById(R.id.subtitle_text); 

		tmp.setText(subTitle);
		tmp.setTypeface(makeBold ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
	}

	private void setLauncherButtonTitle(RelativeLayout button, String title) {
		((TextView) button.findViewById(R.id.title_text)).setText(title);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.msg_ceo_chat_launch:
			tracker.send(GATrackerMaps.EVENT_MSGHOME_OPEN_CEO_CHAT);
			cleverTap.event.push("Clicked Talk to CEO button - Messages Home Screen");
			
			parentActivity.onFragmentReplaceRequest(new ChatCEOFragment(), true);
			break;
		case R.id.msg_sa_chat_launch:
			tracker.send(GATrackerMaps.EVENT_MSGHOME_OPEN_LIVE_QUOTE);
			cleverTap.event.push("Clicked Live Quote button - Messages Home Screen");
			
			parentActivity.onFragmentReplaceRequest(new ChatAdvisorFragment(), true);
			break;
		case R.id.msg_inbox_launch:
			tracker.send(GATrackerMaps.EVENT_MSGHOME_OPEN_INBOX);
			cleverTap.event.push("Clicked Inbox button - Messages Home Screen");
			
			parentActivity.onFragmentReplaceRequest(new MessageListFragment(), true);
			break;
		}
	}
}
