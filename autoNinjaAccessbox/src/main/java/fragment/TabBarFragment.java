package fragment;


import java.util.ArrayList; 
import java.util.Arrays;
import java.util.List;

import com.myaccessbox.appcore.R;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import config.ConfigInfo;
import config.StaticConfig;


public class TabBarFragment extends Fragment implements View.OnClickListener {
	
	MyOnTabChangeListener parent; //parent activity implementing MyOnTabChangeListener
	int _activeTab; //index of active tab
	
	private static List<Integer> _tabIds = new ArrayList<Integer>(); //R.ids of tabViews
	private static List<LinearLayout> _tabViews = new ArrayList<LinearLayout>(); 
	
	int tabCount = StaticConfig.getTabCount(); //#tabCount to be displayed
	
	public interface MyOnTabChangeListener { //the interface that its parent needs to implement
		public void onTabChanged(ConfigInfo.Tab selectedTab);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//Log.d("TabBarFrag", "onCreateView");
		//inflate the tabbar with all 5 tabs
		View ret = inflater.inflate(R.layout.tabbar_frag_layout, container, false);
		
		_tabViews = new ArrayList<LinearLayout>();

		int count = 0;
		//iterate through all tabIds and set clickListeners / hide if necessary
		for (int x:_tabIds) {
			LinearLayout tempLay = (LinearLayout) ret.findViewById(x);
			
			ConfigInfo.Tab tInfo;
			if (count < tabCount) {
				_tabViews.add(tempLay); //add to tabviews if visible
				
				tempLay.setOnClickListener(this);
				
				tInfo = StaticConfig.getTabInfoAtPosition(count);
				
				((ImageView) tempLay.findViewById(R.id.tab_icon)).setImageResource(tInfo.getDrawableIconResourceID());
				((TextView) tempLay.findViewById(R.id.tab_title)).setText(tInfo.getTitle());
				((TextView) tempLay.findViewById(R.id.tab_title)).setSelected(true); //line to make maquee work if required
				tempLay.setVisibility(View.VISIBLE);
			}
			else {
				tempLay.setVisibility(View.GONE);
			}
			count ++;
		}
		return ret;
	}

	@Override
	public void onClick(View v) {
		int x = getTabIndex(v.getId());
		if (_activeTab != x) {// if clicked tab not active, process the click
			
			setActiveTab(x);
			
			//notify parent that a new tab was clicked
			parent.onTabChanged(StaticConfig.getTabInfoAtPosition(_activeTab));
		}
		//else ignore
		//Log.d("TabBarFrag", "onClick");
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		//Log.d("TabBarFrag", "onAttach called!");
		parent = (MyOnTabChangeListener) activity; //to make sure parent implements the interface
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Log.d("TabBarFrag", "onCreate");
		_activeTab = -1; //initialize activeTab to -1 (as not defined)
		_tabIds = Arrays.asList(new Integer[]{R.id.tab1, R.id.tab2, R.id.tab3, R.id.tab4, R.id.tab5});
	}
	
	public void setActiveTab(int tabIndex) {// public method to be called by containing activity to set/switch tabs
		//Note: this function only updates the view of the tab-bar
		if (_activeTab != -1) {//remove the orange background
			LinearLayout tempLay = _tabViews.get(_activeTab);
			tempLay.setSelected(false);
			((TextView) tempLay.findViewById(R.id.tab_title)).setSelected(true); //line to make maquee work if required		
		}
		
		_activeTab = tabIndex;
		_tabViews.get(tabIndex).setSelected(true);
		//Log.d("TabBarFrag", "newActiveTab: " + _activeTab);
	}
	
	private int getTabIndex(int tabId) {//convert R.id to List-Index 
		return _tabIds.indexOf(tabId);
	}
}
