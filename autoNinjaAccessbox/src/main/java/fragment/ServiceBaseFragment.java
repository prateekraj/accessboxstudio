package fragment;

import googleAnalytics.GATrackerMaps;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.StringTokenizer;

import myJsonData.MyJSONData;
import service.AsyncInvokeURLTask;
import service.ServiceLocations;
import utils.Utils;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.anim;
import com.myaccessbox.appcore.R.drawable;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

import config.StaticConfig;

import db.CarDetailsDatasource;
import db.DatabaseHelper;
import db.LocationsDataSource;
import db.ServiceBookingDataSource;
import db.TableContract.CarDetailsDB;
import db.TableContract.LocationsDB;
import db.TableContract.ServiceBookingDB;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import dialog.MyPickerDialogFrag;

public class ServiceBaseFragment extends MyHotlineMultiSelectFragment implements View.OnClickListener, MyPickerDialogFrag.onSendResultListener {

	public static boolean validTime = false;

	TextView dueDateTv;
	TextView dueRemarkTv;
	TextView alreadyServiced; // has click

	TextView nextServDateInputTv; // has click
	RadioGroup lastServicedAtRG;
	TextView submitManual, textNoCar; // has click
	LinearLayout service_book_llyt;
	Spinner carList;
	LinearLayout carSpinnerLL;
	ArrayAdapter<String> dataAdapter;
	TextView pickServiceCenterDropTv; // has click
	TextView selectAppointmentDateTv; // has click
	TextView selectAppointmentTimeTv; // has click
	TextView bookServiceButton; // has click
	TextView retry;
	CheckBox myCarPickUp;
	CheckBox marutiMobileSupport;
	private String selected_loc;
	TextView bookServiceByCallButton; // has click

	RelativeLayout alreadyExpanded;
	// RelativeLayout alreadyHeader;
	LinearLayout alreadyHolder;
	// RelativeLayout otherContent;
	ImageView bottomLine;
	static ProgressDialog progressDialog;
	Context context;
	String manualNextServDateRawResultFromDialog = "";
	String manualBookServDateRawResultFromDialog = "";
	private static int CHECK_BOOKING_STATUS = 0;
	private static final int REQUEST_CODE_MANUAL_NEXT_SERV_DATE = 11;
	private static final int REQUEST_CODE_SERVICE_BOOKING_DATE = 22;
	private static final int REQUEST_CODE_SERVICE_BOOKING_TIME = 33;
	private static final int REQUEST_CODE_SERVICE_CENTER_CHOICE = 44;
	private static final int REQUEST_CODE_SERVICE_BOOKING_AVAILABILITY = 55;
	private static final int REQUEST_CODE_SERVICE_BOOKING_TODAY = 66;
	LocationsDataSource locationDataSource;
	boolean alreadyServicedOpen = false;
	RelativeLayout bookServiceLyt;
	RelativeLayout bookByCallLyt;
	RelativeLayout no_internetLyt;
	
	
	protected Dialog dlgChangeCar;
	public CarDetailsDatasource carDetailDataSource;
	Animation.AnimationListener animListener = new Animation.AnimationListener() {
		@Override
		public void onAnimationStart(Animation animation) {
			if (alreadyServicedOpen) {
				alreadyExpanded.setVisibility(View.VISIBLE);
				alreadyServiced.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.orange_minus, 0);
				dueDateTv.setVisibility(View.INVISIBLE);
				dueRemarkTv.setVisibility(View.INVISIBLE);
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			if (!alreadyServicedOpen) {
				alreadyExpanded.setVisibility(View.GONE);
				alreadyServiced.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.orange_plus, 0);
				dueDateTv.setVisibility(View.VISIBLE);
				dueRemarkTv.setVisibility(View.VISIBLE);
			}
		}
	};

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Log.d("ServiceBaseFrag", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_service, container, false);
		context = getActivity();
		locationDataSource = new LocationsDataSource(context);
		service_book_llyt = (LinearLayout) v.findViewById(R.id.service_book_llyt);
		textNoCar = (TextView) v.findViewById(R.id.textNoCar);
		alreadyHolder = (LinearLayout) v.findViewById(R.id.already_q_holder);
		alreadyExpanded = (RelativeLayout) v.findViewById(R.id.already_q_expanded);
		bottomLine = (ImageView) v.findViewById(R.id.bottom_line);

		dueDateTv = (TextView) v.findViewById(R.id.due_date_tv);
		dueRemarkTv = (TextView) v.findViewById(R.id.due_remark_tv);
		alreadyServiced = (TextView) v.findViewById(R.id.already_qtv);
		bookServiceByCallButton = (TextView) v.findViewById(R.id.book_by_call_button);
		carList = (Spinner) v.findViewById(R.id.car_list);
		carSpinnerLL = (LinearLayout) v.findViewById(R.id.select_car_layout);
		nextServDateInputTv = (TextView) v.findViewById(R.id.next_serv_date_input);
		lastServicedAtRG = (RadioGroup) v.findViewById(R.id.radio_grp_workshop);
		((RadioButton) v.findViewById(R.id.radio_advaith)).setText(StaticConfig.DEALER_FIRST_NAME);
		submitManual = (TextView) v.findViewById(R.id.submit_manual_next_date);
		pickServiceCenterDropTv = (TextView) v.findViewById(R.id.choose_service_center_tv);
		selectAppointmentDateTv = (TextView) v.findViewById(R.id.choose_service_date_tv);
		selectAppointmentTimeTv = (TextView) v.findViewById(R.id.choose_service_time_tv);
		bookServiceButton = (TextView) v.findViewById(R.id.submit_book_service_request);
		retry = (TextView) v.findViewById(R.id.retry_button);
		myCarPickUp = (CheckBox) v.findViewById(R.id.pick_up_car_check_box);
		marutiMobileSupport = (CheckBox) v.findViewById(R.id.maruti_mobile_support_check_box);
		carDetailDataSource = new CarDetailsDatasource(getActivity());
		carList = (Spinner) v.findViewById(R.id.car_list);
		dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_multiplecar, StaticConfig.myCarListNames);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bookServiceLyt = (RelativeLayout) v.findViewById(R.id.serv_other_content);
		bookByCallLyt = (RelativeLayout) v.findViewById(R.id.call_book_service);
		no_internetLyt = (RelativeLayout) v.findViewById(R.id.no_internet_lyt);
		carList.setAdapter(dataAdapter);
		carList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String chosenModelName = parent.getItemAtPosition(position).toString();
				StringTokenizer st = new StringTokenizer(chosenModelName, ",");
				String model = st.nextToken();
				String regNum = st.nextToken().trim();
				for (int i = 0; i < StaticConfig.myCarList.size(); i++) {

					if (StaticConfig.myCarList.get(i).getModelName().equalsIgnoreCase(model)
							&& StaticConfig.myCarList.get(i).getReg_num().equalsIgnoreCase(regNum)) {
						StaticConfig.myCar = StaticConfig.myCarList.get(i);
						StaticConfig.position = position;
						UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
						ContentValues cv = new ContentValues();
						cv.put(UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION, position);
						userDetailDataSource.updateUserDetails(cv);
						// dbhelper.getCarList();
					}
				}
				setSpinnerData();
				refreshData();
				otherSettings();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		if (StaticConfig.FEATURE_MARUTI_MOBILE_SUPPORT) {
			marutiMobileSupport.setVisibility(View.VISIBLE);
		} else {
			marutiMobileSupport.setVisibility(View.GONE);
			marutiMobileSupport.setSelected(false);
		}

		alreadyServiced.setOnClickListener(this);
		nextServDateInputTv.setOnClickListener(this);
		submitManual.setOnClickListener(this);
		pickServiceCenterDropTv.setOnClickListener(this);
		selectAppointmentDateTv.setOnClickListener(this);
		selectAppointmentTimeTv.setOnClickListener(this);
		bookServiceButton.setOnClickListener(this);
		retry.setOnClickListener(this);
		bookServiceByCallButton.setOnClickListener(this);

		// init code
		alreadyServiced.setText("Already Serviced?");
		alreadyServicedOpen = false;
		alreadyServiced.setCompoundDrawablesWithIntrinsicBounds(0, 0, alreadyServicedOpen ? R.drawable.orange_minus : R.drawable.orange_plus, 0);
		alreadyExpanded.setVisibility(alreadyServicedOpen ? View.VISIBLE : View.GONE);
		dueDateTv.setVisibility(alreadyServicedOpen ? View.INVISIBLE : View.VISIBLE);
		dueRemarkTv.setVisibility(alreadyServicedOpen ? View.INVISIBLE : View.VISIBLE);

		return v;
	}

	public void otherSettings() {
		if((Utils.isNetworkConnected(context)
				&& !locationDataSource.getLocationIdsWith0())
				&& null != StaticConfig.LOCS_SERVICE_STATIONS
				&& StaticConfig.LOCS_SERVICE_STATIONS.length > 0){
			bookServiceLyt.setVisibility(View.VISIBLE);
			bookByCallLyt.setVisibility(View.VISIBLE);
			no_internetLyt.setVisibility(View.GONE);
		} else if(!Utils.isNetworkConnected(context)
				  || locationDataSource.getLocationIdsWith0() 
				  || null == StaticConfig.LOCS_SERVICE_STATIONS 
	              || StaticConfig.LOCS_SERVICE_STATIONS.length <= 0){
			bookServiceLyt.setVisibility(View.GONE);
			bookByCallLyt.setVisibility(View.VISIBLE);
			no_internetLyt.setVisibility(View.VISIBLE);
			//Toast.makeText(getActivity(),"Please check your internet connection to book service via app", Toast.LENGTH_LONG).show();
		}
		if (null == StaticConfig.LOCS_SERVICE_STATIONS || StaticConfig.LOCS_SERVICE_STATIONS.length == 0) {
			ServiceLocations.setLocations(getActivity());
		}
		if (StaticConfig.myCarList.isEmpty()) {
			//carDetailDataSource = new CarDetailsDatasource(getActivity());
			if (carDetailDataSource.getCarList() > 0) {
				service_book_llyt.setVisibility(View.VISIBLE);
				textNoCar.setVisibility(View.GONE);
			} else {
				service_book_llyt.setVisibility(View.GONE);
				textNoCar.setVisibility(View.VISIBLE);
				textNoCar.setText(R.string.no_car_added);
			}
		} else if (null != StaticConfig.LOCS_SERVICE_STATIONS && StaticConfig.LOCS_SERVICE_STATIONS.length > 0) {
			service_book_llyt.setVisibility(View.VISIBLE);
			textNoCar.setVisibility(View.GONE);
		} /*else {
			service_book_llyt.setVisibility(View.GONE);
			textNoCar.setVisibility(View.VISIBLE);
			textNoCar.setText(R.string.try_msg);

		}*/
		if (null != StaticConfig.LOCS_SERVICE_STATIONS) {
			if (StaticConfig.LOCS_SERVICE_STATIONS.length > 1) {
				pickServiceCenterDropTv.setOnClickListener(this);
				pickServiceCenterDropTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.triangle_down, 0);
			} else if (StaticConfig.LOCS_SERVICE_STATIONS.length == 1) {
				pickServiceCenterDropTv.setText(StaticConfig.LOCS_SERVICE_STATIONS[0].trim());
			}
		}
	}

	public void setSpinnerData() {
		if (StaticConfig.myCarListNames.size() <= 0) {
			carList.setVisibility(View.GONE);
			carSpinnerLL.setVisibility(View.GONE);
		} else {
			dataAdapter.notifyDataSetChanged();
			carList.setSelection(StaticConfig.position);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		// first thing to do is to call super!
		super.onAttach(activity);

		// Log.d("ServiceBaseFragment", "OnAttach Called!");
	}

	@Override
	public void onResume() {
		// ** NOTE: onResume is too early to getMeasuredHeight! ** //
		super.onResume();
		cleverTap.event.push("Opened Book Service Screen");
		setSpinnerData();
		otherSettings();
		carDetailDataSource = new CarDetailsDatasource(getActivity());
		tracker.send(GATrackerMaps.VIEW_SERVICE);

		toolbarListener.setText("Book Service");
		// setActionBarBackEnabled(false);

		NotificationManager nm = (NotificationManager) ((FragmentActivity) parentActivity).getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel("ServiceDue", 1);

		refreshData();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		DialogFragment pickerFragDialog;
		final DatabaseHelper dbhelper = new DatabaseHelper(getActivity());
		UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
		ContentValues cv = new ContentValues();
		switch (v.getId()) {
		case R.id.already_qtv:
			alreadyServicedOpen = !alreadyServicedOpen;

			Animation a;
			if (alreadyServicedOpen) {
				tracker.send(GATrackerMaps.EVENT_SERVICE_ALREADY_SERVICED);
				a = AnimationUtils.loadAnimation(getActivity(), R.anim.open);
			} else {
				a = AnimationUtils.loadAnimation(getActivity(), R.anim.close);
			}
			a.setAnimationListener(animListener);
			alreadyExpanded.startAnimation(a);
			break;
		case (R.id.next_serv_date_input):
			// pickerFragDialog = new DatePickerFragment();
			if (!MyPickerDialogFrag.isPickerDialogOpen()) {
				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_DATE_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_MANUAL_NEXT_SERV_DATE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
			}
			// Date Picker dialogue
			break;
		case (R.id.submit_manual_next_date):
			tracker.send(GATrackerMaps.EVENT_SERVICE_SET_DATE_SERVICE);

			// logic in refreshData function is such that
			// this button will not be visible/clickable/accessible
			// if myCar data does not exist
			// Hence, only UPDATE next service date
			if (manualNextServDateRawResultFromDialog.equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Next Service Date can't be blank!", Toast.LENGTH_LONG).show();
			} else {
				String lastServicedAt = "";
				int x = lastServicedAtRG.getCheckedRadioButtonId();
				if (x == R.id.radio_advaith)
					lastServicedAt = StaticConfig.DEALER_FIRST_NAME;
				else if (x == R.id.radio_non_advaith)
					lastServicedAt = "Non " + StaticConfig.DEALER_FIRST_NAME;

				cv.put(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE, MyJSONData.getServerFormatFromChosen(manualNextServDateRawResultFromDialog));
				cv.put(CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT, lastServicedAt);

				if (carDetailDataSource.updateCarDetails(cv, StaticConfig.myCar.getCarId()) > 0) {
					refreshData();
					alreadyServiced.performClick();
					Cursor c = carDetailDataSource.getDetailsOfCarId();
					MyCarBaseFragment.pingInsuPUCUpdateToServer(c, getActivity());
				}
			}
			break;
		case (R.id.choose_service_center_tv):
			if (!MyPickerDialogFrag.isPickerDialogOpen() && null != StaticConfig.LOCS_SERVICE_STATIONS
					&& StaticConfig.LOCS_SERVICE_STATIONS.length > 0) {
				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_SERVICE_LIST_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_SERVICE_CENTER_CHOICE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "listPicker");
			} else if (null == StaticConfig.LOCS_SERVICE_STATIONS || StaticConfig.LOCS_SERVICE_STATIONS.length == 0) {
				Toast.makeText(this.getActivity(), "Fetching locations..Please wait for sometime", Toast.LENGTH_SHORT).show();
			}
			break;
		case (R.id.choose_service_date_tv):
			// pickerFragDialog = new DatePickerFragment();
			if (!MyPickerDialogFrag.isPickerDialogOpen()) {
				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_DATE_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_SERVICE_BOOKING_DATE);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
			}
			// Date picker dialogue
			break;
		case (R.id.choose_service_time_tv):
			// pickerFragDialog = new TimePickerFragment();
			if (!MyPickerDialogFrag.isPickerDialogOpen()) {
				pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_TIME_PICKER);
				pickerFragDialog.setTargetFragment(this, REQUEST_CODE_SERVICE_BOOKING_TIME);
				pickerFragDialog.show(getActivity().getSupportFragmentManager(), "timePicker");
			}
			// Time picker dialogue
			break;
		case (R.id.retry_button):
			if(Utils.isNetworkConnected(context)){
			 	fetchLocations(context);
			}else{
				Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
			}
			
			break;
		case (R.id.submit_book_service_request):
			if (pickServiceCenterDropTv.getVisibility() == View.VISIBLE && pickServiceCenterDropTv.getText().toString().trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please select a service center of your choice before proceeding!", Toast.LENGTH_LONG).show();
				return;
			} else if (pickServiceCenterDropTv.getVisibility() == View.VISIBLE
					&& !pickServiceCenterDropTv.getText().toString().trim().equalsIgnoreCase("")) {
				selected_loc = pickServiceCenterDropTv.getText().toString();
			} else if (pickServiceCenterDropTv.getVisibility() == View.INVISIBLE) {
				selected_loc = StaticConfig.LOCS_SERVICE_STATIONS[0];
			}
			if (manualBookServDateRawResultFromDialog.trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please set a service appointment date before proceeding!", Toast.LENGTH_LONG).show();
				return;
			}
			if (selectAppointmentTimeTv.getText().toString().trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please set a service appointment time before proceeding!", Toast.LENGTH_LONG).show();
				return;
			}
			if (!ServiceBaseFragment.validTime) {
				showDialog(REQUEST_CODE_SERVICE_BOOKING_TIME);
				return;
			}
			if (StaticConfig.SERVICE_BOOKING_TIME_CRITERIA) {
				CHECK_BOOKING_STATUS = Utils.validBooking(manualBookServDateRawResultFromDialog.trim());
				if (CHECK_BOOKING_STATUS != 0) {
					handleCheckBooking(CHECK_BOOKING_STATUS);
					return;
				}
			}

			tracker.send(GATrackerMaps.EVENT_SERVICE_BOOK_INAPP);
			if (Utils.isNetworkConnected(getActivity())) {

				cleverTap.event.push("Clicked Book Service button - Book Service Screen");
				progressDialog = ProgressDialog.show(getActivity(), "Booking your Service", "Please wait for a moment");
				progressDialog.setCancelable(true);
				String nextBookServiceNewDateFormat = MyJSONData.getServerFormatFromChosen(manualBookServDateRawResultFromDialog);
				int location_id = locationDataSource.getLocationId(selected_loc.trim());
				/*
				 * MyJSONData own = new MyJSONData(getActivity(),
				 * MyJSONData.TYPE_OWN);
				 */
				try {
					MultipartEntity mpEntity = new MultipartEntity();
					System.out.println("SERVICE FOR CAR: " + StaticConfig.myCar.getModelName() + "CAR ID: " + StaticConfig.myCar.getCarId());
					try {
						mpEntity.addPart("phone", new StringBody("" + userDetailDataSource.getUserOwnNumber()));
						mpEntity.addPart("car_id", new StringBody("" + StaticConfig.myCar.getCarId()));
						/*
						 * mpEntity.addPart("workshop",new StringBody("" +
						 * selected_loc.trim()));
						 */
						mpEntity.addPart("location_id", new StringBody("" + location_id));
						mpEntity.addPart("date", new StringBody("" + nextBookServiceNewDateFormat));
						mpEntity.addPart("time", new StringBody("" + selectAppointmentTimeTv.getText().toString().trim()));
						mpEntity.addPart("mms", new StringBody("" + (marutiMobileSupport.isChecked() ? "1" : "0")));
						mpEntity.addPart("pick_up", new StringBody("" + (myCarPickUp.isChecked() ? "1" : "0")));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
						public void onPostExecute(String result) {
							progressDialog.dismiss();
							System.out.println("CAR ID:  " + StaticConfig.myCar.getCarId());
							System.out.println("SERVICE RESPONSE:" + result);
							try {
								JSONObject response = new JSONObject(result);
								if (response.has("error") && response.getString("error").equalsIgnoreCase("false")) {
									cleverTap.event.push("Service Booked Successfully");
									Toast.makeText(getActivity(),
											"Your service booking request has been received. We will get back to you with the confirmation shortly.",
											Toast.LENGTH_LONG).show();
									updateToDB(response);
								} else if (response.has("error") && response.getString("error").equalsIgnoreCase("true")) {
									Toast.makeText(getActivity(), response.getString("text"), Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

					}, AsyncInvokeURLTask.REQUEST_TYPE_POST, mpEntity);
					String executeURL = StaticConfig.API_BOOK_SERVICE + "?" + StaticConfig.getCMSPass();
					task.execute(executeURL);

					// clear service booking form, this should avoid double
					// submissions!
					selectAppointmentDateTv.setText("");
					manualBookServDateRawResultFromDialog = "";
					selectAppointmentTimeTv.setText("");
					pickServiceCenterDropTv.setText("");
				} catch (UnsupportedEncodingException e) {
					// ignore
				} catch (Exception e) {
					// ignore
				}
			} else {
				Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
			}
			//}
			break;
		case (R.id.book_by_call_button):
			tracker.send(GATrackerMaps.EVENT_SERVICE_BOOK_BY_CALL);
			cleverTap.event.push("Clicked Book by Call button - Book Service Screen");

			Intent i = new Intent(Intent.ACTION_CALL);

			if (StaticConfig.HOTLINE_FETCHED && StaticConfig.SERVICE_HOTLINE_SELECTOR != null && !StaticConfig.SERVICE_HOTLINE_SELECTOR.isEmpty()) {
				if (StaticConfig.SERVICE_HOTLINE_SELECTOR.size() == 1) {
					i.setData(Uri.parse("tel:" + StaticConfig.SERVICE_HOTLINE_SELECTOR.get(0).getNumber()));
					startActivity(i);
				} else {
					if (!HotlinePickerDialogFrag.isPickerDialogOpen()) {
						pickerFragDialog = HotlinePickerDialogFrag.newInstance("Book Service", StaticConfig.SERVICE_HOTLINE_SELECTOR);
						pickerFragDialog.setTargetFragment(this, REQUEST_CODE_HOTLINE_SUB_PICKER);
						pickerFragDialog.show(getActivity().getSupportFragmentManager(), "hotlinePicker");
					}
				}
			} else if (!StaticConfig.HOTLINE_FETCHED) {
				Toast.makeText(getActivity(), "Fetching hotlines.. please wait", Toast.LENGTH_SHORT).show();
			}

			break;
		}
	}

	@SuppressWarnings("deprecation")
	public void showDialog(int requestCode) {
		AlertDialog ad = new AlertDialog.Builder(context).create();
		switch (requestCode) {
		case (REQUEST_CODE_MANUAL_NEXT_SERV_DATE):
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_TODAY):
			ad.setCancelable(false);
			ad.setTitle("Service Availability Alert");
			ad.setMessage("If you want to book a service for today, please use our Hotline facility to contact Service Booking team of the concerned branch. ");
			ad.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					selectAppointmentDateTv.setText("");
					selectAppointmentTimeTv.setText("");
					dialog.dismiss();
				}
			});
			ad.show();
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_AVAILABILITY):
			ad.setCancelable(false);
			ad.setTitle("Service Availability Alert");
			ad.setMessage("Please select a date after 2 days as this slot is not available or use our Hotline facility to contact Service Booking team of the concerned branch. ");
			ad.setButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// selectAppointmentDateTv.setText("");
					// selectAppointmentTimeTv.setText("");
					dialog.dismiss();
				}
			});
			ad.show();
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_DATE):
			ad.setCancelable(false);
			ad.setTitle("Service Date Alert");
			ad.setMessage("Service is closed on a Sunday.Please select a weekday between Monday - Saturday or use our Hotline facility to contact Service Booking team of the concerned branch. ");
			ad.setButton("OK", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					selectAppointmentDateTv.setText("");
					selectAppointmentTimeTv.setText("");
					dialog.dismiss();
				}
			});
			ad.show();
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_TIME):
			ad = new AlertDialog.Builder(context).create();
			ad.setCancelable(false);
			ad.setTitle("Service Time Alert");
			ad.setMessage("Service time should be between 9 am - 5 pm");
			ad.setButton("OK", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					selectAppointmentTimeTv.setText("");
					dialog.dismiss();
				}
			});
			ad.show();
			break;
		}
	}

	@Override
	public void onSendResult(String setString, int requestCode) {
		switch (requestCode) {
		case (REQUEST_CODE_MANUAL_NEXT_SERV_DATE):
			manualNextServDateRawResultFromDialog = setString;
			nextServDateInputTv.setText(MyJSONData.formatDateDialogToDisplay(setString));
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_DATE):
			if (StaticConfig.SERVICE_BOOKING_TIME_CRITERIA) {
				if (Utils.validDate(setString) == 1) {
					showDialog(REQUEST_CODE_SERVICE_BOOKING_DATE);
				} else if (Utils.validDate(setString) == 2) {
					showDialog(REQUEST_CODE_SERVICE_BOOKING_TODAY);
				} else {
					manualBookServDateRawResultFromDialog = setString;
					selectAppointmentDateTv.setText(MyJSONData.formatDateDialogToDisplay(setString));
				}
			} else {
				manualBookServDateRawResultFromDialog = setString;
				selectAppointmentDateTv.setText(MyJSONData.formatDateDialogToDisplay(setString));
			}
			break;
		case (REQUEST_CODE_SERVICE_BOOKING_TIME):
			selectAppointmentTimeTv.setText(setString);
			if (!ServiceBaseFragment.validTime) {
				showDialog(REQUEST_CODE_SERVICE_BOOKING_TIME);
			}
			break;
		case (REQUEST_CODE_SERVICE_CENTER_CHOICE):
			pickServiceCenterDropTv.setText(setString);
			tracker.send(GATrackerMaps.getServiceWorkshopChooseEvent(setString));
			break;
		case (REQUEST_CODE_HOTLINE_SUB_PICKER):
			super.onSendResult(setString, requestCode);
			break;
		}
	}

	private void updateToDB(JSONObject response) {
		ContentValues cv = new ContentValues();
		ServiceBookingDataSource serviceBookingDS = new ServiceBookingDataSource(getActivity());
		try {
			if (response.has("message_id")) {
				cv.put(ServiceBookingDB.COL_BOOKING_MESSAGE_ID, response.getString("message_id"));
			}
			if (response.has("service_booking_id")) {
				cv.put(ServiceBookingDB.COL_BOOKING_ID, response.getString("service_booking_id"));

			}
			if (response.has("car_id")) {
				cv.put(ServiceBookingDB.COL_CAR_ID, response.getString("car_id"));
			}
			serviceBookingDS.insertBookingDetails(cv);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void refreshData() {
		// final DatabaseHelper dbhelper = new
		// DatabaseHelper(getActivity());
		if (StaticConfig.myCar.getCarId() > 0) {
			alreadyHolder.setVisibility(View.VISIBLE);
			bottomLine.setVisibility(View.VISIBLE);

			String nxtSrvDate = carDetailDataSource.getNextServiceDate(StaticConfig.myCar.getCarId());

			if (nxtSrvDate.trim().equalsIgnoreCase("") || nxtSrvDate.equalsIgnoreCase("0000-00-00")) {
				dueDateTv.setText("---");
				alreadyServiced.setText("Set Date ");
			} else {
				dueDateTv.setText(MyJSONData.formatDateServerToDisplay(nxtSrvDate));
				alreadyServiced.setText("Already Serviced?");
			}
		} else {
			alreadyHolder.setVisibility(View.GONE);
			bottomLine.setVisibility(View.GONE);
		}
	}

	private void handleCheckBooking(int checkbooking) {
		switch (checkbooking) {
		/*
		 * case 1: //no sunday showDialog(REQUEST_CODE_SERVICE_BOOKING_DATE);
		 * break;
		 */
		case 2:
			// 2 days after
			showDialog(REQUEST_CODE_SERVICE_BOOKING_AVAILABILITY);
			break;
		/*
		 * case 3: //today showDialog(REQUEST_CODE_SERVICE_BOOKING_TODAY);
		 * break;
		 */
		}
	}

	public void fetchLocations(final Context cxt) {
		if(cxt == context){
		progressDialog = ProgressDialog.show(getActivity(), "Setting locations", "Please wait for a moment");
		}
		String executeURL = StaticConfig.API_FETCH_LOCATIONS;
		executeURL += "?" + StaticConfig.getCMSPass();
		// System.out.println("HOTLINES URL: " + executeURL);

		try {
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {

				private JSONObject jsonObj = null;
				private JSONObject jSonText = null;
				private int new_change_set = 0;
				private JSONArray JsonLocationsArray = null;
				LocationsDataSource locDatasource = new LocationsDataSource(cxt);
				ContentValues cv = null;

				public void onPostExecute(String result) {
					// System.out.println("LOCATIONS:"+result);

					if (null != result) {
						try {
							System.out.println("LOCATIONS RESULT" + result);
							jsonObj = new JSONObject(result);
							if (jsonObj.has("error") && jsonObj.getString("error").equalsIgnoreCase("false")) {
								jSonText = jsonObj.getJSONObject("text");
								// System.out.println("change set loc"+jSonText.optString("change_set"));
								new_change_set = Integer.parseInt(jSonText.optString("change_set"));
								JsonLocationsArray = jSonText.getJSONArray("locations");
							} else {
								jSonText = jsonObj.getJSONObject("text");
							}
							if (jsonObj.has("error")
									&& jsonObj.getString("error").equalsIgnoreCase("false")
									&& (locDatasource.getSavedLocChangedSet() < new_change_set)) {
								locDatasource.deleteLocDb();
								for (int i = 0; i < JsonLocationsArray.length(); i++) {
									JSONObject c = JsonLocationsArray.getJSONObject(i);
									try {
										cv = new ContentValues();
										cv.put(LocationsDB.COL_LOC_NAME, c.optString("name".trim()));
										cv.put(LocationsDB.COL_LOC_ID, Integer.parseInt(c.optString("id".trim())));
										//cv.put(LocationsDB.COL_LOC_ID, 0);
										cv.put(LocationsDB.COL_LOC_SERVICE_ENABLED, Integer.parseInt(c.optString("service_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_CHAT_CEO_ENABLED, Integer.parseInt(c.optString("chat_ceo_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_CHAT_ADVISOR_ENABLED, Integer.parseInt(c.optString("chat_advisor_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_ENQUIRY_ENABLED, Integer.parseInt(c.optString("enquiry_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_PAYMENT_ENABLED, Integer.parseInt(c.optString("payment_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_TEST_DRIVE_ENABLED, Integer.parseInt(c.optString("test_drive_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_TEST_DRIVE_ENABLED, Integer.parseInt(c.optString("test_drive_enabled".trim())));
										cv.put(LocationsDB.COL_LOC_CHANGE_SET, new_change_set);
										locDatasource.insertLocations(cv);
									} catch (SQLException e) {
										e.printStackTrace();
									}
								}
								ServiceLocations.setLocations(cxt);
								
							}
							if(context == cxt ){
								otherSettings();
							}
							
							if (progressDialog != null && progressDialog.isShowing()) {
								progressDialog.dismiss();

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}

			}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
			task.execute(executeURL);
		} catch (Exception e1) {
			// Log.e(TAG, "Exception: " + e1.toString());
		}
	}
}
