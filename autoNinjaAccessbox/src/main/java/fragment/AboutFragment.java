package fragment;

import googleAnalytics.GATrackerMaps;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;
import com.myaccessbox.appcore.R.layout;

public class AboutFragment extends MyFragment implements View.OnClickListener {
	
	private static String TAG = AboutFragment.class.getName();
	
	TextView tvAboutAccessboxWeblink;
	TextView tvAboutPrivacyPolicy;
	
	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		
		//Log.d("MyCarBaseFragment", "OnAttach Called!");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		//Log.d(TAG, "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_about, container, false); 
		
		tvAboutAccessboxWeblink = (TextView) v.findViewById(R.id.about_accessbox_weblink);
		tvAboutPrivacyPolicy = (TextView) v.findViewById(R.id.about_privacy_policy);
		
		tvAboutAccessboxWeblink.setOnClickListener(this);
		tvAboutPrivacyPolicy.setOnClickListener(this);
		return v;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		tracker.send(GATrackerMaps.VIEW_ABOUT);
		cleverTap.event.push("Opened About Accessbox Screen");
		toolbarListener.setText("About Accessbox");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.about_accessbox_weblink:
			tracker.send(GATrackerMaps.EVENT_ABOUT_VISIT_ACCESSBOX);
			cleverTap.event.push("Clicked Hyperlink - About Acessbox");
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.autoninja.in/"));
			startActivity(i);
			break;
		case R.id.about_privacy_policy:
			//TODO: Privacy Policy dialog pop-up once GA is activated!
			//Will be done later! 
			break;
		}
	}
}
