package fragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Random;

import myJsonData.MyJSONData;

import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import service.BackgroundService;
import service.NewSMSBroadcast;
import utils.Utils;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.myaccessbox.appcore.R;

import config.StaticConfig;
import db.TableContract.UserDB;
import db.UserDetailsDataSource;
import activity.MainActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginPasswordFragment extends LoginBaseFragment implements View.OnClickListener {
	
	static EditText passText;
	JSONObject urlResult;
	InputMethodManager imm;
	MyJSONData loginData;
	View view ;
	String passOnFile;
	String mobileFrmFile;
	Boolean goFlag;
	
	private static final String TAG = LoginPasswordFragment.class.getSimpleName();
	
	ProgressDialog progressDialog;
	static ProgressDialog progressDialogWaitForOTP;
	Handler handler = new Handler();
	static TextView resendText;
	
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup container, Bundle savedInstanceState )
	{
		
		view =  inflater.inflate(R.layout.fragment_login,
                container, false);
		cleverTap.event.push("OTP Screen Opened");
		view.findViewById(R.id.burgerButton).setVisibility(View.INVISIBLE);
		passText=(EditText)view.findViewById(R.id.login_mobile_no);
		//passText.setAllCaps(true);
		passText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		passText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
		passText.setHint("Enter the password");
		
		
		TextView title=(TextView)view.findViewById(R.id.title_text);
		title.setText("Enter the password"); 
		
		imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		
		TextView previousText=(TextView)view.findViewById(R.id.login_previous);
		TextView loginText=(TextView)view.findViewById(R.id.login_submit);
		resendText=(TextView)view.findViewById(R.id.resend_otp);
		if(StaticConfig.OTP_RESEND){
		resendText.setVisibility(View.VISIBLE);
		resendText.setOnClickListener(this);
		}
		
		previousText.setOnClickListener(this);
		loginText.setOnClickListener(this);
		
		
		return view;
	}

	@Override
	public void onClick(View v) {
		MyJSONData loginData;
		
		//hide keypad
		imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
		
		switch(v.getId())
		{
		case R.id.login_previous:
			loginListener.onFragmentReplace((Fragment)new LoginMobileFragment(),false);
			break;
			
		case R.id.resend_otp:
			String urlDataResend = "";
			StaticConfig.OTP_PROGRESS_BAR_RUNNING = true;
			progressDialogWaitForOTP = ProgressDialog.show(getActivity(), 
					"Resending OTP", "Reading your OTP please wait");

			try {
				urlDataResend = StaticConfig.API_REQUEST_REGISTER_FOR_OTP;
				urlDataResend += "?phone=" + URLEncoder.encode(StaticConfig.FIELD_LOGIN_NUMBER, "UTF-8");

				AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
					public void onPostExecute(String result) {
						JSONObject returnMsg;
						try {
							returnMsg = new JSONObject(result);
							if (returnMsg.has("success")) {
								/*Toast.makeText(getSherlockActivity(), "Please check your SMS for the generated one time password",
										Toast.LENGTH_LONG).show();*/
							} else {
								Toast.makeText(getActivity(), returnMsg.getString("error"), Toast.LENGTH_LONG).show();
							}
						} catch (JSONException e) {
							// Log.e(TAG, "Json exception: " +
							// e.getMessage());
						}
						//progressDialogWaitForOTP.dismiss();
					}
				}, AsyncInvokeURLTask.REQUEST_TYPE_GET);

				task.execute(urlDataResend);
			} catch (Exception e) {
				// Log.e(TAG, "Default exectpion: " +
				// e.getMessage());

			}
			handler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					progressDialogWaitForOTP.dismiss();
					
				}
			}, 20000);
			break;

		case R.id.login_submit:
			final UserDetailsDataSource userDetailDataSource = new UserDetailsDataSource(getActivity());
			//Log.i("custom", "in submit");

			if(passText.getText().toString().trim().length()==5)
			{
				if(Utils.isNetworkConnected(getActivity()))
				{  
					//progress dialog
					progressDialog=ProgressDialog.show(getActivity(), "Authenticating your password", "Please wait a moment");

					//loginData=new MyJSONData(getActivity(), MyJSONData.TYPE_LOGIN);
					/*mobileFrmFile=loginData.fetchData(MyJSONData.FIELD_LOGIN_NUMBER);*/
					mobileFrmFile = StaticConfig.FIELD_LOGIN_NUMBER;

					//adding password to login file
					StaticConfig.FIELD_LOGIN_OTP = passText.getText().toString().trim();
					//MyJSONData.editMyData(getActivity(), new String[] {MyJSONData.FIELD_LOGIN_OTP}, new String[] {passText.getText().toString()}, MyJSONData.TYPE_LOGIN);

					try {


						//writing password to login file
						


						//generating url
						String urlData = StaticConfig.API_CHECK_OTP_LOGIN;
						urlData+="?phone=" + URLEncoder.encode(mobileFrmFile, "UTF-8");
						urlData+="&otp=" + URLEncoder.encode(passText.getText().toString(), "UTF-8");

						

						//calling asynchronous task to fetch
						AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {
							public void onPostExecute(String result) {
								
								//progress bar
								progressDialog.dismiss();
								
								try {
									urlResult=new JSONObject(result);
									if(urlResult.has("success")) 
									{ 
										/*String[] keys=new String[] {MyJSONData.FIELD_OWN_NUMBER,
																	MyJSONData.FIELD_LAST_REMIND_CHECK,
																	MyJSONData.FIELD_LAST_API_CHECK,
																	MyJSONData.FIELD_RANDOM_HOUR,
																	MyJSONData.FIELD_OWN_OTP};*/
										ContentValues cv = new ContentValues();
										cv.put(UserDB.COL_FIELD_OWN_NUMBER, Long.parseLong(mobileFrmFile.trim()));
										cv.put(UserDB.COL_FIELD_OWN_REFERRAL_CODE, StaticConfig.FIELD_REF_CODE.trim());
										cv.put(UserDB.COL_FIELD_LAST_API_CHECK, "01-01-2013:P");
										cv.put(UserDB.COL_FIELD_RANDOM_HOUR, new Random().nextInt(12));
										cv.put(UserDB.COL_FIELD_OWN_OTP, passText.getText().toString());
										StaticConfig.LOGGED_PHONE_NUMBER = Long.parseLong(mobileFrmFile.trim());
										goFlag = userDetailDataSource.insertUserDetails(cv);
										
										//CleverTap Profile call
										HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
										profileUpdate.put("Identity", userDetailDataSource.getUserOwnNumber()); // String or number
										profileUpdate.put("OTP", userDetailDataSource.getUserOtp());
										profileUpdate.put("Referral Code", userDetailDataSource. getReferralCode());
										profileUpdate.put("Dealer", StaticConfig.DEALER_FULL_NAME);
										profileUpdate.put("OS", "Android");
										
										//CleverTap Events On Successful Login
										HashMap<String, Object> prodViewedAction = new HashMap<String, Object>();
										prodViewedAction.put("Identity", userDetailDataSource.getUserOwnNumber());
										prodViewedAction.put("OTP", userDetailDataSource.getUserOtp());
										prodViewedAction.put("Referral Code", userDetailDataSource. getReferralCode());
										 
										try {
											cleverTap = CleverTapAPI.getInstance(getActivity());
											cleverTap.profile.push(profileUpdate);
											cleverTap.event.push("LOGIN SUCCESSFUL");
											cleverTap.event.push("LOGIN SUCCESSFUL", prodViewedAction);
										} catch (CleverTapMetaDataNotFoundException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (CleverTapPermissionsNotSatisfied e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										//directing to mainactivity
										new Intent(getActivity(), MainActivity.class);
										
										if (goFlag) {
											Toast.makeText(getActivity().getApplicationContext(), "Successfully authenticated! \nFetching car details matching your credentials...", Toast.LENGTH_LONG).show();
											
											getActivity().stopService(new Intent(getActivity(), BackgroundService.class));

											Intent i = new Intent(getActivity(), MainActivity.class); 						
																					
											i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
											startActivity(i);
											getActivity().finish();


										}
										else
										{
											//Log.e(TAG, "could not save file");
										}
									}
									else
									{
										Toast.makeText(getActivity(), "Incorrect password! Please verify the password and try again.", Toast.LENGTH_LONG).show();
										passText.setText("");
										
										//Log.e(TAG, "url authentication failed. reason: " + urlResult.getString("error"));
									}
								} catch (JSONException e) {
																		
									//Log.e(TAG, "error parsing server reply - "+result);
								}
							}
						}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
						task.execute(urlData);

					} 
					catch (UnsupportedEncodingException e)
					{
						//Log.e(TAG, e.getMessage());
						
					} catch (Exception e) {
						
						//Log.e(TAG, e.getMessage());
					} 

				}
				else
				{
					Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
				}


			}
			else
			{
				Toast.makeText(getActivity(), "Please enter a valid 5 letter password.", Toast.LENGTH_LONG).show();
			}

		}
	
}
	@Override
	public void onResume() {
		super.onResume();
		//loginData=new MyJSONData(getActivity(), MyJSONData.TYPE_LOGIN);
		/*if (!loginData.fetchData(MyJSONData.FIELD_LOGIN_OTP).equals(""))*/
		if (!StaticConfig.FIELD_LOGIN_OTP.equals(""))
		{ 
				//passOnFile=loginData.fetchData(MyJSONData.FIELD_LOGIN_OTP);
			passOnFile = StaticConfig.FIELD_LOGIN_OTP;
				
				passText.setText(passOnFile);
		
		}else if(StaticConfig.OTP_PROGRESS_BAR_RUNNING){
			StaticConfig.OTP_PROGRESS_BAR_RUNNING = false;
			progressDialogWaitForOTP = ProgressDialog.show(getActivity(), 
					"Fetching OTP", "Reading your OTP please wait");
			handler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					progressDialogWaitForOTP.dismiss();
					
				}
			}, 20000);
		}
		
		
	}
	
	/*@Override
	public void onPause() {
		if(progressDialogWaitForOTP.isShowing()){
			progressDialogWaitForOTP.dismiss();
		}
		super.onPause();
	}*/
	
	public static void otpBroadcasted(){
		if(!StaticConfig.FIELD_LOGIN_OTP.equalsIgnoreCase(""))
		passText.setText(StaticConfig.FIELD_LOGIN_OTP);
		if(!passText.getText().toString().equalsIgnoreCase("")){
			resendText.setVisibility(View.GONE);
		}
		progressDialogWaitForOTP.dismiss();
	}
}
