package com.myaccessbox.custom.widgets;


import config.StaticConfig;
import android.content.Context;
import android.content.pm.FeatureInfo;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomFontTextView extends TextView {
	Typeface tf;
	public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
    	if(!StaticConfig.FEATURE_CUSTOM_FONT.equalsIgnoreCase("")){
    		setTypeface(Typeface.createFromAsset(getContext().getAssets(), StaticConfig.FEATURE_CUSTOM_FONT), 1);
    	}
    }

}
