package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;

import entity.ServiceHistoryDataPoint;

public class ServiceHistoryListAdapter extends ArrayAdapter<ServiceHistoryDataPoint> {
	Context context;
	int layoutResId;
	ServiceHistoryDataPoint history[] = null;

	public ServiceHistoryListAdapter(Context context, int layoutResourceId, ServiceHistoryDataPoint[] objects) {
		super(context, layoutResourceId, objects);
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.history = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		HistoryHolder holder = null;
		
		if (row == null) {
			LayoutInflater lInflater = ((Activity) context).getLayoutInflater();
			
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new HistoryHolder();
			holder.tvDate = (TextView) row.findViewById(R.id.row_list_service_date);
			holder.tvWorkType = (TextView) row.findViewById(R.id.row_list_worktype);
			holder.tvMileage = (TextView) row.findViewById(R.id.row_list_mileage);
			holder.tvAmount = (TextView) row.findViewById(R.id.row_list_amount);
			
			row.setTag(holder);
		}
		else {
			holder = (HistoryHolder) row.getTag();
		}
		
		//row.setBackgroundColor(context.getResources().getColor(R.drawable.tooltip_row_bg));
		
		ServiceHistoryDataPoint hist = history[position];
		holder.tvDate.setText(hist.getDisplayDate());
		holder.tvWorkType.setText(hist.getServiceType());
		holder.tvMileage.setText(hist.getMileageOrAmount());
		holder.tvAmount.setText(hist.getAmount());
		
		return row;
	}
	
	static class HistoryHolder {
		TextView tvDate;
		TextView tvWorkType;
		TextView tvMileage;
		TextView tvAmount;
	}
}
