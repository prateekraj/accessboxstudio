package adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.myaccessbox.appcore.R;

import entity.Messages;

public class MessageAdapter extends ArrayAdapter<Messages> {
	Context context;
	int layoutResId;
	Messages mess[] = null;

	public MessageAdapter(Context context, int layoutResourceId, Messages[] objects) {
		super(context, layoutResourceId, objects);
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.mess = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		MessageHolder holder = null;
		
		if (row == null) {
			LayoutInflater lInflater = ((Activity) context).getLayoutInflater();
			
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new MessageHolder();
			holder.tvSubject = (TextView) row.findViewById(R.id.row_list_subject);
			holder.tvBody = (TextView) row.findViewById(R.id.row_list_detail);
			holder.tvDate = (TextView) row.findViewById(R.id.row_list_date);
			
			row.setTag(holder);
		}
		else {
			holder = (MessageHolder) row.getTag();
		}
		
		Messages msg = mess[position];
		holder.tvSubject.setText(msg.getSubject());
		holder.tvBody.setText(msg.getBody());
		holder.tvDate.setText(msg.getDate());
		
		if (msg.isRead()) { //unbold the line if message is read
			holder.tvSubject.setTypeface(Typeface.DEFAULT);
			holder.tvBody.setTypeface(Typeface.DEFAULT);
			holder.tvDate.setTypeface(Typeface.DEFAULT);
			//row.setBackgroundColor(0);
		}
		else {
			holder.tvSubject.setTypeface(Typeface.DEFAULT_BOLD);
			holder.tvBody.setTypeface(Typeface.DEFAULT_BOLD);
			holder.tvDate.setTypeface(Typeface.DEFAULT_BOLD);
			//row.setBackgroundColor(0xFFFFFFFF);
		}
		return row;
	}
	
	static class MessageHolder {
		TextView tvSubject;
		TextView tvBody;
		TextView tvDate;
	}
}
