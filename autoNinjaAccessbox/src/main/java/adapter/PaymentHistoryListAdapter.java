package adapter;


import com.myaccessbox.appcore.R;

import entity.PaymentHistoryDataPoint;

import adapter.RewardHistoryListAdapter.HolderHistory;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PaymentHistoryListAdapter extends ArrayAdapter<PaymentHistoryDataPoint>{

	Context context;
	int layoutResId;
	PaymentHistoryDataPoint history[] = null;
	public PaymentHistoryListAdapter(Context context, int textViewResourceId,
			PaymentHistoryDataPoint[] objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.layoutResId = textViewResourceId;
		this.history = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		HistoryHolder holder = null;
		if(row == null) {
			LayoutInflater lInflater = ((Activity)context).getLayoutInflater();
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new HistoryHolder();
			holder.tvStatus = (TextView) row.findViewById(R.id.row_list_transaction_status);
			holder.tvDate = (TextView) row.findViewById(R.id.row_list_transaction_date);
			holder.tvAmount = (TextView) row.findViewById(R.id.row_list_transaction_amount);
			row.setTag(holder);
		} else {
			holder = (HistoryHolder) row.getTag();
		}
		
		PaymentHistoryDataPoint rHist = history[position];
		holder.tvStatus.setText(rHist.getTransaction_types());
		holder.tvDate.setText(rHist.getTransaction_timestamps());
		holder.tvAmount.setText(rHist.getTransaction_amounts());
		return row;
	}
	
	static class HistoryHolder {
		TextView tvStatus;
		TextView tvAmount;
		TextView tvDate;
	}

}
