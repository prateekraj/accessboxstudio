package adapter;

import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;

import entity.RewardHistoryDataPoint;

import android.app.Activity;
import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RewardHistoryListAdapter extends ArrayAdapter<RewardHistoryDataPoint>{

	Context context;
	int layoutResId;
	RewardHistoryDataPoint history[] = null;
	
	public RewardHistoryListAdapter(Context context, int layoutResourceId,
			RewardHistoryDataPoint[] objects) {
		super(context, layoutResourceId, objects);
		
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.history = objects;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		HolderHistory holder = null; 
		
		if(row == null) {
			LayoutInflater lInflater = ((Activity)context).getLayoutInflater();
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new HolderHistory();
			holder.tvReward = (TextView) row.findViewById(R.id.row_list_reward);
			holder.tvRewardAction =(TextView) row.findViewById(R.id.row_list_reward_action);
			holder.tvRewardDate = (TextView) row.findViewById(R.id.row_list_reward_date);
			holder.tvRewardRemark = (TextView) row.findViewById(R.id.row_list_reward_remark);
			row.setTag(holder);
		} else {
			holder = (HolderHistory) row.getTag();
		}
		
		RewardHistoryDataPoint rHist = history[position];
		holder.tvReward.setText(rHist.get_reward());
		holder.tvRewardAction.setText("( "+rHist.get_actionType()+" )");
		holder.tvRewardDate.setText(rHist.get_date());
		holder.tvRewardRemark.setText(rHist.get_rewardRemark());
		return row;
	}
	
	public static class HolderHistory {
		
		TextView tvRewardDate;
		TextView tvReward;
		TextView tvRewardAction;
		TextView tvRewardRemark;
		
	}

}
