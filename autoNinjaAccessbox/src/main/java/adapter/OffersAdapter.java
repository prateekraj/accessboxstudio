package adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.myaccessbox.appcore.R;
import com.myaccessbox.appcore.R.id;

import entity.Offers;

public class OffersAdapter extends ArrayAdapter<Offers> {
	Context context;
	int layoutResId;
	Offers offers[] = null;

	public OffersAdapter(Context context, int layoutResourceId, Offers[] objects) {
		super(context, layoutResourceId, objects);
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.offers = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		OffersHolder holder = null;
		
		if (row == null) {
			LayoutInflater lInflater = ((Activity) context).getLayoutInflater();
			
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new OffersHolder();
			holder.tvTeaser = (TextView) row.findViewById(R.id.text_offers_teaser);
			holder.tvRibbonText = (TextView) row.findViewById(R.id.text_offers_ribbon_txt);
			holder.tvTitle = (TextView) row.findViewById(R.id.text_offers_title);
			holder.imgType = (ImageView) row.findViewById(R.id.img_offers_type);
			
			row.setTag(holder);
		}
		else {
			holder = (OffersHolder) row.getTag();
		}
		
		Offers ofrs = offers[position];
		try {
		holder.tvTitle.setText(ofrs.getSubject());
		}
		catch (NullPointerException e) {
			if (holder == null) {
				//Log.e("OferList | NPE | ", "Msg: Holder Null");
			}
			else if (holder.tvTitle == null) {
				//Log.e("OferList | NPE | ", "Msg: Holder.tvTitle null");
			}
			else if (ofrs == null) {
				//Log.e("OferList | NPE | ", "Msg: ofrsNull");
			}
		}
		holder.tvTeaser.setText(ofrs.getTeaser());
		holder.tvTitle.setSelected(true);
		
		//set imgType src based on the type of this Offer use ofrs.getType
		Integer x = Offers.TYPE_RESOURCE_MAP.get(ofrs.getType());
		if (x != null) {
			holder.imgType.setImageResource(x);
		} else {
			holder.imgType.setImageResource(android.R.color.transparent);
		}
		
		//set Ribbon visible with tagline if ribbon data is present
		if (ofrs.getRibbonText().trim().equalsIgnoreCase("")) {
			holder.tvRibbonText.setVisibility(View.GONE);
		}
		else {
			holder.tvRibbonText.setVisibility(View.VISIBLE);
			holder.tvRibbonText.setText(ofrs.getRibbonText());
		}
		return row;
	}
	
	static class OffersHolder {
		TextView tvTitle;
		TextView tvTeaser;
		TextView tvRibbonText;
		ImageView imgType;
	}
}
