package adapter;

import java.io.File;

import com.myaccessbox.appcore.R;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import config.StaticConfig;

import entity.ChatMessages;
import fragment.AccidentCritical1Fragment;
import fragment.ChatAdvisorFragment;
import fragment.ViewImageFragment;

public class ChatAdapter extends BaseAdapter{
	LayoutInflater mInflater;
	int layoutChatLeft;
	int layoutChatRight;
	ChatMessages chat[] = null;
	private BookServiceListner bkBookServiceListner;
	boolean visibility = true;
	private Activity context;

	private static final int TYPE_ROW_LEFT = 0;
	private static final int TYPE_ROW_RIGHT = 1;
	private static final int VIEW_TYPE_COUNT = 2;

	// Using this counstructor for CEO
	public ChatAdapter(Activity context,LayoutInflater inflater, int layoutRowLeft, int layoutRowRight, ChatMessages[] objects, boolean visibility) {
		this.context = context;
		this.layoutChatLeft = layoutRowLeft;
		this.layoutChatRight = layoutRowRight;
		this.mInflater = inflater;
		this.chat = objects;
		this.visibility = visibility;
	}

	// Using this counstructor for Service Advisor
	public ChatAdapter(Activity context,LayoutInflater inflater, int layoutRowLeft, int layoutRowRight, ChatMessages[] objects, Object object) {
		this.context = context;
		this.layoutChatLeft = layoutRowLeft;
		this.layoutChatRight = layoutRowRight;
		this.mInflater = inflater;
		this.chat = objects;
		try {
			bkBookServiceListner = (BookServiceListner) object;
		} catch (ClassCastException e) {
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ChatHolder holder = null;
		int type = getItemViewType(position);
		final ChatMessages msg = chat[position];
		if (row == null) {
			holder = new ChatHolder();
			switch (type) {
				case TYPE_ROW_LEFT :
					row = mInflater.inflate(layoutChatLeft, parent, false);
					holder.imgMsgImage = (ImageView) row.findViewById(R.id.chat_message_image);
					break;
				case TYPE_ROW_RIGHT :
					row = mInflater.inflate(layoutChatRight, parent, false);
					holder.imgMsgImage = null;
					if (visibility) {
						holder.txtBookService = (TextView) row.findViewById(R.id.bookServiceFromChat);
						holder.txtBookService.setVisibility(View.VISIBLE);
						holder.txtBookService.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								if (bkBookServiceListner != null)
									bkBookServiceListner.bookServiceClicked();
							}
						});
					}
					break;
			}
			holder.tvMessage = (TextView) row.findViewById(R.id.chat_message);
			holder.tvSender = (TextView) row.findViewById(R.id.chat_sender);
			holder.tvStatus = (TextView) row.findViewById(R.id.chat_status);

			row.setTag(holder);
		} else {
			holder = (ChatHolder) row.getTag();
		}
		holder.tvSender.setText(msg.getSenderName());
		holder.tvStatus.setText(msg.getDate());
		holder.tvMessage.setText(msg.getMessage());

		if (holder.imgMsgImage != null) {
			holder.imgMsgImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent("show image");
					intent.putExtra("bitmap",msg.getMessage()); 
					System.out.println("MESSGAE IMAGE clicked:"+ msg.getMessage());
					LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
				}
			});
			if (msg.getMessageType() == 1) {
				holder.tvMessage.setVisibility(View.GONE);
				holder.imgMsgImage.setVisibility(View.VISIBLE);
				// File thumbFile = new File(ChatAdvisorFragment.getAlbumDir(),
				// "thumbs/" + msg.getMessage());
				
				File thumbFile = new File(ChatAdvisorFragment.getAlbumDir(), msg.getMessage());
				Bitmap bm = null;
				if(null!=BitmapFactory.decodeFile(thumbFile.getAbsolutePath())){
					 bm = BitmapFactory.decodeFile(thumbFile.getAbsolutePath());
				}else{
					 bm = BitmapFactory.decodeFile(msg.getMessage());
				}
				System.out.println("MESSGAE IMAGE:"+ msg.getMessage());
				holder.imgMsgImage.setImageBitmap(bm);
			} else {
				holder.tvMessage.setVisibility(View.VISIBLE);
				holder.imgMsgImage.setVisibility(View.GONE);
			}
		}

		if (!msg.isRead()) { // bold the line if message is unread
			holder.tvMessage.setTypeface(Typeface.DEFAULT_BOLD);
		} else {
			holder.tvMessage.setTypeface(Typeface.DEFAULT);
		}
		return row;
	}

	static class ChatHolder {
		TextView tvMessage;
		ImageView imgMsgImage;
		TextView tvSender;
		TextView tvStatus;
		TextView txtBookService;
	}

	@Override
	public int getCount() {
		return chat.length;
	}

	@Override
	public Object getItem(int position) {
		return chat[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		if (((ChatMessages) getItem(position)).getSenderId() == ChatMessages.SENDER_USER) {
			return TYPE_ROW_LEFT;
		} else {
			return TYPE_ROW_RIGHT;
		}
	}

	@Override
	public int getViewTypeCount() {
		return VIEW_TYPE_COUNT;
	}
	// interface for book service call from chat row
	public interface BookServiceListner {
		public void bookServiceClicked();
	}
}
