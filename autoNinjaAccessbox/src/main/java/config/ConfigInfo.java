package config;

import java.io.Serializable;
import java.util.ArrayList;

import fragment.MyFragment;



import android.os.Parcel;
import android.os.Parcelable;

public class ConfigInfo {
	/*
	 * Launcher Info Class Def:
	 */
	public static class Launcher implements Serializable {
		private static final long serialVersionUID = 1L;

		private String _label;
		private String _name;
		private int _drawableRes;
		private boolean _hasCounter;
		private int _launchType;
		private int _associatedTabID = -1;
		private Class<?> _associatedBaseClass = null;
		private int _extraFragmentInitValue = 0;

		public static final int LAUNCH_TAB = 21;
		public static final int LAUNCH_ACTIVITY = 7;
		public static final int LAUNCH_FRAGMENT = 35;
		public static final int LAUNCH_LOCATE = 42;
		public static final int LAUNCH_PROFILE = 49;
		public static final int LAUNCH_CUSTOM = 0;

		// Base Launcher
		public Launcher(String label, String name, int drawableID,
				boolean hasCounter, int launchType) {
			this._label = label;
			this._name = name;
			this._drawableRes = drawableID;
			this._hasCounter = hasCounter;
			this._launchType = launchType;
		}

		// Launcher with tabId specified
		public Launcher(String label, String name, int drawableID,
				boolean hasCounter, int launchType, int assocTabID) {
			this(label, name, drawableID, hasCounter, launchType);
			_associatedTabID = assocTabID;
		}

		// Launcher with tabId and Fragment extra init-value specified
		public Launcher(String label, String name, int drawableID,
				boolean hasCounter, int launchType, int assocTabID,
				int extraFragInitValue) {
			this(label, name, drawableID, hasCounter, launchType, assocTabID);
			_extraFragmentInitValue = extraFragInitValue;
		}

		// Launcher with Launch Activity/Fragment base class defined
		public Launcher(String label, String name, int drawableID,
				boolean hasCounter, int launchType,
				Class<?> assocActivityOrFragment) {
			this(label, name, drawableID, hasCounter, launchType);
			_associatedBaseClass = assocActivityOrFragment;
		}

		// Base Launcher without counter
		public Launcher(String label, String name, int drawableID,
				int launchType) {
			this(label, name, drawableID, false, launchType);
		}

		// Base Launcher with tabId specified but without counter
		public Launcher(String label, String name, int drawableID,
				int launchType, int assocTabID) {
			this(label, name, drawableID, false, launchType, assocTabID);
		}

		// Launcher with tabId and Fragment extra init-value specified but
		// without counter
		public Launcher(String label, String name, int drawableID,
				int launchType, int assocTabID, int extraFragInitValue) {
			this(label, name, drawableID, false, launchType, assocTabID,
					extraFragInitValue);
		}

		// Base Launcher with Launch Activity/Fragment specified but without
		// counter
		public Launcher(String label, String name, int drawableID,
				int launchType, Class<?> assocActivityOrFragment) {
			this(label, name, drawableID, false, launchType,
					assocActivityOrFragment);
		}

		public String getLabel() {
			return _label;
		}

		public String getName() {
			return _name;
		}

		public int getDrawableResID() {
			return _drawableRes;
		}

		public boolean hasCounter() {
			return _hasCounter;
		}

		public int getLaunchType() {
			return _launchType;
		}

		public int getAssociatedTabID() {
			return _associatedTabID;
		}

		public int getExtraFragmentInitValue() {
			return _extraFragmentInitValue;
		}

		public Class<?> getAssociatedActivity() {
			return _associatedBaseClass;
		}

		public Class<?> getAssociatedFragment() {
			return _associatedBaseClass;
		}
	}

	/*
	 * End of Launcher Info Class
	 */

	/*
	 * Tab Info Class Def:
	 */
	public static class Tab {
		private String _tabName;
		private String _title;
		private int _drawableIconResourceID;
		private Class<? extends MyFragment> _baseFragmentClass;

		public Tab(String title, String tabName, int drawableIconResID,
				Class<? extends MyFragment> baseFragmentClass) {
			_title = title;
			_tabName = tabName;
			_drawableIconResourceID = drawableIconResID;
			_baseFragmentClass = baseFragmentClass;
		}

		public String getTabName() {
			return _tabName;
		}

		public String getTitle() {
			return _title;
		}

		public int getDrawableIconResourceID() {
			return _drawableIconResourceID;
		}

		public Class<? extends MyFragment> getbaseFragmentClass() {
			return _baseFragmentClass;
		}
	}

	/*
	 * End of Tab Info Class
	 */

	/*
	 * Hotline Info Class Def:
	 */
	public static class Hotline {
		private String _name = "";
		private String _label = "";
		private int _drawableIconResourceID;
		private int _id;
		private ArrayList<GroupEntity> _numbers;

		public Hotline(String label, String name, int iconId, int id,
				ArrayList<GroupEntity> numbers) {
			this._label = label;
			this._name = name;
			this._drawableIconResourceID = iconId;
			this._numbers = numbers;
			this._id = id;
		}

		public String getName() {
			return _name;
		}

		public String getLabel() {
			return _label;
		}

		public int getIconDrawableId() {
			return _drawableIconResourceID;
		}

		public ArrayList<GroupEntity> getNumbers() {
			return _numbers;
		}

		public static class GroupEntity implements Parcelable {

			private String _entityLabel;
			private String _entityNumber;
			private String _entityName;
			private int _entityID;

			public GroupEntity(String label, String number, String name, int id) {
				this._entityLabel = label;
				this._entityNumber = number;
				this._entityName = name;
				this._entityID = id;
			}

			private GroupEntity(Parcel in) {
				this._entityLabel = in.readString();
				this._entityNumber = in.readString();
				this._entityName = in.readString();
				this._entityID = in.readInt();

			}

			public String getLabel() {
				return _entityLabel;
			}

			public String getNumber() {
				return _entityNumber;
			}

			@Override
			public int describeContents() {
				return 0;
			}

			public String getName() {
				return _entityName;
			}

			public int getId() {
				return _entityID;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags) {
				dest.writeString(_entityLabel);
				dest.writeString(_entityNumber);
				dest.writeString(_entityName);
				dest.writeLong(_entityID);
			}

			public static final Parcelable.Creator<GroupEntity> CREATOR = new Parcelable.Creator<ConfigInfo.Hotline.GroupEntity>() {

				@Override
				public GroupEntity createFromParcel(Parcel source) {
					return new GroupEntity(source);
				}

				@Override
				public GroupEntity[] newArray(int size) {
					return new GroupEntity[size];
				}

			};
		}

	}
	/*
	 * End of Hotline Info Class
	 */
}
