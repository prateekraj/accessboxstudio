package config;


import android.widget.ArrayAdapter;

import activity.PaymentActivity;
import config.ConfigInfo.Hotline.GroupEntity;

import entity.MyCar;
import fragment.AboutFragment;
import fragment.AccidentHomeFragment;
import fragment.EnquiryFragment;
import fragment.HotlinesFragment;
import fragment.MessagesHomeFragment;
import fragment.MyCarBaseFragment;
import fragment.OffersListFragment;
import fragment.PaymentFragmentCitrus;
import fragment.PaymentHistoryFragment;
import fragment.ServiceBaseFragment;
import fragment.TooltipsListFragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.myaccessbox.appcore.R;

public class StaticConfig {
	
	public static final String DEALER_FULL_NAME = "Test Dealer"; 
	public static final String DEALER_MAP_SEARCH_NAME = "Dealer+Google+Map+Search+String"; 
	public static final String DEALER_FIRST_NAME = "Testing"; 
	public static final String DEALER_FACEBOOK_PAGE = "";
	public static final String DEALER_APPLINKS_PAGE = "http://bit.ly/testing_app";
	public static final String DEALER_CMS_VALUE = "testing";
	public static final String DEALER_WEBSITE_LINK = ""; //NOTE: t
	
	/*
	 * This one works as dealer's bank account number
	 */
	public static final String DEALER_CUSTOM_PARAMETER_FOR_CITRUS = "00003775";
	

	
	/*
	 * OTP related stuff
	 */ 
	public static String FIELD_LOGIN_OTP = "";
	public static boolean OTP_PROGRESS_BAR_RUNNING = false;
	
	/*
	 * Launcher-Names to fetch launcher-details and launcher-position by NAME
	 */
	public static final String LAUNCH_NAME_MYCAR = "mycar";
	public static final String LAUNCH_NAME_SERVICE = "service";
	public static final String LAUNCH_NAME_MESSAGES = "messages";
	public static final String LAUNCH_NAME_OFFERS = "offers";
	public static final String LAUNCH_NAME_ACCIDENT = "accident";
	public static final String LAUNCH_NAME_LOCATE = "locate";
	public static final String LAUNCH_NAME_TOOLTIPS = "tooltips";
	public static final String LAUNCH_NAME_HOTLINES = "hotlines";
	public static final String LAUNCH_NAME_PAYMENT = "pay_now";
	public static final String LAUNCH_NAME_ABOUT = "about";
	public static final String LAUNCH_NAME_PROFILE = "profile";
	public static final String LAUNCH_NAME_SERVICE_HISTORY = "service_history";
	public static final String LAUNCH_NAME_FACEBOOK = "dealer_facebook_page";
	public static final String LAUNCH_NAME_LIVE_QUOTE = "live_quote";
	public static final String LAUNCH_NAME_PAPERLESS = "go_paperless";
	public static final String LAUNCH_NAME_CEO_CHAT = "ceo_chat";
	public static final String LAUNCH_NAME_SHARE_APP = "share_app";
	public static final String LAUNCH_NAME_RATEUS = "rate_us";
	public static final String LAUNCH_NAME_CONTACT_US = "contact_us";
	public static final String LAUNCH_NAME_REWARD_HISTORY = "reward_history";
	public static final String LAUNCH_NAME_PAY = "pay_now"; 
	public static final String LAUNCH_NAME_ENQUIRY = "enquiry";
	public static final String LAUNCH_NAME_PAYMENT_HISTORY = "payment_history";
	
	/*
	 * Feature list on-off switcher 
	 */
	public static final boolean FEATURE_OTP_LOGIN_ENABLED = true;
	public static final boolean FEATURE_MESSAGES_ENABLED = true;
	public static final boolean FEATURE_TOOLTIPS_ENABLED = true;
	public static final boolean FEATURE_OFFERS_ENABLED = true;
	public static final boolean FEATURE_CHAT_ENABLED = true;
	public static final boolean FEATURE_DOCUMENTS_ENABLED = true;
	public static final boolean FEATURE_MARUTI_MOBILE_SUPPORT = false;
	public static final boolean FEATURE_MULTI_BRAND_ENABLED = false;
	public static final boolean FEATURE_REFERRAL_CODE = true;
	//Chat Multi-City can be enabled only if chat itself is enabled 
	public static final boolean FEATURE_CHAT_MULTI_CITY_ENABLED = false && FEATURE_CHAT_ENABLED;
	public static final boolean FEATURE_REGISTRATION_NUMBER_LOGIN = false;
	public static final boolean FEATURE_REWARD_POINT = false;
	public static final boolean FEATURE_CANCEL_SERVICE_ENABLED = false;
	public static boolean FEATURE_SPLASH_SCREEN_ENABLED = false;
	public static final boolean SERVICE_BOOKING_TIME_CRITERIA = true;
	public static final String BOOK_SERVICE_BEFORE = "17:00:00";
	public static final String BOOK_SERVICE_START_SLOT = "09:00:00";
	public static final String BOOK_SERVICE_END_SLOT = "20:00:00";
	public static final boolean OTP_RESEND = false;
	public static final boolean FEATURE_PAYMENT_HISTORY_FEATURE = true;
	/*
	 * Custom Fonts Defn
	 */
	public static final String FEATURE_CUSTOM_FONT = "";
	//EXAMPLE: FEATURE_CUSTOM_FONT = "fonts/TitleBold.ttf";
	
	/** multiple car requirements */
	public static ArrayList<MyCar> myCarList = new ArrayList<MyCar>();
	public static int TOTAL_NUMBER_OF_CARS = 0;
	// public static String [] myCarListNames = { };
	public static List<String> myCarListNames = new ArrayList<String>();
	public static int cardId = myCarListNames.size() + 1; // just for testing from server.
	public static int position = 0;
	//public static boolean IsFirstTime = true;
	public static MyCar myCar = new MyCar();
	public static long LOGGED_PHONE_NUMBER = 0;
	public static String FIELD_QUOTE_PHOTO_ATTEMPT_PATH = "";
	public static String FIELD_OWN_MESSAGE_REPLY_TO_CEO = "";
	public static String FIELD_LOGIN_NUMBER = "";
	public static String FIELD_REF_CODE = "";
	public static final int SERVICE_INTERVAL_1 = 30;
	public static final int SERVICE_INTERVAL_2 = 15;
	public static final int SERVICE_INTERVAL_3 = 5;
	public static final int SERVICE_INTERVAL_Last = 1;
	public static final int INSURANCE_INTERVAL_1 = 75;
	public static final int INSURANCE_INTERVAL_2 = 60;
	public static final int INSURANCE_INTERVAL_3 = 45;
	public static final int INSURANCE_INTERVAL_4 = 15;
	public static final int INSURANCE_INTERVAL_Last = 5;
	
	
	/*
	 * Dealers information for Contact Us
	 */
	public static final String ADDRESS_SHOWROOM = "Test Dealer, C-47, Shivalik, Basant Kaur Marg, " +
			"Block A, Malviya Nagar, New Delhi, Delhi 110017";
	
	public static final String ADDRESS_WORSHOP = "Test Dealer, C-47, Shivalik, Basant Kaur Marg, " +
			"Block A, Malviya Nagar, New Delhi, Delhi 110017";
	
	public static final String TELEPHONE_SHOWROOM = "09876543211";
	public static final String TELEPHONE_WORKSHOP = "09876543211";
	public static final String FAX_SHOWROOM = "02299999999";
	public static final String FAX_WORKSHOP = "02299999999";
	public static final String EMAIL_SHOWROOM = "tester@autoninja.in";
	public static final String EMAIL_WORKSHOP = "tester@autoninja.in";
	
	/*
	 * Tab-Names to fetch tab-details and tab-position by NAME
	 */
	public static final String TAB_NAME_MYCAR = "mycar";
	public static final String TAB_NAME_SERVICE = "service";
	public static final String TAB_NAME_MESSAGES = "messages";
	public static final String TAB_NAME_OFFERS = "offers";
	public static final String TAB_NAME_ACCIDENT = "accident";
	public static final String TAB_NAME_ABOUT = "about";
	
	/*
	 * Tab Selectors Configuration
	 * NOTE: Sequence of static list defn important!
	 * 		 TabList is used by other lists to get tab-position
	 */
	private static ArrayList<ConfigInfo.Tab> _tabInfoList = new ArrayList<ConfigInfo.Tab>() {
		private static final long serialVersionUID = 1L;

		{
			add(new ConfigInfo.Tab("My Car", TAB_NAME_MYCAR, R.drawable.mycar_tab, MyCarBaseFragment.class));
			add(new ConfigInfo.Tab("Messages", TAB_NAME_MESSAGES, R.drawable.messages_tab, MessagesHomeFragment.class)); //Change to MessageList fragment for chat-disabled clients 
			add(new ConfigInfo.Tab("Service", TAB_NAME_SERVICE, R.drawable.services_tab, ServiceBaseFragment.class));
			add(new ConfigInfo.Tab("Offers", TAB_NAME_OFFERS, R.drawable.offers_tab, OffersListFragment.class));
			add(new ConfigInfo.Tab("Accident", TAB_NAME_ACCIDENT, R.drawable.accidenthelp_tab, AccidentHomeFragment.class));
		}
	};
	
	/*
	 * Launcher Buttons Configuration
	 */
	private static ArrayList<ConfigInfo.Launcher> _launchButtonInfoList = new ArrayList<ConfigInfo.Launcher>() {
		private static final long serialVersionUID = 1L;
		{
			add(new ConfigInfo.Launcher("My Car", LAUNCH_NAME_MYCAR, R.drawable.mycar_press, true, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MYCAR)));
			add(new ConfigInfo.Launcher("Messages", LAUNCH_NAME_MESSAGES, R.drawable.messages_press, true, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MESSAGES)));
			add(new ConfigInfo.Launcher("Service", LAUNCH_NAME_SERVICE, R.drawable.services_press, false, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_SERVICE)));
			add(new ConfigInfo.Launcher("Locate", LAUNCH_NAME_LOCATE, R.drawable.locate_press, false, ConfigInfo.Launcher.LAUNCH_LOCATE));
			//add(new ConfigInfo.Launcher("Accident", LAUNCH_NAME_ACCIDENT, R.drawable.accidenthelp_press, false, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_ACCIDENT)));
			add(new ConfigInfo.Launcher("Enquiry", LAUNCH_NAME_ENQUIRY, R.drawable.accidenthelp_press, false, ConfigInfo.Launcher.LAUNCH_FRAGMENT,EnquiryFragment.class) );
			add(new ConfigInfo.Launcher("Offers", LAUNCH_NAME_OFFERS, R.drawable.offers_press, false, ConfigInfo.Launcher.LAUNCH_TAB,  getTabPositionFromName(TAB_NAME_OFFERS)));
			add(new ConfigInfo.Launcher("Live Quote", LAUNCH_NAME_LIVE_QUOTE, R.drawable.service_quote_press, true, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MESSAGES), MessagesHomeFragment.REDIRECT_TO_ADVISOR));
			add(new ConfigInfo.Launcher("Go Paperless", LAUNCH_NAME_PAPERLESS, R.drawable.go_paperless_press, false, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MYCAR), MyCarBaseFragment.REDIRECT_TO_DOCUMENT_INSURANCE));
		//	add(new ConfigInfo.Launcher("Hotlines", LAUNCH_NAME_HOTLINES, R.drawable.hotlines_home_press, false, ConfigInfo.Launcher.LAUNCH_FRAGMENT, HotlinesFragment.class));
		//	add(new ConfigInfo.Launcher("Reward History", LAUNCH_NAME_REWARD_HISTORY, R.drawable.rewards_home, ConfigInfo.Launcher.LAUNCH_FRAGMENT, RewardHistoryFragment.class));
			add(new ConfigInfo.Launcher("Pay Now", LAUNCH_NAME_PAY, R.drawable.payment, ConfigInfo.Launcher.LAUNCH_FRAGMENT, PaymentFragmentCitrus.class));
		}
	};
	
	/*
	 * Menu Items List Configuration
	 */
	private static ArrayList<ConfigInfo.Launcher> _menuItemInfoList = new ArrayList<ConfigInfo.Launcher>() {
		private static final long serialVersionUID = 1L;
		{
			add(new ConfigInfo.Launcher("Tooltips", LAUNCH_NAME_TOOLTIPS, R.drawable.burger_tooltips, ConfigInfo.Launcher.LAUNCH_FRAGMENT, TooltipsListFragment.class));
			add(new ConfigInfo.Launcher("Hotlines", LAUNCH_NAME_HOTLINES, R.drawable.burger_hotlines, ConfigInfo.Launcher.LAUNCH_FRAGMENT, HotlinesFragment.class));
			add(new ConfigInfo.Launcher("Service History", LAUNCH_NAME_SERVICE_HISTORY, R.drawable.burger_service_history, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MYCAR), MyCarBaseFragment.REDIRECT_TO_SERVICE_HISTORY));
			//add(new ConfigInfo.Launcher("Go Paperless", LAUNCH_NAME_PAPERLESS, R.drawable.buger_gopaperless, false, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MYCAR), MyCarBaseFragment.REDIRECT_TO_DOCUMENT_INSURANCE));
			add(new ConfigInfo.Launcher("Facebook Connect", LAUNCH_NAME_FACEBOOK, R.drawable.burger_facebook_logo, ConfigInfo.Launcher.LAUNCH_CUSTOM));
			//add(new ConfigInfo.Launcher("Payment History", LAUNCH_NAME_PAYMENT_HISTORY, R.drawable.payment_history, ConfigInfo.Launcher.LAUNCH_FRAGMENT, PaymentHistoryFragment.class));
			add(new ConfigInfo.Launcher("Talk to the CEO", LAUNCH_NAME_CEO_CHAT, R.drawable.burger_profile, ConfigInfo.Launcher.LAUNCH_TAB, getTabPositionFromName(TAB_NAME_MESSAGES), MessagesHomeFragment.REDIRECT_TO_CEO));
			add(new ConfigInfo.Launcher("Recommend this App", LAUNCH_NAME_SHARE_APP, R.drawable.burger_share_app, ConfigInfo.Launcher.LAUNCH_CUSTOM));
		//	add(new ConfigInfo.Launcher("Rate the App", LAUNCH_NAME_RATEUS, R.drawable.burger_feedback, ConfigInfo.Launcher.LAUNCH_CUSTOM));
			add(new ConfigInfo.Launcher("About Accessbox", LAUNCH_NAME_ABOUT, R.drawable.burger_about, ConfigInfo.Launcher.LAUNCH_FRAGMENT, AboutFragment.class));
		//	add(new ConfigInfo.Launcher("Payment History", LAUNCH_NAME_PAYMENT_HISTORY, R.drawable.payment_history, ConfigInfo.Launcher.LAUNCH_FRAGMENT, PaymentHistoryFragment.class));
		//	add(new ConfigInfo.Launcher("Contact Us", LAUNCH_NAME_CONTACT_US, R.drawable.burger_contact_us, ConfigInfo.Launcher.LAUNCH_FRAGMENT, AboutDealerFragment.class));
		//	add(new ConfigInfo.Launcher("Reward History", LAUNCH_NAME_REWARD_HISTORY, R.drawable.burger_service_history, ConfigInfo.Launcher.LAUNCH_FRAGMENT, RewardHistoryFragment.class));
		}
	};
	
	/*
	 * ALL CALL Numbers go here:
	 */
	/*public static final String CALL_SERVICE			=	"04446005600"; 
	public static final String CALL_POLICE			=	"100"; 
	public static final String CALL_TOWING			=	"";
	public static final String CALL_ONROAD_SERVICE	=	"07708096006"; 
	public static final String CALL_INSURANCE		=	"07358081314";
	//NOTE: Background service will check for CALL_PUC.equalsIgnoreCase("") 
	//      to check for availability of the PUC number for reminder CFA messsage
	public static final String CALL_PUC				=	"";
	public static final String CALL_NEW_CAR			=	"";
	public static final String CALL_USED_CAR		=	"";
	//public static final String CALL_DRIVING_SCHOOL	=	"09008644417";
	public static final String CALL_EXTENDED_WARRANTY =	"07708033375";
	public static final String CALL_BUY_ACCESSORIES	=	"";
	public static final String CALL_CAR_FINANCE		=	"";
	public static final String CALL_CUSTOMER_CARE	=	"07708033375";*/
	
	public static final String CALL_POLICE			=	"100"; 
	public static  ArrayList<GroupEntity> ONROAD_SERVICE_SELECTOR = new ArrayList<GroupEntity>(); 
	public static  ArrayList<GroupEntity> TOWING_SELECTOR = new ArrayList<GroupEntity>();
	public static  ArrayList<GroupEntity> INSURANCE_HOTLINE_SELECTOR = new ArrayList<GroupEntity>();
	public static  ArrayList<GroupEntity> PUC_HOTLINE_SELECTOR = new ArrayList<GroupEntity>();
	public static  ArrayList<GroupEntity> SERVICE_HOTLINE_SELECTOR = new ArrayList<GroupEntity>();
	
			
	/*
	 * Hotline Buttons Configuration
	 */
	public static ArrayList<ArrayList<GroupEntity>> masterHotlineSelector = new ArrayList<ArrayList<GroupEntity>>();
	public static ArrayList<ConfigInfo.Hotline> _hotlinesButtonInfoList = new ArrayList<ConfigInfo.Hotline>();
	public static int masterHotlineSelectorSize;

	/*
	 * Hotline-Names to fetch hotline-details and hotline-position by NAME
	 */
	public static final String HOTLINE_NEW_CAR = "hotline_new_car";
	public static final String HOTLINE_USED_CAR = "hotline_used_car";
	public static final String HOTLINE_DRIVING_SCHOOL = "hotline_driving_school";
	public static final String HOTLINE_RENEW_PUC = "hotline_renew_puc";
	public static final String HOTLINE_RENEW_INSURANCE = "hotline_renew_insurance";
	public static final String HOTLINE_EXTENDED_WARRANTY = "hotline_extended_warranty";
	public static final String HOTLINE_ACCESSORIES = "hotline_accessories";
	public static final String HOTLINE_MOS = "hotline_mos";
	public static final String HOTLINE_ONROAD_ASSISTANCE = "onroad_assistance";
	public static final String HOTLINE_CAR_FINANCE = "hotline_car_finance";
	public static final String HOTLINE_SERVICE = "hotline_service";
	public static final String HOTLINE_TOWING = "hotline_towing";
	public static final String HOTLINE_CUSTOMER_CARE = "hotline_customer_care";
	
	/**
	 * setting up the icon list
	 */
	public static ArrayList<Integer> _hotlinesIconsList = new ArrayList<Integer>() {
		private static final long serialVersionUID = 1L;
		{

			add(R.drawable.hotline_new_car);//1
			add(R.drawable.hotline_used_car);//2
			add(R.drawable.hotline_book_service);//3
			add(R.drawable.hotline_accessories);//4
			add(R.drawable.hotline_customer_care);//5
			add(R.drawable.hotline_renew_insurance);//6
			add(R.drawable.hotline_car_loan);//7
			add(R.drawable.hotline_extended_warranty);//8
			add(R.drawable.hotline_renew_puc);//9
			add(R.drawable.hotline_learn_driving);//10
			add(R.drawable.hotline_onroad_assitance);//11
			add(R.drawable.hotline_towing);//12

		}
	};
	
	/*
	 * ALL Network Call URLs and other weblinks go here:
	 * (further possibilities include parameters to send to these API links)
	 */
	public static final String API_VERSION = "v2";
	//public static final String API_CMS_DEALER_BASE_NEW = "http://staging.accessbox.in/accessbox/public/" + DEALER_CMS_VALUE + "/apis/" + API_VERSION + "/";
	public static final String API_CMS_DEALER_BASE = "http://www.5shells.com/cms/" + DEALER_CMS_VALUE + "/";
	public static final String API_CMS_DEALER_BASE_NEW = "http://dashboard.accessbox.in/" + DEALER_CMS_VALUE + "/apis/" + API_VERSION + "/";
	public static final String IMAGES_FOLDER = "http://dashboard.accessbox.in/";
	public static final String API_FETCH_DEALER_DATA =API_CMS_DEALER_BASE_NEW +"appbuffer";
	public static final String API_REQUEST_REGISTER_FOR_OTP = API_CMS_DEALER_BASE_NEW + "registerrequest";//
	public static final String API_CHECK_OTP_LOGIN = API_CMS_DEALER_BASE_NEW+ "applogin";//
	public static final String API_FETCH_CMS_DATA = API_CMS_DEALER_BASE_NEW + "appdata";
	public static final String API_DATES_RECORD = API_CMS_DEALER_BASE_NEW + "datesrecord";
	public static final String API_BOOK_SERVICE = API_CMS_DEALER_BASE_NEW + "bookservicewithcancellation";
	public static final String API_CANCEL_SERVICE = API_CMS_DEALER_BASE_NEW + "cancelservicebooking";
	public static final String API_FETCH_HOTLINES = API_CMS_DEALER_BASE_NEW + "gethotlines";
	public static final String API_SEND_HOTLINE_DETAIL = API_CMS_DEALER_BASE_NEW + "loghotlines";
	public static final String API_FETCH_LOCATIONS = API_CMS_DEALER_BASE_NEW + "getalllocations";
	
// replace urls start
	public static final String API_MESSAGE_IMAGES_BASE = IMAGES_FOLDER + "images/dataimages/messages/";
	public static final String API_TOOLTIP_IMAGES_BASE = IMAGES_FOLDER + "images/dataimages/tooltips/";
	public static final String API_OFFER_IMAGES_BASE = IMAGES_FOLDER + "images/dataimages/offers/";
	//replace urls end

	//public static final String API_PING_FIRST_RECORD = API_CMS_DEALER_BASE + "first-record.php";
	/*public static final String API_REFER_A_FRIEND = API_CMS_DEALER_BASE + "refer-a-friend.php";//replace 
*/	public static final String API_REFER_A_FRIEND = API_CMS_DEALER_BASE_NEW + "referafriend";//replace 
	//public static final String API_REPORT_PROBLEM = API_CMS_DEALER_BASE + "report.php";
	public static final String API_POST_CHAT_MESSAGE = API_CMS_DEALER_BASE_NEW + "chat";//????
	public static final String API_DOWNLOAD_DOCUMENTS = API_CMS_DEALER_BASE + "documents/";
	public static final String API_REWARD_POINTS = API_CMS_DEALER_BASE_NEW + "rp_search_history.php";
	public static final String API_OFFER_AVAIL = API_CMS_DEALER_BASE_NEW + "logoffers";
	public static final String API_ENQUIRY = API_CMS_DEALER_BASE_NEW + "enquiry";
	public static final String API_CITRUS_RESPONSE_TO_SERVER = API_CMS_DEALER_BASE_NEW + "citruspaymentgateway";
	public static final String API_POST_ADD_CAR = API_CMS_DEALER_BASE_NEW +"addcar";
	public static final String API_POST_UPDATE_CAR = API_CMS_DEALER_BASE_NEW + "updatecar";
	public static final String API_POST_DELETE_CAR = API_CMS_DEALER_BASE_NEW + "deletecar";
	public static final String API_PSF_RATING = API_CMS_DEALER_BASE_NEW + "logpsfratings";
	public static final String API_INSURANCE_RENEWAL = API_CMS_DEALER_BASE_NEW + "loginsurancerenewal";
	public static final String API_GO_PAPERLESS = API_CMS_DEALER_BASE_NEW + "fileupload";
	public static final String API_BILL_GENERATOR_URL = API_CMS_DEALER_BASE_NEW + "billgeneratorcitrus";

	/*
	 * All locations Center List goes in the following 
	 */
	/*public static final String [] SERVICE_STATIONS = {
		"Ambattur Personal",
		"Ambattur Prosper",
		"Keelkattalai"
	};*/
	
	public static String [] LOCS_SERVICE_STATIONS;
	public static String [] LOCS_CHAT_CEO;
	public static String [] LOCS_CHAT_ADVISOR ;
	public static String [] LOCS_ENQUIRY;
	public static String [] LOCS_PAYMENT;
	public static String [] LOCS_TEST_DRIVE;
	/*
	 * Enquiry Type List goes in the following String Array
	 */
	public static final String [] ENQUIRY_TYPE_LIST = {
		"Used Car",
		/*"New Car",
		"Insurance",
		"Car Loan",
		"Accessories"*/
	};
	public static boolean HOTLINE_FETCHED = true;
	
	
	/*
	 * List of mascot image drawable refrences goes here
	 * Keep transparent element (android.R.color.transparent) in 
   * IMAGE_LIST array for disabling Mascot_In_Menu feature!
	 */
	public static final int[] MASCOT_IMAGES_LIST = {R.drawable.logo};
	
	/*
	 * Locator Options List goes in the following String Array
	 */
	public static final int LOCATOR_SERVICE_CENTERS = 1;
	public static final int LOCATOR_PETROL_PUMPS = 2;
	public static final int LOCATOR_POLICE_STATION = 3;
	
	public static final Map<Integer, String> LOCATOR_OPTIONS = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 1L;
		{
			put(LOCATOR_POLICE_STATION, "Police Station");
			put(LOCATOR_SERVICE_CENTERS, "Authorized Service Centers");
			put(LOCATOR_PETROL_PUMPS, "Petrol Pumps");
		}
	};
	
	public static String[] getLocatorList() {
		String[] ret = new String[LOCATOR_OPTIONS.size()];
		int ctr = 0;
		for (Map.Entry<Integer, String> entry : LOCATOR_OPTIONS.entrySet()) {
			ret[ctr] = entry.getValue();
			ctr ++;
		}
		return ret;
	}
	
	/*
	 * 
	 * If App is using buffer.php on 5shells server, the app no longer needs this password!
	 * But, if app uses buffer.php (or similar) on dealer server (in case OTP generation and storing happens thru their existing SMS gateway)
	 * 		then this dp will still be needed! :-|
	 * 
	private static final String dp = "i#i$spCriingfB2f3ia([+f)%&*])"; //Pratham DP
	private static String getPass() {
		try {
			return "dp=" + URLEncoder.encode(dp, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			//return ""
		}
		return "";
	}
	*/

	private static final String cmsDP = "sdfT&*w5e#sto([+peW7)%y9pqf])"; //CMS Pass (pd)
	public static String TYPE_OF_DOC = "";
	public static String FIELD_OWN_DOC_PHOTO_ATTEMPT_PATH = "";
	public static String getCMSPass() {
		try {
			return "pd=" + URLEncoder.encode(cmsDP, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			//return ""
		}
		return "";
	}

	public static ConfigInfo.Launcher getLaunchInfoAtPosition(int position) {
		try {
			return _launchButtonInfoList.get(position);
		}
		catch (IndexOutOfBoundsException e) {
			//return null outside
		}
		return null;
	}
	
	public static int getLaunchersCount() {
		return _launchButtonInfoList.size();
	}
	
	public static ConfigInfo.Tab getTabInfoAtPosition(int position) {
		try {
			return _tabInfoList.get(position);
		}
		catch (IndexOutOfBoundsException e) {
			//return null outside
		}
		return null;
	}
	
	public static int getTabCount() {
		return _tabInfoList.size();
	}

	public static ConfigInfo.Launcher getMenuItemAtPosition(int position) {
		try {
			return _menuItemInfoList.get(position);
		}
		catch (IndexOutOfBoundsException e) {
			//return null outside
		}
		return null;
	}
	
	public static int getMenuItemsCount() {
		return _menuItemInfoList.size();
	}
	
	public static ArrayList<ConfigInfo.Launcher> getMenuItemsList() {
		return _menuItemInfoList;
	}
	
	public static int getTabPositionFromName(String tabName) {
		for (int j = 0; j < _tabInfoList.size(); j ++) {
			ConfigInfo.Tab x = _tabInfoList.get(j);
			
			if (x.getTabName().equalsIgnoreCase(tabName)) {
				return j;
			}
		}
		return -1;
	}
	
	public static ConfigInfo.Tab getTabInfoFromName(String tabName) {
		//may return null
		return getTabInfoAtPosition(getTabPositionFromName(tabName));
	}
	
	public static int getLaunchButtonPositionFromName(String name) {
		for (int j = 0; j < _launchButtonInfoList.size(); j ++) {
			ConfigInfo.Launcher x = _launchButtonInfoList.get(j);
			
			if (x.getName().equalsIgnoreCase(name)) {
				return j;
			}
		}
		return -1;
	}
	
	public static int getMenuItemPositionFromName(String name) {
		for (int j = 0; j < _menuItemInfoList.size(); j ++) {
			ConfigInfo.Launcher x = _menuItemInfoList.get(j);
			
			if (x.getName().equalsIgnoreCase(name)) {
				return j;
			}
		}
		return -1;
	}
	
	public static ConfigInfo.Launcher getLauncherInfoFromName(String name) {
		//search in both Launcher Button List as well as Menu Item List
		//with preference given to Launcher Button List info
		//will return 'null' if not found
		int a = getLaunchButtonPositionFromName(name);
		if (a != -1) {
			return getLaunchInfoAtPosition(a);
		}
		else {
			return getMenuItemAtPosition(getMenuItemPositionFromName(name)); 
		}
	}
	
	public static String getDealerLocationMapLink() {
		//TODO: put locator logic here later
		//TODO: add necessary parameters like GPS coordinates, etc.
		return "geo:0,0?q=" + DEALER_MAP_SEARCH_NAME;
	}
	
	public static int getHotlinesCount() {
		return _hotlinesButtonInfoList.size();
	}
	
	public static ArrayList<ConfigInfo.Hotline> getHotlineList() {
		return _hotlinesButtonInfoList;
	}
	
	public static ConfigInfo.Hotline getHotlineAtPosition(int index) {
		try {
			return _hotlinesButtonInfoList.get(index);
		}
		catch (IndexOutOfBoundsException e) {
			//return null outside
		}
		return null;
	}
	
	public static int getHotlinePositionFromName(String name) {
		for (int j = 0; j < _hotlinesButtonInfoList.size(); j ++) {
			ConfigInfo.Hotline x = _hotlinesButtonInfoList.get(j);
			
			if (x.getName() == name) {
				return j;
			}
		}
		return -1;
	}
	
	public static ConfigInfo.Hotline getHotlineInfoFromName(String name) {
		return getHotlineAtPosition(getHotlinePositionFromName(name));
	}
}
